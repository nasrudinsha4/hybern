<?php
class M_models extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

/**
* Name: model_list
* Parameters:- order (asc or desc)
* Use: fetches model details from db in order specified by user
* tables used : model
*/
	function model_list($limit_per_page = '' , $start_index = null,$ord = '',$cond=null,$column = '',$single=null)
	{
		$this->db->select('*,model.dt_updated,model.model_id,model.dt_created as Date,
		c.city_name as crange,
		category.city_name as category,
		company.city_name as company,
		make.city_name as make,
		feel.city_name as feel,
		size.city_name as size,
		thickness.city_name as thickness,
		variety.city_name as variety,
		loc.city_name as clocation,hsn.city_email_code,hsn.city_name as hsn_text');
		$this->db->from('model');
		if(!empty($cond))
			$this->db->or_where($cond);
		if($ord=='desc'or $ord=='asc')
			$this->db->order_by($column, $ord);
		else{
			$this->db->order_by("model_name", 'asc');
			$this->db->order_by("model_price", 'asc');
			$this->db->order_by("model_quantity", 'asc');
		}
    	$this->db->join('city c','c.city_id = model.model_range','left');
		$this->db->join('city loc','loc.city_id = model.model_location','left');
		$this->db->join('city hsn','hsn.city_id = model.model_HSN_SAC','left');
		$this->db->join('city make','make.city_id = model.model_make','left');
		$this->db->join('city feel','feel.city_id = model.model_feel','left');
		$this->db->join('city size','size.city_id = model.model_size','left');
		$this->db->join('city thickness','thickness.city_id = model.model_thickness','left');
		$this->db->join('city company','company.city_id = model.model_company','left');
		$this->db->join('city variety','variety.city_id = model.model_variety','left');
		$this->db->join('city category','category.city_id = model.model_category','left');
		if(!empty($single))
			$this->db->join('gallery g','g.model_id = model.model_id','left');
		if(isset($limit_per_page) &&  $limit_per_page != '')
			$this->db->limit($limit_per_page,$start_index);
		$query=$this->db->get();
	//print_r($this->db->last_query());exit;				
	return $query->result_array();
	}

/**
* Name: get_sel_model
* Parameters:- model id
* Use: fetches model details from db of particular model id
* tables used : model
*/
	function get_sel_gallery($id,$page){
		$ids = array($id);
		if ($page == 'model') {
			$model = $this->get_sel_model($id);
			$ids[1] = $model[0]['model_variety'];
		}
		$this->db->select('g.gallery_title,g.gallery_id,g.gallery_image,g.model_id');
		$this->db->from('gallery g');
		$this->db->where_in('g.model_id', $ids);
		if ($page != 'model') {
			$this->db->where('g.page_name', $page);
		}
		$query=$this->db->get();
		return $query->result_array();
	}

	function get_sel_model($id)
	{
		$this->db->select('m.*,hsn.city_name as hsn,hsn.city_email_code as hsn_per');
		$this->db->from('model m');
		$this->db->join('city hsn','hsn.city_id = m.model_HSN_SAC','left');
		$this->db->where('m.model_id', $id);
		$query=$this->db->get();
		return $query->result_array();
	}

/**
* Name: newmodel_delete
* Parameters:- model id
* Use: deletes model details from db of particular model id
* tables used : model
*/
	function newmodel_delete($id)
	{
		$this->db->where('model_id', $id);
		$delete=$this->db->delete('model');
		return $delete;
	}

/**
* Name: newmodel_delete
* Parameters:- model id and all fields of model form
* Use: updates model details from db of particular model id
* tables used : model
*/
	function update_newmodel($data, $id)
	{
		$this->db->where('model_id', $id);
		$this->db->set($data);
		$id=$this->db->update('model');
		return $id;
	}

/**
* Name: save_newmodel
* Parameters:- all fields of model form
* Use: inserts model details into db
* tables used : model
*/
	function save_newmodel($data,$more_models)
	{
		$insert_query = $this->db->insert_string('model', $data);
		$insert = $this->db->query($insert_query);
		if ($insert) {
			for($k=0;$k<count($more_models['more_sizes']);$k++) {
				if (!empty($more_models['more_sizes'][$k]) && !empty($more_models['more_prices'][$k]) && !empty($more_models['more_qnty'][$k])) {				
					$model_name = explode(' ',$data['model_name']);
					$model_name[1] = $more_models['more_model_name'][$k]; 
					$data['model_name'] = implode(' ',$model_name);
					$data['model_quantity'] = $more_models['more_qnty'][$k];
					$data['model_price'] = $more_models['more_prices'][$k];
					$data['model_size'] = $more_models['more_sizes'][$k];
					$insert1 = $this->db->insert('model',$data);
				}
			}
		}
		if ($insert)
			return true;
		else
			return fals;
	}

	function save_gallery($data)
	{
		$this->db->set($data);
		$insert=$this->db->insert('gallery');
		if($insert)
			return $this->db->insert_id();
		else
			return false;
	}

	function update_gallery($data, $id)
	{
		$this->db->where('gallery_id', $id);
		$this->db->set($data);
		$id=$this->db->update('gallery');
		return $id;
	}

	function delete_gallery($id){
		$this->db->where('gallery_id', $id);
		$delete=$this->db->delete('gallery');
		return $delete;
	}

	function check_model_name($name){
		$this->db->select('model_id');
		$this->db->from('model');
		$this->db->where($name);
		$query=$this->db->get();
		return $query->result_array();
	}

	function get_sorted_pages($k){ 
		$this->db->select('model_name,model_id');
		$this->db->from('model');
		$this->db->where('mode_category',$k);
		$this->db->order_by("orderby", 'asc');
		$query=$this->db->get();
		return $query->result_array();
	}

	function change_order($count,$data){
		$this->db->trans_start();
		$j=0;
		for($i=1;$i<=$count;$i++){
			$this->db->query('UPDATE model SET orderby='.$i.' WHERE model_id='.$data[$j][0].';');
			$j++;
		}
		$this->db->trans_complete();
		if($this->db->trans_status() === FALSE)
			return false;
		else
			return true;
	}

	function fetch_models(){
		$this->db->select('*');
		$this->db->from('model');
		$this->db->where('is_display',1);
		$this->db->order_by("model_variety", 'asc');
		$this->db->order_by("orderby", 'asc');
		$query=$this->db->get();
		return $query->result_array();
	}
//latest code
	function get_category($id)
	{
		$this->db->select('city_id,city_name');
		$this->db->from('city');
		$this->db->where('company', $id);
		$this->db->order_by("order_number", "asc");
		$this->db->order_by("city_name", "asc");
		$query=$this->db->get();
		return $query->result_array();
	}	

	function get_sizes_groupwise($id)
	{
		$this->db->select('city_id,city_name');
		$this->db->from('city');
		$this->db->where('group', $id);
		$this->db->order_by("order_number", "asc");
		$this->db->order_by("city_name", "asc");
		$query=$this->db->get();
		return $query->result_array();
	}	

	function get_master_data($arr) {
		$this->db->select('*');
		$this->db->from('city');
		$this->db->where('city_id', $arr['id']);
		$this->db->order_by("order_number", "asc");
		$this->db->order_by("city_name", "asc");
		$query=$this->db->get();
		$res = $query->result_array();
		if ($res[0]['thickness'] != null)
		$res[0]['thickness'] = unserialize($res[0]['thickness']);
		return $res;
	}

	function check_avl_bal($where){
		$this->db->select('model_quantity');
		$this->db->from('model');
		$this->db->where('model_id', $where['model_id']);
		$this->db->where('model_range', $where['model_range']);
		$query=$this->db->get();
		//print_r($this->db->last_query());
		if (count($query->result_array()) > 0) {
			$this->db->select('model_quantity');
			$this->db->from('model');
			$this->db->where('model_id', $where['model_id']);
			$this->db->where('model_quantity >=', $where['quantity']);
			$query=$this->db->get();
			if (count($query->result_array()) > 0)
				return 1;
			else
				return 2;
		} else
			return 0;
	}
	public function get_make_by_cat($id) {
		$this->db->select('distinct(model_make)');
		$this->db->from('model');
		$this->db->where('model_category', $id);
		$query=$this->db->get();
		return $query->result_array();	
	}
}
?>
