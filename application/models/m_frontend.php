<?php
class M_frontend extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function model_list($id=1,$cond = '')
	{
		$this->db->select('*');
		$this->db->from('model');
		if($id==1)
			$this->db->where('is_display', $id);
		if($cond)
			$this->db->where_in('model_id', $cond);
		//$this->db->order_by("category,model_name");
		$query=$this->db->get();
		return $query->result_array();
	}

	function filter_results($limit_per_page = '' , $start_index = null,$data,$amt,$make = null){
		
		$this->db->select('*');
		$this->db->from('model');
		$this->db->where('is_display', 1);
		$this->db->where('model_price between '.$amt[0].' and '.$amt[1]);		
		if(!empty($make))		
		$this->db->where($make);
		$this->db->rlike($data);
		$this->db->order_by("model_name");
		if(isset($limit_per_page) &&  $limit_per_page != '')
			$this->db->limit($limit_per_page,$start_index);		
		$query=$this->db->get();
		return $query->result_array();		
	}

	function cart_list($cond = ''){
		if(isset($cond) && !empty($cond)){
			$this->db->select('model_id,model_name,model_image,model_price,model_features,model_quantity');
			$this->db->from('model');
			$this->db->where_in('model_id', $cond);
			//$this->db->order_by("category,model_name");
			$query=$this->db->get();
			return $query->result_array();
		}
	}

	function all_models($id=1)
	{
		$this->db->select('*');
		$this->db->from('model');
		if($id==1)
			$this->db->where('is_display', $id);
		$this->db->order_by("category,orderby");
		$query=$this->db->get();
		return $query->result_array();
	}

	function get_location($id)
	{
		$this->db->select('*');
		$this->db->from('contact_us cnt');
		$this->db->join('city c', 'c.city_id = cnt.city_id');
		$this->db->join('location l', 'l.location_id = cnt.location_id');
		$this->db->where('c.`city_id`', $id);
		$query=$this->db->get();
		return $query->result_array();
	}

	function get_city()
	{
		$this->db->select('*');
		$this->db->from('city');
		$this->db->order_by("city.order_number");
		$query=$this->db->get();
		return $query->result_array();
	}

	function get_locationcity($id)
	{
		$this->db->select('*');
		$this->db->from('contact_us cnt');
		$this->db->join('city c', 'c.city_id = cnt.city_id');
		$this->db->join('location l', 'l.location_id = cnt.location_id');
		if($id!=0)
			$this->db->where('cnt.`location_id`', $id);
		$this->db->order_by("c.order_number");
		$this->db->order_by("cnt.contact_us_name");
		$query=$this->db->get();
		return $query->result_array();
	}

	function get_latd_long()
	{
		$this->db->select('`con`.`google_latitude`,l.`location_name`,
`con`.`google_longitude`,`con`.`google_marker`,`con`.`contact_us_name`');
		$this->db->from('contact_us con');
		$this->db->join('location l', 'l.location_id = con.location_id');
		$query=$this->db->get();
		return $query->result_array();
	}

	function get_city_latlong($city_id)
	{
		$this->db->select('`con`.`google_latitude`,l.`location_name`,
`con`.`google_longitude`,`con`.`google_marker`,`con`.`contact_us_name`');
		$this->db->from('contact_us con');
		$this->db->join('location l', 'l.location_id = con.location_id');
		if($city_id != 0)
			$this->db->where('`con`.`city_id`', $city_id);
		$query=$this->db->get();
		return $query->result_array();
	}

	function get_location_latlong($location_id,$group)
	{
		$this->db->select('`con`.`google_latitude`,l.`location_name`,
`con`.`google_longitude`,`con`.`google_marker`,`con`.`contact_us_name`');
		$this->db->from('contact_us con');
		$this->db->join('location l', 'l.location_id = con.location_id');
		if($location_id != 0)
			$this->db->where('`con`.`location_id`', $location_id);
		if($group != 0)
			$this->db->where('`con`.`group`', $group);
		$query=$this->db->get();
		return $query->result_array();
	}

	function get_category($id)
	{
		$this->db->select('*');
		$this->db->from('model');
		if($id!=0)
			$this->db->where('`category`', $id);
		$this->db->where('is_display', 1);
		$this->db->order_by("category,orderby");
		$query=$this->db->get();
		return $query->result_array();
	}

	function get_model($model)
	{
		$this->db->select('`model`.model_id,category,model_name,model_image,
model_caption,brochure,model_features,model_overview,
model_details,is_display,seo_title,seo_keyword,
seo_details,GROUP_CONCAT(`gallery`.`gallery_title`) as gallery_title,
GROUP_CONCAT(`gallery`.`gallery_image`) as gallery_image');
		$this->db->from('model');
		$this->db->join('gallery', 'model.model_id = gallery.model_id');
		$this->db->where('`model_name`', $model);
		$query=$this->db->get();
		if($query->num_rows() > 0)
			return $query->row_array();
		else
			return 0;
	}

	function get_service()
	{
		$this->db->select('*');
		$this->db->from('service');
		$this->db->like('service_title', 'service');
		$query=$this->db->get();
		return $query->row_array();
	}

	function accidental_repair()
	{
		$this->db->select('*');
		$this->db->from('service');
		$this->db->like('service_title', 'Accidental Repair');
		$query=$this->db->get();
		return $query->row_array();
	}

	function get_insurance()
	{
		$this->db->select('*');
		$this->db->from('service');
		$this->db->like('service_title', 'insurance');
		$query=$this->db->get();
		return $query->row_array();
	}

	function get_preowned()
	{
		$this->db->select('*');
		$this->db->from('service');
		$this->db->like('service_title', 'owned');
		$query=$this->db->get();
		return $query->row_array();
	}

	function maruti_driving_school()
	{
		$this->db->select('*');
		$this->db->from('service');
		$this->db->like('service_title', 'maruti');
		$query=$this->db->get();
		return $query->row_array();
	}

	function get_careers()
	{
		$this->db->select('*');
		$this->db->from('service');
		$this->db->like('service_title', 'care');
		$query=$this->db->get();
		return $query->row_array();
	}

	function get_extended_warranty()
	{
		$this->db->select('*');
		$this->db->from('service');
		$this->db->like('service_title', 'extended');
		$query=$this->db->get();
		return $query->row_array();
	}

	function about_us()
	{
		$this->db->select('*');
		$this->db->from('service');
		$this->db->like('service_title', 'about');
		$query=$this->db->get();
		return $query->row_array();
	}

	function get_sales()
	{
		$this->db->select('*');
		$this->db->from('service');
		$this->db->like('service_title', 'sales');
		$query=$this->db->get();
		return $query->row_array();
	}

	function get_salesmenu($id = 1)
	{
		$this->db->select('service_title,route');
		$this->db->from('service');
		$this->db->where('is_display', $id);
		$this->db->order_by("order_number");
		$query=$this->db->get();
		return $query->result_array();
	}

	function get_workshopmenu($id = 2)
	{
		$this->db->select('service_title,route');
		$this->db->from('service');
		$this->db->where('is_display', $id);
		$this->db->order_by("order_number");
		$query=$this->db->get();
		return $query->result_array();
	}

	function get_insurancemenu($id = 3)
	{
		$this->db->select('service_title,route');
		$this->db->from('service');
		$this->db->where('is_display', $id);
		$query=$this->db->get();
		return $query->row_array();
	}

	function get_dynamic($name)
	{
		$this->db->select('*');
		$this->db->from('service');
		$this->db->like('service_title', $name, 'after');
		$query=$this->db->get();
		return $query->row_array();
	}

	function get_loc_details($id){
		$this->db->select('group');
		$this->db->from('contact_us');
		$this->db->where('location_id',$id);
		$query=$this->db->get();
		return $query->result_array();
	}

	function get_footerpages($id = 4){
		$this->db->select('service_title,route');
		$this->db->from('service');
		$this->db->where('is_display', $id);
		$this->db->where('service_title != "Careers"');
		$this->db->order_by("order_number");
		$query=$this->db->get();
		return $query->result_array();
	}

	function get_emailcode($city_id) {
		$this->db->select('city_email_code');
		$this->db->from('city');
		$this->db->where('city_id', $city_id);
		$query=$this->db->get();
		return $query->row_array();
	}

	function getcity_grpwise($circle_id,$page) {
		$page=(int)$page;
		$cond='cu.group & '.$page.'<> 0 and cu.location_id='.$circle_id;
		$this->db->distinct();
		$this->db->select('c.city_name,cu.city_id');
		$this->db->from('contact_us cu');
		$this->db->where($cond);
		$this->db->join('city c', 'c.city_id = cu.city_id');
		$this->db->order_by("c.order_number");
		$query=$this->db->get();
		return $query->result_array();
	}

	function getallcity_grp() {
		$this->db->distinct();
		$this->db->select('c.city_name,cu.city_id,cu.group,cu.location_id');
		$this->db->from('contact_us cu');
		$this->db->join('city c', 'c.city_id = cu.city_id');
		$this->db->order_by("c.order_number");
		$query=$this->db->get();
		return $query->result_array();
	}

	function getlocation_by_group($group_id) {
		$this->db->distinct();
		$this->db->select('l.location_name,l.location_id');
		$this->db->from('contact_us cu');
		$this->db->join('location l', 'l.location_id = cu.location_id');
		$this->db->where('cu.group & ' . $group_id);
		$query=$this->db->get();
		return $query->result_array();
	}

	function getcity_by_group($group_id) {
		$this->db->select('c.city_name');
		$this->db->from('contact_us cu');
		$this->db->join('city c', 'c.city_id = cu.city_id');
		$this->db->where('cu.group & ' . $group_id);
		$this->db->order_by("c.city_name");
		$query=$this->db->get();
		return $query->result_array();
	}

	function getloc_city_by_group($group_id) {
		$this->db->select('l.`location_name` as city,GROUP_CONCAT(c.`city_name` order by c.`order_number`) as loc');
		$this->db->from('contact_us cu');
		$this->db->join('location l', 'l.location_id = cu.location_id');
		$this->db->join('city c', 'c.city_id = cu.city_id');
		$this->db->where('cu.group & ' . $group_id);
		$this->db->order_by("l.`order_number`,c.`order_number`");
		$this->db->group_by("l.`location_name`");
		$query=$this->db->get();
		return $query->result_array();
	}

	function save_feedback($save_feedback){
		$this->db->set($save_feedback);
		$insert=$this->db->insert('statistic_report');
		if($insert)
			return $this->db->insert_id();
		else
			return false;
	}

	function save_newsletter($save_newsletter){
		$this->db->set($save_newsletter);
		$insert=$this->db->insert('newsletter');
		if($insert)
			return $this->db->insert_id();
		else
			return false;
	}

	function fetch_city_loc_ids($city_name){
		$this->db->select('c.city_id,c.location_id');
		$this->db->from('city c');
		$this->db->where('c.city_name',$city_name);
		$query=$this->db->get();
		return $query->result_array();
	}

	function fetch_gallery(){
		$this->db->select('*');
		$this->db->from('gallery');
		$query=$this->db->get();
		return $query->result_array();
	}
}
?>
