<?php
class M_services extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

/**
* Name: service_list
* Parameters:- order (asc or desc or 0)
* Use: fetches service details from db in order specified by user. asc order by default.
* tables used : service
*/
	function service_list($ord,$page_id)
	{
		$this->db->select('*');
		$this->db->from('service');
		$this->db->where('is_display',$page_id);
		if($ord=='desc' or $ord=='asc')
			$this->db->order_by("service_title", $ord);
		else
			$this->db->order_by("order_number", 'asc');
		$query=$this->db->get();
		return $query->result_array();
	}

	function get_sorted_menu(){
		$this->db->select('service_title,service_id');
		$this->db->from('service');
		$this->db->where('is_display',1);
		$this->db->order_by("order_number", 'asc');
		$query=$this->db->get();
		return $query->result_array();
	}

	function get_sorted_sales (){
		$this->db->select('service_title,service_id');
		$this->db->from('service');
		$this->db->where('is_display',2);
		$this->db->order_by("order_number", 'asc');
		$query=$this->db->get();
		return $query->result_array();
	}

	function get_sorted_insurance (){
		$this->db->select('service_title,service_id');
		$this->db->from('service');
		$this->db->where('is_display',3);
		$this->db->order_by("order_number", 'asc');
		$query=$this->db->get();
		return $query->result_array();
	}

	function get_sorted_footer(){
		$this->db->select('service_title,service_id');
		$this->db->from('service');
		$this->db->where('is_display',4);
		$this->db->where('service_title != "careers"');
		$this->db->order_by("order_number", 'asc');
		$query=$this->db->get();
		return $query->result_array();
	}
/**
* Name: get_services
* Parameters:- service id
* Use: fetches service details from db of perticular service id
* tables used : service
*/
	function get_services($id)
	{
		$this->db->select('*');
		$this->db->from('service');
		$this->db->where('service_id', $id);
		$query=$this->db->get();
		return $query->result_array();
	}

/**
* Name: service_delete
* Parameters:- service id
* Use: deletes service details from db of perticular service id
* tables used : service
*/
	function service_delete($id)
	{
		$this->db->where('service_id', $id);
		$delete=$this->db->delete('service');
		return $delete;
	}

/**
* Name: service_delete
* Parameters:- service id  and all fields of service form
* Use: updates service details from db of perticular service id
* tables used : service
*/
	function update_services($data, $id)
	{
		$this->db->where('service_id', $id);
		$this->db->set($data);
		$id=$this->db->update('service');
		return $id;
	}

/**
* Name: save_services
* Parameters:- all fields of service form
* Use: inserts service details into db
* tables used : service
*/
	function save_services($data)
	{
		$this->db->set($data);
		$insert=$this->db->insert('service');
		if($insert)
			return $this->db->insert_id();
		else
			return false;
	}

	function change_order($count,$data){
		$this->db->trans_start();
		$j=0;
		for($i=1;$i<=$count;$i++){
			$this->db->query('UPDATE service SET order_number='.$i.' WHERE service_id='.$data[$j][0].';');
			$j++;
		}
		$this->db->trans_complete();
		if($this->db->trans_status() === FALSE)
			return false;
		else
			return true;
	}
}
?>
