<?php
class M_location extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

/**
* Name: get_location
* Parameters:-
* Use: fetches location_name and  location_id  from db in asc order
* tables used : location
*/
	function get_location()
	{
		$this->db->select('location_id,location_name');
		$this->db->from('location');
		$this->db->order_by("order_number", "asc");
		$this->db->order_by("location_name", "asc");
		$query=$this->db->get();
		return $query->result_array();
	}

/**
* Name: get_city
* Parameters:- location id
* Use: fetches city details from db of perticular location id in asc order
* tables used : city
*/
	function get_city($id)
	{
		$this->db->select('*');
		$this->db->from('city');
		$this->db->where('location_id', $id);
		$this->db->order_by("order_number", "asc");
		$this->db->order_by("city_name", "asc");
		$query=$this->db->get();
		return $query->result_array();
	}

/**
* Name: get_city_location
* Parameters:- city id
* Use: fetches location(state) and city details from db of perticular city id
* tables used : city,location
*/
	function get_city_location($id=null)
	{
		$this->db->select('*');
		$this->db->from('city c');
		$this->db->join('location l', 'l.location_id = c.location_id');
		if($id!=null)
			$this->db->where('city_id', $id);
		$this->db->order_by('l.order_number','asc');
		$this->db->order_by('c.order_number','asc');
		$query=$this->db->get();
		return $query->result_array();
	}

/**
* Name: save_city
* Parameters:- all fields from location form
* Use: inserts city details into db
* tables used : city
*/
	function save_city($data)
	{
		$this->db->set($data);
		$insert=$this->db->insert('city');
		if($insert)
			return $this->db->insert_id();
		else
			return false;
	}

/**
* Name: update_city
* Parameters:- all fields from location form and city id
* Use: updates city details from db
* tables used : city
*/
	function update_city($data, $id)
	{
		$this->db->where('city_id', $id);
		$this->db->set($data);
		$update=$this->db->update('city');
		return $update;
	}

/**
* Name: save_location
* Parameters:- all fields from location form
* Use: inserts location details into db
* tables used : location
*/
	function save_location($data)
	{
		$this->db->set($data);
		$insert=$this->db->insert('location');
		if($insert)
			return $this->db->insert_id();
	}

/**
* Name: delete_city
* Parameters:- city id
* Use: deletes city details from db of perticular city id
* tables used : city
*/
	function delete_city($cit_id)
	{
		$this->db->select('*');
		$this->db->from('model');
		$this->db->like('model_category',$cit_id);
		$this->db->or_like('model_company',$cit_id);
		$this->db->or_like('model_variety',$cit_id);
		$this->db->or_like('model_location',$cit_id);
		$this->db->or_like('model_size',$cit_id);
		$this->db->or_like('model_thickness',$cit_id);
		$this->db->or_like('model_range',$cit_id);
		$this->db->or_like('model_HSN_SAC',$cit_id);
		$this->db->or_like('model_make',$cit_id);
		$this->db->or_like('model_feel',$cit_id);
		$this->db->or_like('model_group',$cit_id);
		$query=$this->db->get();
		$model_exists = $query->result_array();

		$this->db->select('*');
		$this->db->from('city');
		$this->db->like('company',$cit_id);
		$this->db->or_like('range',$cit_id);
		$this->db->or_like('hsn_sac',$cit_id);
		$this->db->or_like('feel',$cit_id);
		$this->db->or_like('group',$cit_id);
		$this->db->or_like('make',$cit_id);
		$this->db->or_like('thickness',$cit_id);
		$query=$this->db->get();
		$master_exists = $query->result_array();

		if (count($model_exists) < 1 && count($master_exists) < 1) {
			$this->db->where('city_id', $cit_id);
			$delete=$this->db->delete('city');
			return $delete;
		}
		return false;
	}

/**
* Name: delete_location
* Parameters:- location id
* Use: deletes location details from db of perticular location id
* tables used : location
*/
	function delete_location($loc_id)
	{
		$this->db->where('location_id', $loc_id);
		$delete=$this->db->delete('location');
		return $delete;
	}

	function update_circle($data,$id){
		$this->db->where('location_id', $id);
		$this->db->set($data);
		$update=$this->db->update('location');
		return $update;
	}

	function save_circle($data){
		$this->db->set($data);
		$insert=$this->db->insert('location');
		if($insert)
			return $this->db->insert_id();
	}

	function check_circle_name ($name){
		$this->db->select('location_id');
		$this->db->from('location');
		$this->db->where('location_name', $name);
		$query=$this->db->get();
		return $query->result_array();
	}

	function check_location_name ($name,$location_id){
		$this->db->select('city_id');
		$this->db->from('city');
		$this->db->where('city_name', $name);
		$this->db->where('location_id', $location_id);
		$query=$this->db->get();
		return $query->result_array();
	}

	function change_order($count,$data,$table){
		$this->db->trans_start();
		$j=0;
		for($i=1;$i<=$count;$i++){
			$this->db->query('UPDATE '.$table.' SET order_number='.$i.' WHERE '.$table.'_id='.$data[$j][0].';');
			$j++;
		}
		$this->db->trans_complete();
		if($this->db->trans_status() === FALSE)
			return false;
		else
			return true;
	}

	function get_max_orderno($table,$loc=null){
		$this->db->select('max(order_number) as max');
		$this->db->from($table);
		if(!empty($loc))
		$this->db->where('location_id', $loc);
		$query=$this->db->get();
		return $query->row_array();
	}
	
	function create_fields($field){
		$data=array($field=>array('type'=>'INT','NULL'=>true));
		$this->load->dbforge();
		$this->dbforge->add_column('model',$data);
	}
	function delete_fields($field){
		$this->load->dbforge();
		$this->dbforge->drop_column('model',$field);
	}	
	function modify_fields($old,$new){
		$data=array('model_'.lcfirst($old)=>array('name'=>'model_'.lcfirst($new),'type'=>'INT','NULL'=>true));
		$this->load->dbforge();
		$this->dbforge->modify_column('model',$data);
	}
	
}
?>
