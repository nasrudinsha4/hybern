<?php
class M_homepage extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function get_dashboard_data($cond){
		$this->db->select('s.form_type,l.location_name as city,c.city_name as location,c.city_id,count(s.form_type) as total');
		$this->db->group_by('s.form_type,s.city');
		$this->db->from('statistic_report s');
		$this->db->join('city c', 's.city = c.city_id');
		$this->db->join('location l', 'c.location_id=l.location_id');
		$this->db->order_by('l.order_number,c.order_number,s.form_type');
		$this->db->where($cond);
		$query=$this->db->get();
		return $query->result_array();
	}
/**
* Name: homepage_add
* Parameters:-  all fields of home page form
* Use: inserts homepage details into db
* tables used : settings
*/
	public function homepage_add($data)
	{
		$insert=$this->db->insert('settings', $data);
		if($insert)
			return $this->db->insert_id();
	}

/**
* Name: get_hompage_data
* Parameters:-
* Use: fetches homepage details from db
* tables used : settings
*/
	public function get_hompage_data()
	{
		$this->db->select('*');
		$this->db->from('settings');
		$query=$this->db->get();
		return $query->result_array();
	}

/**
* Name: homepage_update
* Parameters:- all fields of home page form and homepage id
* Use: updates homepage details from db
* tables used : settings
*/
	public function homepage_update($data, $id)
	{
		$this->db->where('settings_id', $id);
		$this->db->set($data);
		$update=$this->db->update('settings');
		return $update;
	}

	function delete_statistic_report($cond){
		$this->db->where($cond);
		$this->db->delete('statistic_report');
		return $this->get_stats_date();
	}

	function get_stats_date(){
		$this->db->select('min(date(date_created)) as date_created');
		$this->db->from('statistic_report');
		$query=$this->db->get();
		return $query->row_array();
	}
}
?>
