<?php
class M_billing extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

/**
* Name: billing_list
* Parameters:-  order(asc or desc)
* Use: fetches all billing details from db in order specified by user. asc order by default
*/
	function billing_list($ord,$cond=null)
	{
		$this->db->select('*');
		if(!empty($cond)) {
			foreach($cond as $k => $v) {
				if ($k != 'dt_created') 
					$this->db->or_like('cnt.'.$k,$v);
				else
					$this->db->or_like('bi.'.$k,$v);
			}
		}
		$this->db->from('billing cnt');
		$this->db->join('billing_items bi','bi.billing_id=cnt.billing_id');
		$this->db->order_by("cnt.billing_id", 'desc');
		$query=$this->db->get();
		return $query->result_array();
	}

/**
* Name: update_newbilling
* Parameters:- billing id and mandatory or all fields of billing form
* Use: updates billing details from db of supplied billing id
* tables used : billing
*/
	function update_newbilling($data,$post,$today,$id)
	{
		$this->db->trans_start();
		$this->db->where('billing_id', $id);
		$this->db->set($data);
		if ($id=$this->db->update('billing'))
			$this->generate_billing_items($data,$post,$today,'');
		$this->db->trans_complete();		
		if($this->db->trans_status() === FALSE)
			return false;
		else
			return true;
	}

/**
* Name: save_newbilling
* Parameters:-  mandatory or all fields of billing form
* Use: inserts billing details into db
* tables used : billing
*/
	function save_newbilling($data,$post,$today,$type = '')
	{
		$this->db->trans_start();
		$this->db->set($data);
		if ($insert=$this->db->insert('billing'))
			$this->generate_billing_items($data,$post,$today,$type);
		$this->db->trans_complete();
		if($this->db->trans_status() === FALSE)
			return false;
		else
			return true;
	}

/**
* Name: billing_edit
* Parameters:-  billing id
* Use: fetches billing details from db of supplied billing id
* tables used : billing
*/
	function billing_edit($id)
	{
		if(isset($id)and!empty($id)) {
			$this->db->select('*');
			$this->db->from('billing cnt');
			$this->db->join('billing_items bi','bi.billing_id=cnt.billing_id');
			$this->db->where('cnt.billing_id', $id);
			$query=$this->db->get();
			return $query->result_array();
		}
	}

/**
* Name: billing_delete
* Parameters:-  billing id
* Use: deletes billing details from db of supplied billing id
* tables used : billing
*/
	function billing_delete($id)
	{
		$this->db->trans_start();
		$this->db->where('billing_id', $id);
		$delete=$this->db->delete('billing_items');
		$this->db->where('billing_id', $id);
		$delete=$this->db->delete('billing');
		$this->db->trans_complete();
		if($this->db->trans_status() === FALSE)
			return false;
		else
			return true;
		
	}

	function validate_billing($id){
			$this->db->select('billing_id');
			$this->db->from('billing');
			$this->db->where('city_id', $id);
			$query=$this->db->get();
			return $query->result_array();
	}
	
	function save_billing_items($data,$model_data){
		$this->db->insert_batch('billing_items',$data);
		for($i=0;$i<count($model_data);$i++){
			$this->db->where('model_id', $model_data[$i]['model_id']);
			unset($model_data[$i]['model_id']);
			$this->db->set('model_quantity',$model_data[$i]['model_quantity'],FALSE);
			$this->db->update('model');
		}
	}

	function generate_billing_items($data,$post,$today,$type) {
		$total = 0;
		if ($type == '') { 
			for($i=0;$i<$post['row_count'];$i++){
					$sub='model_quantity-'.$post['quantity_row_'.$i];
					$data['update_models'][$i]=array(
						'model_quantity'=>$sub,
						'model_id'=>$post['modelno_row_'.$i],
					);
					$total = $total + $post['amount_row_'.$i];
					$data['billing'][$i]=array(
						'billing_id'=>(($post['hidden']!="")? $post['hidden'] : $this->db->insert_id()),
						'dt_created'=>$today,
						'billing_date'=>date('Y-m-d',strtotime($post['billing_date'])),
						'delivery_date'=>date('Y-m-d',strtotime($post['delivery_date'])),
						//'model'=>addslashes($post['model_row_'.$i]),
						'model'=>$post['model_row_'.$i],
						'location'=>$post['model_location'],
						'quantity'=>$post['quantity_row_'.$i],
						'price'=>$post['price_row_'.$i],
						'taxable_value'=>$post['taxable_val_row_'.$i],
						'discount'=>$post['discount_row_'.$i],
						'HSN_SAC'=>$post['HSN_SAC_row_'.$i],
						'gst_rate'=>$post['gstrate_row_'.$i],
						'gst_amt'=>$post['gst_amount_row_'.$i],
						'amount'=>$post['amount_row_'.$i],
						'invoice_advance'=>$post['advance'],
						'invoice_balance'=>$total - $post['advance'],
					);
			}
		} else {
			foreach($post as $key => $res) {
				$post[$key]['billing_id'] = $this->db->insert_id();
			}
			$this->db->insert_batch('billing_items',$post);
		}

		if(isset($data['billing'])){
			$this->save_billing_items($data['billing'],$data['update_models']);
		}


	}


}
?>
