<?php
class M_admin extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}
/**
* Name: check_login_details
* Parameters:-  username, password
* Use: Check if user exists
* Common Function: Yes
*/

	function check_login_details($data)
	{
		$this->db->select('*');
		$this->db->from('admin_details');
		$this->db->where($data);
		$query=$this->db->get();
		if($query->num_rows()==1)
			return $query->row_array();
		else
			return null;
	}

/**
* Name: dataUpdate_admin
* Parameters:-  admin id, last_active
* Use: saves time and date of last activity made by user
* Common Function: Yes
*/
	function dataUpdate_admin($check_login, $saveArray)
	{
		$this->db->where('admin_id', $check_login);
		$this->db->set($saveArray);
		$id=$this->db->update('admin_details');
		return $id;
	}
}
?>
