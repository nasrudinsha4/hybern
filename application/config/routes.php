<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|

|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
//$route['default_controller'] = 'client/frontend';
$route['default_controller'] = 'client/frontend';
$route['size-guide'] = 'client/frontend/size_guide';
$route['buying-guide'] = 'client/frontend/buying_guide';
$route['our-story'] = 'client/frontend/aboutus';
$route['contact'] = 'client/frontend/contactus';
$route['gallery'] = 'client/frontend/gallery';
$route['billing'] = 'client/frontend/billing';
$route['save-billing'] = 'client/frontend/save_billing';
$route['thankyou'] = 'client/frontend/thankyou';
$route['filter_results'] = 'client/frontend/filter_results';
$route['category'] = 'client/frontend/category';
$route['addtocart'] = 'client/frontend/addtocart';
$route['delete_cart'] = 'client/frontend/delete_cart';
$route['cart'] = 'client/frontend/cart';
$route['products/(:num)'] = 'client/frontend/shop';
$route['products/(:num)/(:num)'] = 'client/frontend/shop/$1/$1';
$route['single/(:any)'] = 'client/frontend/single';
$route['model/(:any)'] = 'client/frontend/model_car';
$route['services'] = 'client/frontend/services';
$route['insurance'] = 'client/frontend/insurance';
$route['maruti-driving-school'] = 'client/frontend/maruti_driving_school';
$route['careers'] = 'client/frontend/careers';
$route['pre-owned-cars'] = 'client/frontend/preowned';
$route['extended-warranty'] = 'client/frontend/extended_warranty';
$route['accidental-repair'] = 'client/frontend/accidental_repair';
$route['about-us'] = 'client/frontend/about_us';
$route['sitemap'] = 'client/frontend/sitemap';
$route['googlemap'] = 'client/frontend/googlemap';
$route['get_city'] = 'client/frontend/get_city';
$route['get_location'] = 'client/frontend/get_location';
$route['send_contactus_feedback'] = 'client/frontend/send_contactus_feedback';
$route['get_category'] = 'client/frontend/get_category';
$route['send_insurance_feedback'] = 'client/frontend/send_insurance_feedback';
$route['send_maruti_feedback'] = 'client/frontend/send_maruti_feedback';
$route['send_preowned_feedback'] = 'client/frontend/send_preowned_feedback';
$route['send_extended_warranty_feedback'] = 'client/frontend/send_extended_warranty_feedback';
$route['send_accidental_feedback'] = 'client/frontend/send_accidental_feedback';
$route['send_service_feedback'] = 'client/frontend/send_service_feedback';
$route['send_career_feedback'] = 'client/frontend/send_career_feedback';
$route['send_testdrive'] = 'client/frontend/send_testdrive';
$route['get_city_latlong'] = 'client/frontend/get_city_latlong';
$route['sales_enquiry'] = 'client/frontend/sales_enquiry';
$route['get_location_latlong'] = 'client/frontend/get_location_latlong';
$route['get_grouplocation_latlong'] = 'client/frontend/get_grouplocation_latlong';
$route['get_loc_details'] = 'client/frontend/get_loc_details';
$route['getcity_grpwise'] = 'client/frontend/getcity_grpwise';
$route['save_newsletter'] = 'client/frontend/save_newsletter';
$route['404_override'] = 'pagenotfound';

//backend
$route['admin']="layout/admin_login";
$route['logout']="admin/logout";
$route['check-avl-bal']="admin/models/check_avl_bal";
$route['admin/dashboard']="admin/admin_dashboard/index";
$route['admin/add_homepage']="admin/admin_dashboard/add_homepage";
$route['admin/home-page']="admin/admin_dashboard/home_page";
$route['admin/masters']="admin/location/index";
$route['location_edit/(:num)']="admin/location/location_edit";
$route['location_delete/(:any)']="admin/location/location_delete";
$route['circle_delete/(:any)']="admin/location/circle_delete";
$route['admin/save-location']="admin/location/save_location";
$route['admin/save-billing']="admin/billing/save_billing";
$route['admin/billing']="admin/billing/index";
$route['billing_edit/(:num)']="admin/billing/billing_edit";
$route['billing_delete/(:num)']="admin/billing/billing_delete";
$route['admin/billing-order/(:any)']="admin/billing/index";
$route['admin/pages']="admin/services/index";
$route['admin/service-order/(:any)']="admin/services/index";
$route['admin/save-services']="admin/services/save_services";
$route['get_services/(:num)']="admin/services/get_services";
$route['service_delete/(:num)']="admin/services/service_delete";
$route['admin/model']="admin/models/index";
$route['admin/save_newmodel']="admin/models/save_newmodel";
$route['gallery_edit/(:num)/(:any)']="admin/models/gallery_edit";
$route['model_edit/(:num)']="admin/models/model_edit/";
$route['newmodel_delete/(:num)']="admin/models/newmodel_delete";
$route['admin/model-order/(:any)/(:any)']="admin/models/index";
$route['save_gallery']="admin/models/save_gallery";
$route['save_newmodel_files']='admin/models/save_newmodel_files';
$route['delete_gallery']="admin/models/delete_gallery";
$route['findcity']="admin/billing/findcity";
$route['get-location']="admin/billing/get_location";
$route['check_model_name']="admin/models/check_model_name";
$route['check_circle_name']="admin/location/check_circle_name";
$route['check_location_name']="admin/location/check_location_name";
$route['services-get_sorted_pages']="admin/services/get_sorted_pages";
$route['models-get_sorted_pages']="admin/models/get_sorted_pages";
$route['changepassword']="layout/admin_login/changepassword";
$route['delfiles']="admin/models/delfiles";
$route['get_category']="admin/models/get_category";
$route['get_master_data']="admin/models/get_master_data";
$route['get_sizes_groupwise']="admin/models/get_sizes_groupwise";
$route['services-change_order']="admin/services/change_order";
$route['models-change_order']="admin/models/change_order";
$route['location-change_order']="admin/location/change_order";
$route['validate_billing']="admin/billing/validate_billing";
$route['statistic-report']="admin/admin_dashboard/statistic_report";
$route['delete_statistic_report']="admin/admin_dashboard/delete_statistic_report";
//routes for feedback forms
$route['submit_feedback']="client/frontend/submit_feedback";
/*
routes for feedback forms and dynamic pages and 404 pages
the below line should be placed at the end of the file
*/
//$route[':any'] = 'client/frontend/dynamic_page';

/* End of file routes.php */
/* Location: ./application/config/routes.php */
