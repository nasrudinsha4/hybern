<?php
	define("BASE_URL","https://hybern.co.in/");
	//define("BASE_URL","http://127.0.0.1/hybern/");
	///////////////////////////////////
	// Do NOT modify the lines below //
	///////////////////////////////////
	define('TITLE_CIL','HYBERN');
	define('MAIL_HOST','mscan.opspl.com');
	define('MAIL_PROTOCOL','smtp');
	define('SMTP_PORT','25');
	define('SMTP_USERNAME','');
	define('SMTP_PASSWORD','');
	define('FEEDBACK_SUB','Enquiry - ');
	define('TEST_DRIVE','Book a Test Drive');
	define('EMAILCODE','@chowguleind.com');
	define('FEEDBACK_TO','website.enquiry' . EMAILCODE);
	define('FEEDBACK_FROM','website' . EMAILCODE);
	define('FEEDBACK_FROM_NAME','CIL - Website');
	define('FROM_ADD','website' . EMAILCODE);
	define('FROM_NAME','Website');
	define('TO_ADD','feedback.service' . EMAILCODE);
	define('SALES_FEEDBACK','feedback.sales' . EMAILCODE);

	/* Login Page */
	define('INVALID_USER', 'Incorrect login details');
	define('TITLE','HOME');
	define('PG_TITLE_LOGIN','MEMBER LOGIN');
	define('PG_TITLE_LOCATION','MASTERS');
	define('PG_TITLE_BILLING','BILLING');
	define('PG_TITLE_HOME','HOME PAGE');
	define('SAVED','Record Inserted Successfully');
	define('UPDATED','Record Updated Successfully');
	define('DELETED','Record Deleted Successfully');
	define('ERROR_UPDATED','Unable to Update the Record');
	define('ERROR_DELETED','Unable to Delete the Record');
	define('ERROR_SAVED','Unable to Save the Record');
	define('PAGES','PAGES');
	define('MODELS','Products (Models)');
	define('CONFIRM_DELETE',"This record will be deleted permanently. Are you sure?");
	define('CONFIRM_DELETE2',"Records will be deleted permanently and cannot be recovered.<br>Are you sure you wish to delete?");
	define('NO_RECORDS_FOUND',"No records found");
	define('FEEDBACK_SUCCESS','Your query has been submitted successfully. <br>We will get back to you shortly.');
	define('DIR','./uploads/');
	define('IMAGEDIR','images/');
	define('DIRCLIENT','./uploads/client/');
	define('DIR_THUMB','./uploads/thumbnails/');
	define('DIR_BROCHURE','./uploads/brochures/');
	define('GALLERY','./uploads/gallery/');
	define("OUTLETS", serialize(array('Sales'=>1,'Service'=>2,'Pre-owned_Cars'=>4,'Driving_School'=>8,'SML'=>16)));
	define("ALL_OUTLETS",255);
	define("CATEGORY", serialize(array(1=>'Hatchbacks',2=>'Sedans',3=>'Vans',4=>'MUVs')));
	define("PAGE_TYPE",serialize(array(1=>'Sales Menu',2=>'Workshop Menu',3=>'Insurance Menu',4=>'Footer')));
	define('MULTI_OUTLETS','Red-marker.png');
	define("MARKER",serialize(array('Sales'=>'Blue-marker.png','Service'=>'Green-marker.png','Pre-owned_Cars'=>'Yellow-marker.png','Driving_School'=>'Orange-marker.png','SML'=>'Purple-marker.png')));
	define("INTERESTED_IN",serialize(array('Exchange'=>1,'Purchase'=>2,'Sales'=>3)));
	define("SERVICE_TYPE",serialize(array('Pick up Drop off'=>1,'Night Shift Service'=>2,'Door Step Service(MMS)'=>3,'Express Service - Wait & Take Service'=>4)));

	//statistic report
	define("FORM_TYPE",serialize(array('Sales Enquiry'=>1,'Book a Test Drive'=>2,'Maruti Driving School'=>3,'Pre-Owned Cars'=>4,'Extended Warranty'=>5,'Services'=>6,'Accidental Repair'=>7,'Insurance'=>8,'Careers'=>9,'Contact us'=>10,'Body Repair'=>11,'Feedback'=>12,'Sales Feedback'=>13)));
	define('AVAILABLE_DATA','Information available from ');
	define("SIZE_GROUPS", serialize(array(234=>'Single',235=>'Double',236=>'Queen',237=>'King')));
?>
