<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta http-equiv="content-language" content="en" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Login</title>
		<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>css/bootstrap.min.css" />
		<link rel="icon" href="<?php echo base_url(); ?>images/favicon.png" type="image/png"/>
		<script type="text/javascript" src="<?php echo BASE_URL; ?>js/functions.js"></script>
		<script type="text/javascript">var SITE_URL = "<?= BASE_URL ?>";</script>
		<script type="text/javascript" src="<?php echo BASE_URL;?>js/jquery.min.js"></script>
		<script type="text/javascript" src="<?= BASE_URL ?>js/md5.js"></script>
</head>
<body style="padding-top:5%;background: rgba(175, 207, 243, 0.47);">
<div class="container">
		<div class="row">
		<div class="col-md-4 col-md-offset-4">
		<center>
			<img style="padding: 5%;" src="<?php echo BASE_URL ?>images/logo.png" alt=""/>
		</center>
				<div class="panel panel-default" style="box-shadow: 2px 2px 5px 3px rgba(0,0,0,0.61);">
				<div class="panel-heading">
				<h3 class="panel-title">Sign In</h3>
				</div>			<?php	if(isset($error_msg) and !empty($error_msg)){?>
			<div id="error" style="color: #CA1515;text-align: center;padding: 5px;"><?php echo $error_msg;?></div>
			<?php }?>
				<div class="panel-body">
						<form method="post" name="login" id="form_login" action="<?php echo BASE_URL; ?>admin" >
						<input type="hidden" name="md5pass" id="md5pass">
					<fieldset>
						<div id='divtxt_username_err' style="color: #CA1515;"></div>
							<div class="form-group">
								<input class="form-control" onkeypress="return filterInput(2,event,false,'')" name="txt_username" id="txt_username" placeholder="Username"  type="text" >
							</div>
							<div id='divtxt_password_err' style="color: #CA1515;"></div>
							<div class="form-group">
								<input class="form-control" onkeypress="return filterInput(3,event,false,'');" name="txt_password" id="txt_password" placeholder="Password" type="password" >
							</div>
						<input class="btn btn-lg btn-success btn-block" onclick="return submit_login()" type="submit" value="Login">
						</fieldset>
						</form>
					</div>
			</div>
		</div>
	</div>
</div>
	<script type="text/javascript">
	$(document).ready(function(){
		$('#txt_username').focus();
		$('#txt_username').keypress(function() {
			$('#divtxt_username_err').html('');
			$('#error').html('');
		});
		$('#txt_password').keypress(function() {
			$('#divtxt_password_err').html('');
			$('#error').html('');
		});
			$('#error').delay(3000).fadeOut('slow');
	});
	function submit_login(){
		var blnflag=0;
		if($('#txt_username').val()==''){
			$('#txt_username').focus();
			$('#divtxt_username_err').html("Enter username");
			blnflag=1;
		}
		if($('#txt_password').val()==''){
			$('#divtxt_password_err').html("Enter password");
			blnflag=1;
		}
		if(blnflag<1)
		{
			var username = $('#txt_username').val();
			var password = $('#txt_password').val();
			$('#md5pass').val(md5(password));
			$('#form_login').submit();
		}
		else
			return false;
	}
	</script>
