<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta http-equiv="content-language" content="en" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php if(isset($page_title)) echo $page_title." | ".TITLE_CIL; else echo TITLE_CIL;?></title>
		<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>css/style.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>css/bootstrap-select.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>css/bootstrap-editable.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL; ?>css/font-awesome.min.css" />
		<link rel="icon" href="<?php echo base_url(); ?>images/favicon.png" type="image/png"/>
		<script type="text/javascript" src="<?php echo BASE_URL;?>js/jquery.min.js"></script>
		<script type="text/javascript" src="<?php echo BASE_URL; ?>js/validation/jquery.validate.min.js"></script>
		<script type="text/javascript" src="<?php echo BASE_URL; ?>js/constants.js"></script>
		<script type="text/javascript" src="<?php echo BASE_URL; ?>tinymce/js/tinymce/tinymce.min.js"></script>
		<script type="text/javascript">var SITE_URL = "<?= BASE_URL ?>";</script>
		<script type="text/javascript" src="<?= BASE_URL ?>js/bootstrap-select.min.js"></script>
		<script type="text/javascript" src="<?= BASE_URL ?>js/bootstrap-treeview.js"></script>
		<script type="text/javascript" src="<?= BASE_URL ?>js/md5.js"></script>
		<script type="text/javascript" src="<?= BASE_URL ?>filejs/vendor/jquery.ui.widget.js"></script>
		<script type="text/javascript" src="<?= BASE_URL ?>filejs/jquery.iframe-transport.js"></script>
		<script type="text/javascript" src="<?= BASE_URL ?>filejs/jquery.fileupload.js"></script>
		<script type="text/javascript" src="<?= BASE_URL ?>filejs/jquery.fileupload-process.js"></script>
		<script type="text/javascript" src="<?= BASE_URL ?>js/jquery-ui-min.js"></script>
		<script type="text/javascript" src="<?php echo BASE_URL; ?>js/bootstrap.min.js"></script>
		<?php if(isset($page_title) and $page_title='DASHBOARD'){ ?>
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>css/datetimepicker.min.css">
		<link rel="stylesheet" href="<?php echo BASE_URL; ?>css/bootstrap-table-min.css">
		<script type="text/javascript" src="<?php echo BASE_URL; ?>js/datetimepicker_moment.min.js"></script>
		<script type="text/javascript" src="<?php echo BASE_URL; ?>js/bootstrap_datetimepicker.min.js"></script>
		<script type="text/javascript" src="<?php echo BASE_URL; ?>js/bootstrap-table-min.js"></script>

		<script type="text/javascript" src="<?php echo BASE_URL; ?>js/jspdf.js"></script>
		<script type="text/javascript" src="<?php echo BASE_URL; ?>js/jspdf_autotable.js"></script>
		<script type="text/javascript" src="<?php echo BASE_URL; ?>js/bootstrap_table_export.js"></script>
		<script type="text/javascript" src="<?php echo BASE_URL; ?>js/jquery_table_export.js"></script>
		<?php } ?>
		<script type="text/javascript" src="<?php echo BASE_URL; ?>js/bootstrap-editable.min.js"></script>
		<script type="text/javascript" src="<?php echo BASE_URL; ?>js/jquery.tablesorter.min.js"></script>
    <script type="text/javascript" src="<?php echo BASE_URL; ?>js/spin.min.js"></script>
		<script type="text/javascript" src="<?php echo BASE_URL; ?>js/javascript.js"></script>

	</head>
	<body>
	<div id="spinner"></div>
		<div class="container-fluid">
			<div class="row">
				<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
						<div class="container-fluid">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
							<a class="navbar-brand" href="<?php echo base_url(); ?>admin/home-page"><img src="<?php echo base_url(); ?>images/small-logo.png"/></a>
						</div>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li <?php if($this->uri->segment("1")=="home-page"){ ?> class="menu-active" <?php } else{ ?> class=" " <?php } ?>><a href="<?php echo base_url(); ?>admin/home-page">Home Page</a></li>
								<li <?php if($this->uri->segment("1")=="model"  or $this->uri->segment("1")=="model_gallery"  or $this->uri->segment("1")=="new_model" or $this->uri->segment("1")=="model-order"){ ?> class="menu-active" <?php } else{ ?> class=" " <?php } ?>><a href="<?php echo base_url(); ?>admin/model">Models</a></li>
								<li <?php if($this->uri->segment("1")=="location" or $this->uri->segment("1")=="location_edit" or $this->uri->segment("1")=="new-location"){ ?> class="menu-active" <?php } else{ ?> class=" " <?php } ?>><a href="<?php echo BASE_URL ?>admin/masters">Masters</a></li>
								<li <?php if($this->uri->segment("1")=="Billing" or $this->uri->segment("1")=="new_bill"){ ?> class="menu-active" <?php } else{ ?> class=" " <?php } ?>><a href="<?php echo BASE_URL ?>admin/billing">Billing</a></li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo ucfirst($admin_user_name);?> <b class="caret"></b></a>
									<ul class="dropdown-menu">
										<li><a href="#change" data-toggle="modal" onclick="getElementById('user_add').reset();">Change Password</a></li>
										<li><a href="<?php echo BASE_URL;?>logout"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
									</ul>
								</li>
							</ul>
						</div><!-- /.navbar-collapse -->
					</div>
				</nav><!-- /.navbar-ends -->
			</div><!-- /.row-ends -->
		<div class="modal " data-backdrop="static" id="change" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-md">
					<div class="modal-content">
						<div class="modal-header modal-header-info padgt0">
							<h3 id="head">Change Password</h3>
						</div>
						<form role="form" class="form-horizontal" id="user_add" name="user_add" method="POST" >
							<div class="modal-body">
								<input type="hidden" id="hdn_id" name="hdn_id">
								<fieldset>
									<div class="perror" id="perror" style="color: #CA1515;"></div>
									<!-- Text input-->
									<div class="form-group">
										<div class="col-md-8 col-md-offset-2">
											<input id="lusername" readonly value="<?php echo $this->session->userdata('username');?>" ng-model="address" name="username" type="text" placeholder="Username" ng-unique="name" ng-pattern="/^[a-z](?:[a-z0-9_-]+)*$/i" class="form-control input-md mr1stinput" focus="true" autofocus required="">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-8 col-md-offset-2">
											<input id="lpassword" ng-model="address" name="password" type="password" placeholder="New Password" ng-unique="name" ng-pattern="/^[a-z](?:[a-z0-9_-]+)*$/i" class="form-control input-md mr1stinput" focus="true" autofocus required="">
										</div>
									</div>
									<div class="form-group">
										<div class="col-md-8 col-md-offset-2">
											<input id="ccpassword" ng-model="address" name="cpassword" type="password" placeholder="Confirm Password" ng-unique="name" ng-pattern="/^[a-z](?:[a-z0-9_-]+)*$/i" class="form-control input-md mr1stinput" focus="true" autofocus required="">
										</div>
									</div>
								</fieldset>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-primary pull-left col-md-offset-2" onclick="changepassword(<?php echo $this->session->userdata('admin_id');?>)" id="chupdate"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
								<button type="reset" class="btn btn-danger pull-left" data-dismiss="modal" ng-click="reset(user_add)"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
							</div>
						</form>

				</div>
				</div>
			</div>
