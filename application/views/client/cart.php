<div class="contact">
<div class="container">
<div class="contact form">
	<table id="cart" class="table table-hover table-condensed">
    				<thead>
						<tr>
							<th style="width:50%">Product</th>
							<th style="width:10%">Price</th>
							<th style="width:8%">Quantity</th>
							<th style="width:10%" class="">Subtotal</th>
							<th style="width:10%"></th>
						</tr>
					</thead>
					<tbody>
							<?php $total = 0;$i=0; if(isset($model)){
							foreach($model as $val){
							 $total = $total + $val['model_price'];
							 $_SESSION['prod_id'][bin2hex($val['model_id'])]['qnty'] = ((isset($_SESSION['prod_id']) && isset($_SESSION['prod_id'][bin2hex($val['model_id'])]['qnty'])) ? $_SESSION['prod_id'][bin2hex($val['model_id'])]['qnty'] : 1); 
							 ?>
						<tr id='tr-<?=bin2hex($val['model_id'])?>'>
                            <td data-th="Product">
								<div class="row">
									<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"><img src="<?=BASE_URL.'uploads/'.$val['model_image']?>" alt="..." class="img-responsive"/></div>
									<div class="col-lg-4 col-md-4 col-sm-8 col-xs-8">
										<h4 class="nomargin"><?=$val['model_name']?></h4>
										<p><?=$val['model_features']?></p>
									</div>
								</div>
							</td>
							<td data-th="Price"><?=$val['model_price']?></td>
							<td data-th="Quantity">
								<input type="number" data-sub-id="<?=$i?>"  onchange="if(this.value<this.min || this.value>this.max) this.value='1';change_total(this.value,'<?=$i?>',<?=bin2hex($val['model_id'])?>);" onKeyDown="if(this.value.length==this.max.length && event.keyCode!=8)return false;" class="form-control text-center" min="1" max="<?=$val['model_quantity']?>" name="qnty" value="<?=$_SESSION['prod_id'][bin2hex($val['model_id'])]['qnty']?>">
							</td>
							<td data-th="Subtotal" data-price="<?=$val['model_price']?>" id="<?='cart'.$i?>" class=""><?=$val['model_price']?></td>
							<td class="actions" data-th="">
								<button onclick="delete_cart(<?=bin2hex($val['model_id'])?>);" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button>								
							</td>
						</tr>
                        <?php
						 	
							$_SESSION['prod_id'][bin2hex($val['model_id'])]['sub_total'] = $val['model_price'];
							$_SESSION['amount'] = $total;
							 $i++; }}?>
					</tbody>
					<tfoot>
						
						<tr>
							<td colspan="2"></td>
							<td ><strong> Total </strong></td>
							<td colspan="2"><strong class="cart-total"><?=number_format($total, 2, '.', '');?></strong></td>

						</tr>
						<tr>
							<td><a href="<?=BASE_URL.'products/0/0'?>" class=" btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
							<td colspan="3" class=""></td>
							<td><?php if (isset($model) && !empty($model)) { ?><a href="<?=BASE_URL.'billing'?>" class="  btn btn-success">Place Order <i class="fa fa-angle-right"></i></a><?php } ?></td>
						</tr>
					</tfoot>
				</table>

</div></div></div>