<style>
.cd-gallery {
  width: 90%;
  max-width: 620px;
  margin: 1.5em auto;
}
.bx-viewport .row .description {margin: 2.5em auto;}
.bx-viewport .row {margin-left: 0px;margin-right: 0px;}
.description > h4 > ul{line-height: 30px;}
.cd-gallery::after {
  clear: both;
  content: "";
  display: table;
}
.cd-gallery > li {
  box-shadow: 8px 8px 6px rgb(18 17 17 / 11%) !important;
  border: 1px solid #a5a3a3;
  overflow: hidden;
  position: relative;
  margin-bottom: 2em;
  border-radius: .25em;
  box-shadow: 0 1px 4px rgba(0, 0, 0, 0.1);
}
.cd-gallery > li > a {
  display: block;
}
@media only screen and (min-width: 768px) {
  .cd-gallery {
    margin: 2em auto;
  }
  .cd-gallery > li {
    margin-right: 4%;
    margin-bottom: 2.5em;
  }
  .cd-gallery > li:nth-of-type(2n) {
    margin-right: 0;
  }
}
@media only screen and (min-width: 1048px) {
  .cd-gallery {
    margin: 2.5em auto;
  }
  .no-touch .cd-gallery > li:hover .cd-dots li.selected a {
    /* Slider dots - change background-color of the selected dot when hover over the its parent list item */
    background: #2f2933;
    border-color: #141313;
  }
  .no-touch .cd-gallery > li:hover .cd-dots a {
    /* Slider dots - change dot border-color when hover over the its parent list item */
    border-color: #141313;
  }
  .no-touch .cd-gallery > li:hover li.move-right, .no-touch .cd-gallery > li:hover li.move-left {
    /* show preview items when hover over the its parent list item */
    opacity: 0.3;
  }
}

.cd-item-wrapper {
  position: relative;
  overflow: hidden;
  margin: 3em 0;
}
.cd-item-wrapper li {
  position: absolute;
  top: 0;
  left: 25%;
  height: 100%;
  width: 50%;
  opacity: 0;
  /* Force Hardware Acceleration */
  -webkit-transform: translateZ(0);
  -moz-transform: translateZ(0);
  -ms-transform: translateZ(0);
  -o-transform: translateZ(0);
  transform: translateZ(0);
  -webkit-backface-visibility: hidden;
  backface-visibility: hidden;
  will-change: transform, opacity;
  -webkit-transform: translateX(200%) scale(0.7);
  -moz-transform: translateX(200%) scale(0.7);
  -ms-transform: translateX(200%) scale(0.7);
  -o-transform: translateX(200%) scale(0.7);
  transform: translateX(200%) scale(0.7);
  -webkit-transition: -webkit-transform 0.4s, opacity 0.4s;
  -moz-transition: -moz-transform 0.4s, opacity 0.4s;
  transition: transform 0.4s, opacity 0.4s;
}
.cd-item-wrapper li.selected {
  /* selected item */
  position: relative;
  opacity: 1;
  -webkit-transform: translateX(0) scale(1.3);
  -moz-transform: translateX(0) scale(1.3);
  -ms-transform: translateX(0) scale(1.3);
  -o-transform: translateX(0) scale(1.3);
  transform: translateX(0) scale(1.3);
}
.cd-item-wrapper li.move-left {
  /* item on left - preview visible */
  -webkit-transform: translateX(-100%) scale(0.7);
  -moz-transform: translateX(-100%) scale(0.7);
  -ms-transform: translateX(-100%) scale(0.7);
  -o-transform: translateX(-100%) scale(0.7);
  transform: translateX(-100%) scale(0.7);
  opacity: 0.3;
}
.cd-item-wrapper li.move-right {
  /* item on right - preview visible */
  -webkit-transform: translateX(100%) scale(0.7);
  -moz-transform: translateX(100%) scale(0.7);
  -ms-transform: translateX(100%) scale(0.7);
  -o-transform: translateX(100%) scale(0.7);
  transform: translateX(100%) scale(0.7);
  opacity: 0.3;
}
.cd-item-wrapper li.hide-left {
  /* items hidden on the left */
  -webkit-transform: translateX(-200%) scale(0.7);
  -moz-transform: translateX(-200%) scale(0.7);
  -ms-transform: translateX(-200%) scale(0.7);
  -o-transform: translateX(-200%) scale(0.7);
  transform: translateX(-200%) scale(0.7);
}
.cd-item-wrapper li img {
  display: block;
  width: 100%;
}
@media only screen and (min-width: 1048px) {
  .cd-item-wrapper li.move-left,
  .cd-item-wrapper li.move-right {
    /* hide preview items */
    opacity: 0;
  }
  .cd-item-wrapper li.focus-on-left {
    /* class added to the .selected and .move-right items when user hovers over the .move-left item (item preview on the left) */
    -webkit-transform: translateX(3%) scale(1.25);
    -moz-transform: translateX(3%) scale(1.25);
    -ms-transform: translateX(3%) scale(1.25);
    -o-transform: translateX(3%) scale(1.25);
    transform: translateX(3%) scale(1.25);
  }
  .cd-item-wrapper li.focus-on-left.move-right {
    -webkit-transform: translateX(103%) scale(0.7);
    -moz-transform: translateX(103%) scale(0.7);
    -ms-transform: translateX(103%) scale(0.7);
    -o-transform: translateX(103%) scale(0.7);
    transform: translateX(103%) scale(0.7);
  }
  .cd-item-wrapper li.focus-on-right {
    /* class added to the .selected and .move-left items when user hovers over the .move-right item (item preview on the right) */
    -webkit-transform: translateX(-3%) scale(1.25);
    -moz-transform: translateX(-3%) scale(1.25);
    -ms-transform: translateX(-3%) scale(1.25);
    -o-transform: translateX(-3%) scale(1.25);
    transform: translateX(-3%) scale(1.25);
  }
  .cd-item-wrapper li.focus-on-right.move-left {
    -webkit-transform: translateX(-103%) scale(0.7);
    -moz-transform: translateX(-103%) scale(0.7);
    -ms-transform: translateX(-103%) scale(0.7);
    -o-transform: translateX(-103%) scale(0.7);
    transform: translateX(-103%) scale(0.7);
  }
  .cd-item-wrapper li.hover {
    /* class added to the preview items (.move-left or .move-right) when user hovers over them */
    opacity: 1 !important;
  }
  .cd-item-wrapper li.hover.move-left {
    -webkit-transform: translateX(-97%) scale(0.75);
    -moz-transform: translateX(-97%) scale(0.75);
    -ms-transform: translateX(-97%) scale(0.75);
    -o-transform: translateX(-97%) scale(0.75);
    transform: translateX(-97%) scale(0.75);
  }
  .cd-item-wrapper li.hover.move-right {
    -webkit-transform: translateX(97%) scale(0.75);
    -moz-transform: translateX(97%) scale(0.75);
    -ms-transform: translateX(97%) scale(0.75);
    -o-transform: translateX(97%) scale(0.75);
    transform: translateX(97%) scale(0.75);
  }
}

.cd-dots {
  /* not visible in the html document - created using jQuery */
  position: absolute;
  bottom: 240px;
  left: 50%;
  right: auto;
  -webkit-transform: translateX(-50%);
  -moz-transform: translateX(-50%);
  -ms-transform: translateX(-50%);
  -o-transform: translateX(-50%);
  transform: translateX(-50%);
  padding: .2em;
}
.cd-dots::after {
  clear: both;
  content: "";
  display: table;
}
.cd-dots li {
  display: inline-block;
  float: left;
  margin: 0 5px;
  pointer-events: none;
}
.cd-dots li.selected a {
  background: #2f2933;
  border-color: #2f2933;
}
.cd-dots a {
  display: block;
  height: 6px;
  width: 6px;
  border-radius: 50%;
  border: 1px solid #141313;
  /* image replacement */
  overflow: hidden;
  text-indent: 100%;
  white-space: nowrap;
  -webkit-transition: border-color 0.2s, background-color 0.2s;
  -moz-transition: border-color 0.2s, background-color 0.2s;
  transition: border-color 0.2s, background-color 0.2s;
}
@media only screen and (min-width: 1048px) {
  .cd-dots li {
    pointer-events: auto;
  }
  .cd-dots li.selected a {
    background: #cccccc;
    border-color: #141313;
  }
  .cd-dots a {
    height: 8px;
    width: 8px;
    border-color: #141313;
    /* fix a bug in IE9/10 - transparent anchor not clickable */
    background-color: #141313;
  }
}

.cd-item-info {
	height: 100%;
	margin-bottom: 30px;
  text-align:center;
  padding: 0 2em;
}
.cd-item-info::after {
  clear: both;
  content: "";
  display: table;
}
.cd-item-info b, .cd-item-info .cd-price, .cd-item-info .cd-new-price {
  font-weight: bold;
  font-size: 2rem;
}
.cd-item-info b {
}
.cd-item-info b a {
  color: #2f2933;
  text-align:center;

}
 .cd-item-info .cd-new-price {
  /* .cd-new-price not visible in the html document - created using jQuery */
  float: right;
}
.cd-item-info .cd-price {
  color: #de0101;
  position: relative;
  margin-left: 10px;
  -webkit-transition: color 0.2s;
  -moz-transition: color 0.2s;
  transition: color 0.2s;
}
.cd-item-info .cd-price::after {
  /* crossing line - visible if price is on sale */
  content: '';
  position: absolute;
  top: 50%;
  bottom: auto;
  -webkit-transform: translateY(-50%);
  -moz-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  -o-transform: translateY(-50%);
  transform: translateY(-50%);
  left: 0;
  height: 2px;
  width: 0%;
  background-color: #a5d05e;
  opacity: 0;
  -webkit-transition: width 0.2s 0s, opacity 0s 0.2s;
  -moz-transition: width 0.2s 0s, opacity 0s 0.2s;
  transition: width 0.2s 0s, opacity 0s 0.2s;
}
.cd-item-info .cd-price.on-sale::after {
  opacity: 1;
  width: 100%;
  -webkit-transition: width 0.2s 0s, opacity 0s 0s;
  -moz-transition: width 0.2s 0s, opacity 0s 0s;
  transition: width 0.2s 0s, opacity 0s 0s;
}
.cd-item-info .cd-new-price {
  /* new price - visible if price is on sale */
  color: #e76363;
  opacity: 0;
  -webkit-transform: translateX(5px);
  -moz-transform: translateX(5px);
  -ms-transform: translateX(5px);
  -o-transform: translateX(5px);
  transform: translateX(5px);
  -webkit-transition: -webkit-transform 0.2s, opacity 0.2s;
  -moz-transition: -moz-transform 0.2s, opacity 0.2s;
  transition: transform 0.2s, opacity 0.2s;
}
.cd-item-info .cd-new-price.is-visible {
  -webkit-transform: translateX(0);
  -moz-transform: translateX(0);
  -ms-transform: translateX(0);
  -o-transform: translateX(0);
  transform: translateX(0);
  opacity: 1;
}
@media only screen and (min-width: 768px) {
  .cd-item-info b, .cd-item-info .cd-price, .cd-item-info .cd-new-price {
  }
}
.btn-outline-secondary:hover {
    color: #2f2933;}
.no-js .move-right,
.no-js .move-left {
  display: none;
}

/* -------------------------------- 
xcredits 
-------------------------------- */
.credits {
  width: 90%;
  margin: 2em auto;
  text-align: center;
}

.no-touch .credits a:hover {
  text-decoration: underline;
}
.btn-outline-secondary {
    color: #2f2933;
    font-size: 20px;
    font-weight: bold;
    background-color: #ffff;
    /* background-image: none; */
    border-color: #2f2933;
}
</style>
<div class="bx-viewport ">
  <div class="row">
    <div class='col-md-7'>
      <ul class="cd-gallery">
        <li>
            <a href="#0">
              <ul class="cd-item-wrapper">
                  <li class="selected">
                    <img src="<?=BASE_URL.'uploads/'.$model_list[0]['model_image']?>" alt="Preview image">
                  </li>
                  <?php foreach($model_list as $mod){ if(isset($mod['gallery_image'])  && !empty($model_list[0]['gallery_image'])){?>
                  <li class="move-right" >
                    <img src="<?=BASE_URL.'uploads/'.$mod['gallery_image']?>" alt="Preview image">
                  </li>
                  <?php }}?>
              </ul> <!-- cd-item-wrapper -->
            </a>

            <div class="cd-item-info">
              <b><a href="#0"><?=$model_list[0]['model_name']?></a></b><br><br>
              <p >
									<del>
										MRP : <?=number_format($model_list[0]['model_price'] + $model_list[0]['model_price']*50/100,2)?>
									</del><br>
									<em class="cd-price">
										Rs. <?=$model_list[0]['model_price']?>
									</em>
								</p><br>
          <?=$model_list[0]['model_caption']?><br><br>
          <button onclick="addtocart(<?=bin2hex($model_list[0]['model_id'])?>);" type="button" class="btn btn-sm btn-outline-secondary ">Add To Cart</button>
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <button id="view_cart" <?=isset($_SESSION['prod_id']) ? '' : 'readyonly disabled'?> onclick="check_cart_sess();" type="button" class="btn btn-sm btn-outline-secondary ">Go To Cart</button>
      
        </li>

        <!-- other list items here -->
      </ul> <!-- cd-gallery -->
    </div>  
    <div class="col-md-5 description">
      <h2>
        Description
      </h2>
      <br>
      <h4>
        <ul>
        <li><b>Title : </b><?=$model_list[0]['model_caption']?></li>
        <li>
          <table width='100%'>
            <tr><td><b>Company : </b><?=$model_list[0]['company']?></td><td><b>Variety : </b><?=$model_list[0]['variety']?></td></tr>
            <tr><td><b>Category : </b><?=$model_list[0]['category']?></td><td><b>Range : </b><?=$model_list[0]['crange']?></td></tr>
            <tr><td><b>Thickness : </b><?=$model_list[0]['thickness']?></td><td><b>Size : </b><?=$model_list[0]['size']?></td></tr>
          </table>
        </li>
        <li><b>Features : </b><?=$model_list[0]['model_features']?></li>
        <li><b>Overview : </b><?=$model_list[0]['model_overview']?></li>
        <li><b>Details : </b><?=$model_list[0]['model_details']?></li>

        <li><b>Available Stock : </b><?=$model_list[0]['model_quantity']?></li>
        <li><a download href="<?=!empty($model_list[0]['brochure']) ? BASE_URL.'uploads/brochures/'.$model_list[0]['brochure'] : '#'?>">Download Brochure</a></li>

      </ul>
      </h4>
    </div>
  </div>
</div>

<script src='../js/jquery.js'></script>
<script>
jQuery(document).ready(function($) {
	var galleryItems = $('.cd-gallery').children('li');

	galleryItems.each(function(){
		var container = $(this),
			// create slider dots
			sliderDots = createSliderDots(container);
		//check if item is on sale
		updatePrice(container, 0);

		// update slider when user clicks one of the dots
		sliderDots.on('click', function(){
			var selectedDot = $(this);
			if(!selectedDot.hasClass('selected')) {
				var selectedPosition = selectedDot.index(),
					activePosition = container.find('.cd-item-wrapper .selected').index();
				if( activePosition < selectedPosition) {
					nextSlide(container, sliderDots, selectedPosition);
				} else {
					prevSlide(container, sliderDots, selectedPosition);
				}

				updatePrice(container, selectedPosition);
			}
		});

		// update slider on swipeleft
		container.find('.cd-item-wrapper').on('swipeleft', function(){
			var wrapper = $(this);
			if( !wrapper.find('.selected').is(':last-child') ) {
				var selectedPosition = container.find('.cd-item-wrapper .selected').index() + 1;
				nextSlide(container, sliderDots);
				updatePrice(container, selectedPosition);
			}
		});

		// update slider on swiperight
		container.find('.cd-item-wrapper').on('swiperight', function(){
			var wrapper = $(this);
			if( !wrapper.find('.selected').is(':first-child') ) {
				var selectedPosition = container.find('.cd-item-wrapper .selected').index() - 1;
				prevSlide(container, sliderDots);
				updatePrice(container, selectedPosition);
			}
		});

		// preview image hover effect - desktop only
		container.on('mouseover', '.move-right, .move-left', function(event){
			hoverItem($(this), true);
		});
		container.on('mouseleave', '.move-right, .move-left', function(event){
			hoverItem($(this), false);
		});

		// update slider when user clicks on the preview images
		container.on('click', '.move-right, .move-left', function(event){
			event.preventDefault();
			if ( $(this).hasClass('move-right') ) {
				var selectedPosition = container.find('.cd-item-wrapper .selected').index() + 1;
				nextSlide(container, sliderDots);
			} else {
				var selectedPosition = container.find('.cd-item-wrapper .selected').index() - 1;
				prevSlide(container, sliderDots);
			}
			updatePrice(container, selectedPosition);
		});
	});

	function createSliderDots(container){
		var dotsWrapper = $('<ol class="cd-dots"></ol>').insertAfter(container.children('a'));
		container.find('.cd-item-wrapper li').each(function(index){
			var dotWrapper = (index == 0) ? $('<li class="selected"></li>') : $('<li></li>'),
				dot = $('<a href="#0"></a>').appendTo(dotWrapper);
			dotWrapper.appendTo(dotsWrapper);
			dot.text(index+1);
		});
		return dotsWrapper.children('li');
	}

	function hoverItem(item, bool) {
		( item.hasClass('move-right') )
			? item.toggleClass('hover', bool).siblings('.selected, .move-left').toggleClass('focus-on-right', bool)
			: item.toggleClass('hover', bool).siblings('.selected, .move-right').toggleClass('focus-on-left', bool);
	}

	function nextSlide(container, dots, n){
		var visibleSlide = container.find('.cd-item-wrapper .selected'),
			navigationDot = container.find('.cd-dots .selected');
		if(typeof n === 'undefined') n = visibleSlide.index() + 1;
		visibleSlide.removeClass('selected');
		container.find('.cd-item-wrapper li').eq(n).addClass('selected').removeClass('move-right hover').prevAll().removeClass('move-right move-left focus-on-right').addClass('hide-left').end().prev().removeClass('hide-left').addClass('move-left').end().next().addClass('move-right');
		navigationDot.removeClass('selected')
		dots.eq(n).addClass('selected');
	}

	function prevSlide(container, dots, n){
		var visibleSlide = container.find('.cd-item-wrapper .selected'),
			navigationDot = container.find('.cd-dots .selected');
		if(typeof n === 'undefined') n = visibleSlide.index() - 1;
		visibleSlide.removeClass('selected focus-on-left');
		container.find('.cd-item-wrapper li').eq(n).addClass('selected').removeClass('move-left hide-left hover').nextAll().removeClass('hide-left move-right move-left focus-on-left').end().next().addClass('move-right').end().prev().removeClass('hide-left').addClass('move-left');
		navigationDot.removeClass('selected');
		dots.eq(n).addClass('selected');
	}

	function updatePrice(container, n) {
		var priceTag = container.find('.cd-price'),
			selectedItem = container.find('.cd-item-wrapper li').eq(n);
		if( selectedItem.data('sale') ) { 
			// if item is on sale - cross old price and add new one
			priceTag.addClass('on-sale');
			var newPriceTag = ( priceTag.next('.cd-new-price').length > 0 ) ? priceTag.next('.cd-new-price') : $('<em class="cd-new-price"></em>').insertAfter(priceTag);
			newPriceTag.text(selectedItem.data('price'));
			setTimeout(function(){ newPriceTag.addClass('is-visible'); }, 100);
		} else {
			// if item is not on sale - remove cross on old price and sale price
			priceTag.removeClass('on-sale').next('.cd-new-price').removeClass('is-visible').on('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
				priceTag.next('.cd-new-price').remove();
			});
		}
	}
});
</script>