<!-- contact -->
<div class="contact">
	<div id="shopify-section-contact-top" class="shopify-section index-section full-image-blocks"><div data-section-id="contact-top" class="bg_section" style="background-image: url(https://ucarecdn.com/2bc3690a-d364-4d69-9290-7ac1847c3295/-/format/auto/-/preview/3000x3000/-/quality/lighter/);">
  
  <div class="contaner bannerimg">

    
     <h2>Mattress Size Guide</h2>
    
    		<p>Our mattresses are available in all standard sizes, starting from <br>the smallest single size, going up to the biggest king size with double<br> and queen sizes in between.<br> In case you have a non-standard size cot, we also customise the size<br> of the mattress to suit your requirement.</p>      
  </div>
  
</div>

<style> 
    .submitbtn {
      width: 38% !important;
      background-color: #48494b;
      color: #fff !important;
      padding: 12px 15px !important;
      font-size: 15px !important;
      text-decoration: none !important;
    }
  .formwrap label{ font-size:16px; line-height:24px; color:#000;}
  .email{ float:left; width:100%;}
  .formwrap .email input{ width:100% !important;}
  .name  .zcwf_lblLeft .zcwf_col_fld {
    width: 100% !important;
  }
  .phone  .zcwf_lblLeft .zcwf_col_fld {
    width: 100% !important;
  }

.formwrap .name{ float:left;width:48%;} 
.formwrap .phone{ float:right;width:48%;} 
  .formwrap{ width:100%;}
.formwrap input, select,textarea{
    width:100%;
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    border: 1px solid #CCC;
    box-shadow: 1px 1px 4px #EBEBEB;
    -moz-box-shadow: 1px 1px 4px #EBEBEB;
    -webkit-box-shadow: 1px 1px 4px #EBEBEB;
    border-radius: 3px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    padding: 10px;
    outline: none;
}
  
.formwrap{
   margin:25px auto !important;
   max-width:700px;
  width:100%;
    padding:40px;
    background-color: #d3d3d3 !important;
  
    padding-top: 9px !important;
    padding-left: 40px !important;
   
}
  
  
  .bg_section{ background-size:cover;}
  .breadcrumbs-container{ margin-bottom:0px !important;}
  .contaner{
    padding-top: 30px;
    max-width: 1200px;
    margin-left: auto !important;
    margin-right: auto !important;
   }
  
  .bannerimg{ padding:5% 0% 5% 0%;}
  
  .bannerimg h2{     
    font-family:"Helvetica Neue", sans-serif;
    
    color:#000 !important;
    font-weight:normal;
    line-height:36px;
    font-size:30px;  
    text-transform: uppercase;
  }
  .bannerimg p{     
      font-family:"Helvetica Neue", sans-serif;
      line-height:23px;
      font-size:14px;  
    color:#000 !important;
  }
  
  .cnt{ text-align:center;}
  
  .cnt h3{     
    font-family:"Helvetica Neue", sans-serif;
    
    color:#000 !important;
    font-weight:normal;
    line-height:36px;
    font-size:30px;  
    text-transform: uppercase;
  }
  .cnt p{     
      font-family:"Helvetica Neue", sans-serif;
      line-height:23px;
      font-size:14px;  
    color:#000 !important;
  }
  .contact h2,.bannerimg p{text-align:right !important;}
  .guiding-cols h4{
	font-size: 15px!important;
    line-height: 1.4em!important;
    letter-spacing: 0px!important;
    color: #4c5154!important;
    text-transform: none!important;
	font-weight: 700;
	padding-top: 10px;
    padding-bottom: 10px;
}
.guiding-cols{padding-top: 3em;padding-bottom: 3em;}
.guiding-cols p{font-size: 14px;}
.p-2 h3{
font-size: 25px;
    margin-bottom: 1em;
    margin-top: 3em;
    color: #48494b;
}
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}
td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}
</style>

		<div class="container">
				<div class='p-2'>
					<h3 ><b>How do I find the correct size?</b></h3>
					<p><b>STEP 1:</b> Measure the length and width of the inner edges of your bed</p>
					
					<img width='85%' src="https://ucarecdn.com/297b409d-54ac-4953-9ec8-0fc7b8288807/-/format/auto/-/preview/3000x3000/-/quality/lighter/" alt="" class="gf_image" data-gemlang="en">
					<p><b>STEP 2:</b> The measurements will help you understand what bed type you have</p>
				</div>
				<div class='guiding-cols col-md-3'>
					<img width='100%' src="https://ucarecdn.com/929097a6-c9cd-47f4-8b35-51c2796db9b8/-/format/auto/-/preview/3000x3000/-/quality/lighter/" alt="" class="gf_image" data-gemlang="en">
					<h4>For a standard single size bed</h4>
					<p>Width is always 36 inches. The length can be 72 inches, 75 inches or 78 inches.</p>
				</div>
				<div class='guiding-cols col-md-3'>
					<img width='100%' src="https://ucarecdn.com/929097a6-c9cd-47f4-8b35-51c2796db9b8/-/format/auto/-/preview/3000x3000/-/quality/lighter/" alt="" class="gf_image" data-gemlang="en">
					<h4>For a standard double size bed</h4>
					<p>Width is always 48 inches. The length can be 72 inches, 75 inches or 78 inches.</p>
				</div>
				<div class='guiding-cols col-md-3'>
					<img width='100%' src="https://ucarecdn.com/d12e6c77-f1da-45dd-9469-2ebee404325c/-/format/auto/-/preview/3000x3000/-/quality/lighter/" alt="" class="gf_image" data-gemlang="en">
					<h4>For a standard queen size bed</h4>
					<p>Width is always 60 inches. The length can be 72 inches, 75 inches or 78 inches.</p>
				</div>
				<div class='guiding-cols col-md-3'>
					<img width='100%' src="https://ucarecdn.com/b4b84a9d-740a-46d8-b5c1-f7244c155fcc/-/format/auto/-/preview/3000x3000/-/quality/lighter//" alt="" class="gf_image" data-gemlang="en">
					<h4>For a standard king size bed</h4>
					<p>Width is always 72 inches. The length can be 72 inches, 75 inches or 78 inches.</p>
				</div>
				<div class='p-2'>
					<p><b>STEP 3:</b> Use the size chart to identify the mattress size best suited for your need</p>
				</div>
				<div class='guiding-cols col-md-12'>
					<table><tbody><tr><th>Bed Type</th><th>Width (Inches)</th><th>Length (Inches)</th><th>Width (Feet)</th><th>Length (Feet)</th></tr><tr><td>Single</td><td>36</td><td>72<br>75<br>78</td><td>3</td><td>6<br>6.25<br>6.50</td></tr><tr><td>Double</td><td>48</td><td>72<br>75<br>78</td><td>4</td><td>6<br>6.25<br>6.50</td></tr><tr><td>Queen</td><td>60</td><td>72<br>75<br>78</td><td>5</td><td>6<br>6.25<br>6.50</td></tr><tr><td>King</td><td>72</td><td>72<br>75<br>78</td><td>6</td><td>6<br>6.25<br>6.50</td></tr></tbody></table>
					<br><br><b>Note: Height refers to the thickness of mattress that ranges between 5” - 10”</b>
				</div>
		</div>
