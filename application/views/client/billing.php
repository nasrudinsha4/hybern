<style>
	.ser-top h3{margin-top: 0px;}
</style>
<div class="contact  front-end">
	<div class="container">
	<div class="ser-top">
			<h3 >Billing</h3>
			<div class="ser-t">
				<b></b>
				<span><i></i></span>
				<b class="line"></b>
			</div>
			</div>
		<form  enctype="multipart/form-data" onsubmit="return validate_bill();" name="save_billing" id="save_billing" action="<?=BASE_URL?>save-billing" method="post" role="form" class="form-horizontal">
			<div class="tab-pane in active" id="billing">
				<br>
				<div class="form-group" id="bill_customer_details">
					<div class="col-md-12">
						<input style="margin-bottom:3%;display:inline !important;width:47%;" title="" class="form-control  contact_us resmdl" placeholder="Name" name="billing_name" id="billing_name" type="text">
						<input type="email" style="margin-bottom:3%;display:inline !important;width:47%;float: right;" title="" placeholder="Email" name="billing_email" id="billing_email" class="form-control contact_us resmdl">
						<input maxlength="10" style="margin-bottom:3%;display:inline !important;width:47%;"  title="" placeholder="Phone" name="billing_phone" id="billing_phone" class="form-control contact_us" type="text">
						<input maxlength="6" data-original-title="Customer pincode" style="margin-bottom:3%;display:inline !important;width:47%;float: right;" data-toggle="tooltip" title="" placeholder="Pincode" name="billing_pincode" id="billing_pincode" class="form-control contact_us" type="text">
						<textarea  style="display:inline !important;width:47%;"  title="" placeholder="Address" name="billing_address" id="billing_address" rows="5" class="form-control contus-textar"></textarea>
						<textarea  style="display:inline !important;width:47%;float: right;" title="" placeholder="Landmark, Area, Colony, Village" name="billing_notes" id="billing_notes" rows="5" class="form-control contus-textar"></textarea>
					</div>
				</div>
			</div>
			<?=($this->session->flashdata('error_msg') ? '<div class="alert-danger error alert">'.$this->session->flashdata('error_msg').'</div>' : '' )?>
			<div  style="display:none;" class="alert-danger error alert"></div>
			<button type="submit" class="billbtn btn btn-success pull-right">
				<span class="glyphicon glyphicon-floppy-disk"></span> Submit
			</button>
		</form>
	</div>
</div>