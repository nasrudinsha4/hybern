 <?php $url = explode('/',$_SERVER['REQUEST_URI']);?>
  <form id="sidebar">
          <div class="col-xs-12 col-md-3 col-sm-12 col-lg-3">
            <aside class="sidebar">
              <!-- widget categories start -->
              <div class="widget widget-categories">
                <h3 class="animated fadeInDownShort go"><a role="button" data-toggle="collapse" href="#widget-categories-collapse" aria-expanded="true" aria-controls="widget-categories-collapse" class="">Filter by category <i class="fa fa-angle-double-down" aria-hidden="true"></i></a></h3>
                <div class="collapse in" id="widget-categories-collapse" aria-expanded="true" role="tabpanel" style="">
                  <!-- widget body start -->
                  <div class="widget-body">
                    <ul class="list-unstyled" id="categories" role="tablist" aria-multiselectable="true">
                      <li class="panel"><a class="<?=($url[count($url) - 2] != 0 ? 'collapsed' : '')?>" role="button" href="<?=BASE_URL?>products/0/0">List All<span>&nbsp;<!--for count--></span></a></li>
                      <?php foreach($masters['Category'] as $mast){ 
                        $true = (($url[count($url) - 2] != 0 and isset($model_list) and !empty($model_list) and ($model_list[0]['model_category'] == $mast['city_id'])) ? '1' : '0');
                        ?>
                      <li class="panel">
                        <a class="" href="<?=BASE_URL?>products/<?=$mast['city_id']?>" ><?=$mast['city_name']?></a>
                      <?php /* <a class="<?=($true == 1 ? '' : 'collapsed')?>" if (isset($mast['make'])) { ?>
                        <ul style="<?=($true == 1 ? 'height:auto;' : '')?>" id="<?=$mast['city_name']?>" class="list-unstyled panel-collapse collapse <?=($true == 1 ? 'in' : '')?>" role="menu" aria-expanded="false" style="height: 0px;">
                          <?php  foreach($masters['Make'] as $cat){?>
                          <li><a href="<?=BASE_URL.'products/'.bin2hex($cat['city_id'])?>/0" class="animated <?=(bin2hex($cat['city_id']) == $url[count($url) - 2] ? 'orange' : '' )?> fadeInLeftShort go"><?=$cat['city_name']?></a></li>
                        <?php }?>
                        </ul>
                          <?php } */ ?>  
                      </li>
                          <?php } ?>                      
                    </ul>
                  </div>
                </div>
              </div>
              <!-- widget categories end -->
              <!-- widget color start -->
              <!--div class="widget widget-color">
                <h3 class="animated fadeInDownShort go"><a role="button" data-toggle="collapse" href="#widget-color-collapse" aria-expanded="true" aria-controls="widget-color-collapse">Filter by Color <i class="fa fa-angle-double-down" aria-hidden="true"></i></a></h3>
                <div class="collapse in" id="widget-color-collapse" aria-expanded="true" role="tabpanel">
                  <div class="widget-body">
                    <div class="checkbox blue">
                      <input type="checkbox" value="blue" id="check-blue" checked="">
                      <label data-toggle="tooltip" data-placement="top" title="Blue" for="check-blue"></label>
                    </div>
                    <div class="checkbox red">
                      <input type="checkbox" value="red" id="check-red">
                      <label data-toggle="tooltip" data-placement="top" title="Red" for="check-red"></label>
                    </div>
                    <div class="checkbox dark-gray">
                      <input type="checkbox" value="dark-gray" id="check-dark-gray">
                      <label data-toggle="tooltip" data-placement="top" title="Dark Gray" for="check-dark-gray"></label>
                    </div>
                    <div class="checkbox orange">
                      <input type="checkbox" value="orange" id="check-orange">
                      <label data-toggle="tooltip" data-placement="top" title="Orange" for="check-orange"></label>
                    </div>
                    <div class="checkbox pink">
                      <input type="checkbox" value="pink" id="check-pink">
                      <label data-toggle="tooltip" data-placement="top" title="Pink" for="check-pink"></label>
                    </div>
                    <div class="checkbox purple">
                      <input type="checkbox" value="purple" id="check-purple">
                      <label data-toggle="tooltip" data-placement="top" title="Purple" for="check-purple"></label>
                    </div>
                  </div>
                </div>
              </div-->
              <!-- widget color end -->
              <div class="widget widget-checkbox">
                <h3 class="animated fadeInDownShort go"><a role="button" data-toggle="collapse" href="#widget-price-collapse" aria-expanded="true" aria-controls="widget-size-collapse">Filter by price <i class="fa fa-angle-double-down" aria-hidden="true"></i></a></h3>
                <div class="collapse in" id="widget-price-collapse" aria-expanded="true" role="tabpanel">
                  <div class="widget-body">
                    <div class="price-range-div">
                      <label>From :</label>
                      <input type="text" id="from" class="price-range">
                    </div>
                    <div class="price-range-div">
                      <label style="width:50px;" >To :</label>
                      <input type="text" id="to" class="price-range">
                    </div>
                      <input type="hidden" id="price" name="price" readonly="" value='3000-30000' class="price-range">
                      <input type="hidden" id="id" name="id" readonly="" value='<?=($this->uri->segment(2)) ? $this->uri->segment(2) : 0;?>' class="price-range">
                    <div id="slider-range" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all"><div class="ui-slider-range ui-widget-header ui-corner-all" ></div><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 15%;"></span><span class="ui-slider-handle ui-state-default ui-corner-all" tabindex="0" style="left: 60%;"></span></div>
                  </div>
                </div>
              </div>
              <!-- widget sixe start -->
              <div class="widget widget-checkbox">
                <h3 class="animated fadeInDownShort go"><a role="button" data-toggle="collapse" href="#widget-size-collapse" aria-expanded="false" class="collapse" aria-controls="widget-size-collapse">Filter by size <i class="fa fa-angle-double-down" aria-hidden="true"></i></a></h3>
                <div class="collapse" id="widget-size-collapse" aria-expanded="false" role="tabpanel">
                  <div class="widget-body">
                    <?php foreach($masters['Size'] as $mast){?>
                    <div class="checkbox">
                      <input  name="<?=strtolower($mast['location_name']).'_'.$mast['city_name']?>" onchange="filter_results();" id="<?=$mast['city_name']?>" type="checkbox" value="<?=$mast['city_id']?>">
                      <label for="<?=$mast['city_name']?>"><?=$mast['city_name']?></label>
                      <span>&nbsp;<!--for count--></span> </div>
                     <?php }?>
                  </div>
                </div>
              </div>
              <!-- widget size end -->
              <!-- widget company start -->
              <div class="widget widget-checkbox">
                <h3 class="animated fadeInDownShort go"><a role="button" data-toggle="collapse" href="#widget-company-collapse" aria-expanded="true" aria-controls="widget-size-collapse">Filter by company <i class="fa fa-angle-double-down" aria-hidden="true"></i></a></h3>
                <div class="collapse in" id="widget-company-collapse" aria-expanded="true" role="tabpanel">
                  <div class="widget-body">
                    <?php foreach($masters['Company'] as $mast){?>
                    <div class="checkbox">
                      <input  onchange="filter_results();" name="<?=strtolower($mast['location_name']).'_'.$mast['city_name']?>" id="<?=$mast['city_name']?>" type="checkbox" value="<?=$mast['city_id']?>">
                      <label for="<?=$mast['city_name']?>"><?=$mast['city_name']?></label>
                      <span></span> </div>
                     <?php }?> 
                  </div>
                </div>
              </div>
              <!-- widget company end -->
              <!-- widger Feel start-->
              <div class="widget widget-checkbox">
                <h3 class="animated fadeInDownShort go"><a role="button" data-toggle="collapse" href="#widget-discount-collapse" aria-expanded="true" aria-controls="widget-discount-collapse">Filter by feel <i class="fa fa-angle-double-down" aria-hidden="true"></i></a></h3>
                <div class="collapse in" id="widget-discount-collapse" aria-expanded="true" role="tabpanel">
                  <div class="widget-body">
                    <?php foreach($masters['Feel'] as $mast){?>
                    <div class="checkbox">
                      <input onchange="filter_results();" id="<?=$mast['city_name']?>"  name="<?=strtolower($mast['location_name']).'_'.$mast['city_name']?>" type="checkbox" value="<?=$mast['city_id']?>">
                      <label for="<?=$mast['city_name']?>"><?=$mast['city_name']?></label>
                      <span></span> </div>
                     <?php }?>
                  </div>
                </div>
              </div>
              <!-- widger feel end -->
            </aside>
          </div>
          </form>