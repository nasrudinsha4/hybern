<!--footer-->
<!-- <div class="map">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2473478.1088362797!2d-107.97470132744915!3d52.74110675936634!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x5304f6bf47ed992b%3A0x05049e3295772690!2sMargao%2C+SK%2C+Goa!5e0!3m2!1sen!2sin!4v1460541578241"  allowfullscreen></iframe>
</div> -->

<div class="footer">
	<div class="container">
		<div class="col-md-7 footer-bottom">
			<h4>Our social media handle</h4>
			<p>Follow Us on social media to keep upto date about the latest trends and products.</p>
		</div>
			<div class="col-md-5 footer-bottom1 ">
			<h4>Follow Us</h4>
				<ul class="social">
						
						<li><a href="#"><i></i></a></li>
						<li><a href="#"><i class="icon"></i></a></li>
						<li><a href="#"><i class="icon1"></i></a></li>
						<li><a href="#"><i class="icon2"></i></a></li>
						
					</ul>
			</div>
				<div class="clearfix"> </div>
	</div>	
</div>
<div class="loader-6 loader-layer"></div>
<div class="loader-6 loader-6-bg">
	<div  id="loader-6">
		<span></span>
		<span></span>
		<span></span>
		<span></span>
	</div>
</div>
<!-- for bootstrap working -->
	<script  type="text/javascript" src="<?= BASE_URL ?>js/jquery.min.js"></script>
	<script  type="text/javascript" src="<?= BASE_URL ?>js/bootstrap.min.js"></script>

	<script type="text/javascript" src="<?= BASE_URL ?>js/client/script.js"></script>
	<script type="text/javascript" src="<?= BASE_URL ?>js/constants.js"></script>
	<script type="text/javascript" src="<?= BASE_URL ?>js/client/site_functions.js?<?= time() ?>"></script>
	<script type="text/javascript">var SITE_URL = "<?= BASE_URL ?>";</script>
	<link href='//fonts.googleapis.com/css?family=Muli:400,300' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
	<script src="<?= BASE_URL ?>js/client/easyResponsiveTabs.js" type="text/javascript"></script>
	<script src="<?= BASE_URL ?>js/client/imagezoom.js"></script>
	<script defer src="<?= BASE_URL ?>js/client/jquery.flexslider.js"></script>
	<script defer src="<?= BASE_URL ?>js/client/jquery-ui.js"></script>
		<script  type="text/javascript" src="<?= BASE_URL ?>css/client/css-animate.js"></script>
	<script  type="text/javascript" src="<?= BASE_URL ?>css/client/grid-list-view.js"></script>
	<script  type="text/javascript" src="<?= BASE_URL ?>css/client/price-range.js"></script>
<!-- //for bootstrap working -->

</body>
</html>