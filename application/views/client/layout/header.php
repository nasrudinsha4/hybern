<!DOCTYPE HTML>
<html>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?= $page_title ?></title>
		<!---  SEO keywords-->
		<meta name="keywords" content="<?php echo ((isset($service_keyword)) ? $service_keyword : null); ?>">
		<meta name="description" content="<?php echo ((isset($service_description)) ? $service_description : null); ?>">

		<!--stylesheet for design-->
		<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>css/client/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="<?php echo BASE_URL ?>css/client/styles.css">
		<!--stylesheet for Media Queries-->
		<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>css/client/galleryeffect.css">
<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>css/client/flexslider.css">
<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>css/client/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>css/client/product-3.css">
<link rel="stylesheet" type="text/css" href="<?= BASE_URL ?>css/client/jquery-ui.css">

	<link rel="icon" href="<?= BASE_URL ?>images/favicon.png" type="image/png"/>

	</head>
	<body>
    <div class="header" <?=((isset($header_height))? "style='height:800px !important;'": "style='height:92px !important;'")?> >
	<div class="head" >
		<div class="container">
			<div class="navbar-top">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						 <div class="navbar-brand logo ">
							<h1 class="animated wow pulse" data-wow-delay=".5s">
							<a href="<?=BASE_URL?>"><span><b>H</b></span>ybern <i>&nbsp;&nbsp;&nbsp;&nbsp;Comfort Solutions</i></a></h1>
						</div>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					 <ul class="nav navbar-nav link-effect-4">
						<li class="<?=($page == 'home' ? 'active':'')?>"><a href="<?=BASE_URL?>" data-hover="Home">Home</a> </li>
						<li class="<?=(isset($menu) && $menu == 'products' ? 'active':'')?>">
							<div class="dropdown">
							<a href="<?=BASE_URL?>products/0/0">Products</a>
							<div class="dropdown-content">
								<?php
								foreach($masters['Category'] as $cat){ ?>
								<a class="submenu" href="<?=BASE_URL.'products/'.$cat['city_id']?>"><?=$cat['city_name']?></a>
								<?php }?>
							</div>
							</div>
						</li>
						<li class="<?=(isset($menu) && $menu == 'about' ? 'active':'')?>">
							<div class="dropdown">
								<a href="#" >About</a>
								<div class="dropdown-content">
									<a class="submenu" href="<?=BASE_URL?>our-story">Our Story</a>
									<a class="submenu" href="<?=BASE_URL?>buying-guide">Buying Guide</a>
									<a class="submenu" href="<?=BASE_URL?>size-guide">Size Guide</a>
								</div>
							</div>
						</li>
						<li class="<?=($page == 'Gallery' ? 'active':'')?>"><a href="<?=BASE_URL?>gallery"  data-hover="Gallery">Gallery</a></li>
						<li class="<?=($page == 'contact' ? 'active':'')?>"><a href="<?=BASE_URL?>contact" data-hover="Contact">Contact</a></li>
					  </ul>
					</div><!-- /.navbar-collapse -->
				</div>
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
				<a class="wp-chat " style="width: 34px;" href="https://api.whatsapp.com/send/?phone=917020106982&amp;text=Hi!+I+agree+to+receive+notifications+via+Whatsapp.&amp;app_absent=0" target="_blank">
      <svg width="32" height="32" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 48 48" version="1.1">
        <g id="surface1">
        <path style=" fill:#FFFFFF;" d="M 4.867188 43.304688 L 7.5625 33.46875 C 5.898438 30.589844 5.027344 27.324219 5.027344 23.980469 C 5.03125 13.515625 13.546875 5 24.015625 5 C 29.09375 5.003906 33.859375 6.980469 37.445313 10.566406 C 41.027344 14.152344 43.003906 18.921875 43 23.992188 C 42.996094 34.460938 34.476563 42.972656 24.015625 42.972656 C 24.011719 42.972656 24.015625 42.972656 24.015625 42.972656 L 24.007813 42.972656 C 20.828125 42.972656 17.707031 42.175781 14.933594 40.664063 Z "></path>
        <path style=" fill:#FFFFFF;" d="M 4.867188 43.804688 C 4.734375 43.804688 4.609375 43.75 4.511719 43.65625 C 4.386719 43.527344 4.339844 43.34375 4.386719 43.171875 L 7.023438 33.535156 C 5.390625 30.628906 4.527344 27.328125 4.527344 23.980469 C 4.53125 13.238281 13.273438 4.5 24.015625 4.5 C 29.222656 4.503906 34.117188 6.53125 37.796875 10.214844 C 41.476563 13.894531 43.503906 18.789063 43.5 23.992188 C 43.496094 34.734375 34.753906 43.472656 24.015625 43.472656 C 20.824219 43.472656 17.671875 42.6875 14.871094 41.195313 L 4.996094 43.785156 C 4.953125 43.796875 4.910156 43.804688 4.867188 43.804688 Z "></path>
        <path style=" fill:#CFD8DC;" d="M 24.015625 5 C 29.09375 5.003906 33.859375 6.980469 37.445313 10.566406 C 41.027344 14.152344 43.003906 18.921875 43 23.992188 C 42.996094 34.460938 34.476563 42.972656 24.015625 42.972656 L 24.007813 42.972656 C 20.828125 42.972656 17.707031 42.175781 14.933594 40.664063 L 4.867188 43.304688 L 7.5625 33.46875 C 5.898438 30.589844 5.027344 27.324219 5.027344 23.980469 C 5.03125 13.515625 13.546875 5 24.015625 5 M 24.015625 42.972656 L 24.015625 42.972656 M 24.015625 42.972656 L 24.015625 42.972656 M 24.015625 4 C 12.996094 4 4.03125 12.960938 4.027344 23.980469 C 4.027344 27.347656 4.875 30.664063 6.488281 33.601563 L 3.902344 43.039063 C 3.808594 43.386719 3.90625 43.753906 4.15625 44.007813 C 4.347656 44.199219 4.605469 44.304688 4.867188 44.304688 C 4.953125 44.304688 5.039063 44.292969 5.121094 44.269531 L 14.808594 41.730469 C 17.636719 43.199219 20.808594 43.972656 24.007813 43.976563 C 35.03125 43.976563 43.996094 35.011719 44 23.996094 C 44.003906 18.65625 41.925781 13.636719 38.152344 9.859375 C 34.378906 6.082031 29.355469 4.003906 24.015625 4 Z "></path>
        <path style=" fill:#40C351;" d="M 35.175781 12.832031 C 32.195313 9.851563 28.234375 8.207031 24.019531 8.207031 C 15.316406 8.207031 8.234375 15.28125 8.230469 23.980469 C 8.230469 26.960938 9.066406 29.863281 10.644531 32.375 L 11.019531 32.972656 L 9.425781 38.792969 L 15.398438 37.226563 L 15.976563 37.570313 C 18.398438 39.007813 21.175781 39.769531 24.007813 39.769531 L 24.015625 39.769531 C 32.710938 39.769531 39.789063 32.691406 39.792969 23.992188 C 39.796875 19.777344 38.15625 15.8125 35.175781 12.832031 Z "></path>
        <path style=" fill-rule:evenodd;fill:#FFFFFF;" d="M 19.269531 16.046875 C 18.914063 15.253906 18.539063 15.238281 18.199219 15.226563 C 17.921875 15.214844 17.605469 15.214844 17.289063 15.214844 C 16.976563 15.214844 16.460938 15.332031 16.027344 15.808594 C 15.589844 16.28125 14.363281 17.429688 14.363281 19.765625 C 14.363281 22.097656 16.066406 24.355469 16.300781 24.671875 C 16.539063 24.984375 19.585938 29.929688 24.40625 31.832031 C 28.414063 33.410156 29.230469 33.097656 30.097656 33.019531 C 30.96875 32.9375 32.90625 31.871094 33.300781 30.761719 C 33.695313 29.65625 33.695313 28.707031 33.578125 28.507813 C 33.460938 28.308594 33.144531 28.191406 32.667969 27.953125 C 32.195313 27.714844 29.863281 26.570313 29.425781 26.410156 C 28.992188 26.253906 28.675781 26.175781 28.359375 26.648438 C 28.042969 27.121094 27.132813 28.191406 26.855469 28.507813 C 26.578125 28.824219 26.304688 28.863281 25.828125 28.628906 C 25.355469 28.390625 23.828125 27.890625 22.015625 26.273438 C 20.605469 25.015625 19.652344 23.464844 19.375 22.988281 C 19.097656 22.515625 19.34375 22.257813 19.582031 22.019531 C 19.796875 21.808594 20.058594 21.464844 20.296875 21.1875 C 20.53125 20.910156 20.609375 20.714844 20.769531 20.398438 C 20.925781 20.082031 20.847656 19.804688 20.730469 19.566406 C 20.613281 19.328125 19.691406 16.984375 19.269531 16.046875 Z "></path>
        </g>
        </svg>
    </a>
			<div class="header-left animated wow fadeInLeft animated" data-wow-delay=".5s" >
			
					<ul>
						<li class="header-phone"><i class="glyphicon glyphicon-earphone"></i>+91 70201 06982</li>
						<li><a href="javascript:void(0)" onclick="check_cart_sess()"><i class="glyphicon glyphicon-shopping-cart"></i><span id="cart-bal"><?=(isset($_SESSION['prod_id']) ? $_SESSION['qty'] : 0)?></span> Items</li></a>
					</ul>
					
				</div>
				
			  <div class="clearfix"></div>	
			</div>

	</div>
	<!---->
	<?php 
	if($page == 'home'){?>
	<div class="banner">
		<div class="container">
			<div class="banner-text"><h2 >H</h2></div>
			<span><img src="<?= BASE_URL ?>images/li-1.png" alt=""></span>
			
			<p>Energizing You, Night After Night</p>
		</div>							
	</div>
	<?php }?>
	</div>
	</div>