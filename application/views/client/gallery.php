<!-- gallery -->
<div class="gallery">
	<div class="container">
	<div class="ser-top">
			<h3>Gallery</h3>
			<div class="ser-t">
				<b></b>
				<span><i></i></span>
				<b class="line"></b>
			</div>
</div>	
		<div class="gal-btm">
		<div class="col-md-12 gal-gd-sec">
				<a href="#image-1" >
					<figure>
						<img src="images/frontend_gallery/banner1.jpg" class="img-responsive" alt="">
						<figcaption >
							<h3>Classic Interior</h3>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							</p>
							
						</figcaption>
					</figure>
				</a>
			</div>
			<div class="col-md-12 gal-gd-sec">
				<a href="#image-2" >
					<figure>
						<img src="images/frontend_gallery/exp_bnr.jpg" class="img-responsive" alt="">
						<figcaption >
							<h3>Classic Interior</h3>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							</p>
							
						</figcaption>
					</figure>
				</a>
			</div>
			<div class="col-md-12 gal-gd-sec ">
				<a href="#image-3" >
					<figure>
						<img src="images/frontend_gallery/foam-mattress.jpg" class="img-responsive" alt="">
						<figcaption >
							<h3>Classic Interior</h3>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit
							</p>
							
						</figcaption>
					</figure>
				</a>
				

			</div>
			<div class="col-md-6 gal-gd-sec ">
				<a href="#image-4" >
				<figure>
						<img src="images/frontend_gallery/img_l2.jpg" class="img-responsive" alt="">
						<figcaption class="gal-text">
							<h3>Classic Interior</h3>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit
							</p>
							
						</figcaption>
					</figure>
				</a>
			</div>
			<div class="col-md-6 gal-gd-sec ">
				<a href="#image-6" >
					<figure>
					<img src="images/frontend_gallery/img111.jpg" class="img-responsive" alt="">
						<figcaption class="gal-text">
							<h3>Classic Interior</h3>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							</p>
							
						</figcaption>
					</figure>
				</a>
			</div>
			<div class="col-md-6 gal-gd-sec ">
				<a href="#image-5" >
					<figure>
						<img src="images/frontend_gallery/Lotus.jpg" class="img-responsive" alt="">
						<figcaption class="gal-text">
							<h3>Classic Interior</h3>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit
							</p>
							
						</figcaption>
					</figure>
				</a>
			</div>
			<div class="col-md-6 gal-gd-sec ">
				<a href="#image-7" >
					<figure>
						<img src="images/frontend_gallery/Ortho-Spine.jpg" class="img-responsive" alt="">
						<figcaption>
							<h3>Classic Interior</h3>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							</p>
							
						</figcaption>
					</figure>
				</a>
			</div>
			<div class="col-md-12 gal-gd-sec ">
				<a href="#image-7" >
					<figure>
						<img src="images/frontend_gallery/Dual-Bond.jpg" class="img-responsive" alt="">
						<figcaption>
							<h3>Classic Interior</h3>
							<p>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							</p>
							
						</figcaption>
					</figure>
				</a>
			</div>
		
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- //gallery -->
	<div class="lb-overlay" id="image-1">
		<img src="images/frontend_gallery/banner1.jpg" alt="image1" />
		<div class="gal-info">							
			<h3>Classic Interior</h3>
				<p>Neque porro quisquam est, qui dolorem ipsum 
					quia dolor sit amet, consectetur, adipisci velit, 
					sed quia non numquam eius modi tempora incidunt ut
					labore et dolore magnam aliquam quaerat voluptatem.</p>
		</div>
		<a href="gallery.html" class="lb-close">Close</a>
	</div>
	<div class="lb-overlay" id="image-2">
		<img src="images/frontend_gallery/exp_bnr.jpg" alt="image1" />
		<div class="gal-info">							
			<h3>Classic Interior</h3>
				<p>Neque porro quisquam est, qui dolorem ipsum 
					quia dolor sit amet, consectetur, adipisci velit, 
					sed quia non numquam eius modi tempora incidunt ut
					labore et dolore magnam aliquam quaerat voluptatem.</p>
		</div>
		<a href="gallery.html" class="lb-close">Close</a>
	</div>
	<div class="lb-overlay" id="image-3">
		<img src="images/frontend_gallery/thumb_active-layer.jpg" alt="image1" />
		<div class="gal-info">							
			<h3>Classic Interior</h3>
				<p>Neque porro quisquam est, qui dolorem ipsum 
					quia dolor sit amet, consectetur, adipisci velit, 
					sed quia non numquam eius modi tempora incidunt ut
					labore et dolore magnam aliquam quaerat voluptatem.</p>
		</div>
		<a href="gallery.html" class="lb-close">Close</a>
	</div>
	<div class="lb-overlay" id="image-4">
		<img src="images/frontend_gallery/Endure-Wide.jpg" alt="image1" />
		<div class="gal-info">							
			<h3>Classic Interior</h3>
				<p>Neque porro quisquam est, qui dolorem ipsum 
					quia dolor sit amet, consectetur, adipisci velit, 
					sed quia non numquam eius modi tempora incidunt ut
					labore et dolore magnam aliquam quaerat voluptatem.</p>
		</div>
		<a href="gallery.html" class="lb-close">Close</a>
	</div>
	<div class="lb-overlay" id="image-5">
		<img src="images/frontend_gallery/Lotus.jpg" alt="image1" />
		<div class="gal-info">							
			<h3>Classic Interior</h3>
				<p>Neque porro quisquam est, qui dolorem ipsum 
					quia dolor sit amet, consectetur, adipisci velit, 
					sed quia non numquam eius modi tempora incidunt ut
					labore et dolore magnam aliquam quaerat voluptatem.</p>
		</div>
		<a href="gallery.html" class="lb-close">Close</a>
	</div>
	<div class="lb-overlay" id="image-6">
		<img src="images/frontend_gallery/rsz_recharge-wide.jpg" alt="image1" />
		<div class="gal-info">							
			<h3>Classic Interior</h3>
				<p>Neque porro quisquam est, qui dolorem ipsum 
					quia dolor sit amet, consectetur, adipisci velit, 
					sed quia non numquam eius modi tempora incidunt ut
					labore et dolore magnam aliquam quaerat voluptatem.</p>
		</div>
		<a href="gallery.html" class="lb-close">Close</a>
	</div>
	<div class="lb-overlay" id="image-7">
		<img src="images/frontend_gallery/Ortho-Spine.jpg" alt="image1" />
		<div class="gal-info">							
			<h3>Classic Interior</h3>
				<p>Neque porro quisquam est, qui dolorem ipsum 
					quia dolor sit amet, consectetur, adipisci velit, 
					sed quia non numquam eius modi tempora incidunt ut
					labore et dolore magnam aliquam quaerat voluptatem.</p>
		</div>
		<a href="gallery.html" class="lb-close">Close</a>
	</div>
	<div class="lb-overlay" id="image-8">
		<img src="images/ga7.jpg" alt="image1" />
		<div class="gal-info">							
			<h3>Classic Interior</h3>
				<p>Neque porro quisquam est, qui dolorem ipsum 
					quia dolor sit amet, consectetur, adipisci velit, 
					sed quia non numquam eius modi tempora incidunt ut
					labore et dolore magnam aliquam quaerat voluptatem.</p>
		</div>
		<a href="gallery.html" class="lb-close">Close</a>
	</div>
<!--//content-->
<div class=" news-letter ">
	<div class="container">
		<div class="test-top-g">
			<h3>Get notified about our products</h3>
			<img src="<?= BASE_URL ?>images/li-1.png" alt="">
		</div>
		<form action="<?=BASE_URL?>save_newsletter" method="post">
			<div class="col-md-3 contact-form1">
				<input type="hidden" value="/" name="location" >
				<input type="text" class='form-control lg' placeholder="Full Name" name="Name"  required="">
			</div>
			<div class="col-md-3 contact-form1">
				<input type="text" class='form-control' placeholder="Email Address" name="Email"  required="">
			</div>
			<div class="col-md-3 contact-form1">
				<input type="text" class='form-control' placeholder="Mobile No." name="Mobile"  required="">
			</div>
			<div class="col-md-3 contact-form1">
				<input type="submit" class='form-control btn' value="Submit">
			</div>	
			<div class="clearfix"> </div>
		</form>
	</div>
</div>