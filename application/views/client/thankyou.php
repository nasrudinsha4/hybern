<style>
	.ser-top h3{margin-top: 0px;}
	.ser-top{margin-bottom: 1em;}
</style>
<div class="contact">
	<div class="container">
	<div class="ser-top">
			<h3 >Receipt</h3>
			<div class="ser-t">
				<b></b>
				<span><i></i></span>
				<b class="line"></b>
			</div>
			</div>
	</div>
</div>
	<!-- PAGE - START -->
	<div class="content products">
		<!-- Container start -->
		<div class="container">
			<?php if(isset($_SESSION['thankyou'])) {?>
				<div class="container">
<div class="row">
	<div class="col-xs-12">
		<hr style="margin-top: 0px !important;margin-bottom: 5px !important;">
		<div class="row">
			<div class="col-xs-6">
				<address>
				<strong>Billed To:</strong>
					<div class="billAdd">
					<?=$_SESSION['thankyou']['billing_name']?><br>
					<?=$_SESSION['thankyou']['billing_phone']?><br>
					<?=$_SESSION['thankyou']['billing_email']?><br>
					Add: <?=$_SESSION['thankyou']['billing_address']?><br>
					</div>
				</address>
			</div>
			<div class="col-xs-6 text-right">
				<address>
					<strong class="OwnName">M/s S M A ENTERPRISES</strong>
					<div class="OwnAdd">Shop no 11, Jamia masjid bldg<br>Jamia masjid road Goa<br> Madgaon</div>
				</address>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6">
				<address>
					<strong>Payment Method & Delivery:</strong><br>
					Cash On Delivery,<br>
					Delivery within 6 working days.
				</address>
			</div>
			<div class="col-xs-6 text-right">
				<address>
					<strong>Bill Date:</strong><div class="BillDate"><?=date('M d, Y',strtotime($_SESSION['thankyou']['dt_created']))?></div>
				</address>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default overflow-scroll">
			<div class="panel-heading width200">
				<h3 class="panel-title"><strong>Order summary</strong></h3>
			</div>
			<div class="panel-body width200">
			<table id="invoicetable" class="table">
			<thead style="font-size:12px;font-weight:bold;">
									<tr>
									
									<td>Invoice No.</td>
                 					<td>HSN/SAC</td>
									<td>Description</td>
									<td>Location</td>
									<td>Qty</td>
									<td>Rate</td>
									<td>Discount</td>
									<td>Taxable<br>Value</td>
										<td colspan="2" class="align-cent">CGST
											<div>
												<div class="span1 t">Rate
												</div>
												<div class="t r span2">Amt
												</div>
											</div></td>
										<td colspan="2" class="align-cent">SCGST
											<div>
												<div class="span1 t">Rate
												</div>
												<div class="t r span2">Amt
												</div>
											</div></td>
											<td>Total Amount</td>
									</tr>
								</thead>
			<tbody>
			<?php $total = 0 ;foreach($_SESSION['thankyou']['product'] as $key => $prod) {
				$total +=$prod['amount'];   ?>
			
			
			<tr>
			<td onclick="check_checkbox2(1);">20181206191949</td>
			<td onclick="check_checkbox2(1);"><?=$prod['HSN_SAC']?></td>
			<td onclick="check_checkbox2(1);"><?=$prod['model']?></td>
			<td onclick="check_checkbox2(1);"><?=$prod['location']?></td>
			<td onclick="check_checkbox2(1);"><?=$prod['quantity']?></td>
			<td onclick="check_checkbox2(1);"><?=$prod['price']?></td>
			<td onclick="check_checkbox2(1);">0.00</td>
			<td onclick="check_checkbox2(1);"><?=$prod['taxable_value']?></td>
			<td onclick="check_checkbox2(1);"><?=$prod['gst_rate']?></td>
			<td class="align-rit" onclick="check_checkbox2(1);"><?=$prod['gst_amt']?></td>
			<td onclick="check_checkbox2(1);"><?=$prod['gst_rate']?></td>
			<td class="align-rit" onclick="check_checkbox2(1);"><?=$prod['gst_amt']?></td>
			<td class="align-rit" onclick="check_checkbox2(1);"><?=number_format((float)$prod['amount'], 2, '.', '')?></td>
			</tr>
		<?php }?>
			</tbody>
			<tfoot>
				<tr>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
					<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
					<td style="text-align: right !important;">Total:</td><td class="align-rit"><?=number_format((float)$total, 2, '.', '')?></td>
				</tr>
			</tfoot>
			</table>
			</div>
		</div>
	</div>
</div>
</div>
		<?php } ?>
	</div>

