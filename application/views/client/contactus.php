<!-- contact -->
	<div class="contact">
	<div id="shopify-section-contact-top" class="shopify-section index-section full-image-blocks"><div data-section-id="contact-top" class="bg_section" style="background-image: url(//cdn.shopify.com/s/files/1/0389/7812/5956/files/bg_1024x1024@2x.jpg?v=1637647525);">
  
  <div class="contaner bannerimg">

    
     <h2>We are here to help</h2>
    
 
      
    		<p>Feel free to drop a line to our expert support team.</p>
			<p>We are here to answer any questions you might have for us. Feel free to reach out with your query, suggestion, or about partnering with us. 

<br><b>Call us at +91 70201 06982 </b>from Monday to Saturday between 9:30 AM TO 6:30 PM or drop an email to contact@hybern.co.in

<br><br>
You can also get in touch by filling up this quick form and we'll get back to you at the earliest.</p>
      
    <br>
    <a href="#" class="submitbtn">Get in Touch </a>
    
  </div>
  
</div>

<style> 
    .submitbtn {
      width: 38% !important;
      background-color: #48494b;
      color: #fff !important;
      padding: 12px 15px !important;
      font-size: 15px !important;
      text-decoration: none !important;
    }
  .formwrap label{ font-size:16px; line-height:24px; color:#000;}
  .email{ float:left; width:100%;}
  .formwrap .email input{ width:100% !important;}
  .name  .zcwf_lblLeft .zcwf_col_fld {
    width: 100% !important;
  }
  .phone  .zcwf_lblLeft .zcwf_col_fld {
    width: 100% !important;
  }

.formwrap .name{ float:left;width:48%;} 
.formwrap .phone{ float:right;width:48%;} 
  .formwrap{ width:100%;}
.formwrap input, select,textarea{
    width:100%;
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    border: 1px solid #CCC;
    box-shadow: 1px 1px 4px #EBEBEB;
    -moz-box-shadow: 1px 1px 4px #EBEBEB;
    -webkit-box-shadow: 1px 1px 4px #EBEBEB;
    border-radius: 3px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    padding: 10px;
    outline: none;
}
  
.formwrap{
   margin:25px auto !important;
   max-width:700px;
  width:100%;
    padding:40px;
    background-color: #d3d3d3 !important;
  
    padding-top: 9px !important;
    padding-left: 40px !important;
   
}
  
  
  .bg_section{ background-size:cover;}
  .breadcrumbs-container{ margin-bottom:0px !important;}
  .contaner{
    padding-top: 30px;
    max-width: 1200px;
    margin-left: auto !important;
    margin-right: auto !important;
   }
  
  .bannerimg{ padding:5% 0% 5% 0%;}
  
  .bannerimg h2{     
    font-family:"Helvetica Neue", sans-serif;
    
    color:#000 !important;
    font-weight:normal;
    line-height:36px;
    font-size:30px;  
    text-transform: uppercase;
  }
  .bannerimg p{     
      font-family:"Helvetica Neue", sans-serif;
      line-height:23px;
      font-size:14px;  
    color:#000 !important;
  }
  
  .cnt{ text-align:center;}
  
  .cnt h3{     
    font-family:"Helvetica Neue", sans-serif;
    
    color:#000 !important;
    font-weight:normal;
    line-height:36px;
    font-size:30px;  
    text-transform: uppercase;
  }
  .cnt p{     
      font-family:"Helvetica Neue", sans-serif;
      line-height:23px;
      font-size:14px;  
    color:#000 !important;
  }
  
  
</style>


<div style="clear:both"></div></div>
		<div class="container">

		
			<div class="ser-top">
			<h3 >Ask us anything</h3>
			<div class="ser-t">
				<b></b>
				<span><i></i></span>
				<b class="line"></b>
			</div>
			</div>	
			<form action="<?=BASE_URL?>send_contactus_feedback" method="post">
			<div class="contact-grids1">
						<div class="col-md-3 contact-form1">
							<h4>Name</h4>
							<input type="text"  name="Name" placeholder="" required="">
						</div>
						<div class="col-md-3 contact-form1">
							<h4>Email</h4>
							<input type="email" name="Email" placeholder="" required="">
						</div>
						<div class="col-md-3 contact-form1">
							<h4>Mobile</h4>
							<input type="text" name="Mobile" placeholder="" required="">
						</div>
						<div class="col-md-3 contact-form">
							<input type="submit" class="radius" value="Submit" >
						</div>
						<div class="clearfix"> </div>
				<div class="contact-me animated wow slideInUp" data-wow-delay=".5s">
					<h4>Message</h4>
				
						<textarea name="Message"  placeholder="" required=""> </textarea>
						
				
				</div>
			</div>
			</form>
			<div class="contact-grids">
				<div class="col-md-4 contact-grid ">
					<div class="contact-grid1">
						<div class="con-ic">
							<i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>
						</div>
						
							<h4>Address</h4>
							<p>Opp Bank of Baroda, Gandhi Market Rd, near Pimpal Katta, New Market, Margao, Goa 403601</p>
						
					</div>
				</div>
				<div class="col-md-4 contact-grid ">
					<div class="contact-grid1">
						<div class="con-ic">
							<i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>
					</div>
							<h4>Call Us</h4>
							<p>+91 70201 06982</p>
						
					</div>
				</div>
				<div class="col-md-4 contact-grid ">
					<div class="contact-grid1">
						<div class="con-ic">
							<i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>
						</div>
							<h4>Email</h4>
							<p><a href="mailto:contact@hybern.co.in">contact@hybern.co.in</a></p>
						
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
	
<!-- //contact -->
<!--//content-->
<div class=" news-letter ">
	<div class="container">
		<div class="test-top-g">
			<h3>Get notified about our products</h3>
			<img src="<?= BASE_URL ?>images/li-1.png" alt="">
		</div>
		<form action="<?=BASE_URL?>save_newsletter" method="post">
			<div class="col-md-3 contact-form1">
				<input type="hidden" value="/" name="location" >
				<input type="text" class='form-control lg' placeholder="Full Name" name="Name"  required="">
			</div>
			<div class="col-md-3 contact-form1">
				<input type="text" class='form-control' placeholder="Email Address" name="Email"  required="">
			</div>
			<div class="col-md-3 contact-form1">
				<input type="text" class='form-control' placeholder="Mobile No." name="Mobile"  required="">
			</div>
			<div class="col-md-3 contact-form1">
				<input type="submit" class='form-control btn' value="Submit">
			</div>	
			<div class="clearfix"> </div>
		</form>
	</div>
</div>