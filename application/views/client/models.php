<div class="pld">
  <!-- PAGE - START -->
  <div class="content products">
    <!-- Container start -->
    <div class="container">
      <div class="animatedParent">
        <!-- Row start -->
        <div class="row">
      		<?php include_once('layout/sidebar.php'); ?>
          <div class="main-filter-data col-xs-12 col-md-9 col-sm-12 col-lg-9">
            <!-- product header start -->
            <!--div class="products-header">
              <div class="row">
                <div class="col-xs-6 col-sm-4">
                  <form class="form-inline products-per-page">
                    <div class="form-group">
                      <label>Show:</label>
                    </div>
                    <div class="form-group">
                      <select class="form-control">
                        <option>6</option>
                        <option selected="selected">20</option>
                        <option>18</option>
                        <option>24</option>
                        <option>ALL</option>
                      </select>
                    </div>
                  </form>
                </div>
                <div class="col-xs-6 col-sm-8">
                  <!-- Grid and list start -->

                  <!--form class="form-inline order-by">
                    <div class="form-group">
                      <label>Sort by:</label>
                    </div>
                    <div class="form-group">
                      <select class="form-control">
                        <option>Default</option>
                        <option selected="selected">Popularity</option>
                        <option>Price: low to high</option>
                        <option>Price: high to low</option>
                      </select>
                    </div>
                  </form>
                </div>
              </div>
            </div-->
            <!-- Product header end -->
			<div class="mid-popular">
    <?php 
    if(isset($model_list) and !empty($model_list)){
        $i = 0; 
        foreach($model_list as $val){
          $i++;?>
					<div onclick="view_products('<?=BASE_URL.'single/'.bin2hex($val['model_id'])?>')" class="<?=($i <= 3 ? 'no-top-pad' : '')?> col-sm-4 item-grid item-gr  simpleCart_shelfItem">
						<div class="grid-pro">
							<div class="women">
								<a href="javascript:void(0)">
									<img width='100%' src="<?=BASE_URL.'uploads/thumbnails/'.$val['model_image']?>" alt=""></a>
								<h6>
										<a href="javascript:void(0)"><?=$val['model_name']?></a>
								</h6>
								<p >
									<del>
										MRP : <?=number_format($val['model_price'] + $val['model_price']*50/100,2)?>
									</del><br>
									<em class="item_price">
										Price : Rs <?=$val['model_price']?>
									</em>
								</p>
							</div>
						</div>
					</div>

		<?php } }else echo "No Results Found."; ?>
				<div class="clearfix"></div>
				</div>
        <div class="pagination-wrapper">
            <?=$links?>
            </div>
</div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>
