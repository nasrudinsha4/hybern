		<section>
			<div class="content-wrapper">
				<div class="pagination3">
					<h2>Sitemap</h2>
				</div>
				<div class="information-cont">
					<ul class="sitemap">
						<li><a class="blueclass" href="<?= BASE_URL ?>">HOME</a></li>
						<?php for($i=1;$i<count($menu);$i++){
						if($i==1){?>
						<li><?php $men = explode(" ", $menu[$i]); ?>
							<a class="blueclass"><?= strtoupper($men[0])?></a>
							<ul>
								<li><a class="orangeclass" href="<?= BASE_URL ?>models">New Cars</a></li>
								<?php foreach($salesmenu as $sale){?>
								<li><a class="orangeclass" href="<?= BASE_URL.strtolower(str_replace(" ",'-',$sale['service_title'])) ?>"><?= $sale['service_title']?></a></li>
								<?php	} ?>
							</ul>
						</li>
						<?php } else if($i==2){?>  <?php $men = explode(" ", $menu[$i]); ?>
						<li><a class="blueclass"><?= strtoupper($men[0])?></a>
							<ul>
								<?php foreach($workshopmenu as $workshop){?>
								<li><a class="orangeclass" href="<?= BASE_URL.strtolower(str_replace(" ",'-',$workshop['service_title'])) ?>"><?= $workshop['service_title']?></a></li>
								<?php	} ?>
							</ul>
						</li>
						<?php } else {?>   <?php $men = explode(" ", $menu[$i]); ?>
						<li><a class="blueclass" href="<?= BASE_URL.$insurancemenu['route'] ?>"><?= strtoupper($men[0])?></a></li>
						<?php } }?>
						<li><a class="blueclass" href="<?= BASE_URL ?>contact-us">CONTACT US</a></li>
						<?php foreach($foot as $foot){?>
						<li><a class="blueclass" href="<?= BASE_URL.strtolower(str_replace(" ",'-',$foot['service_title']))?>"><?= strtoupper($foot['service_title'])?></a></li>
								<?php	} ?>
						<li><a class="blueclass" href="<?= BASE_URL ?>careers">CAREERS</a></li>
					</ul>
				</div>
			</div>
		</section>
