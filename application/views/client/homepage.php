<style>
.jumbotron {
    background-color: #f8f8f8;
	padding-top: 2%;
    padding-bottom: 5%;
}
.highlights-banners-icon{height:2.8rem;width: auto;
    max-width: 35%;
    flex: 1 0 auto;
    margin-right: 20px;
    background-position: center;
    background-size: contain;
    background-repeat: no-repeat;
    overflow: hidden;}
.highlights-banners-heading{
	font-family: Helvetica, "Helvetica Neue", Arial, "Lucida Grande", sans-serif;
	font-size: 0.9375rem;
	font-style: normal;
    font-weight: 700;
    letter-spacing: 0em;
}
.highlights-banners-text {
    width: calc(100% - 1.875rem);
    min-width: 0;
    padding-right: 20px;
    line-height: 1.4;
}
.ser-top {margin-bottom: 4em;color: #48494b;margin-top: 5em !important;}
.jumbotron>.col-md-2 {
	position: relative;
    display: flex;
	align-items: center;
	color: #4d4d4d;
}
.solutions-img{position: relative;} 
.promo-block{position: absolute; z-index: 1; top: 0; right: 0;padding: 25px;color: #48494b;line-height: 3em;}
.promo-block--header{font-weight:bold;text-align: right;font-size: 25px;color: #48494b;}
.featured .promo-block--header,.featured .promo-block{color: #fff;}

.fluid-container{padding-right: 10%;
    padding-left: 10%;}
.row{margin-bottom:15px;}
.home-col{padding-right:0px;}

</style>
<div class="jumbotron">
	<div class="col-md-2  col-md-offset-1">
        <div class="highlights-banners-icon">
			<img 	width="auto" height="100%" src="<?= BASE_URL.GALLERY.'iron.png'?>" class="highlights-banners-custom-icon">
		</div>
		<div class="highlights-banners-text">
			<span class="highlights-banners-heading">
				5 Decades of Expertise
			</span>
		</div>
    </div>
	<div class="col-md-2">
		<div class="highlights-banners-icon">
			<img src='<?= BASE_URL.GALLERY.'bulb.png'?>' class="highlights-banners-custom-icon">
		</div>
		<div class="highlights-banners-text">
			<span class="highlights-banners-heading">
			Best in Class Innovation
			</span>
		</div>
	</div>	
	<div class="col-md-2">
		<div class="highlights-banners-icon">
            <img src='<?= BASE_URL.GALLERY.'badge.png'?>' class="highlights-banners-custom-icon">
		</div>
		<div class="highlights-banners-text">
		<span class="highlights-banners-heading">
            Superior Quality
            </span>
        </div>
	</div>
	<div class="col-md-2">
		<div class="highlights-banners-icon">
            <img src='<?= BASE_URL.GALLERY.'thumb.png'?>'  class="highlights-banners-custom-icon">
		</div>
		<div class="highlights-banners-text">    
			<span class="highlights-banners-heading">
            Unrivaled Trust
            </span>
        </div>
	</div>
	<div class="col-md-2">
		<div class="highlights-banners-icon">
            <img src='<?= BASE_URL.GALLERY.'hand.png'?>'  class="highlights-banners-custom-icon">
		</div>
		<div class="highlights-banners-text">    
			<span class="highlights-banners-heading">Safety &amp; Hygiene Standards</span>
         </div>
	</div>
</div>

<div class='fluid-container'>
	<div class="ser-top">
		<h3>Explore Our Sleep Solutions</h3>
		<div class="ser-t">
			<b></b>
			<span><i></i></span>
			<b class="line"></b>
		</div>
	</div>
	<div class='row category'>
		<div class="home-col col-md-7">
			<div class="solutions-img">
				<img  src="<?= BASE_URL.GALLERY.'mattress.png'?>" class="img-responsive" alt="">
				<div class="promo-block">
					<h2 class="promo-block--header">
						Mattresses
					</h2>
					<p class="promo-block--text">
						Research-backed &amp; crafted for every need
					</p>
					<button class=" btn btn-primary pull-right">
						Shop Now
					</button>
				</div>
			</div>
		</div>	
		<div class="home-col col-md-5">
			<div class="solutions-img">
				<img  src="<?= BASE_URL.GALLERY.'beds.png'?>" style='height:460px;'  class="img-responsive" alt="">
				<div class="promo-block">
					<h2 class="promo-block--header">
						Beds
					</h2>
					<p class="promo-block--text">
						The next step to good sleep
					</p>
					<button class=" btn btn-primary pull-right">
						Shop Now
					</button>
				</div>	
			</div>
		</div>	
	</div>	
	<div class='row category'>
		<div class="home-col col-md-5">
			<div class="solutions-img">
				<img  src="<?= BASE_URL.GALLERY.'pillow.png'?>" style='height:429px;' class="img-responsive" alt="">
				<div class="promo-block">
					<h2 class="promo-block--header">
						Pillows
					</h2>
					<p class="promo-block--text">
						Pick from cozy, contoured & cooling
					</p>
					<button class=" btn btn-primary pull-right">
						Shop Now
					</button>
				</div>	
			</div>
		</div>	
		<div class="home-col col-md-7">
			<div class="solutions-img">
				<img  src="<?= BASE_URL.GALLERY.'bedsheet.png'?>" style='height:429px;' class="img-responsive" alt="">
				<div class="promo-block">
					<h2 class="promo-block--header">
						Bedsheets
					</h2>
					<p class="promo-block--text">
						Bring home comfort
					</p>
					<button class=" btn btn-primary pull-right">
						Shop Now
					</button>
				</div>
			</div>
		</div>	
	</div>
	<div class="ser-top">
		<h3>Featured Ranges</h3>
		<div class="ser-t">
			<b></b>
			<span><i></i></span>
			<b class="line"></b>
		</div>
	</div>
	<div class='row featured'>
		<div class="home-col col-md-6">
			<div class="solutions-img">
				<img  src="<?= BASE_URL.GALLERY.'duropedic.png'?>" class="img-responsive" alt="">
				<div class="promo-block">
					<h2 class="promo-block--header">
						Duropedic
					</h2>
					<p class="promo-block--text">
						India's No. 1 recommended orthopedic mattresses
					</p>
					<button class=" btn btn-primary pull-right">
						Shop Now
					</button>
				</div>
			</div>
		</div>	
		<div class="home-col col-md-6">
			<div class="solutions-img">
				<img  src="<?= BASE_URL.GALLERY.'energise.png'?>"  class="img-responsive" alt="">
				<div class="promo-block">
					<h2 class="promo-block--header">
						Energise
					</h2>
					<p class="promo-block--text">
						Anti stress technology mattresses
					</p>
					<button class=" btn btn-primary pull-right">
						Shop Now
					</button>
				</div>	
			</div>
		</div>	
	</div>	
	<div class='row featured'>
		<div class="home-col col-md-6">
			<div class="solutions-img">
				<img  src="<?= BASE_URL.GALLERY.'essential.png'?>" class="img-responsive" alt="">
				<div class="promo-block">
					<h2 class="promo-block--header">
						Nature Living
					</h2>
					<p class="promo-block--text">
						Eco-friendly latex mattresses
					</p>
					<button class=" btn btn-primary pull-right">
						Shop Now
					</button>
				</div>	
			</div>
		</div>	
		<div class="home-col col-md-6">
			<div class="solutions-img">
				<img  src="<?= BASE_URL.GALLERY.'living.png'?>" class="img-responsive" alt="">
				<div class="promo-block">
					<h2 class="promo-block--header">
						Essential
					</h2>
					<p class="promo-block--text">
						High quality affordable mattresses
					</p>
					<button class=" btn btn-primary pull-right">
						Shop Now
					</button>
				</div>
			</div>
		</div>	
	</div>
</div>	
<!--content-->
	<!---->
	<div class="test">
			<div class="container">
				<div class="test-top-g">
					<h3>Customer Testimonials</h3>
					<img src="<?= BASE_URL ?>images/li-1.png" alt="">
				 </div>
				<div class="test-top">
					<div class="col-md-4 test-top1 test-to wow fadeInUp animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">
						<div class=" test-top2">
							<img src="<?= BASE_URL ?>images/qu.png" alt="">
							<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings </p>
							
							<div class="clearfix"> </div>
						</div>
						<div class="test-top3">
						<img src="<?= BASE_URL ?>images/th.jpg" class="img-responsive" alt="">
							<h6> The standard chunk  </h6>
							<div class="clearfix"> </div>
						</div>
					</div>
					<div class="col-md-4 test-top1 wow fadeInDown animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">
						<div class=" test-top2">
							<img src="<?= BASE_URL ?>images/qu.png" alt="">
							<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings </p>
							
							<div class="clearfix"> </div>
						</div>
						<div class=" test-top3">
							<img src="<?= BASE_URL ?>images/th1.jpg" class="img-responsive" alt="">
							<h6> The great explorer </h6>
							<div class="clearfix"> </div>
						</div>
					</div>
					<div class="col-md-4 test-top1 wow fadeInDown animated animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInDown;">
						<div class=" test-top2">
							<img src="<?= BASE_URL ?>images/qu.png" alt="">
							<p>But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings </p>
							
							<div class="clearfix"> </div>
						</div>
						<div class=" test-top3">
						<img src="<?= BASE_URL ?>images/th2.jpg" class="img-responsive" alt="">
							<h6> The great explorer</h6>
							<div class="clearfix"> </div>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
		<!---->
<!--//content-->
<div class=" news-letter ">
	<div class="container">
		<div class="test-top-g">
			<h3>Get notified about our products</h3>
			<img src="<?= BASE_URL ?>images/li-1.png" alt="">
		</div>
		<form action="<?=BASE_URL?>save_newsletter" method="post">
			<div class="col-md-3 contact-form1">
				<input type="hidden" value="/" name="location" >
				<input type="text" class='form-control lg' placeholder="Full Name" name="Name"  required="">
			</div>
			<div class="col-md-3 contact-form1">
				<input type="text" class='form-control' placeholder="Email Address" name="Email"  required="">
			</div>
			<div class="col-md-3 contact-form1">
				<input type="text" class='form-control' placeholder="Mobile No." name="Mobile"  required="">
			</div>
			<div class="col-md-3 contact-form1">
				<input type="submit" class='form-control btn' value="Submit">
			</div>	
			<div class="clearfix"> </div>
		</form>
	</div>
</div>