<!-- contact -->
	<div class="contact">
	<div id="shopify-section-contact-top" class="shopify-section index-section full-image-blocks"><div data-section-id="contact-top" class="bg_section" style="background-image: url(https://ucarecdn.com/0f1d0ca7-e384-4223-acce-8f982328ea32/-/format/auto/-/preview/3000x3000/-/quality/lighter/);">
  
  <div class="contaner bannerimg">

    
     <h2>Mattress Buying Guide</h2>
    
    		<p>From our rich experience of 5 decades we know how crucial choosing<br>the right mattress is to get deep and healthy sleep.<br> The science of choosing the right mattress goes beyond just the material of the mattress. <br>Your lifestyle needs and sleep habits are what truly determines which mattress<br>is made to lend you the perfect sleep you deserve.</p>
      
  </div>
  
</div>

<style> 
    .submitbtn {
      width: 38% !important;
      background-color: #48494b;
      color: #fff !important;
      padding: 12px 15px !important;
      font-size: 15px !important;
      text-decoration: none !important;
    }
  .formwrap label{ font-size:16px; line-height:24px; color:#000;}
  .email{ float:left; width:100%;}
  .formwrap .email input{ width:100% !important;}
  .name  .zcwf_lblLeft .zcwf_col_fld {
    width: 100% !important;
  }
  .phone  .zcwf_lblLeft .zcwf_col_fld {
    width: 100% !important;
  }

.formwrap .name{ float:left;width:48%;} 
.formwrap .phone{ float:right;width:48%;} 
  .formwrap{ width:100%;}
.formwrap input, select,textarea{
    width:100%;
    box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    border: 1px solid #CCC;
    box-shadow: 1px 1px 4px #EBEBEB;
    -moz-box-shadow: 1px 1px 4px #EBEBEB;
    -webkit-box-shadow: 1px 1px 4px #EBEBEB;
    border-radius: 3px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    padding: 10px;
    outline: none;
}
  
.formwrap{
   margin:25px auto !important;
   max-width:700px;
  width:100%;
    padding:40px;
    background-color: #d3d3d3 !important;
  
    padding-top: 9px !important;
    padding-left: 40px !important;
   
}
  
  
  .bg_section{ background-size:cover;}
  .breadcrumbs-container{ margin-bottom:0px !important;}
  .contaner{
    padding-top: 30px;
    max-width: 1200px;
    margin-left: auto !important;
    margin-right: auto !important;
   }
  
  .bannerimg{ padding:5% 0% 5% 0%;}
  
  .bannerimg h2{     
    font-family:"Helvetica Neue", sans-serif;
    
    color:#000 !important;
    font-weight:normal;
    line-height:36px;
    font-size:30px;  
    text-transform: uppercase;
  }
  .bannerimg p{     
      font-family:"Helvetica Neue", sans-serif;
      line-height:23px;
      font-size:14px;  
    color:#000 !important;
  }
  
  .cnt{ text-align:center;}
  
  .cnt h3{     
    font-family:"Helvetica Neue", sans-serif;
    
    color:#000 !important;
    font-weight:normal;
    line-height:36px;
    font-size:30px;  
    text-transform: uppercase;
  }
  .cnt p{     
      font-family:"Helvetica Neue", sans-serif;
      line-height:23px;
      font-size:14px;  
    color:#000 !important;
  }
  .contact h2,.bannerimg p{text-align:right !important;}
  .guiding-cols h4{
	font-size: 15px!important;
    line-height: 1.4em!important;
    letter-spacing: 0px!important;
    color: #4c5154!important;
    text-transform: none!important;
	font-weight: 700;
	padding-top: 10px;
    padding-bottom: 10px;
}
.guiding-cols{padding-top: 3em;padding-bottom: 3em;}
.guiding-cols p{font-size: 14px;}
</style>

		<div class="container">
				<div class="ser-top">
					<h3 >Identify Lifestyle Need</h3>
					<div class="ser-t">
						<b></b>
						<span><i></i></span>
						<b class="line"></b>
					</div>
				</div>	
				<div class='p-2'>
							As a first step, it is important to ask yourself what is your primary need with respect to a mattress. Our wide range of mattresses are carefully curated based on our thorough research to cater to you and your family’s varying sleep needs
				</div>
				<div class='guiding-cols col-md-3'>
					<img width='100%' src="https://ucarecdn.com/d0ebdeca-c959-4b57-b294-06cfc5ab53a0/-/format/auto/-/preview/3000x3000/-/quality/lighter/" alt="" class="gf_image" data-gemlang="en">
					<h4>Does your day demand a lot from you?</h4>
					<p>You need a mattress that helps you wake up feeling energised every morning</p>
				</div>
				<div class='guiding-cols col-md-3'>
					<img width='100%' src="https://ucarecdn.com/95d6bcd5-ceba-4991-a24f-ae7e49989201/-/format/auto/-/preview/3000x3000/-/quality/lighter/" alt="" class="gf_image" data-gemlang="en">
					<h4>Are you struggling with poor back health?</h4>
					<p>You need a mattress that lends you certified orthopaedic support</p>
				</div>
				<div class='guiding-cols col-md-3'>
					<img width='100%' src="https://ucarecdn.com/7452d6c9-ec2c-4ba5-9e86-f6be56c99308/-/format/auto/-/preview/3000x3000/-/quality/lighter/" alt="" class="gf_image" data-gemlang="en">
					<h4>Is value-for-money and wholesome comfort your priority?</h4>
					<p>You need mattress upgrade that promises quality and comfort at a delightful price</p>
				</div>
				<div class='guiding-cols col-md-3'>
					<img width='100%' src="https://ucarecdn.com/0ff244ae-e924-425b-9590-34880e934976/-/format/auto/-/preview/3000x3000/-/quality/lighter/" alt="" class="gf_image" data-gemlang="en">
					<h4>Is sustainability and long-lasting comfort your concern?</h4>
					<p>You need a mattress that is eco-friendly and helps you feel closest  to nature</p>
				</div>
		</div>
		<div class="container">
			<div class="ser-top">
				<h3 >Understand the Material</h3>
				<div class="ser-t">
					<b></b>
					<span><i></i></span>
					<b class="line"></b>
				</div>
			</div>	
			<div class='p-2'>
				Once you have identified your primary lifestyle need, it is recommended to know what kind of material will work for you based on your current sleeping habits. All our mattress ranges are designed to offer a suitable mattress based on material preference.
			</div>
			<div class="row">
				<div class='guiding-cols col-md-4'>
					<img width='100%' style='padding: 0px 100px 0px 100px;' src="https://ucarecdn.com/4e1b34d9-190a-4c62-84b9-57e6920cd2e9/-/format/auto/-/preview/3000x3000/-/quality/lighter/" alt="" class="gf_image" data-gemlang="en">
					<h4>Pocket Spring</h4>
					<p>Individually cased coil springs to give differentiated support along with optimum bounce
						<br>● Zero Partner Disturbance

						<br>● Long Lasting

						<br>● Supportive</p>
				</div>
				<div class='guiding-cols col-md-4'>
					<img width='100%' style='padding: 0px 100px 0px 100px;' src="https://ucarecdn.com/fe428bc8-428f-4e48-b6d9-bced306618ab/-/format/auto/-/preview/3000x3000/-/quality/lighter/" alt="" class="gf_image" data-gemlang="en">
					<h4>Bonnell Spring</h4>
					<p>Hourglass shaped interconnected springs for a firm durable support and bounce
					<br>● Long Lasting

					<br>● Supportive</p>
				</div>
				<div class='guiding-cols col-md-4'>
					<img width='100%' style='padding: 0px 100px 0px 100px;' src="https://ucarecdn.com/db916108-cf9b-45fe-b3ff-c055c4c3a918/-/format/auto/-/preview/3000x3000/-/quality/lighter/" alt="" class="gf_image" data-gemlang="en">
					<h4>Coir</h4>
					<p>Naturally supportive and cooling with a firm feel
					<br>● Durable

					<br>● Fresh & Hygienic

					<br>● Highly Supportive</p>
				</div>
			</div>
			<div class="row">
				<div class='guiding-cols col-md-4'>
					<img width='100%' style='padding: 0px 100px 0px 100px;' src="https://ucarecdn.com/cdb5811a-7085-4fca-ba0c-a2edff718653/-/format/auto/-/preview/3000x3000/-/quality/lighter/" alt="" class="gf_image" data-gemlang="en">
					<h4>Foam</h4>
					<p>Available in different densities to suit different needs

						<br>● Versatile

						<br>● Supportive</p>
				</div>
				<div class='guiding-cols col-md-4'>
					<img width='100%' style='padding: 0px 100px 0px 100px;' src="https://ucarecdn.com/99a9f5b3-a5ea-42de-ba89-ad41ccfcd1c6/-/format/auto/-/preview/3000x3000/-/quality/lighter/" alt="" class="gf_image" data-gemlang="en">
					<h4>Memory Foam</h4>
					<p>Highly responsive foam with a luxurious comfort.

						<br>● Long Lasting

						<br>● Supportive

						<br>● Rich Comfort

						<br>● Pressure Relief</p>
				</div>
				<div class='guiding-cols col-md-4'>
					<img width='100%' style='padding: 0px 100px 0px 100px;' src="https://ucarecdn.com/eea935b1-b3fa-4d89-9ea7-aa3826537336/-/format/auto/-/preview/3000x3000/-/quality/lighter/" alt="" class="gf_image" data-gemlang="en">
					<h4>Latex Foam</h4>
					<p>All natural foam with superior comfort and natural ventilation

					<br>● 100% Natural

					<br>● Biodegradable

					<br>● Long Lasting

					<br>● Supportive

						</p>
				</div>
			</div>
		</div>
		<div class="container">
				<div class="ser-top">
					<h3 >Know Your Sleeping Position</h3>
					<div class="ser-t">
						<b></b>
						<span><i></i></span>
						<b class="line"></b>
					</div>
				</div>	
				<div class='p-2'>
					All of us have a primary or favourite sleeping position. Knowing what kind of sleeper you are can further help you choose a mattress that enhances your sleep experience. Every mattress range of ours has options with varied firmness levels for you to choose from.
				</div>
				<div class='guiding-cols col-md-3'>
					<img width='100%' src="https://ucarecdn.com/e72feb26-e7fa-4472-b1ff-124e097f4075/-/format/auto/-/preview/3000x3000/-/quality/lighter/" alt="" class="gf_image" data-gemlang="en">
					<h4>Back Sleeper</h4>
					<p>Choose a mattress that is medium firm to firm, it will provide you the ample spine alignment demanded by your back.</p>
				</div>
				<div class='guiding-cols col-md-3'>
					<img width='100%' src="https://ucarecdn.com/316569b2-bd19-4bb9-b458-858a06478230/-/format/auto/-/preview/3000x3000/-/quality/lighter/" alt="" class="gf_image" data-gemlang="en">
					<h4>Side Sleeper</h4>
					<p>Choose a mattress that takes the shape of your body contours, providing support to stress points around shoulders and hips, most commonly felt in this sleeping style.</p>
				</div>
				<div class='guiding-cols col-md-3'>
					<img width='100%' src="https://ucarecdn.com/a2ff032d-f401-490f-b3af-c44b9ddba7a4/-/format/auto/-/preview/3000x3000/-/quality/lighter/" alt="" class="gf_image" data-gemlang="en">
					<h4>Stomach Sleeper</h4>
					<p>Choose a mattress that supports and cushions the midsection while keeping the spine aligned. It shouldn’t be too soft or too firm.</p>
				</div>
				<div class='guiding-cols col-md-3'>
					<img width='100%' src="https://ucarecdn.com/123536a1-2ebc-4264-bb22-f87c205d081d/-/format/auto/-/preview/3000x3000/-/quality/lighter/" alt="" class="gf_image" data-gemlang="en">
					<h4>Toss & Turn Sleeper</h4>
					<p>Avoid a very firm mattress for a smooth roll-over. A pocket spring mattress can contain motion and keep it from travelling across the mattress, for undisturbed sleep.</p>
				</div>
		</div>
		<div class="container">
				<div class="ser-top">
					<h3 >Getting Started with your mattress</h3>
					<div class="ser-t">
						<b></b>
						<span><i></i></span>
						<b class="line"></b>
					</div>
				</div>	
				<div class='p-2'>
					We would like to be there for you with you throughout your journey to a rich sleep experience. These quick pointers will help you adapt effortlessly to your new mattress.
				</div>
				<div class='guiding-cols col-md-12'>
					<p>
					● Once unwrapped, leave the mattress uncovered for a few hours to let it breathe and feel fresh.

					<br>● If you have ordered our roll pack mattress, please ensure that it is unpacked as per the instructions in the manual to regain its expected shape.

					<br>● Change is never easy even when it is for good. Your new mattress may feel slightly different and new to your body at first,<br>&nbsp;&nbsp;&nbsp;kindly allow time for your body to adjust and adapt to get the best experience.</p>
				</div>
		</div>
