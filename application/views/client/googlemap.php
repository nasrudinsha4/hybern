<!DOCTYPE HTML>
<html>
	<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Google Map</title>
	<!--stylesheet for design-->
	<link rel="stylesheet" type="text/css" href="css/styles.css">
	<!--stylesheet for Media Queries-->
	<link rel="stylesheet" type="text/css" href="css/mediaqueries.css">
	<!--[if gte IE 9]>
	<style type="text/css">
	.gradient {
		filter: none;
	}
	</style>
<![endif]-->
<style>
body {
	padding: 0;
	margin: 0;
}
</style>
<!-- scripts-->
	<script  type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
	<script type="text/javascript" src="js/constants.js"></script>
	<script src="http://maps.googleapis.com/maps/api/js"></script>
	<script>
		var obj = '<?php if(!empty($loc)) echo json_encode($loc); ?>';
		var zoom = 7;
		var centerlocation = new google.maps.LatLng(16.954928, 74.3326812);
		var locations = (obj != '') ? jQuery.parseJSON(obj) : null;
		var iconBase = '<?= BASE_URL ?>';
	function initialize() {
		var map = new google.maps.Map(document.getElementById('google_map'), {
		zoom: zoom,
		center: centerlocation,
		mapTypeId: google.maps.MapTypeId.HYBRID
	});

		var infowindow = new google.maps.InfoWindow();

		var marker, i, tmp;

		for (i = 0; i < locations.length; i++) {
			if(locations[i]['google_marker'] == "")
				tmp = IMAGEDIR + 'redmarker.png';
			else
				tmp = IMAGEDIR + locations[i]['google_marker'];
			marker = new google.maps.Marker({
				position: new google.maps.LatLng(locations[i]['google_latitude'], locations[i]['google_longitude']),
				map: map,
				icon: iconBase + tmp,
			});
			google.maps.event.addListener(marker, 'mouseover', (function(marker, i) {
	return function() {
	infowindow.setContent('<b>' +locations[i]['location_name'] + '</b><br/>' + locations[i]['contact_us_name']);
	infowindow.open(map, marker);
	}
	})(marker, i));
		}
	}
	google.maps.event.addDomListener(window, 'load', initialize);
	</script>
	<script>
$(document).ready(function(){

	var width=$(window).width();
	var height=$(window).height();
	$('body').css('width',width);
	$('body').css('height',height);
	$('body').attr('id', 'google_map');
	initialize()

});
	</script>
</head>
<body>
</body>
</html>
