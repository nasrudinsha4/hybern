<div class="modal-content">
	<div class="modal-header modal-header-info">
		<h3 class="locationpage"></h3>
	</div>
	<div class="modal-body">
		<form id="location_form" onsubmit="return validate_location();" action="<?php echo BASE_URL; ?>admin/save-location" method="post" role="form" class="form-horizontal">

			<div id="myTabContent" >
				<div class="tab-pane active in" id="pageadd">
					<br/>
					<div>
					<div class="form-group delnext1">
						<div class="col-md-12">
							<input data-toggle="tooltip" title="<?php echo 'Enter master'; ?>" class="form-control circle_txt" placeholder="<?php echo 'Master'; ?>" type="text" id='location' name='location' >
							<input type="hidden" id='hdnlocation' name='hdnlocation' >
							<select data-live-search="true" onchange="master_attr()" data-toggle="tooltip" title="<?php echo 'Select master';?>" class="selectpicker form-control circle_sel" name=location_id id="location_id">
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-12">
							<input data-toggle="tooltip" title="<?php echo 'Enter master data'; ?>" class="form-control" placeholder="<?php echo 'Master data'; ?>" type="text" name='city0' id='city0' >
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6">
							<input data-toggle="tooltip" title="Enter attribute" class="form-control" placeholder="Master attribute" type="text"  name='email0' id='email0' >
						</div>
						<div class="col-md-6">
							<Select data-toggle="tooltip" title="Select company" data-live-search="true" class="selectpicker form-control"  name='company' id='company' >
								<option value=0>Select company</option>
								<?php if(isset($company) and !empty($company)){
									foreach($company as $comp){?>
								<option value="<?php echo $comp['city_id'];?>" ><?php echo $comp['city_name'];?></option>
								<?php }}?>
							</select>	
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6">
							<Select data-toggle="tooltip" title="Select group" data-live-search="true" class="selectpicker form-control"  name='group' id='group' >
								<option value=0>Select group</option>
								<?php if(isset($group) and !empty($group)){
								foreach($group as $val) {?>
								<option value="<?php echo $val['city_id'];?>" ><?php echo $val['city_name'];?></option>
								<?php }}?>
							</select>
							<Select data-toggle="tooltip" title="Select make" data-live-search="true" class="selectpicker form-control"  name='make' id='make' >
								<?php if(isset($make) and !empty($make)){
									foreach($make as $make){?>
								<option value="<?php echo $make['city_id'];?>" ><?php echo $make['city_name'];?></option>
								<?php }}?>
							</select>
						</div>
						<div class="col-md-6">
							<Select data-toggle="tooltip" multiple title="Select thickness" data-live-search="true" class="selectpicker form-control"  name='thickness[]' id='thickness' >
								<?php if(isset($thickness) and !empty($thickness)){
									foreach($thickness as $thick){?>
								<option value="<?php echo $thick['city_id'];?>" ><?php echo $thick['city_name'];?></option>
								<?php }}?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6">
							<Select data-toggle="tooltip" title="Select range" data-live-search="true" class="selectpicker form-control"  name='range' id='range' >
								<option value=0>Select range</option>
								<?php if(isset($range) and !empty($range)){
									foreach($range as $val){?>
								<option value="<?php echo $val['city_id'];?>" ><?php echo $val['city_name'];?></option>
								<?php }}?>
							</select>
						</div>
						<div class="col-md-6">
							<Select data-toggle="tooltip" title="Select feel" data-live-search="true" class="selectpicker form-control"  name='feel' id='feel' >
								<option value=0>Select feel</option>
								<?php if(isset($feel) and !empty($feel)){
									foreach($feel as $val){?>
								<option value="<?php echo $val['city_id'];?>" ><?php echo $val['city_name'];?></option>
								<?php }}?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6">
							<Select data-toggle="tooltip" title="Select HSN_SAC" data-live-search="true" class="selectpicker form-control"  name='hsn_sac' id='hsn_sac' >
								<option value=0>Select HSN_SAC</option>
								<?php if(isset($hsn) and !empty($hsn)){
									foreach($hsn as $val){?>
								<option value="<?php echo $val['city_id'];?>" ><?php echo $val['city_name'];?></option>
								<?php }}?>
							</select>
						</div>
						<div class="col-md-6">
							<input data-toggle="tooltip" title="Enter warranty" class="form-control" placeholder="Master warranty" type="text"  name='warranty' id='warranty' >
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6">
							<textarea data-toggle="tooltip" title="Enter feature" class="form-control" placeholder="Master feature" type="text"  name='features' id='features' ></textarea>
						</div>
						<div class="col-md-6">
							<textarea data-toggle="tooltip" row="5" title="Enter description" class="form-control" placeholder="Master description" type="text"  name='description' id='description' ></textarea>
						</div>
					</div>
					<div class="form-group delnext2">
						<div class="col-md-5 ">
							<input type=hidden name="hidden" id=hidden value=1 >
							<input type=hidden name="status" id=status >
							<input type=text class="hide_text" name="hidden_loc" id=hidden_loc >
							<input type=text class="hide_text" name="cit_id" id="cit_id" >
							<input type=text class="hide_text" name="loc_id" id="loc_id" >
						</div>
						<!--
						<div class="col-md-1">
							<img onclick="add_row();" src="<?php //echo BASE_URL; ?>images/plus.png" id="add0" />
						</div> -->
					</div>
				</div>
					<hr>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-10">
							<div class="col-md-3">
								<button type="submit" class="btn btn-success pull-left resmdlbtn2"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
							</div>
							<div class="col-md-3">
								<button type="reset" class="btn btn-danger pull-left" data-dismiss="modal" ng-click="reset(user_add)"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
