<br/><br/>
<div class="form-group fixed-head">
	<div class='col-md-3'>
		<h2><?= $page_title; ?></h2>
	</div>
	<div class='col-md-5 fixed-head-content' align='center'><br>
		<?php if($this->session->flashdata('success_msg')){?>
			<div class="green_message"><?php echo $this->session->flashdata('success_msg');?></div>
			<?php } else if($this->session->flashdata('error_msg')){?>
			<div class="red_message"><?php echo $this->session->flashdata('error_msg');?></div>
		<?php }?>
	</div>
	<div class='col-md-4 fixed-head-content'>
		<button disabled type="button" onclick="reset_form('loc_form');" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Add Master</button> &nbsp;&nbsp;
		<button type="button" onclick="reset_form('location_form');" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Master Data</button> &nbsp;&nbsp;
		<button type="button"  onclick="getlocation('sort');" class="btn btn-primary" data-toggle="modal" data-target="#userdanger">Sort Items</button>
	</div>
</div>
<div class="fixed-body col-md-12 table-responsive">
	<table class="table table-bordered table-hover table-condensed">
		<thead>
			<tr class="info">
				<th>Masters</th>
				<th>Master attribute</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php if(isset($loc) and !empty($loc)){
				foreach($loc as $loc => $val) { ?>
			<tr class="active">
				<td colspan=2><b> <?php	$temp=explode('-',$loc); echo $temp[1];?></b></td>
				<td>
					<center>
						<!--a <?php if(count($val)!=0) echo "style='margin-right: 19px;'"; ?> ><img data-toggle="modal" data-target=".bs-example-modal-lg" <?php if(count($val)!=0) echo 'onclick=edit_loc('.$temp[0].',"'.$temp[1].'","circle_edit")'; else echo 'onclick=edit_loc('.$temp[0].',"'.$temp[1].'","location_edit")';  ?> src="<?php echo BASE_URL; ?>images/edit.png" alt=Edit title=Edit height=15px width=15px></a-->&nbsp;
						<a><img <?php if(count($val)!=0) echo "style='display:none;'"; ?> data-toggle="modal" data-target=".deletemodal" onclick="confirmdelete('<?php echo $temp[0].'/'.$temp[1]; ?>','circle_delete');" src="<?php echo BASE_URL; ?>images/delete.png" alt=Delete title=Delete height=15px width=15px ></a>
					</center>
				</td>
			</tr>
			<?php $groups = unserialize(SIZE_GROUPS); foreach($val as $val) { ?>
			<tr class="tblsub">
				<td>&nbsp;&nbsp;&nbsp;<?php	echo $val['city_name'];?> <?=($val['group'] > 0 ? ' -- '.$groups[$val['group']]:''); ?></td>
				<td> <?php	echo $val['city_email_code']; ?></td>
				<td>
					<center>
						<a><img data-toggle="modal" data-target=".bs-example-modal-lg" onclick="get_details(<?php echo $val['city_id']; ?>,'location_edit');" src="<?php echo BASE_URL; ?>images/edit.png" alt=Edit title=Edit height=15px width=15px></a>&nbsp;
						<a><img data-toggle="modal" data-target=".deletemodal" onclick="confirmdelete(<?php echo $val['city_id'].'.'.$val['location_id']; ?>,'location_delete');" src="<?php echo BASE_URL; ?>images/delete.png" alt=Delete title=Delete height=15px width=15px ></a>
						<?php if($temp[1] == 'Variety') {?>
						<a><img data-toggle="modal" data-target=".gallery" onclick="get_details(<?php echo $val['city_id']; ?>,'gallery_edit');set_gallery_name('<?=htmlentities($val['city_name'])?>');" src="<?php echo BASE_URL; ?>images/galleryicon.png" title="Gallery" style="cursor:pointer;font-size: medium;margin-left: 0px;" class="glyphicon glyphicon-picture" height=20px width=20px></a>
						<?php }?>
					</center>
				</td>
			</tr>
			<?php }}}else {?>
			<tr>
				<td colspan=3>No records to display</td>
			</tr>
			<?php }?>
		</tbody>
	</table>
</div>
<!--delete pop -->
<div class="modal deletemodal" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header-danger">
				<h3 id="head">Delete</h3>
			</div>
			<div class="modal-body">
				<h4><?php  echo CONFIRM_DELETE;  ?></h4>
			</div>
			<div class="modal-footer btndel">
				<button type="button" ng-click="removeUser(user)" class="btn btn-success pull-left deleting" data-dismiss="modal">Ok</button>
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>
<!--add/edit pop -->
<div class="modal bs-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div id='toggleDemo' class="modal-dialog modal-md">
		<?php include_once('location.php'); ?>
	</div>
</div>
<!--sort popup -->
<!--gallery pop -->
<div class="modal gallery"  data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div id='toggleDemo' class="modal-dialog modal-lg">
				<?php include_once('modelgallery.php'); ?>
	</div>
</div>
<div class="modal sort" id="userdanger" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header modal-header-info">
				<h3 id="head">Drag table row and change order</h3>
			</div>
			<div class="modal-body">
				<ul class="nav nav-tabs mr-bodzer">
					<li class="active" id="me"><a href="#circle_it" data-toggle="tab">Masters</a></li>
					<li id="gp"><a href="#location_it" data-toggle="tab">Master Data</a></li>
				</ul>
				<div id="myTabContent" class="tab-content">
					<div class="tab-pane" id="location_it"><br>
						<div class="form-group">
							<div class="col-md-12">
								<div class="alert alert-info sortmsg_hide" style="width: 57%;display:none;">Please Wait...
								</div>
							<select onchange="getcity(this.value,'sort');" data-live-search="true" data-toggle="tooltip" title="<?php echo 'Select master';?>" class="selectpicker form-control" name=sort_location id="sort_location">
							</select><br>
								<table id="location_sortable" class="table sort_item">
									<tbody class="location_modal-body-sort"></tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="tab-pane active in" id="circle_it"><br>
						<div class="form-group">
							<div class="col-md-12">
								<div class="alert alert-info sortmsg_hide" style="width: 57%;">Please Wait...
								</div>
								<table id="circle_sortable" class="table sort_item">
									<tbody class="circle_modal-body-sort"></tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			<div class="modal-footer footerbtns">
				<button type="button" ng-click="removeUser(user)" class="btn btn-success pull-left myTabContent resmdlbtn2" onclick="save_order('location');location.reload();" data-dismiss="modal">Ok</button>
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
</div>
