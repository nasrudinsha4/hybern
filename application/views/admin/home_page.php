<br/><br/>
<body onload="set_char('txt_title','minchar1',70);set_char('seoinp','minchar2',155);set_char('seo_details','minchar3',155);">
	<form  onsubmit="return validate_homepage();"  action="<?php echo BASE_URL; ?>admin/add_homepage" enctype="multipart/form-data" method=post  class="form-horizontal" role="form">
	<div class="form-group">
		<div class='col-md-4'>
			<h2><?= $page_title; ?></h2>
		</div>
		<div class='col-md-4' align='center'><br>
			<?php if($this->session->flashdata('success_msg')){?>
				<div class="green_message"><?php echo $this->session->flashdata('success_msg');?></div>
				<?php } else if($this->session->flashdata('error_msg')){?>
				<div class="red_message"><?php echo $this->session->flashdata('error_msg');?></div>
			<?php }?>
		<div style='width:250px;' class="error" align='center'></div>
		</div>
	</div>
		<!-- Form Name -->
		<?php if(isset($list) and !empty($list)){
			foreach($list as $list){
			}
			}?>
		<!-- Text input-->
		<legend>
		<h4>Banner</h4>
		</legend>
		<div class="form-group">
			<label class="col-md-2 control-label" for="textinput">Banner</label>
			<div class="col-md-3">
				<a data-toggle="modal" data-target=".lightbox">	<img data-html="true" data-toggle="tooltip"
					title="<span>
					<strong>Select image (jpg, png, gif)<strong><br>
					<strong>1366 x 520 (300kb)</strong>
					</span>" class=jslghtbx-thmb src="<?php if(!empty($list)) echo BASE_URL.DIR_THUMB.$list['banner_image']; else echo BASE_URL.'images/thumbnail.png';?>" alt="thumbnail" id="thumbnail">
				</a>
			</div>
			<div class="col-md-2" id="for_browse">
				<input type="button" class="btn btn-primary form-control" value="Browse" onclick="HandleBrowseClick('browse');"/>
				<input type=hidden id="home_page_id" name="home_page_id" value="<?php if(!empty($list)) echo $list['settings_id']; else echo ""; ?>" >
				<input type="file" id="browse" name="fileupload" style="display: none" onChange="image_name(this,'filename','thumbnail');" />
				<input style="display: none" value="<?php if(!empty($list)) echo $list['banner_image'];?>" id="filename" name="filename" readonly  type="text" placeholder="Banner" class="form-control">
				<input type=hidden id='banner_height' >
				<input type=hidden id='banner_width'>
				<input type=hidden id='banner_size'>
			</div>
		</div>
		<!-- Text input-->
		<div class="form-group">
			<label class="col-md-2 control-label" for="textinput">Caption </label>
			<div class="col-md-4 ">
				<input data-toggle="tooltip" title="Text on hover of image" type="text"  value="<?php if(!empty($list)) echo $list['banner_caption'];?>" id="txt_caption" name="txt_caption" placeholder="Caption" class="input-sm form-control">
			</div>
		</div>
		<!-- Text input-->
		<div class="form-group">
			<label class="col-md-2 control-label" for="textinput">Tagline</label>
			<div class="col-md-8 ">
				<input data-toggle="tooltip" title="Text over image" type="text"  value="<?php if(!empty($list)) echo $list['banner_tagline'];?>"  id="txt_tagline" name="txt_tagline" placeholder="Tagline" class="input-sm form-control">
			</div>
		</div>
		<legend>
			<h4>SEO Information</h4>
		</legend>

		<!-- Text input-->
		<div class="form-group">
			<label class="col-md-2 control-label" for="textinput">Title</label>
			<div class="col-md-8 ">
				<input data-toggle="tooltip" title="Title" type="text"  value="<?php if(!empty($list)) echo $list['seo_title'];?>" id="txt_title" name="txt_title" onkeyup="set_char(this.id,'minchar1',70);" onchange="set_char(this.id,'minchar1',70);" maxlength="70" placeholder="Title" class="input-sm form-control home_title">
			</div>
			<label id="minchar1">70</label>
		</div>
		<!-- Text input-->
		<div class="form-group">
			<label class="col-md-2 control-label" for="textinput">Keyword</label>
			<div class="col-md-8 ">
				<input data-toggle="tooltip" title="Keywords for meta tags" value="<?php if(!empty($list)) echo $list['seo_keyword'];?>" id="seoinp" name="txt_keyword" onkeyup="set_char(this.id,'minchar2',155);" onchange="set_char(this.id,'minchar2',155);" maxlength="155" type="text" placeholder="Keyword" class="input-sm form-control home_seoinp">
			</div>
			<label id="minchar2">155</label>
		</div>
		<!-- Text input-->
		<div class="form-group">
			<label class="col-md-2 control-label" for="textinput">Details</label>
			<div class="col-md-8 ">
				<textarea data-toggle="tooltip" title="Description for meta tags" name="txt_details" id="seo_details" onkeyup="set_char(this.id,'minchar3',155);" onchange="set_char(this.id,'minchar3',155);" maxlength="155" rows="3" placeholder="Details" class="input-sm form-control home_details" ><?php if(!empty($list)) echo $list['seo_details'];?></textarea>
			</div>
			<label id="minchar3">155</label>
		</div>
		<div class="form-group btn_group">
			<div class="col-md-offset-2 col-md-10">
				<div class="save">
					<button type="submit" class="btn btn-success pull-left"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
				</div>
				<?php if(!empty($list)) {?>
				<div id="edit" class="">
					<button type="button" class="btn btn-primary pull-left Edit"><span class="glyphicon glyphicon-pencil"></span> Edit</button>
				</div>
				<?php }?>
				<div class="cancel">
					<button id="cancel" type="reset" class="btn btn-danger pull-left"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
				</div>
			</div>
		</div>
	</form>
	<!--lightbox popup -->
	<div class="modal lightbox" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
		<img height="346px" width="910px" class="jslghtbx-thmb1 light" id="homepage_popup" src="<?php echo BASE_URL.DIR.$list['banner_image']; ?>" alt="thumbnail">
	</div>
