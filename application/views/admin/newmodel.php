<div class="modal-content">
	<div class="modal-header modal-header-info pad">
		<h3>Add Product<div style='width:240px;' class="error" align='center'></div></h3>
	</div>

	<div class="modal-body">
		<form id="model" action="<?php echo BASE_URL; ?>admin/save_newmodel" enctype="multipart/form-data" method=post role="form" class="form-horizontal" >
			<ul class="nav nav-tabs mr-bodzer" style="font-size:12px;">
				<li class="active" id="ac"><a style="padding: 7px 12px !important;" href="#pageadd" data-toggle="tab">Add Product</a></li>
				<li onclick='remove_duplicate_models();' id="gp"><a style="padding: 7px 12px !important;" href="#more_models" data-toggle="tab">Add More Products</a></li>
				<li id="gp"><a style="padding: 7px 12px !important;" href="#overview" data-toggle="tab">Overview</a></li>
				<li id="gp"><a style="padding: 7px 12px !important;" href="#details" data-toggle="tab">Details</a></li>
				<li id="gp"><a style="padding: 7px 12px !important;" href="#seo" data-toggle="tab">SEO Information</a></li>
			</ul>
			<div id="myTabContent" class="tab-content">
				<div class="tab-pane active in" id="pageadd">
					<br/>
					<?php $sizes_arr = Array();
					$options = array("Variety","Thickness","Range","Feel","Make","HSN_SAC","Size");
					if(isset($masters) and !empty($masters)){
					$cnt=0;$cntr=0;$row=0;
					foreach($masters as $key => $mast){ 
					if($row==$cntr){?> 
					<div class="form-group">
					<?php }?>
						<div class="col-md-3">
							<select 
							data-live-search="true" 
							<?php if($mast[$cnt]['location_name']=='Size' || $mast[$cnt]['location_name']=='Thickness') 
								echo 'onchange=generate_modelname();'; 
							if ($mast[$cnt]['location_name']=='Variety') 
								echo 'onchange=generate_modelname();get_master_data(this.value)';
							if ($mast[$cnt]['location_name']=='Group') 
								echo 'onchange=get_sizes_groupwise(this.value)';	
							if($mast[$cnt]['location_name']=='Company') 
								echo 'onchange="get_category(this.value);"';?> 
							data-toggle="tooltip" required
							title="Select <?= ($mast[$cnt]['location_name'] != 'HSN_SAC'? lcfirst($mast[$cnt]['location_name']) : $mast[$cnt]['location_name']) ;?>" 
							class="required selectpicker form-control <?php if($row!=$cntr)  echo 'resmdl';?>" 
							name="model_<?= ($mast[$cnt]['location_name'] != 'HSN_SAC'? lcfirst($mast[$cnt]['location_name']) : $mast[$cnt]['location_name']) ;?>" 
							id="model_<?= ($mast[$cnt]['location_name'] != 'HSN_SAC'? lcfirst($mast[$cnt]['location_name']) : $mast[$cnt]['location_name']) ;?>" >
								<?php 
									foreach($mast as $val){
                  					if($mast[$cnt]['location_name'] =='Size')
										$sizes_arr = $mast;
									if(!in_array($mast[$cnt]['location_name'],$options)){
									?>
								<option value="<?php echo $val['city_id'];?>" ><?php echo $val['city_name'];?></option>
								<?php } $cnt++;
								}$cntr++; ?>
							</select>
						</div>
						<?php if($cntr==10){?> 
						<div class="thickness col-md-3">
							<input data-toggle="tooltip" onchange="generate_modelname();" onkeyup="generate_modelname();" title="Thickness of the product" placeholder="Product thickness" type="text" name="model_thickness_text" id="model_thickness_text" class="form-control">
						</div>
						<?php }?>
					<?php if($cntr==($row+4) || $cntr==count($masters)){ $row=$cntr; ?>
					</div>	
					<?php }}
				
				}?>

					<div class="form-group">
						<div class="col-md-3">
							<input data-toggle="tooltip" title="Warranty of the product" placeholder="Product warranty" type="text" name="model_warranty" id="model_warranty" class="required form-control">
						</div>
						<div class="col-md-3">
							<input data-toggle="tooltip" title="Price of the product" placeholder="Product price" type="text" name="model_price" id="model_price" class="required form-control">
						</div>
						<div class="col-md-6">
							<input data-toggle="tooltip" title="Quantity of the product" placeholder="Product quantity" type="text" name="model_quantity" id="model_quantity" class="required form-control resmdl">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6 ">
							<input data-toggle="tooltip" title="Text on hover of image" placeholder="Product caption" type="text" name="model_caption" id="model_caption" class="required form-control">
						</div>
						<div class="col-md-6">
							<input data-toggle="tooltip" title="Name of the product" placeholder="Product name" type="text" name="model_name" id="model_name" class="form-control resmdl">
							<input type="hidden" id="model_name_xml" name="model_name_xml">
						</div>
					</div>					
					<div class="form-group">
						<div class="col-md-6 imgdiv">
							<img onclick="btn_browse.click();" width="140" height="80" id="modelthumb" class=jslghtbx-thmb src="<?php echo BASE_URL.'images/350x200.png';?>" alt="thumbnail" data-html="true" data-toggle="tooltip" title="<span>
							<strong>Select image (jpg, png, gif)<strong><br>
							<strong>350 x 200 (250kb)<strong>
							</span>">
						<div class="imgbar" style="display:none;">
							<div style="height: 3px;margin: 0px;" class="progress progress-striped active">
								<div id="img-bar" class="progress-bar progress-bar-info"></div>
							</div>
					<a href="#" class="abort-img"><span class="glyphicon glyphicon-remove-circle"></span> </a>
						</div>
						</div>
						<div class="col-md-6 ">
							<textarea data-toggle="tooltip" title="Features of product" class="required form-control resmdl" placeholder="Product features" name="model_features" id="model_features" style="width:100%;" rows="5"></textarea>
						</div> 
							<input type="button" id="btn_browse" style="display:none;" onclick="HandleBrowseClick('modelfile');" />
					</div>
					<div class="form-group">
						<div class="col-sm-4 ">
					<label onclick="check_checkbox2('is_display');" for="textinput">Display</label>
							<input data-toggle="tooltip" title="Enable or disable page" type="checkbox" class="rgtmarg" name="is_display" value=1 id="is_display" >
						</div>
						<label class="col-sm-2" for="textinput"></label>
						<div class="col-sm-4" style="position: relative;">
							<input type=text id="brochure_name" name="brochure_name" readonly class="form-control" data-html="true" data-toggle="tooltip" title="<span>
							<strong>Brochure (pdf)<strong>
							</span>">
							<input id="pdfbar" class="form-control progress-bar progress-bar-info">
				<a href="#" class="abort-pdf"><span class="glyphicon glyphicon-remove-circle"></span> </a>
						</div>
						<div class="col-sm-2 ">
							<input  style="display:none;" type="file" id="brochure" name="brochure" onChange="image_name(this,'brochure_name','brochure_name');" >
							<input data-toggle="tooltip" title="Upload brochure" type="button" class="btn btn-primary form-control resmdl2" value="Browse" onclick="HandleBrowseClick('brochure');"/>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-2">
						<input  type="hidden" name="model_image" id="model_image" />
							<input type="file" id="modelfile" name="modelfile" style="display: none" onChange="image_name(this,'model_image','modelthumb');"/>
							<input type=hidden id="model_height"/>
							<input type=hidden id="model_width"/>
							<input type=hidden id="model_img_size"/>
							<input type=text class="hide_text" id="img_temp" name="img_temp"/>
							<input type=text class="hide_text" id="bro_temp" name="bro_temp"/>
							<input type=hidden name="hidden" id="hidden"/>
						</div>
					</div>					
				</div>
				<div class="tab-pane" id="seo">
					<br/>
					<div class="form-group">
						<div class="col-sm-10 ">
							<input data-toggle="tooltip" title="Title" class="form-control" placeholder="Title" type="text" name='seo_title' id="txt_title" maxlength=70 onkeyup="set_char(this.id,'minchar1',70);" onchange="set_char(this.id,'minchar1',70);" >
						</div>
						<label id="minchar1">70</label>
					</div>
					<div class="form-group">
						<div class="col-sm-10 ">
							<input data-toggle="tooltip" title="Keywords for meta tags" class="form-control" placeholder="Keyword" type="text" name='seo_keyword' id="seoinp" maxlength=155 onkeyup="set_char(this.id,'minchar2',155);" onchange="set_char(this.id,'minchar2',155);" >
						</div>
						<label id="minchar2">155</label>
					</div>
					<div class="form-group">
						<div class="col-sm-10 ">
							<textarea data-toggle="tooltip" title="Description for meta tags" class="form-control" placeholder="Details" name="seo_details" id="seo_details" style="width:100%" rows="4"  maxlength=155 onkeyup="set_char(this.id,'minchar3',155);" onchange="set_char(this.id,'minchar3',155);" ></textarea>
						</div>
						<label id="minchar3">155</label>
					</div>
				</div>
				<div class="tab-pane" id="overview">
					<div class="form-group">
					<br>
						<div class="col-sm-12 ">
							<textarea name="model_overview" id="textarea1" ></textarea>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="more_models">
					<div class="form-group">
					<br>
						<div class="col-md-12 ">
		<table id='more_models_table' class='table table-bordered'><thead><th>Size</th><th>Price</th><th>QTY</th></thead><tbody>
		</tbody></table>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="details">
				<br>
					<div class="form-group">
						<div class="col-sm-12 ">
							<textarea name="model_details" id="textarea2"></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer btndel">
						<button type="submit"  onclick="return validate_newmodel();" class="btn btn-success pull-left"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
						<button type="reset" onclick="delfiles();" class="btn btn-danger pull-left" data-dismiss="modal" ng-click="reset(user_add)"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>
<script>
var sizes_arr = '<?php echo json_encode($sizes_arr);?>';
sizes_arr = jQuery.parseJSON(sizes_arr);
</script>
