<center>
	<div class=error></div>
</center>
<div class="modal-content">
	<div class="modal-header modal-header-info">
		<h3>Add Page</h3>
	</div>
	<div class="modal-body">
		<form  onsubmit="return validate_services();" id=services action="<?php echo BASE_URL; ?>admin/save-services" enctype="multipart/form-data" method=post role="form" class="form-horizontal">
			<ul class="nav nav-tabs mr-bodzer">
				<li class="active" id="ac"><a href="#pageadd" data-toggle="tab">Add Page</a></li>
				<li id="gp"><a href="#overview" data-toggle="tab">Overview</a></li>
				<li id="gp"><a href="#details" data-toggle="tab">Details</a></li>
				<li id="gp"><a href="#Enquiry" data-toggle="tab">Enquiry Form</a></li>
				<li id="gp"><a href="#seo" data-toggle="tab">SEO Information</a></li>
			</ul>
			<div id="myTabContent" class="tab-content">
				<div class="tab-pane active in" id="pageadd">
					<br/>
					<div class="form-group">
					<div class="col-sm-6">
					<select data-toggle="tooltip" title="Select page type" class="form-control" name="is_display" id="is_display" >
								<?php $cat=unserialize(PAGE_TYPE);
									foreach($cat as $k => $cat){?>
								<option  value="<?php echo $k;?>" ><?php echo $cat;?></option>
								<?php }?>
					</select>
					</div>
						<div class="col-sm-6">
							<input class="col-sm-1 resrdbtn" type="checkbox"  name="bigimage" value=1 id="bigimage" >
						<label id="lb_bigimg" onclick="check_checkbox('bigimage');" data-toggle="tooltip" title="Display or hide icon" for="textinput">Big Image</label>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6 ">
							<input data-toggle="tooltip" title="Text for menu title" placeholder="Menu Title" id=title name=title onkeyup="set_titles();set_char('seoinp1','minchar1',160);" onchange="set_titles();set_char('seoinp1','minchar1',160);" type="text" class="form-control">
							<input name=title_xml id=title_xml type="hidden">
						</div>
						<div class="col-md-6 resmdl">
							<input data-toggle="tooltip" title="Text on hover of image" placeholder="Caption" name=caption id=caption type="text" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6 pull-left">
							<label class="col-sm-2 control-label" for="textinput">Image</label>
							<div style="display:none;" class="col-sm-3">
								<input type="file" id="imageupload" name="imageupload"  onChange="image_name(this,'image','bannerthumb');"/>
								<input type="text" id="image" name="image" />
								<input type="hidden" id="imagesize" name="imagesize" >
								<input type="hidden" id="image_height" name="image_height" >
								<input type="hidden" id="image_width" name="image_width" >
							</div>

							<div class="col-sm-6">
								<img width="131.43px" height="38.57px" data-html="true" data-toggle="tooltip" title="<span>
							<strong>Select image (jpg, png, gif)<strong><br>
							<strong>920 x 270 (250kb)<strong>
							</span>" onclick="btn_browse.click();" id=bannerthumb class="jslghtbx-thmb thumb1"  src="<?php echo BASE_URL.'images/920x270.png'; ?>" alt="thumbnail">
							<img width="184.29px" height="38.57px" data-html="true" data-toggle="tooltip" title="<span>
							<strong>Select image (jpg, png, gif)<strong><br>
							<strong>1290 x 270 (250kb)<strong>
							</span>" onclick="btn_browse.click();" id=bannerthumb class="jslghtbx-thmb thumb2"  src="<?php echo BASE_URL.'images/1290x270.png'; ?>" alt="thumbnail">
							</div>
							<div class="col-sm-4 pull-left">
								<input type="button" id="btn_browse" style="display:none;" class="form-control btn btn-primary" onclick="HandleBrowseClick('imageupload');"/>
							</div>
						</div>


						<div class="col-md-6 icon pull-right">
							<label class="col-sm-2 control-label" for="textinput">Icon
							</label>
							<div style="display:none;" class="col-sm-3 ">
								<input type="file" id=logoupload name=logoupload style="display: none" onChange="image_name(this,'logo','logothumb');" />
								<input type="text" id="logo" name="logo" />
								<input type="hidden" id="logosize" name="logosize" >
								<input type="hidden" id="logo_height" name="logo_height" >
								<input type="hidden" id="logo_width" name="logo_width" >
							</div>
							<div class="col-sm-5">
								<img data-html="true" data-toggle="tooltip" title="<span>
							<strong>Select image (jpg, png, gif)<strong><br>
							<strong>230 x 115 (50kb)<strong>
							</span>" onclick="btn_browse1.click();" width="92px" height="38.57px" id=logothumb class=jslghtbx-thmb src="<?php echo BASE_URL.'images/service.png'; ?>" >
							</div>
							<div class="col-sm-4 pull-left">
								<input type="button" id="btn_browse1" style="display: none" class="form-control btn btn-primary" onclick="HandleBrowseClick('logoupload');"/>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="Enquiry">
					<br/>
					<div class="form-group">
						<div class="col-md-6 ">
							<input data-html="true" data-toggle="tooltip" title="<span>Enter email id</span>" class="form-control" placeholder="Email ID" type="text" name=email id=email class="inpbx">
						</div>
						<div class="col-md-6 ">
							<input data-toggle="tooltip" title="Enter a subject for email" class="form-control resmdl" placeholder="Subject" type="text" name=subject id=subject class="inpbx">
						</div>
					</div>
				</div>
				<div class="tab-pane" id="seo">
					<br/>
					<div class="form-group">
						<div class="col-md-10 ">
							<input data-toggle="tooltip" title="Title" style="width:100%" class="form-control" placeholder="Title" type="text" name="seoinp1" id="seoinp1" onkeyup="set_char(this.id,'minchar1',160);" maxlength=160 onchange="set_char(this.id,'minchar1',160);" >
						</div>
						<label class="col-md-2" id="minchar1">160</label>
					</div>
					<div class="form-group">
						<div class="col-md-10 ">
							<input data-toggle="tooltip" title="Keywords for meta tags" style="width:100%" class="form-control" placeholder="Keyword" type="text" name="seoinp2" id="seoinp2" onkeyup="set_char(this.id,'minchar2',300);" maxlength=300 onchange="set_char(this.id,'minchar2',300);" >
						</div>
						<label class="col-md-2" id="minchar2">300</label>
					</div>
					<div class="form-group">
						<div class="col-md-10 ">
							<textarea data-toggle="tooltip" title="Description for meta tags" class="form-control" placeholder="Details" maxlength=256 style="width:100%;" rows="4"  id=description name=description onkeyup="set_char(this.id,'minchar3',256);" onchange="set_char(this.id,'minchar3',256);" ></textarea>
							<input type="hidden" id="hidden" name="hidden" >
						</div>
						<label class="col-md-2" id="minchar3">256</label>
					</div>
				</div>
				<div class="tab-pane" id="overview"><br>
					<div class="form-group">
						<div class="col-md-12 ">
							<textarea name="textarea4" id="textarea4" ></textarea>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="details"><br>
					<div class="form-group">
						<div class="col-md-12 ">
							<textarea name="textarea5" id="textarea5" ></textarea>
						</div>
					</div>
				</div>
				<hr>
				<div class="form-group">
					<div class="col-sm-offset-4 col-sm-10">
						<div class="col-md-2">
							<button type="submit" class="btn btn-success pull-left resmdlbtn"><span class="glyphicon glyphicon-floppy-disk"></span> Save</button>
						</div>
						<div class="col-md-2">
							<button type="reset" class="btn btn-danger pull-left" data-dismiss="modal" ng-click="reset(user_add)"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
						</div>
					</div>
				</div>
		</form>
		</div>
	</div>
</div>
