<br/><br/>
<div class="form-group">
	<div class='col-md-4'>
		<h2><?= $page_title; ?></h2>
	</div>
	<div class='col-md-4' align='center'><br>
		<?php if($this->session->flashdata('success_msg')){?>
			<div class="green_message"><?php echo $this->session->flashdata('success_msg');?></div>
			<?php } else if($this->session->flashdata('error_msg')){?>
			<div class="red_message"><?php echo $this->session->flashdata('error_msg');?></div>
		<?php }?>
	</div>
</div>
<div class="col-md-12">
	<button type="button" onclick="reset_form('model');remove_duplicate_models();" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Add Product</button> &nbsp;&nbsp;
	<?php if(isset($model_filter) and !empty($model_filter)){ ?>	
	<a href="<?php echo BASE_URL; ?>admin/model"><button type="button" class="btn btn-primary" >List All</button></a> &nbsp;&nbsp;
	<?php } ?>
</div>	
<br/>
<div class="col-md-12 table-responsive">
	<div class="">
	<table class="table table-condensed tablesorter table-bordered table-hover">
		<thead>
			<tr class="cntth1 info">
				<th>Sr. No.
				<a class="upimg1" title="desc" <?php if($ord=="desc") echo "style='display:none;'";?> href="<?php echo BASE_URL; ?>admin/model-order/desc/model_id"><span class="glyphicon glyphicon-sort-by-alphabet-alt
"></span></a>
					<a class="upimg1" title="asc" <?php if($ord=="asc" or $ord=="default") echo "style='display:none;'";?> href="<?php echo BASE_URL; ?>admin/model-order/asc/model_id"><span class="glyphicon glyphicon-sort-by-alphabet
"></span></a>
				</th>
				<th>Product Name
					<a class="upimg1" title="desc" <?php if($ord=="desc") echo "style='display:none;'";?> href="<?php echo BASE_URL; ?>admin/model-order/desc/model_name"><span class="glyphicon glyphicon-sort-by-alphabet-alt
"></span></a>
					<a class="upimg1" title="asc" <?php if($ord=="asc" or $ord=="default") echo "style='display:none;'";?> href="<?php echo BASE_URL; ?>admin/model-order/asc/model_name"><span class="glyphicon glyphicon-sort-by-alphabet
"></span></a>
			</th>
				<!--th>Image</th-->
				<th>Displayed</th>
				<th>Price
					<a class="upimg1" title="desc" <?php if($ord=="desc") echo "style='display:none;'";?> href="<?php echo BASE_URL; ?>admin/model-order/desc/model_price"><span class="glyphicon glyphicon-sort-by-order-alt
"></span></a>
					<a class="upimg1" title="asc" <?php if($ord=="asc" or $ord=="default") echo "style='display:none;'";?> href="<?php echo BASE_URL; ?>admin/model-order/asc/model_price"><span class="glyphicon glyphicon-sort-by-order
"></span></a>
				</th>
				<th>Quantity
					<a class="upimg1" title="desc" <?php if($ord=="desc") echo "style='display:none;'";?> href="<?php echo BASE_URL; ?>admin/model-order/desc/model_quantity"><span class="glyphicon glyphicon-sort-by-order-alt
"></span></a>
					<a class="upimg1" title="asc" <?php if($ord=="asc" or $ord=="default") echo "style='display:none;'";?> href="<?php echo BASE_URL; ?>admin/model-order/asc/model_quantity"><span class="glyphicon glyphicon-sort-by-order
"></span></a>
				</th>
				<th>Range</th>
				<th>Location</th>
				<th>Date Modified
					<a class="upimg1" title="desc" <?php if($ord=="desc") echo "style='display:none;'";?> href="<?php echo BASE_URL; ?>admin/model-order/desc/model.dt_created"><span class="glyphicon glyphicon-sort-by-alphabet-alt
"></span></a>
					<a class="upimg1" title="asc" <?php if($ord=="asc" or $ord=="default") echo "style='display:none;'";?> href="<?php echo BASE_URL; ?>admin/model-order/asc/model.dt_created"><span class="glyphicon glyphicon-sort-by-alphabet
"></span></a>				
				</th>
				<th>Action 
				<button onclick="reset_form('sortmodal');" title="Filter" data-toggle="modal" data-target=".sortmodal"  style='width:25px !important;' type="button" class="btn pull-right btn-xs btn-info" >
				<span class="glyphicon glyphicon-filter" aria-hidden ="true"></span></button></th>
			</tr>
		</thead>
		<tbody id='model_body'>
			<?php if(isset($model_list) and !empty($model_list)){
				$i=0;
				foreach ($model_list as $list){?>
			<tr>
				<td><label><?php echo $list['model_id'];?></label></td>
				<td><label><?php echo $list['model_name'];?></label></td>
				<!--td>
					<img width="140" height="80" class=jslghtbx-thmb data-toggle="modal" data-target=".lightbox<?php echo $i;?>" src="<?php echo BASE_URL.DIR_THUMB.$list['model_image']; ?>" alt="thumbnail" id="tablethmnail">
				</td-->
				<td style="text-align: center !important;">
					<img src="<?php if($list['is_display']==1) echo base_url().'images/correct.png';
						else echo base_url().'images/cross.png';?>" alt="correct" >
				</td>
				<td><label><?php  echo $list['model_price']; ?></label></td>
				<td><label><?php  echo $list['model_quantity']; ?></label></td>
 				<td><label><?php  echo $list['crange']; ?></label></td>
				<td>
					<p><?php echo $list['clocation'];?></p>
				</td>
				<td><?php echo $list['dt_updated']; ?></td>
				<td>
					<p>
					<center>
						<a><img data-toggle="modal" data-target=".bs-example-modal-lg" onclick="get_details(<?php echo $list['model_id']; ?>,'model_edit');" src="<?php echo BASE_URL; ?>images/edit.png" alt=Edit title=Edit height=15px width=15px></a>&nbsp;
						<a><img data-toggle="modal" data-target=".gallery" onclick="get_details(<?php echo $list['model_id']; ?>,'gallery_edit');set_gallery_name('<?php echo htmlentities($list['model_name']);?>');" src="<?php echo BASE_URL; ?>images/galleryicon.png" title="Gallery" style="cursor:pointer;font-size: medium;margin-left: 0px;" class="glyphicon glyphicon-picture" height=20px width=20px></a>
						<!--a><img data-toggle="modal" data-target=".deletemodal" onclick="confirmdelete(<?php echo $list['model_id']; ?>,'newmodel_delete');" src="<?php echo BASE_URL; ?>images/delete.png" alt=Delete title=Delete height=15px width=15px ></a-->
					</center>
					</p>
				</td>
			</tr>
			<!--lightbox popup -->
			<div class="modal lightbox<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
				<img class="jslghtbx-thmb1 light" src="<?php echo BASE_URL.DIR.$list['model_image']; ?>" alt="thumbnail">
			</div>
			<?php $i++; }} else echo '<tr><td>No records found<td></tr>'?>
		</tbody>
	</table>
	</div>
	<ul class="pagination" id="myPagerView"></ul> 
</div>

<!--sort pop -->
<div class="modal bs-example-modal-md sortmodal" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header-info">
				<h3 id="head">Filter Items</h3>
			</div>
			<div class="modal-body">
			<form name="sortmodal" id="sortmodal" role="form" class="form-horizontal" action="<?php echo BASE_URL; ?>admin/model" method=post>
						<?php if(isset($masters) and !empty($masters)){
					$cnt=0;$cntr=0;
					foreach($masters as $key => $mast){ 
					if(($cntr%2)==0){?> 
					<div class="form-group">
					<?php }$cntr++;?>
						<div class="col-md-6">
							<select data-live-search="true" <?php if($mast[$cnt]['location_name']=='Company') echo 'onchange="get_category(this.value);"';?> data-toggle="tooltip" title="Select <?php if($mast[$cnt]['location_name'] != 'HSN_SAC') echo lcfirst($mast[$cnt]['location_name']); else echo $mast[$cnt]['location_name'];?>" class="selectpicker form-control <?php if(($cntr%2)==0) echo 'resmdl';?>" name="model_<?php echo lcfirst($mast[$cnt]['location_name']);?>" id="srt_<?php echo lcfirst($mast[$cnt]['location_name']);?>" >
								<option value="">Select <?php if($mast[$cnt]['location_name'] != 'HSN_SAC') echo lcfirst($mast[$cnt]['location_name']); else echo $mast[$cnt]['location_name'];?></option>
								<?php
									foreach($mast as $val){
									if($mast[$cnt]['location_name']!='Variety'){?>
								<option value="<?php echo $val['city_id'];?>" ><?php echo $val['city_name'];?></option>
								<?php } $cnt++;
								} ?>
							</select>
						</div>
					<?php if(($cntr%2)==0 || $cntr==count($masters)){ ?>
					</div>	
					<?php }}}?>
			</div>
			</form>
			<div class="modal-footer btndel">
				<button type="button" onclick="srt_model_list();" ng-click="removeUser(user)" class="btn btn-success pull-left " data-dismiss="modal">Ok</button>
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>
<!--delete pop -->
<div class="modal deletemodal" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header-danger">
				<h3 id="head">Delete</h3>
			</div>
			<div class="modal-body">
				<h4><?php  echo CONFIRM_DELETE;  ?></h4>
			</div>
			<div class="modal-footer btndel">
				<button type="button" ng-click="removeUser(user)" class="btn btn-success pull-left deleting" data-dismiss="modal">Ok</button>
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>
<!--add/edit pop -->
<div class="modal bs-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div id='toggleDemo' class="modal-dialog modal-lg">
		<?php include_once('newmodel.php'); ?>
	</div>
</div>
<!--gallery pop -->
<div class="modal gallery"  data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div id='toggleDemo' class="modal-dialog modal-lg">
				<?php include_once('modelgallery.php'); ?>
	</div>
</div>
<!--sort popup -->
<div class="modal sort" id="userdanger" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header modal-header-info">
				<h3 id="head">Drag table row and change order</h3>
			</div>
			<div class="modal-body">
				<ul class="nav nav-tabs mr-bodzer">
					<?php $cat=unserialize(CATEGORY);$n=unserialize(CATEGORY);
									foreach($cat as $k => $cat){?>
					<li <?php if($k==1) echo 'class="active" id="me"'; else echo 'id="gp"';?> ><a href="#<?php echo $cat;?>" data-toggle="tab"><?php echo $cat;?></a></li>
					<?php }?>
				</ul>
				<div id="myTabContent" class="tab-content">
				<?php foreach($n as $k => $cat){?>
					<div <?php if($k==1) echo ' class="tab-pane active in"'; else echo 'class="tab-pane"';?> id="<?php echo $cat;?>"><br>
						<div class="form-group">
							<div class="col-md-12">
								<div class="alert alert-info sortmsg_hide" style="width: 57%;">Please Wait...
								</div>
								<table id="<?php echo $cat;?>_sortable" class="table sort_item">
									<tbody class="<?php echo $cat;?>_modal-body-sort"></tbody>
								</table>
							</div>
						</div>
					</div>
					<?php }?>
				</div>
			</div>
			<div class="modal-footer footerbtns">
				<button type="button" ng-click="removeUser(user)" class="btn btn-success pull-left resmdlbtn2" onclick="save_order('models');" data-dismiss="modal">Ok</button>
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>