<script>var location_list = <?php echo json_encode($location_list);?></script>
<div class="modal-content">
	<div class="modal-header modal-header-info pad">
		<div><h3>Add Billing
				<div style='width:250px;' class="error" align='center'>
				</div></h3>
		</div>
	</div>
	<div class="modal-body">
		<form onsubmit='return validate_billing();' enctype="multipart/form-data" name=save_billing id=save_billing action="<?php echo BASE_URL; ?>admin/save-billing" method="post" role="form" class="form-horizontal">
			<ul class="nav nav-tabs mr-bodzer" style="font-size:12px;">
				<li class="new_billing" id="ac">
					<a onclick='show_hide_billbtn("ac");' style="padding: 7px 12px !important;"  href="#billing" data-toggle="tab">Add Bill</a>
				</li>
				<li id="gp" class="purchase_his">
					<a onclick='show_hide_billbtn("gp");' style="padding: 7px 12px !important;"  href="#history" data-toggle="tab">Purchase History</a>
				</li>
			</ul>
			<div id="myTabContent" class="tab-content">
				<div class="tab-pane active in" id="billing">
					<br/>
					<div class="form-group" id='bill_customer_details'>
						<div class="col-md-6">
							<input style='margin-bottom:3%;display:inline !important;width:47%;' data-toggle="tooltip" title="Customer name" class="form-control  contact_us resmdl" placeholder="Name" type="text" name=billing_name id="billing_name" >
							<input style='margin-bottom:3%;display:inline !important;width:47%;float: right;' data-toggle="tooltip" title="Customer email" placeholder="Email" type="text" name=billing_email id=billing_email class="form-control contact_us resmdl" >
							<input style='margin-bottom:3%;display:inline !important;width:47%;' data-toggle="tooltip" title="Customer phone number" placeholder="Phone" type="text" name=billing_phone id=billing_phone class="form-control contact_us" >
              				<input style='margin-bottom:3%;display:inline !important;width:47%;float: right;' data-toggle="tooltip" title="Customer GSTIN" placeholder="GSTIN" type="text" name=customer_GSTIN id=customer_GSTIN class="form-control contact_us" >
							<input style='display:inline !important;width:47%;' data-toggle="tooltip" title="Billing date" placeholder="Billing date" type="text" name=billing_date id=billing_date class="form-control contact_us" >
              				<input style='display:inline !important;width:47%;float: right;' data-toggle="tooltip" title="Delivery date" placeholder="Delivery date" type="text" name=delivery_date id=delivery_date class="form-control contact_us" >

						</div>
						<div class="col-md-6 resmdl">
						<textarea style='display:inline !important;width:47%;' data-toggle="tooltip" title="Customer address" placeholder="Address" name=billing_address id=billing_address rows="6" class="form-control contus-textar"></textarea>
						<textarea style='display:inline !important;width:47%;float: right;' data-toggle="tooltip" title="Customer notes" placeholder="Notes" name=billing_notes id=billing_notes rows="6" class="form-control contus-textar"></textarea>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6">
							<select onchange="set_prod_loc(this);" data-live-search="true" data-toggle="tooltip" title="Select product" name="model_name" id="model_name" class="selectpicker form-control">
							<?php foreach($model_list as $list){ ?>
								<option data-price="<?=$list['model_price']?>" data-hsn-text="<?=$list['hsn_text']?>" data-hsn="<?=$list['city_email_code']?>" data-range="<?=$list['model_range']?>" value='<?=$list['model_id']?>' data-location='<?=$list['model_location'];?>'  ><?=$list['model_name'].' '.$list['crange']?></option>
							<?php } ?>
							</select>
						</div>
						<div class="col-md-3 resmdl">
							<input data-toggle="tooltip" title="Quantity of the product" placeholder="Model quantity" type="text" name="quantity" id="quantity" class="form-control">
						</div>
						<div class="col-md-3 resmdl">
							<input data-toggle="tooltip" title="Price of the product" placeholder="Model price" type="text" name="price" id="price" class="form-control">
						</div>
					</div>
					<div class="form-group">
 						<div class="col-md-3">
						<select data-live-search="true" data-toggle="tooltip" title="Select location" name="model_location" id="model_location" class="selectpicker form-control">							
						</select>
						</div>
 						<div class="col-md-3 resmdl">
						<input data-toggle="tooltip" title="Discount" placeholder="Discount" type="text" name="discount" id="discount" class="form-control">
						</div>
 						<div class="col-md-3 resmdl">
						<input data-toggle="tooltip" title="Advance" placeholder="Advance" type="text"  value="0.00" id="advance" name="advance" class="form-control">
						</div>
 						<div class="col-md-3 resmdl">
						<input readonly data-toggle="tooltip" title="Balance" placeholder="Balance" value="0" type="text"  name="balance" id="balance" class="form-control">
						</div>						
					</div>
					<div class="form-group" id='bill_product_details'>
						<div style="font-size:12px;overflow: auto;" class="col-md-12">
							<table id="billaddlist" style="border-collapse: separate;display:none;" class="table">
								<thead style="font-size:12px;font-weight:bold;">
									<tr>
									<td>HSN/SAC</td>
									<td>Description</td>
									<td>Location</td>
									<td>Qty</td>
									<td>Rate</td>
									<td>Discount</td>
									<td>Taxable<br>value</td>
										<td colspan="2" class='align-cent'>CGST
											<div>
												<div class="span1 t">Rate
												</div>
												<div class="t r span2">Amt
												</div>
											</div></td>
										<td colspan="2" class='align-cent'>SCGST
											<div>
												<div class="span1 t">Rate
												</div>
												<div class="t r span2">Amt
												</div>
											</div></td>
											<td>Total</td>
											<td class="hide align-rit">Action</td>
									</tr>
								</thead>
								<tbody>
								</tbody>
								<tfoot class="foot">
									<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td class="align-rit">Total:
											<INPUT type=hidden name="total" id=actual_total>
											<INPUT type=hidden name="total_qty" id=total_qty>
											<INPUT type=hidden name="total_rate" id=total_rate>
											<INPUT type=hidden name="total_discount" id=total_discount>
											<INPUT type=hidden name="total_tv" id=total_tv>
											<INPUT type=hidden name="total_gst" id=total_gst></td>
									<td class='total_qty'></td>
									<td class='total_rate'></td>
									<td class='total_discount'></td>
									<td class='total_tv'></td>
									<td>&nbsp;</td>
									<td class='align-rit total_gst'></td>
									<td>&nbsp;</td>
									<td class='align-rit total_gst'></td>
									<td><span id='total'></span>
									</td>
									<td class="hide">
											<input type="hidden" value=0 id=row_count name="row_count"></td>
									</tr>
									<tr><td style="text-align:right !important;" colspan="11">Advance:</td><td><span id="total_advance"></span></td></tr>
									<tr><td style="text-align:right !important;" colspan="11">Total discount:</td><td><span id="total_dis"></span></td></tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
				<div class="tab-pane" id="history">
					<br/>
					<div class="form-group">
						<div style="font-size:12px;overflow:auto;" class="col-md-12">
							<table id="purchase_history" style="border-collapse: separate;" class="table">
								<thead style="font-size:12px;font-weight:bold;">
									<tr>
									<td>&nbsp;</td>
									<td>Invoice No.</td>
                 					<td>HSN/SAC</td>
									<td>Description</td>
									<td>Location</td>
									<td>Billed</td>
									<td>Delivery</td>
									<td>Advance</td>
									<td>Qty</td>
									<td>Rate</td>
									<td>Discount</td>
									<td>Taxable<br>Value</td>
										<td colspan="2" class='align-cent'>CGST
											<div>
												<div class="span1 t">Rate
												</div>
												<div class="t r span2">Amt
												</div>
											</div></td>
										<td colspan="2" class='align-cent'>SCGST
											<div>
												<div class="span1 t">Rate
												</div>
												<div class="t r span2">Amt
												</div>
											</div></td>
											<td>Total Amount</td>
									</tr>
								</thead>
								<tbody id='myTable'>
								</tbody>
								<tfoot class="foot">
									<tr>							<td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>						<td>&nbsp;</td>							<td>&nbsp;</td>							<td>&nbsp;</td>							<td>&nbsp;</td>							<td>&nbsp;</td>							<td>&nbsp;</td>							<td>&nbsp;</td>							<td>&nbsp;</td>							<td>&nbsp;</td>							<td>&nbsp;</td>
										<td style="text-align: right !important;">Total:</td><td class='align-rit'>
											<span id='purchase_total'>
											</span></td>
									</tr>
								</tfoot>
							</table>
							<ul class="pagination" id="myPager">
							</ul>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" onclick="billaddlist()" class="billbtn btn btn-warning pull-left">
						<span class="glyphicon glyphicon-plus">
						</span> Add
					</button>
					<button type="submit" class="billbtn btn btn-success pull-left">
						<span class="glyphicon glyphicon-floppy-disk">
						</span> Save
					</button>
					<button type="button" id='btnPrint' class=" printbtn btn btn-info pull-left">
						<span class="glyphicon glyphicon-print">
						</span> Print
					</button>
					<button type="reset" class="btn btn-danger pull-left" data-dismiss="modal" ng-click="reset(user_add)">
						<span class="glyphicon glyphicon-remove">
						</span> Cancel
					</button>
					<input type=hidden id=hidden name=hidden >
				</div>
		</form>
	</div>
</div>
</div>