<div class="modal-content">
	<div class="modal-header modal-header-info pad gallery_name">
		<a data-dismiss="modal" href="#" class="pull-right cross"><span class="glyphicon glyphicon-remove"></span> </a>
		<h3></h3>
	</div>
	<div class="modal-body">
		<form id="gallery_model" name="gallery_model"   enctype="multipart/form-data" method=post role="form" class="form-horizontal" >
			<div id="myTabContent" class="tab-content">
				<div style="margin-left: 20px;margin-right: 20px;" >
					<br>
					<div class="form-group">
						<div class="col-sm-6">
							<input onkeypress="enable_gal_btn();" data-toggle="tooltip" data-original-title="Product title" class="form-control" placeholder="Title" type="text" id='title' name='title' >
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-6">
							<div class="bannerthub" id="thumb">
								<img onclick="HandleBrowseClick('gallery_file');" data-html="true" data-toggle="tooltip"title="" data-original-title="<span>
									<strong>Select image (jpg, png, gif)<strong><br>
									<strong>970 x 380 (250kb)<strong>
									</span>" width="388px" height="152px" class="jslghtbx-thmb prvgalthb" id="gallery_thumb" src="<?php echo BASE_URL; ?>images/970x380.jpg" alt="thumbnail">
								<div class="bar" style="display:none;">
									<div style="height: 3px;margin: 0px;" class="progress ">
										<div class="progress-bar"></div>
									</div>
					<a href="#" class="abort"><span class="glyphicon glyphicon-remove-circle"></span> </a>
								</div>
							</div>
							<input class="form-control" type="hidden" id="gallery_image" name="gallery_image"  >
							<input style="display:none;" type="file"  onChange="image_name(this,'gallery_image','gallery_thumb');enable_gal_btn();" id="gallery_file" name="gallery_file"  />
							<input type="hidden" id="gallery_size" >
							<input type="hidden" id="gallery_height" >
							<input type="hidden" id="gallery_width" >
							<input class="hide_text" id="model_id" name="model_id">
							<input type=text class="hide_text" id='image_src'>
						</div>
						<div class="col-sm-offset-1 col-sm-5 prevcont">
							<div style="display:none;" class="preview_container"></div>
						</div>
					</div>
					<div class="form-group col-md-6 actionbtns actgalbtn">
						<button  type="button" class="btn btn-primary addbtns galbtns"><span class="glyphicon glyphicon-floppy-saved"></span> Add</button>
						<button onclick="clear_image();" type="button" class="btn btn-primary clearbtns galbtns"><span class="glyphicon glyphicon-floppy-remove"></span> Clear</button>
						<button onclick="delete_gallery();" type="button" class="btn btn-primary delbtns"><span class="glyphicon glyphicon-trash"></span> Delete</button>
					</div>
					<input class="hide_text" name="sr_number" id="sr_number">
					<input class="hide_text" name="img_id" id="img_id">
					<input class="hide_text" name="img_name" id="img_name">
					<input class="hide_text" name="page_name" id="page_name">
				</div>
			</div>
		</form>
	</div>
</div>
