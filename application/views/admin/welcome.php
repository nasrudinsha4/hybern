<br><br><br>
<legend>
	<h2><?= $page_title; ?></h2>
</legend>
<center>
	<div class="green_message" style="display:none;"></div>
</center>
<center>
	<div class="red_message" style="display:none;"></div>
</center>
<input type="hidden" id="disable_date" value="<?=((isset($stats_date) and !empty($stats_date))? date('d/M/Y', strtotime($stats_date)):'')?>">
<div id="toolbar">
	<div class="form-inline" role="form">
		<div class="form-group">
			<div id="reportrange">
				<i class="glyphicon glyphicon-calendar"></i>&nbsp;
				<span></span>
				<span id="range" style="display:none;"></span><b class="caret"></b>
			</div>
		</div>
		<label id="available_records" style="margin-left: 10px;"><?=((isset($stats_date) and !empty($stats_date))? AVAILABLE_DATA.$stats_date.'.':'')?></label>
	</div>
</div>
<div id="hotdiv">
	<table
		id="table"
		data-toolbar="#toolbar"
		>
		<thead class="table-header">
			<tr>
				<th>&nbsp;</th>
				<th>&nbsp;</th>
				<th colspan="10">
					<center>Enquiry Forms</center>
				</th>
				<th colspan="3">
					<center>Feedback Forms</center>
				</th>
			</tr>
			<tr>
				<th data-field="location">Location</th>
				<th data-field="city">City</th>
				<th data-field="Sales Enquiry">Sales</th>
				<th data-field="Book a Test Drive">Test Drive</th>
				<th data-field="Services">Service</th>
				<th data-field="Accidental Repair">Accidental Repair</th>
				<th data-field="Pre-Owned Cars">TV</th>
				<th data-field="Maruti Driving School">Driving School</th>
				<th data-field="Careers">Careers</th>
				<th data-field="Contact us">Contact Us</th>
				<th data-field="Insurance">Insurance</th>
				<th data-field="Extended Warranty">Extended Warranty</th>
				<th data-field="Sales Feedback">Sales</th>
				<th data-field="Feedback">Service</th>
				<th data-field="Body Repair">Body Shop</th>
			</tr>
		</thead>
	</table>
</div>
<br><br>
<!--delete pop -->
<div class="modal deletemodal" id="statistic_model" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header-danger">
				<h3 id="head">Delete</h3>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div style="padding-Left: 0;" class="col-md-8 form-group">
							<label style="padding-left: 0;padding-top: 7px;" class="col-md-6 control-label lbl_required">Delete records upto</label>
							<div class="input-group datepicker ">
								<input name="todate" id="todate" type='text' placeholder="Select Date"class="form-control" />
								<span class="input-group-addon" onclick="getElementById('todate').click();">
								<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<h4><?php  echo CONFIRM_DELETE2;  ?></h4>
					</div>
				</div>
			</div>
			<div class="modal-footer btndel">
				<button type="button" onclick="delete_statistic_report();" class="btn btn-success pull-left deleting">Ok</button>
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>
