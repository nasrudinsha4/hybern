<br/><br/>
<div class="form-group">
	<div class='col-md-4'>
		<h2><?= $page_title; ?></h2>
	</div>
	<div class='col-md-4' align='center'><br>
		<?php if($this->session->flashdata('success_msg')){?>
			<div class="green_message"><?php echo $this->session->flashdata('success_msg');?></div>
			<?php } else if($this->session->flashdata('error_msg')){?>
			<div class="red_message"><?php echo $this->session->flashdata('error_msg');?></div>
		<?php }?>
	</div>
</div>
<div class="col-md-12">
	<button type="button" onclick="reset_form('services');" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-lg">Add Page</button> &nbsp;&nbsp;
	<button type="button" onclick="get_sorted_pages('services');" class="btn btn-primary" data-toggle="modal" data-target="#userdanger">Sort Items</button>
	<div class="col-md-3 pull-right">
		<form name="service_view" id="service_view" action="<?php echo BASE_URL; ?>admin/pages" method=post>
			<select onchange="document.getElementById('service_view').submit();" data-toggle="tooltip" title="Select Variety" class="form-control" name="pages_id" id="pages_id" >
				<?php $cat=unserialize(PAGE_TYPE);
						foreach($cat as $k => $cat){?>
				<option <?php if(isset($page_id) and $page_id==$k) echo "selected";?> value="<?php echo $k;?>"><?php echo $cat;?></option>
				<?php }?>
			</select>
		</form>
	</div>
</div>
<br/>
<div class="col-md-12 table-responsive  ">
	<div class="table-condensed">
	<table class="table table-bordered table-hover">
		<thead>
			<tr class="cntth1 info">
				<th  class="cntth2">Menu Name</th>
				<th id="cntth2">Image</th>
				<th id="cntth5">Overview</th>
				<th id="cntth5">Details</th>
				<th id="cntth3">Action</th>
			</tr>
		</thead>
		<tbody>
			<?php if(isset($ids) and !empty($ids)){
				$i=0;
				foreach($ids as $list){ ?>
			<tr>
				<td>
					<p>
						<?php echo $list['service_title']; ?>
					</p>
				</td>
				<td>
					<img <?php if(!empty($list['service_icon'])) echo 'width="131.43" height="38.57"'; else echo 'width="129" height="27"'; ?>  class=jslghtbx-thmb data-toggle="modal" data-target=".lightbox<?php echo $i;?>" src="<?php echo BASE_URL.DIR_THUMB.$list['service_image']; ?>" alt="thumbnail" id="tablethmnail">
				</td>
				<td>
					<p><?php echo $list['service_overview']; ?> </p>
				</td>
				<td >
					<p> <?php echo $list['service_details']; ?></p>
				</td>
				<td>
					<p>
					<center>
						<a><img data-toggle="modal" data-target=".bs-example-modal-lg" onclick="get_details(<?php echo $list['service_id']; ?>,'get_services');" src="<?php echo BASE_URL; ?>images/edit.png" alt=Edit title=Edit height=15px width=15px></a>&nbsp;
					<?php if($list['non_deletable']!=1){ ?>	<a><img data-toggle="modal" data-target=".deletemodal" onclick="confirmdelete(<?php echo $list['service_id']; ?>,'service_delete');" src="<?php echo BASE_URL; ?>images/delete.png" alt=Delete title=Delete height=15px width=15px ></a>  <?php }?>
					</center>
					</p>
				</td>
			</tr>
			<!--lightbox popup -->
			<div class="modal lightbox<?php echo $i;?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
			<img <?php if(!empty($list['service_icon'])) echo 'width="736px" height="216px"'; else echo 'width="860px" height="180px"'; ?> class="jslghtbx-thmb1 light" src="<?php echo BASE_URL.DIR.$list['service_image']; ?>" alt="thumbnail">
			</div>
			<?php $i++;}}else {?>
			<tr>
				<td colspan=7 >No records to display</td>
			</tr>
			<?php }?>
		</tbody>
	</table>
	</div>
</div>
<!--delete pop -->
<div class="modal deletemodal" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header-danger">
				<h3 id="head">Delete</h3>
			</div>
			<div class="modal-body">
				<h4><?php  echo CONFIRM_DELETE;  ?></h4>
			</div>
			<div class="modal-footer btndel">
				<button type="button" ng-click="removeUser(user)" class="btn btn-success pull-left deleting" data-dismiss="modal">Ok</button>
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>
<!--add/edit popup -->
<div class="modal bs-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div id='toggleDemo' class="modal-dialog modal-lg">
		<?php include_once('newservice.php'); ?>
	</div>
</div>
<!--sort popup -->
<div class="modal sort" id="userdanger" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header modal-header-info">
				<h3 id="head">Drag table row and change order</h3>
			</div>
			<div class="modal-body">
				<ul class="nav nav-tabs mr-bodzer">
					<li class="active" id="me"><a href="#sales_it" data-toggle="tab">Sales Menu</a></li>
					<li id="gp"><a href="#menu_it" data-toggle="tab">Workshop Menu</a></li>
					<li id="gp"><a href="#insurance_it" data-toggle="tab">Insurance Menu</a></li>
					<li id="gp"><a href="#footer_it" data-toggle="tab">Footer</a></li>
				</ul>
				<div id="myTabContent" class="tab-content">
					<div class="tab-pane active in" id="sales_it"><br>
						<div class="form-group">
							<div class="col-md-12">
								<div class="alert alert-info sortmsg_hide" style="width: 57%;">Please Wait...
								</div>
								<table id="sales_sortable" class="table sort_item">
									<tbody class="sales_modal-body-sort"></tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="menu_it"><br>
						<div class="form-group">
							<div class="col-md-12">
								<div class="alert alert-info sortmsg_hide" style="width: 57%;">Please Wait...
								</div>
								<table id="menu_sortable" class="table sort_item">
									<tbody class="menu_modal-body-sort"></tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="insurance_it"><br>
						<div class="form-group">
							<div class="col-md-12">
								<div class="alert alert-info sortmsg_hide" style="width: 57%;">Please Wait...
								</div>
								<table id="insurance_sortable" class="table sort_item">
									<tbody class="insurance_modal-body-sort"></tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="footer_it"><br>
						<div class="form-group">
							<div class="col-md-12">
								<div class="alert alert-info sortmsg_hide" style="width: 57%;">Please Wait...
								</div>
								<table id="footer_sortable" class="table sort_item">
									<tbody class="footer_modal-body-sort"></tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer footerbtns">
				<button type="button" ng-click="removeUser(user)" class="btn btn-success pull-left myTabContent resmdlbtn2" onclick="save_order('services');" data-dismiss="modal">Ok</button>
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
