<br/><br/>
<div class="form-group">
	<div class='col-md-4'>
		<h2><?= $page_title; ?></h2>
	</div>
	<div class='col-md-4' align='center'><br>
		<?php if($this->session->flashdata('success_msg')){?>
			<div class="green_message"><?php echo $this->session->flashdata('success_msg');?></div>
			<?php } else if($this->session->flashdata('error_msg')){?>
			<div class="red_message"><?php echo $this->session->flashdata('error_msg');?></div>
		<?php }?>
	</div>
</div>
<form id='search_form' action='<?php echo BASE_URL; ?>admin/billing' method=post>
	<input type=hidden name='billing_id' id='bill_id'>
	<input type=hidden name='billing_name' id='bill_name'>
	<input type=hidden name='billing_address' id='bill_address'>
	<input type=hidden name='billing_phone' id='bill_phone'>
	<input type=hidden name='billing_email' id='bill_email'>
	<input type=hidden name='dt_created' id='dt_created'>
</form>
<div class="col-md-12">
<div class="input-group">
	<button type="button" onclick="reset_form('save_billing');" class="btn btn-primary pull-left" data-toggle="modal" data-target=".bs-example-modal-lg">Add billing</button>&nbsp;&nbsp;
	<?php if(isset($model_filter) and !empty($model_filter)){ ?>
	<a href="<?php echo BASE_URL; ?>admin/billing"><button type="button" class="btn btn-primary" >List All</button></a> &nbsp;&nbsp;
	<?php } ?>
	<input type="text" id='searchbox' class="form-control pull-right" style='border-radius: 4px 0px 0px 4px !important;padding: 6px 10px !important;width:18%' placeholder="Search" aria-describedby="basic-addon2">
	<span class="input-group-addon" onclick="submit_search();" id="basic-addon2">GO</span>
</div>

</div>
<br/>
<div id='invoice' >
<div class="container">
<div class="row">
	<div class="col-xs-12">
		<div class="invoice-title row">
			<div class="col-xs-6"><h2>Invoice</h2></div><div class="col-xs-6"><h2 class='pull-right'>Cust. Code# <span id='invoice_bill_no'></span></h2></div>
		</div>
		<hr style='margin-top: 0px !important;margin-bottom: 5px !important;'>
		<div class="row">
			<div class="col-xs-6">
				<address>
				<strong>Billed To:</strong><div class='billAdd'></div>
				</address>
			</div>
			<div class="col-xs-6 text-right">
				<address>
					<strong class='OwnName'></strong>
					<div class='OwnAdd'></div>
				</address>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-6">
				<address>
					<strong>Payment Method:</strong><br>
					Visa ending **** 4242<br>
					jsmith@email.com
				</address>
			</div>
			<div class="col-xs-6 text-right">
				<address>
					<strong>Bill Date:</strong><div class='BillDate'></div>
				</address>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><strong>Order summary</strong></h3>
			</div>
			<div class="" >
			<table id='invoicetable' class='table printtable'>
			<thead style="font-size:12px;font-weight:bold;"></thead>
			<tbody></tbody>
			<tfoot></tfoot>
			</table>
			</div>
		</div>
	</div>
</div>
</div>
</div>
<div class="col-md-12 table-responsive ">
	<div class="">
	<table class="table table-condensed table-bordered table-hover">
		<thead>
			<tr class="cntth1 info">
				<th>Customer Code</th>
				<th>Name</th>
				<th>Phone</th>
				<th>Email</th>
				<th>Address</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id='billing_body'>
			<?php if(isset($billing_list) and !empty($billing_list)){
				$i=0;
				foreach ($billing_list as $list){?>
			<tr>
				<td>
						<?php echo $list['billing_id']; ?>
				</td>
				<td>
					<p>
						<?php echo $list['billing_name']; ?>
					</p>
				</td>
				<td id="lheight">
					<p><?php if(!empty($list['billing_phone'])) echo str_replace(",","<br>",$list['billing_phone']); ?> </p>
				</td>
				<td id="lheight">
					<p><?php if(!empty($list['billing_email'])) echo str_replace(",","<br>",$list['billing_email']); ?></p>
				</td>
				<td>
					<p><?php echo str_replace("\n","<br>",$list['billing_address']); ?></p>
				</td>
				<td>
					<p>
					<center>
					<a><img data-toggle="modal" data-target=".bs-example-modal-lg" onclick="show_hide_billbtn('gp');get_details(<?php echo $list['billing_id']; ?>,'billing_edit','print_view');" src="<?php echo BASE_URL; ?>images/carr.png" alt=Edit title=Edit height=15px width=15px></a>&nbsp;	
					<a><img data-toggle="modal" data-target=".bs-example-modal-lg" onclick="show_hide_billbtn('ac');get_details(<?php echo $list['billing_id']; ?>,'billing_edit');" src="<?php echo BASE_URL; ?>images/edit.png" alt=Edit title=Edit height=15px width=15px></a>&nbsp;
						<a><img data-toggle="modal" data-target=".deletemodal" onclick="confirmdelete(<?php echo $list['billing_id']; ?>,'billing_delete');" src="<?php echo BASE_URL; ?>images/delete.png" alt=Delete title=Delete height=15px width=15px ></a>
					</center>
					</p>
				</td>
			</tr>
			<?php $i++;}}else {?>
			<tr>
				<td colspan=7 >No records to display</td>
			</tr>
			<?php }?>
		</tbody>
	</table>
	<ul class="pagination" id="myPagerView"></ul> 
	</div>
</div>
<!--delete pop -->
<div class="modal deletemodal" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header-danger">
				<h3 id="head">Delete</h3>
			</div>
			<div class="modal-body">
				<h4><?php  echo CONFIRM_DELETE;  ?></h4>
			</div>
			<div class="modal-footer btndel">
				<button type="button" ng-click="removeUser(user)" class="btn btn-success pull-left deleting" data-dismiss="modal">Ok</button>
				<button type="button" class="btn btn-danger pull-left" data-dismiss="modal">Cancel</button>
			</div>
		</div>
	</div>
</div>
<!--add/edit pop -->
<div class="modal bs-example-modal-lg" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
	<div id='toggleDemo' class="modal-dialog modal-lg wide-model">
		<?php include_once('newbilling.php'); ?>
	</div>
</div>
