<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');
class Admin_login extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_admin');
	}

/**
* checks for valid login.
* functions used :-
* check_login_details : checks if login details exists in the db.
* dataUpdate_admin : stores the date of last activity made by user.
*/
	public function index()
	{
		$data="";
		if(!empty($_POST)&&($_POST!=null)) {
			$username=trim($_POST['txt_username']);
			$passowrd=trim($_POST['md5pass']);
			$data=array(
				'LOWER(username)'=>$username,
				'LOWER(user_password)'=>$passowrd
			);
			$check_login=$this->m_admin->check_login_details($data);
			if(!$check_login) {
				$data['error_msg']=INVALID_USER;
			} else {
				$saveArray['last_active']=date("Y-m-d H:i:s");
				$this->m_admin->dataUpdate_admin($check_login['admin_id'], $saveArray);
				$this->session->set_userdata($check_login);
				redirect('admin/home-page');
			}
		}
		$this->load->view('layout/admin_login',$data);
	}

	public function changepassword(){
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$id = $this->input->post('user_id');
		$data = array(
			'user_password' => trim($this->input->post('password'))
		);
		$r=$this->m_admin->dataUpdate_admin($id,$data);
		echo $r;
	}
}
?>
