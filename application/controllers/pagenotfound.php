<?php
class pagenotfound extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['page_title'] =  "404 Page Not Found | " . TITLE_CIL;
		$this->output->set_status_header('404');
		$this->content['content'] = $this->parser->parse('client/404pagenotfound', $data, true);
		$this->renderclientTemplate();
	}
}
?>
