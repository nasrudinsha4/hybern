<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Frontend extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_location');
		$this->load->model('m_models');
		$this->load->model('m_billing');
		$this->load->model('m_frontend');	
		$this->load->library('pagination');	
		session_start();
	}

	public function index()
	{
		$data['list'] = $this->m_homepage->get_hompage_data();
		$data['page_title'] =  $data['list'][0]['seo_title']  ." | " . TITLE_CIL;
		$data['service_keyword'] =  $data['list'][0]['seo_keyword'];
		$data['service_description'] =  $data['list'][0]['seo_details'];
		$data['header_height'] = 1;
		$data['page'] = 'home';
		$data['gallery'] = $this->m_frontend->fetch_gallery();
		$this->content['content'] = $this->parser->parse('client/homepage', $data, true);
		$this->renderclientTemplate();
	}

	public function sitemap()
	{
		$data['menu']=unserialize(PAGE_TYPE);
		$data['salesmenu'] = $this->m_frontend->get_salesmenu();
		$data['workshopmenu'] = $this->m_frontend->get_workshopmenu();
		$data['insurancemenu'] = $this->m_frontend->get_insurancemenu();
		$data['foot']= $this->m_frontend->get_footerpages();
		$data['page_title'] =  ucfirst($this->uri->segment(1)) ." | " . TITLE_CIL;
		$data['service_keyword'] =ucfirst($this->uri->segment(1));
		$data['service_description'] =ucfirst($this->uri->segment(1));
		$this->content['content']=$this->parser->parse('client/sitemap', $data, true);
		$this->renderclientTemplate();
	}

	public function contactus()
	{
		$data['page'] = 'contact';
		$data['page_title'] =  "Contact | " . TITLE_CIL;
		$this->content['content']=$this->parser->parse('client/contactus', $data, true);
		$this->renderclientTemplate();
	}

	public function size_guide()
	{
		$data['page'] = 'size guide';
		$data['page_title'] =  "Size guide | " . TITLE_CIL;
		$data['menu'] = 'about';
		$this->content['content']=$this->parser->parse('client/size_guide', $data, true);
		$this->renderclientTemplate();
	}

	public function buying_guide()
	{
		$data['page'] = 'buying guide';
		$data['page_title'] =  "Buying guide | " . TITLE_CIL;
		$data['menu'] = 'about';
		$this->content['content']=$this->parser->parse('client/buying_guide', $data, true);
		$this->renderclientTemplate();
	}

		public function aboutus()
	{
		$data['page'] = 'Our Story';
		$data['page_title'] =  "Our Story | " . TITLE_CIL;
		$data['menu'] = 'about';
		$this->content['content']=$this->parser->parse('client/aboutus', $data, true);
		$this->renderclientTemplate();
	}

		public function gallery()
	{
		$data['page'] = 'Gallery';
		$data['page_title'] =  "Gallery | " . TITLE_CIL;
		$this->content['content']=$this->parser->parse('client/gallery', $data, true);
		$this->renderclientTemplate();
	}	

	public function shop()
	{
		$total_records = 0;
		$limit = 18;
		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data['page'] = 'Products';
		$data['page_title'] =  ucfirst($this->uri->segment(1)) ." | " . TITLE_CIL;
		$id=($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		if(isset($id) and !empty($id)){ 
			$filter = array('model.model_category'=>$id);
			$data['model_list']=$this->m_models->model_list($limit, $page,'',$filter,'',1);
			$total_records = count($this->m_models->model_list('', '','',$filter,'',1));
		}else{		
			$data['model_list']=$this->m_models->model_list($limit,$page,'','','',1);
			$total_records = count($this->m_models->model_list('','','','','',1));
		}
		$masters=$this->m_location->get_city_location();
		$cnt=0;
		foreach($masters as $mast){
			$data['masters'][$mast['location_name']][$cnt]=$mast;
			$cnt++;
		}	
		// custom paging configuration
		$config['first_link'] = false; 
		$config['last_link']  = false;
		$config['full_tag_open'] = "<ul class='pagination'>";
		$config['full_tag_close'] = '</ul>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['next_link'] = '<i class="fa fa-caret-right"></i>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_link'] = '<i class="fa fa-caret-left"></i>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['base_url'] =  base_url() . 'products/'.$id;
		$config['total_rows'] = $total_records; 
		$config["per_page"] = $limit;
		$config["uri_segment"] = 3;
		//$config['num_links'] = 2;
		$config['use_page_numbers'] = false;
		$this->pagination->initialize($config);
		$data["links"] = $this->pagination->create_links();
		$this->content['content']=$this->parser->parse('client/models', $data, true);
		$this->renderclientTemplate();
	}

		public function category()
	{
		$data['page'] = 'shop';
		$data['page_title'] =  ucfirst($this->uri->segment(1)) ." | " . TITLE_CIL;
		$masters=$this->m_location->get_city_location();
		$cnt=0;
		$data['menu'] = 'products';
		foreach($masters as $mast){
			$data['masters'][$mast['location_name']][$cnt]=$mast;
			$cnt++;
		}
		$this->content['content']=$this->parser->parse('client/category', $data, true);
		$this->renderclientTemplate();
	}

	public function single()
	{
		$data['page'] = 'single';
		$data['page_title'] = "Single | " . TITLE_CIL;
		$id=($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		if(isset($id) and !empty($id)){
			$filter = array('model.model_id'=>hex2bin($id));
			$data['model_list']=$this->m_models->model_list('','','',$filter,'',1);
		}
		$this->content['content']=$this->parser->parse('client/single-model', $data, true);
		$this->renderclientTemplate();
	}

	public function addtocart(){
		if(isset($_SESSION['prod_id'])){
			if(!empty($_POST['prod_id']))
				$_SESSION['prod_id'][$_POST['prod_id']]['id'] = $_POST['prod_id'];	
			if(isset($_POST['sub_total']) && isset($_POST['prod_id'])) {
				$_SESSION['prod_id'][$_POST['prod_id']]['sub_total'] = $_POST['sub_total'];	
				$_SESSION['prod_id'][$_POST['prod_id']]['qnty'] = $_POST['qnty'];				
			}			
		}else{
			$_SESSION['prod_id'] = array();
			$_SESSION['prod_id'][$_POST['prod_id']]['id'] = $_POST['prod_id'];
		}
		if(isset($_POST['amount']))
			$_SESSION['amount'] = $_POST['amount'];	
		
		$_SESSION['qty'] = 0;
		foreach($_SESSION['prod_id'] as $prod) {
			if (isset($prod['qnty']))
				$_SESSION['qty'] += $prod['qnty'];
			else
				$_SESSION['qty']++;		
		} 		
				
		if(isset($_SESSION['prod_id']) && !empty($_SESSION['prod_id']))
			echo $_SESSION['qty'];
		else 
			echo 0;	
	}

	public function cart(){
		//session_destroy();
		$data['page'] = 'cart';
		$data['page_title'] = "Cart | " . TITLE_CIL;
		$cont = array();
		if(isset($_SESSION['prod_id'])){
			foreach($_SESSION['prod_id'] as $key => $val){
					if(!in_array(hex2bin($val['id']),$cont))
						array_push($cont,hex2bin($val['id']));	
			}
		}
		$data['model'] = $this->m_frontend->cart_list($cont);
		
		$this->content['content']=$this->parser->parse('client/cart', $data, true);
		$this->renderclientTemplate();		
	}

	function delete_cart(){
		if(isset($_SESSION['prod_id']) && isset($_POST['prod_id']) && !empty($_POST['prod_id'])){
			$sess = $_SESSION['prod_id'];
			unset($sess[$_POST['prod_id']]);
			$sess = array_filter($sess);
			
			if (count($sess) > 0) 
				$_SESSION['prod_id'] = $sess;
			else  {
				unset($_SESSION['prod_id']);
			}
			echo 1;

		}	
	}

	function filter_results(){
		$data = array();$amt = array();
		$id = $_POST['id'];
		unset($_POST['id']);
		foreach ($_POST as $key => $post){
			$arr = explode('_',$key);
			if(count($arr) > 1){
				$data['model_'.$arr[0]][$arr[1]] = $post;
				$data['model_'.$arr[0]] = array_values($data['model_'.$arr[0]]);
			}else{
				
				$amt = explode('-',$post);			
			}
									
		}
		//$this->load->library('pagination');
        //$limit_per_page = 9;
        //$start_index = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		//$id=($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		if(isset($id) and $id != 0){ 
			$filter = array('model.model_category'=> $id);

			//$data['model_list']=$this->m_models->model_list($limit_per_page, $start_index,'',$filter);
			$res['model_list'] = $this->m_frontend->filter_results('', '',$data,$amt,$filter);
			//$total_records = count($this->m_frontend->filter_results($, $start_index,$data,$amt));
		}else{
			$res['model_list']=$this->m_frontend->filter_results('', '',$data,$amt);
        	//$total_records = count($this->m_frontend->filter_results($limit_per_page, $start_index,$data,$amt));
		}
		//echo $this->db->last_query();exit;
		$masters=$this->m_location->get_city_location();
		$cnt=0;
		foreach($masters as $mast){
			$res['masters'][$mast['location_name']][$cnt]=$mast;
			$cnt++;
		}	
		

		$this->content['content']=$this->parser->parse('client/filter_model', $res, true);
		echo $this->content['content'];
	}	

	public function billing() {
		$data['page_title'] = 'Billing' ." | " . TITLE_CIL;
		$data['page'] = 'Billing';
		//echo '<pre>';print_r($_SESSION);exit;
		$this->content['content'] = $this->parser->parse('client/billing', $data, true);
		$this->renderclientTemplate();		
	}

	public function save_billing() {
		if(isset($_POST)) {
			$saved="";
			$today=date("Y-m-d H:i:s");
			$data=array(
				'billing_name'=>trim($_POST['billing_name']),
				'billing_address'=>trim($_POST['billing_address']),
				'billing_phone'=>trim($_POST['billing_phone']),
				'billing_email'=>trim($_POST['billing_email']),
				'billing_notes'=>trim($_POST['billing_notes']),
				'billing_pincode'=>trim($_POST['billing_pincode']),				
				'balance'=> (isset($_SESSION['amount']) ? $_SESSION['amount'] : 0),
				'dt_created'=>$today
			);
			$products = array();
			foreach($_SESSION['prod_id'] as $key => $prod) {
				$post = $this->m_models->get_sel_model(hex2bin($key));
				
				if (!empty($post[0]['model_location']) && @unserialize($post[0]['model_location']) !== false) {
					$post[0]['model_location'] = unserialize($post[0]['model_location']);
					$post[0]['model_location'] = $post[0]['model_location'][0];
					$loc = $this->m_location->get_city_location($post[0]['model_location']);
					$post[0]['model_location'] = $loc[0]['city_name'];
				}	
				$gst = $post[0]['hsn_per']/2;
				$taxable_val = round($prod['sub_total']/(($post[0]['hsn_per']+100)/100),2);
			 	$amt = round(($gst/100) * $taxable_val,2);
				$products[$key] = array(
					'dt_created'=>$today,
					'billing_date'=>date('Y-m-d'),
					'delivery_date'=>date('Y-m-d',strtotime('+7 days')),
					//'model'=>addslashes($post['model_row_'.$i]),
					'model'=>$post[0]['model_name'],
					'location'=>$post[0]['model_location'],
					'quantity'=>$prod['qnty'],
					'price'=>$post[0]['model_price'],
					'taxable_value'=>$taxable_val,
					//'discount'=>$post['discount_row_'.$i],
					'HSN_SAC'=>$post[0]['hsn'],
					'gst_rate'=>$gst,
					'gst_amt'=>$amt,
					'amount'=>$prod['sub_total'],
					//'invoice_advance'=>$post['advance'],
					//'invoice_balance'=>$total - $post['advance'],
				);
			}
			$saved=$this->m_billing->save_newbilling($data,$products,$today,'client_bill');
			if(!empty($saved)) {
				$data['product'] = $products;
				$_SESSION['thankyou'] = $data;
				unset($_SESSION['prod_id']);
				unset($_SESSION['amount']);
				unset($_SESSION['qty']);
				redirect(BASE_URL.'thankyou'); 
			} else {
				$this->session->set_flashdata('error_msg', ERROR_SAVED);
				redirect($_SERVER['HTTP_REFERER']);
			}
		}		
	}

	public function thankyou() {
		$data['page_title'] = 'Thank You' ." | " . TITLE_CIL;
		$data['page'] = 'Thank You';
		$this->content['content'] = $this->parser->parse('client/thankyou', $data, true);
		 
		 $message='';
		 $ci = get_instance();
		 $ci->load->library('email');
		 $config['protocol'] = MAIL_PROTOCOL;
		 $config['smtp_host'] = MAIL_HOST;
		 $config['smtp_port'] = SMTP_PORT;
		 $config['smtp_user'] = SMTP_USERNAME;
		 $config['smtp_pass'] = SMTP_PASSWORD;
 
		 $config['charset'] = "utf-8";
		 $config['mailtype'] = "html";
		 $config['newline'] = "\r\n";
 
		 $sub=FEEDBACK_SUB.ucfirst('contact us');
 
		 $message .= $this->content['content'];
		 $message .= '<br /><br />--<br />';

		 $ci->email->initialize($config);
		 $this->email->clear();
		 $this->email->from(FEEDBACK_FROM, FEEDBACK_FROM_NAME);
		 $this->email->to(FEEDBACK_TO);
		 $this->email->subject($sub);
		 $this->email->message($message);
		 if (!isset($_SESSION['thankyou']))
		 	$this->email->send();

		 $this->renderclientTemplate();		
	}
	public function send_contactus_feedback()
	{
		$name=trim($this->input->post('Name'));
		$address=trim($this->input->post('Message'));
		$contact=trim($this->input->post('Mobile'));
		$email=trim($this->input->post('Email'));

		$message='';
		$ci = get_instance();
		$ci->load->library('email');
		$config['protocol'] = MAIL_PROTOCOL;
		$config['smtp_host'] = MAIL_HOST;
		$config['smtp_port'] = SMTP_PORT;
		$config['smtp_user'] = SMTP_USERNAME;
		$config['smtp_pass'] = SMTP_PASSWORD;

		$config['charset'] = "utf-8";
		$config['mailtype'] = "html";
		$config['newline'] = "\r\n";

		$sub=FEEDBACK_SUB.ucfirst('contact us');

		$message .= '<pre>Name : '.$name.'</pre>';
		$message .= '<pre>Message : '.$address.'</pre>';
		$message .= '<pre>Contact No. : '.$contact.'</pre>';
		$message .= '<pre>Email-id : '.$email.'</pre>';
		$message .= '<br /><br />--<br />';
		$message .= 'This e-mail is sent from a contact-us form';

		$ci->email->initialize($config);
		$this->email->clear();
		$this->email->from(FEEDBACK_FROM, FEEDBACK_FROM_NAME);
		$this->email->to(FEEDBACK_TO);
		$this->email->subject($sub);
		$this->email->message($message);
		if($this->email->send())
			echo "mail sent";
			$this->content['content'] = $this->parser->parse('client/homepage', $data, true);
			$this->renderclientTemplate();	
	}

	function save_newsletter() {
		$save_newsletter['name'] = trim($this->input->post('Name'));
		$save_newsletter['email'] = trim($this->input->post('Email'));
		$save_newsletter['mobile'] = trim($this->input->post('Mobile'));
		$this->m_frontend->save_newsletter($save_newsletter);
		redirect($this->input->post('location'));
	}
	
}
