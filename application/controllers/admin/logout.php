<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');
/**
* dashboard
* @access			public
* @author			Online
* @copyright
* @package
* @since			21-12-2010
* @version
*/

class logout extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
	}

/**
*logout
*/
	public function index()
	{
		$this->session->unset_userdata('admin_id');
		redirect('admin');
	}

}
?>
