<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');
/**
* dashboard
* @access			public
* @author			Online
* @copyright
* @package
* @since			21-12-2010
* @version
*/

class Models extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model(array('m_models','m_location'));
		$admin_user_id=$this->session->userdata('admin_id');
	}

/**
* displays models view page .
* functions used :-
* model_list : fetches model details from db in order specified by user .
* get_model :  fetches model details from db in asc order .
*/
	public function index()
	{
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$ord=($this->uri->segment(3)) ? $this->uri->segment(3) : "default";
		$column=($this->uri->segment(4)) ? $this->uri->segment(4) : "";
		$data['page_title']=MODELS;
		$filter = array();
		if(isset($_POST) and !empty($_POST)){
			$data['model_filter']=1;
			foreach($_POST as $key => $val){
				if($val != '')
				$filter[$key] = $val;
			}
		}
		$data['model_list']=$this->m_models->model_list('','',$ord,$filter,$column);
		$masters=$this->m_location->get_city_location();
		$cnt=0;
		foreach($masters as $mast){
			$data['masters'][$mast['location_id']][$cnt]=$mast;
			$cnt++;
		}
		$data['ord']=$ord;
		$this->content['content']=$this->parser->parse('admin/models.php', $data, true);
		$this->renderTemplate();
	}

/**
* displays add/new model form for editing.
* functions used :-
* get_sel_model : fetches model details from db of particular model id .
*/
	public function model_edit()
	{
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$id=($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		$data['popup_list']=$this->m_models->get_sel_model($id);
		echo json_encode($data['popup_list']);
	}

	public function gallery_edit()
	{
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$id=($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		$page=($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$data['popup_list']=$this->m_models->get_sel_gallery($id,$page);
		echo json_encode($data['popup_list']);
	}
/**
* deletes model details .
* functions used :-
* newmodel_delete : deletes model details from db of particular model id .
*/
	public function newmodel_delete()
	{
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$id=($this->uri->segment(2)) ? $this->uri->segment(2) : "";
		$delete=$this->m_models->newmodel_delete($id);
		$this->generate_sitemap();
		if(!empty($delete))
			$this->session->set_flashdata('success_msg', DELETED);
		else
			$this->session->set_flashdata('error_msg', ERROR_DELETED);
		redirect('admin/model');
	}

	public function delete_gallery(){
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		if(isset($_POST['img_name']) and !empty($_POST['img_name']) or isset($_POST['img_id']) and !empty($_POST['img_id'])){
			$thumb_path=DIR_THUMB.$_POST['img_name'];
			$upld_path=DIR.$_POST['img_name'];
			$gal_path=GALLERY.$_POST['img_name'];
			if(file_exists($thumb_path))
				unlink($thumb_path);
			if(file_exists($upld_path))
				unlink($upld_path);
			if(file_exists($gal_path))
				unlink($gal_path);
			$delete=$this->m_models->delete_gallery($_POST['img_id']);
			if($delete){
				echo json_encode($delete);
			}
		}
	}

	public function save_gallery(){
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$temp=round(microtime(true));
		$today=date("Y-m-d H:i:s");
		if(isset($_FILES['gallery_file']) and !empty($_FILES['gallery_file'])){
			$extension=pathinfo($_FILES['gallery_file']['name'], PATHINFO_EXTENSION);
			$path=DIR.$temp.'.'.$extension;;
			move_uploaded_file($_FILES['gallery_file']["tmp_name"], $path);
			for($k=0;$k<=1;$k++){
				if($k==0){
					$thumb_width=115.78;
					$thumb_height=50.22;
					$path1=DIR_THUMB.$temp.'.'.$extension;
				}else if($k>0){
					$thumb_width=392;
					$thumb_height=156;
					$path1=GALLERY.$temp.'.'.$extension;
				}
				list($width, $height)=getimagesize($path);
				$new_img=imagecreatetruecolor($thumb_width, $thumb_height);
				switch($extension) {
				case "gif":
					$image=imagecreatefromgif($path);
					break;
				case 'jpeg':
				case "jpg":
					$image=imagecreatefromjpeg($path);
					break;
				case "png":
					$image=imagecreatefrompng($path);
					break;
				}
				imagecopyresampled($new_img, $image, 0, 0, 0, 0, $thumb_width, $thumb_height, $width, $height);
				imagejpeg($new_img, $path1, 100);
			}
		}
		if(empty($_POST['img_id'])){
			if(isset($_FILES['gallery_file']) and !empty($_FILES['gallery_file']))
				$gallery=$temp.'.'.$extension;
			$data2=array(
				'model_id'=>$_POST['model_id'],
				'gallery_title'=>ucfirst(trim($_POST['title'])),
				'gallery_image'=>$gallery,
				'dt_created'=>trim($today),
				'page_name'=>$_POST['page_name']
			);
			if(isset($_FILES['gallery_file']) and !empty($_FILES['gallery_file']))
				$saved=$this->m_models->save_gallery($data2);
		}else if(isset($_POST['img_id']) and !empty($_POST['img_id'])){
			if(isset($_FILES['gallery_file']) and !empty($_FILES['gallery_file']))
				$gallery=$temp.'.'.$extension;
			else
				$gallery=$_POST['gallery_image'];
			$data2=array(
				'gallery_title'=>ucfirst(trim($_POST['title'])),
				'gallery_image'=>$gallery,
			);
			$saved=$this->m_models->update_gallery($data2,$_POST['img_id']);
		}
		echo json_encode($saved);
	}
/**
* saves/updates model details .
* functions used :-
* update_newmodel : updates model details from db of particular service id .
* save_newmodel :  saves model details into db .
*/
	public function save_newmodel_files()
	{ 
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		set_time_limit(0);
		$temp=round(microtime(true));
		if(!empty($_FILES["modelfile"]["name"])) {
			$extension=pathinfo($_FILES["modelfile"]['name'], PATHINFO_EXTENSION);
			$target=DIR .$temp. '.'.$extension;
			move_uploaded_file($_FILES["modelfile"]["tmp_name"], $target);
			$target2=DIR_THUMB .$temp. '.'.$extension;
			list($width, $height)=getimagesize($target);
			$thumb_width=140;
			$thumb_height=80;
			$new_img=imagecreatetruecolor($thumb_width, $thumb_height);
			switch($extension) {
			case "gif":
				$image=imagecreatefromgif($target);
				break;
			case 'jpeg':
			case "jpg":
				$image=imagecreatefromjpeg($target);
				break;
			case "png":
				$image=imagecreatefrompng($target);
				break;
			}
			imagecopyresampled($new_img, $image, 0, 0, 0, 0, $thumb_width, $thumb_height, $width, $height);
			imagejpeg($new_img, $target2, 100);
			$data['modelfile']=$temp.'.'.$extension;
		}
		if(isset($_FILES["brochure"]["name"]) and !empty($_FILES["brochure"]["name"])) {
			$extension=pathinfo($_FILES["brochure"]['name'], PATHINFO_EXTENSION);
			$target=DIR_BROCHURE .$temp. '.'.$extension;
			move_uploaded_file($_FILES["brochure"]["tmp_name"], $target);
			$data['brochure']=$temp.'.'.$extension;
		}
		echo json_encode($data);
	}

	public function save_newmodel()
	{ 
		$admin_user_id = $this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$model_name="";
		$model_name_xml="";
		$price="";
		$image_name="";
		$caption="";
		$features="";
		$overview="";
		$details="";
		$display="";
		$title="";
		$keywords="";
		$seodetails="";
		$brochure="";
		$model_quantity=0;
		if(isset($_POST)) {
			$today=date("Y-m-d H:i:s");
			if(!empty($_POST['model_name']))
				$model_name=$_POST['model_name'];
			if(!empty($_POST['model_name_xml']))
				$model_name_xml=$_POST['model_name_xml'];
			if(!empty($_POST['model_quantity']))
				$model_quantity=$_POST['model_quantity'];
			if(!empty($_POST['model_price']))
				$price=$_POST['model_price'];
			if(!empty($_POST['model_image'])){
				if(!empty($_POST['img_temp']))
					$image_name=$_POST['img_temp'];
				else
					$image_name=$_POST['model_image'];
			}
			if(!empty($_POST['model_caption']))
				$caption=$_POST['model_caption'];
			if(!empty($_POST['model_features']))
				$features=$_POST['model_features'];
			if(!empty($_POST['model_overview']))
				$overview=$_POST['model_overview'];
			if(!empty($_POST['model_details']))
				$details=$_POST['model_details'];
			if(!empty($_POST['is_display']))
				$display=$_POST['is_display'];
			else
				$display=0;
			if(!empty($_POST['seo_title']))
				$title=$_POST['seo_title'];
			if(!empty($_POST['seo_keyword']))
				$keywords=$_POST['seo_keyword'];
			if(!empty($_POST['seo_details']))
				$seodetails=$_POST['seo_details'];
			if(!empty($_POST['brochure_name'])){
				if(isset($_POST['bro_temp']))
					$brochure=$_POST['bro_temp'];
				else
					$brochure=$_POST['brochure_name'];
			}
			$data=array(
				'model_name'=>trim($model_name),
				'model_image'=>trim($image_name),
				'brochure'=>trim($brochure),
				'model_caption'=>trim($caption),
				'model_quantity'=>trim($model_quantity),
				'model_price'=>trim($price),				
				'model_features'=>trim($features),
				'model_overview'=>trim($overview),
				'model_details'=>trim($details),
				'is_display'=>trim($display),
				'seo_title'=>trim($title),
				'seo_keyword'=>trim($keywords),
				'seo_details'=>trim($seodetails),
				'dt_created'=>trim($today)
			);
			foreach($_POST as $k => $val){
				$data[$k]=$val;
				if($k=='model_price'){
					break;
				}
			}

			if(isset($_POST['model_thickness_text']) and !empty($_POST['model_thickness_text'])){
					$data1=array(
							'location_id'=>4,
							'city_name'=>$_POST['model_thickness_text'],
							'city_email_code'=>'',
							'company'=>'',
							'order_number'=>'',
							'dt_created'=>date("Y-m-d H:i:s"),
					);
					$data['model_thickness_text'] = ($_POST['model_thickness_text']);
					$this->m_location->save_city($data1);

			}
			$data['model_location'] = $data['model_location'];
			$data['model_feel'] = $data['model_feel'];
			$data['model_make'] = $data['model_make'];		
			if($_POST['hidden']!="") {
				unset($data['dt_created']);

				$saved=$this->m_models->update_newmodel($data, $_POST['hidden']);
			//if($model_name_xml!=$model_name)
					//$this->generate_sitemap();
				if(!empty($saved))
					$this->session->set_flashdata('success_msg', UPDATED);
				else
					$this->session->set_flashdata('error_msg', ERROR_UPDATED);
				}	 else {

				$saved=$this->m_models->save_newmodel($data,$_POST);
			//	$this->generate_sitemap();
				if(!empty($saved))
    				$this->session->set_flashdata('success_msg', SAVED);
				else
					$this->session->set_flashdata('error_msg', ERROR_SAVED);
				}
				
  		}
		redirect('admin/model');
	}

	function check_model_name(){
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		if(isset($_POST['name']) and !empty($_POST['name'])){
			$data['model_name'] = $_POST['name'][0];
			$data['model_range'] = $_POST['name'][1];
			$result=$this->m_models->check_model_name($data);
			echo json_encode($result);
		}
	}

	function delfiles(){
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		if(isset($_POST['img_path']) and !empty($_POST['img_path'])){
			$thumb_path=DIR_THUMB.$_POST['img_path'];
			$upld_path=DIR.$_POST['img_path'];
			if(file_exists($thumb_path))
				unlink($thumb_path);
			if(file_exists($upld_path))
				unlink($upld_path);
		}
		if(isset($_POST['bro_path']) and !empty($_POST['bro_path'])){
			$upld_path=DIR_BROCHURE.$_POST['bro_path'];
			if(file_exists($upld_path))
				unlink($upld_path);
		}
	}

	function get_sorted_pages(){
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$cat=unserialize(CATEGORY);
		foreach($cat as $k => $cat){
			$data[$cat]=$this->m_models->get_sorted_pages($k);
		}
		echo json_encode($data);
	}

	function change_order(){
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$data=$_POST['models'];
		$cat=unserialize(CATEGORY);
		foreach($cat as $k => $cat){
			$count=count($data[$cat]);
			$status=$this->m_models->change_order($count,$data[$cat]);
		}
	}
	
	function get_category(){
		$data="";
		if(!empty($_POST['company']))
			$data=$this->m_models->get_category($_POST['company']);
		echo json_encode($data);
	}

	function get_sizes_groupwise(){
		$data="";
		if(!empty($_POST['group']))
			$data=$this->m_models->get_sizes_groupwise($_POST['group']);
		echo json_encode($data);
	}

	function check_avl_bal(){
		$status=$this->m_models->check_avl_bal($_POST);
		echo json_encode($status);
	}

	function get_master_data() {
		$data="";
		$data=$this->m_models->get_master_data($_POST);
		$masters=$this->m_location->get_city_location();
		$cnt=0;
		$options = array("Thickness","Range","Feel","Make","HSN_SAC");
		foreach($masters as $mast){
			if(in_array($mast['location_name'],$options)) {
				$data['masters'][$mast['location_name']][$cnt]=$mast;
				$cnt++;
			}
		}
		echo json_encode($data);		
	}
}
?>
