<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');
/**
* dashboard
* @access			public
* @author			Online
* @copyright
* @package
* @since			21-12-2010
* @version
*/

class Location extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_location');
		$admin_user_id=$this->session->userdata('admin_id');
	}

/**
* displays location view page .
* functions used :-
* get_location : fetches location(state) details from db.
* get_city :  fetches city details from db.
*/
	public function index()
	{
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$data['page_title']=PG_TITLE_LOCATION;
		$data['location_list']=$this->m_location->get_location();
		$data['company']=$this->m_location->get_city($master=5);
		$data['thickness']=$this->m_location->get_city($master=4);
		$data['feel']=$this->m_location->get_city($master=12);
		$data['range']=$this->m_location->get_city($master=7);
		$data['hsn']=$this->m_location->get_city($master=10);
		$data['make']=$this->m_location->get_city($master=11);
		$data['group']=$this->m_location->get_city($master=13);
		foreach($data['location_list'] as $location) {
			$data['loc'][$location['location_id'].'-'.$location['location_name']]=$this->m_location->get_city($location['location_id']);
		}
		$this->content['content']=$this->parser->parse('admin/location_list.php', $data, true);
		$this->renderTemplate();
	}

/**
* displays add/new location form for editing.
* functions used :-
* get_city_location : fetches state and cities details from db of particular location id .
*/
	public function location_edit()
	{
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$id=($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		$data['popup_list']=$this->m_location->get_city_location($id);
		$data['popup_list'][0]['thickness'] = ($data['popup_list'][0]['thickness'] !='' && $data['popup_list'][0]['thickness'] != '0'  ? unserialize($data['popup_list'][0]['thickness']) : '');
		echo json_encode($data['popup_list']);
	}

/**
* saves/updates location details .
* functions used :-
* save_location : inserts location(state) details into db.
* save_city : inserts city details into db of particular location id .
* update_city :  updates city details from db of particular location id .
*/
	public function save_location()
	{
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$today=date("Y-m-d H:i:s");
		if(isset($_POST['status']) and !empty($_POST['status'])){
			$circle=array(
				'location_name'=>ucfirst($_POST['location']),
				'dt_created'=>$today
			);

			if($_POST['status']=="add"){
				$max_orderno=$this->m_location->get_max_orderno('location');
				$circle['order_number']=$max_orderno['max']+1;
				$status=$this->m_location->save_circle($circle);
				if(!empty($status)){
					$_POST['location'] = 'model_'.strtolower($_POST['location']);
					$this->m_location->create_fields($_POST['location']);
					$this->session->set_flashdata('success_msg', SAVED);
				}else
					$this->session->set_flashdata('error_msg', ERROR_SAVED);
			}else if($_POST['status']=="edit" or $_POST['status']=="edit_location"){
				unset($circle['dt_created']);
				$status=$this->m_location->update_circle($circle,$_POST['hidden_loc']);
				$this->m_location->modify_fields($_POST['hdnlocation'],$_POST['location']);
				if($_POST['status']=="edit_location"){
					$i=$_POST['hidden'];
					$max_orderno=$this->m_location->get_max_orderno('city',$_POST['hidden_loc']);
					for($j=0; $j<$i; $j++) {
						$max_orderno['max']++;
						$data1=array(
							'location_id'=>$_POST['hidden_loc'],
							'city_name'=>ucfirst($_POST['city' . $j]),
							'city_email_code'=>$_POST['email' . $j],
							'company'=>((isset($_POST['company']) && !empty($_POST['company'])) ? $_POST['company'] : 0),
							'order_number'=>$max_orderno['max'],
							'dt_created'=>$today
						);
						$this->m_location->save_city($data1);
					}
				}
				if(!empty($status))
					$this->session->set_flashdata('success_msg', UPDATED);
				else
					$this->session->set_flashdata('error_msg', ERROR_UPDATED);
			}
		}else{
			if(isset($_POST)) {
				$i=$_POST['hidden'];
				$city_id=$_POST['cit_id'];
				$loc_id=$_POST['loc_id'];
				if(isset($city_id) and isset($loc_id) and !empty($city_id) and !empty($loc_id)) {
					$data2=array(
						'city_name'=>ucfirst($_POST['city0']),
						'city_email_code'=>$_POST['email0'],
						'company'=>((isset($_POST['company']) and !empty($_POST['company'])) ? $_POST['company'] : 0),
						'thickness'=>((isset($_POST['thickness']) && !empty($_POST['thickness'])) ? serialize($_POST['thickness']) : 0),
						'make'=>((isset($_POST['make']) && !empty($_POST['make'])) ? $_POST['make'] : 0),
						'range'=>((isset($_POST['range']) && !empty($_POST['range'])) ? $_POST['range'] : 0),
						'feel'=>((isset($_POST['feel']) && !empty($_POST['feel'])) ? $_POST['feel'] : 0),
						'hsn_sac'=>((isset($_POST['hsn_sac']) && !empty($_POST['hsn_sac'])) ? $_POST['hsn_sac'] : 0),
						'warranty'=>((isset($_POST['warranty']) && !empty($_POST['warranty'])) ? $_POST['warranty'] : 0),
						'features'=>((isset($_POST['features']) && !empty($_POST['features'])) ? $_POST['features'] : 0),
						'description'=>((isset($_POST['description']) && !empty($_POST['description'])) ? $_POST['description'] : 0),
						'group'=>((isset($_POST['group']) && !empty($_POST['group'])) ? $_POST['group'] : 0),
					);
					if($i>1) {
						for($j=1; $j<$i; $j++) {
							$data3=array(
								'location_id'=>$loc_id,
								'city_name'=>ucfirst($_POST['city' . $j]),
								'city_email_code'=>$_POST['email' . $j],
								'company'=>((isset($_POST['company']) and !empty($_POST['company'])) ? $_POST['company'] : 0),
								'dt_created'=>$today
							);
							$this->m_location->save_city($data3);
						}
					}
					$updated=$this->m_location->update_city($data2, $city_id);
					if(!empty($updated))
						$this->session->set_flashdata('success_msg', UPDATED);
					else
						$this->session->set_flashdata('error_msg', ERROR_UPDATED);
				} else {
					$max_orderno=$this->m_location->get_max_orderno('city',$_POST['location_id']);
					for($j=0; $j<$i; $j++) {
						$max_orderno['max']++;
						$data1=array(
							'location_id'=>$_POST['location_id'],
							'city_name'=>ucfirst($_POST['city' . $j]),
							'city_email_code'=>$_POST['email' . $j],
							'company'=>((isset($_POST['company']) && !empty($_POST['company'])) ? $_POST['company'] : 0),
							'thickness'=>((isset($_POST['thickness']) && !empty($_POST['thickness'])) ? serialize($_POST['thickness']) : 0),
							'make'=>((isset($_POST['make']) && !empty($_POST['make'])) ? $_POST['make'] : 0),
							'range'=>((isset($_POST['range']) && !empty($_POST['range'])) ? $_POST['range'] : 0),
							'feel'=>((isset($_POST['feel']) && !empty($_POST['feel'])) ? $_POST['feel'] : 0),
							'hsn_sac'=>((isset($_POST['hsn_sac']) && !empty($_POST['hsn_sac'])) ? $_POST['hsn_sac'] : 0),
							'warranty'=>((isset($_POST['warranty']) && !empty($_POST['warranty'])) ? $_POST['warranty'] : 0),
							'features'=>((isset($_POST['features']) && !empty($_POST['features'])) ? $_POST['features'] : 0),
							'description'=>((isset($_POST['description']) && !empty($_POST['description'])) ? $_POST['description'] : 0),
							'group'=>((isset($_POST['group']) && !empty($_POST['group'])) ? $_POST['group'] : 0),
							'order_number'=>$max_orderno['max'],
							'dt_created'=>$today
						);
						$saved=$this->m_location->save_city($data1);
					}
					if(!empty($saved))
						$this->session->set_flashdata('success_msg', SAVED);
					else
						$this->session->set_flashdata('error_msg', ERROR_SAVED);
				}
			}
		}
		redirect('admin/masters');
	}

/**
* deletes location details .
* functions used :-
* delete_city : deletes city details from db of particular location id .
* get_city : checks if any city details exists into db of particular location id .
* delete_location : deletes location(state) details from db.
*/
	public function location_delete()
	{
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$city_id=($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		$loc_id=($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$delete=$this->m_location->delete_city($city_id);
		$exist=$this->m_location->get_city($loc_id);
		if(!empty($delete))
			$this->session->set_flashdata('success_msg', DELETED);
		else
			$this->session->set_flashdata('error_msg', ERROR_DELETED);
		redirect('admin/masters');
	}

	public function circle_delete(){
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$loc_id=($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		$loc_name=($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
		$delete=$this->m_location->delete_location($loc_id);
		if(!empty($delete)){
			$this->m_location->delete_fields(lcfirst($loc_name));
			$this->session->set_flashdata('success_msg', DELETED);
		}else
			$this->session->set_flashdata('error_msg', ERROR_DELETED);
		redirect('admin/masters');
	}

	public function check_circle_name (){
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		if(isset($_POST['name']) and !empty($_POST['name'])){
			$status=$this->m_location->check_circle_name($_POST['name']);
			echo json_encode($status);
		}
	}

	public function check_location_name (){
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		if(isset($_POST['name']) and !empty($_POST['name']) and isset($_POST['location_id']) and !empty($_POST['location_id']) ){
			$status=$this->m_location->check_location_name($_POST['name'],$_POST['location_id']);
			echo json_encode($status);
		}
	}

	function change_order(){
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		if(isset($_POST['location']) and !empty($_POST['location'])){
			$count=count($_POST['location']);
			$data=$_POST['location'];
			$status=$this->m_location->change_order($count,$data,'city');
		}
		if(isset($_POST['circle']) and !empty($_POST['circle'])){
			$count=count($_POST['circle']);
			$data=$_POST['circle'];
			$status=$this->m_location->change_order($count,$data,'location');
		}
		echo json_encode($status);
	}
}
?>
