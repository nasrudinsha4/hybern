<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');
/**
* dashboard
* @access			public
* @author			Online
* @copyright
* @package
* @since			21-12-2010
* @version
*/

class Billing extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_billing');
		$this->load->model('m_location');
		$admin_user_id=$this->session->userdata('admin_id');
	}

/**
* displays billing view page.
* functions used :-
* billing_list : fetches billing details from db in order specified by user .
* billing : fetches billing details from db in asc order.
*/
	public function index()
	{
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$ord=($this->uri->segment(3)) ? $this->uri->segment(3) : "default";
		$data['page_title']=PG_TITLE_BILLING;
		if(isset($_POST) and !empty($_POST)){
			$data['model_filter']=1;
		}
		$data['billing_list']=$this->m_billing->billing_list($ord,$_POST);
		$data['model_list']=$this->m_models->model_list();
		$data['location_list']=$this->m_location->get_city(6);
		//echo '<pre>';print_r($data['model_list']);exit;
		$cnt=0;
/*		foreach($masters as $mast){
			$data['masters'][$mast['location_id']][$cnt]=$mast;
			$cnt++;
		}*/
		$data['ord']=$ord;
		$this->content['content']=$this->parser->parse('admin/billing.php', $data, true);
		$this->renderTemplate();
	}

	public function get_location(){
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$data['location']=$this->m_location->get_location();
		echo json_encode($data['location']);
	}

/**
* displays list of cities of respected location(state) in select box.
* functions used :-
* get_city : fetches cities of particuler location id .
*/
	function findcity()
	{
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$id=$this->input->post('id');
		$data['city']=$this->m_location->get_city($id);
		echo json_encode($data['city']);
	}
/**
* saves/updates billing details .
* functions used :-
* update_newbilling : updates billing details from db of particular billing id .
* save_newbilling :  saves billing details into db .
*/

	public function save_billing()
	{
		//echo '<pre>';print_r($_POST);exit;
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		if(isset($_POST)) {
			$saved="";
			$today=date("Y-m-d H:i:s");
			$data=array(
				'billing_name'=>trim($_POST['billing_name']),
				'billing_address'=>trim($_POST['billing_address']),
				'billing_phone'=>trim($_POST['billing_phone']),
				'billing_email'=>trim($_POST['billing_email']),
				'customer_GSTIN'=>trim($_POST['customer_GSTIN']),
				'billing_notes'=>trim($_POST['billing_notes']),
				'balance'=>$_POST['balance'],
				'dt_created'=>$today
			);
			if($_POST['hidden']!="") {
				unset($data['dt_created']);
				$saved=$this->m_billing->update_newbilling($data,$_POST,$today,$_POST['hidden']);
				if(!empty($saved))
					$this->session->set_flashdata('success_msg', UPDATED);
				else
					$this->session->set_flashdata('error_msg', ERROR_UPDATED);
			} else {
				$saved=$this->m_billing->save_newbilling($data,$_POST,$today);
				if(!empty($saved))
					$this->session->set_flashdata('success_msg', SAVED);
				else
					$this->session->set_flashdata('error_msg', ERROR_SAVED);
			}
			redirect('admin/billing');
		}
	}

/**
* displays add/new billing form for editing.
* functions used :-
* billing_edit : fetches billing details from db of particular billing id .
*/
	function billing_edit()
	{
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$id=($this->uri->segment(2)) ? $this->uri->segment(2) : "";
		$data['billing_list']=$this->m_billing->billing_edit($id);
		echo json_encode($data['billing_list']);
	}

/**
* deletes billing details .
* functions used :-
* billing_delete : deletes billing details from db of particular billing id .
*/
	function billing_delete()
	{
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$id=($this->uri->segment(2)) ? $this->uri->segment(2) : "";
		$delete=$this->m_billing->billing_delete($id);
		if(!empty($delete))
			$this->session->set_flashdata('success_msg', DELETED);
		else
			$this->session->set_flashdata('error_msg', ERROR_DELETED);
		redirect('admin/billing');
	}

	function validate_billing (){
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		if($_POST['location_id']){
			$res="";
			$res=$this->m_billing->validate_billing($_POST['location_id']);
			echo json_encode($res);
		}
	}
}
?>
