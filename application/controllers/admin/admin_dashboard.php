<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');
/**
* dashboard
* @access			public
* @author			Online
* @copyright
* @package
* @since			21-12-2010
* @version
*/

class Admin_dashboard extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$admin_user_id=$this->session->userdata('admin_id');
		$this->load->model('m_homepage');
		$this->load->model('m_location');
// 		/** Include PHPExcel */
// 		require_once APPPATH.'libraries/PHPExcel.php';
	}

/**
* displays dashboard page.
*/
	public function index()
	{
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$data['page_title']="DASHBOARD";
		$stats_date=$this->m_homepage->get_stats_date();
		if(isset($stats_date['date_created']) and !empty($stats_date['date_created']))
		$data['stats_date']=date('jS F Y', strtotime($stats_date['date_created']));
		$this->content['content']=$this->parser->parse('admin/welcome.php', $data, true);
		$this->renderTemplate();
	}

	public function statistic_report()
	{
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		if(isset($_POST['daterange']) and !empty($_POST['daterange'])){
			$dates=explode('/',$_POST['daterange']);
			$cond=array(
			'date(s.date_created) >='=>$dates[0],//from date
			'date(s.date_created) <='=>$dates[1],// to date
			);
			$report=$this->m_homepage->get_dashboard_data($cond);
			$form_type=unserialize(FORM_TYPE);
			$final_report=array();
			for($i=0;$i<count($report);$i++){
				foreach($form_type as $key => $type){
					if($type==$report[$i]['form_type']){
						$final_report[$report[$i]['city_id']][$key]=$report[$i]['total'];
						$final_report[$report[$i]['city_id']]['location']=$report[$i]['location'];
						$final_report[$report[$i]['city_id']]['city']=$report[$i]['city'];
					}
				}
			}
			$final_report=array_values($final_report);
			echo json_encode($final_report);
		}
	}

/**
* displays home_page form.
* functions used :-
* get_hompage_data : fetches homepage details from db to dislay it in home page form
*/
	public function home_page()
	{
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$data['page_title']=PG_TITLE_HOME;
		$data['list']=$this->m_homepage->get_hompage_data();
		$this->content['content']=$this->parser->parse('admin/home_page.php', $data, true);
		$this->renderTemplate();
	}

/**
* saves home_page form data.
* functions used :-
* homepage_update : updates homepage details from db .
* homepage_add :  inserts homepage details into db
*/
	public function add_homepage()
	{
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$today=date("Y-m-d H:i:s");
		if(isset($_POST)) {
			if(!empty($_FILES['fileupload'])) {
				$name=$_FILES['fileupload']['name'];
				$temp=$_FILES['fileupload']['tmp_name'];
				$target=DIR . $name;
				move_uploaded_file($temp, $target);
				$target2=DIR_THUMB . $_FILES["fileupload"]["name"];
				list($width, $height)=getimagesize($target);
				$thumb_width=170;
				$thumb_height=65;
				$extension=pathinfo($_FILES["fileupload"]['name'], PATHINFO_EXTENSION);
				$new_img=imagecreatetruecolor($thumb_width, $thumb_height);
				switch($extension) {
				case "gif":
					$image=imagecreatefromgif($target);
					break;
				case 'jpeg':
				case "jpg":
					$image=imagecreatefromjpeg($target);
					break;
				case "png":
					$image=imagecreatefrompng($target);
					break;
				}
				imagecopyresampled($new_img, $image, 0, 0, 0, 0, $thumb_width, $thumb_height, $width, $height);
				imagejpeg($new_img, $target2, 100);
			}
			$data=array(
				'banner_caption'=>trim($this->input->post('txt_caption')),
				'banner_tagline'=>trim($this->input->post('txt_tagline')),
				'banner_image'=>trim($this->input->post('filename')),
				'seo_title'=>trim($this->input->post('txt_title')),
				'seo_keyword'=>trim($this->input->post('txt_keyword')),
				'seo_details'=>trim($this->input->post('txt_details')),
				'dt_created'=>$today
			);
			if($_POST['home_page_id']!="") {
				unset($data['dt_created']);
				$id=$this->m_homepage->homepage_update($data, $_POST['home_page_id']);
				if(!empty($id))
					$this->session->set_flashdata('success_msg', UPDATED);
				else
					$this->session->set_flashdata('error_msg', ERROR_UPDATED);
			} else {
				$id=$this->m_homepage->homepage_add($data);
				if(!empty($id))
					$this->session->set_flashdata('success_msg', SAVED);
				else
					$this->session->set_flashdata('error_msg', ERROR_SAVED);
			}
		}
		redirect('admin/home-page');
	}

	function delete_statistic_report(){
		if(isset($_POST) and !empty($_POST)){
			$to =date('Y-m-d', strtotime(str_replace('/','-',$_POST['todate'])));
			$cond= array(
			'date(date_created) <=' => $to,
			);
			$result=$this->m_homepage->delete_statistic_report($cond);
			if(isset($result['date_created']) and !empty($result['date_created'])){
				$result['date_created']=date('jS F Y', strtotime($result['date_created']));
				$result['start_date']=date('d/M/Y', strtotime($result['date_created']));
				$result['end_date']=date('d/M/Y');
				echo json_encode($result);
			}else
				echo 0;
		}
	}
}
?>
