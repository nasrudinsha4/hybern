<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');
/**
* dashboard
* @access			public
* @author			Online
* @copyright
* @package
* @since			21-12-2010
* @version
*/

class Services extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('m_services');
		$admin_user_id=$this->session->userdata('admin_id');
	}

/**
* displays services view page.
* functions used :-
* service : fetches services details from db in order specified by user .
* service_list : fetches services details from db in asc order.
*/
	public function index()
	{
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$page_id="";
		if(isset($_POST['pages_id']) and !empty($_POST['pages_id']))
			$page_id=$_POST['pages_id'];
		else
			$page_id=1;
		$ord=($this->uri->segment(3)) ? $this->uri->segment(3) : "default";
		if($ord!="default")
			$page_id=$this->uri->segment(4);
		$data['page_title']=PAGES;
		$data['ids']=$this->m_services->service_list($ord,$page_id);
		$data['ord']=$ord;
		$data['page_id']=$page_id;
		$this->content['content']=$this->parser->parse('admin/services.php', $data, true);
		$this->renderTemplate();
	}

/**
* displays add/new services form for editing.
* functions used :-
* get_services : fetches services details from db of particular service id .
*/
	public function get_services()
	{
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$id=($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
		$data['popup_list']=$this->m_services->get_services($id);
		echo json_encode($data['popup_list']);
	}

/**
* deletes services details .
* functions used :-
* service_delete : deletes services details from db of particular service id .
*/
	public function service_delete()
	{
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$id=($this->uri->segment(2)) ? $this->uri->segment(2) : "";
		$delete=$this->m_services->service_delete($id);
		$this->generate_sitemap();
		if(!empty($delete))
			$this->session->set_flashdata('success_msg', DELETED);
		else
			$this->session->set_flashdata('error_msg', ERROR_DELETED);
		redirect('admin/pages');
	}

/**
* saves/updates services details .
* functions used :-
* update_services : updates services details from db of particular service id .
* save_services :  saves services details into db .
*/
	public function save_services()
	{
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$temp=round(microtime(true));
		$title="";
		$title_xml="";
		$image="";
		$caption="";
		$logo="";
		$textarea4="";
		$textarea5="";
		$email="";
		$subject="";
		$seoinp1="";
		$seoinp2="";
		$description="";
		$is_display="";
		if(isset($_POST)) {
			$today=date("Y-m-d H:i:s");
			if(!empty($_POST['title']))
				$title=$_POST['title'];
			if(!empty($_POST['title_xml']))
				$title_xml=$_POST['title_xml'];
			if(!empty($_POST['image'])){
				if(!empty($_FILES["imageupload"]["name"]))
					$image=$temp.$_POST['image'];
				else
					$image=$_POST['image'];
			}
			if(!empty($_POST['caption']))
				$caption=$_POST['caption'];
			if(!empty($_POST['logo'])){
				if(!empty($_FILES["logoupload"]["name"]))
					$logo=$temp.$_POST['logo'];
				else
					$logo=$_POST['logo'];
			}
			if(!empty($_POST['textarea4']))
				$textarea4=$_POST['textarea4'];
			if(!empty($_POST['textarea5']))
				$textarea5=$_POST['textarea5'];
			if(!empty($_POST['email']))
				$email=$_POST['email'];
			if(!empty($_POST['subject']))
				$subject=$_POST['subject'];
			if(!empty($_POST['seoinp1']))
				$seoinp1=$_POST['seoinp1'];
			if(!empty($_POST['seoinp2']))
				$seoinp2=$_POST['seoinp2'];
			if(!empty($_POST['description']))
				$description=$_POST['description'];
			if(!empty($_POST['is_display']))
				$is_display=$_POST['is_display'];
			if(isset($_POST['bigimage']) and $_POST['bigimage']==1){
				$logo=null;
			}
			$data=array(
				'service_title'=>trim($title),
				'service_image'=>trim($image),
				'service_caption'=>trim($caption),
				'service_icon'=>trim($logo),
				'service_overview'=>trim($textarea4),
				'service_details'=>trim($textarea5),
				'enquiry_form_email'=>trim($email),
				'enquiry_form_subject'=>trim($subject),
				'seo_title'=>trim($seoinp1),
				'seo_keyword'=>trim($seoinp2),
				'seo_details'=>trim($description),
				'is_display'=>trim($is_display),
				'dt_created'=>trim($today)
			);
			if(!empty($_FILES["imageupload"]["name"])) {
				$target=DIR .$temp. ($_FILES["imageupload"]["name"]);
				move_uploaded_file($_FILES["imageupload"]["tmp_name"], $target);
				$target2=DIR_THUMB .$temp. $_FILES["imageupload"]["name"];
				list($width, $height)=getimagesize($target);
				if(isset($_POST['bigimage']) and $_POST['bigimage']==1){
					$thumb_width=184.29;
					$thumb_height=38.57;
				}else{
					$thumb_width=131.43;
					$thumb_height=38.57;
				}
				$extension=pathinfo($_FILES["imageupload"]['name'], PATHINFO_EXTENSION);
				$new_img=imagecreatetruecolor($thumb_width, $thumb_height);
				switch($extension) {
				case "gif":
					$image=imagecreatefromgif($target);
					break;
				case 'jpeg':
				case "jpg":
					$image=imagecreatefromjpeg($target);
					break;
				case "png":
					$image=imagecreatefrompng($target);
					break;
				}
				imagecopyresampled($new_img, $image, 0, 0, 0, 0, $thumb_width, $thumb_height, $width, $height);
				imagejpeg($new_img, $target2, 100);
			}
			if(!isset($_POST['bigimage'])){
				if(!empty($_FILES["logoupload"]["name"])) {
					$target=DIR.$temp.($_FILES["logoupload"]["name"]);
					move_uploaded_file($_FILES["logoupload"]["tmp_name"], $target);
				}
			}
			if($_POST['hidden']!="") {
				unset($data['dt_created']);
				$saved=$this->m_services->update_services($data, $_POST['hidden']);
				if($title_xml!=$title)
					$this->generate_sitemap();
				if(!empty($saved))
					$this->session->set_flashdata('success_msg', UPDATED);
				else
					$this->session->set_flashdata('error_msg', ERROR_UPDATED);
			} else {
				$saved=$this->m_services->save_services($data);
				$this->generate_sitemap();
				if(!empty($saved))
					$this->session->set_flashdata('success_msg', SAVED);
				else
					$this->session->set_flashdata('error_msg', ERROR_SAVED);
			}
			redirect('admin/pages');
		}
	}

	function get_sorted_pages(){
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		$data['menu']=$this->m_services->get_sorted_menu();
		$data['sales']=$this->m_services->get_sorted_sales();
		$data['insurance']=$this->m_services->get_sorted_insurance();
		$data['footer']=$this->m_services->get_sorted_footer();
		echo json_encode($data);
	}

	function change_order(){
		$admin_user_id=$this->session->userdata('admin_id');
		if(empty($admin_user_id))
			redirect('logout');
		if(isset($_POST['menus']) and !empty($_POST['menus'])){
			$count=count($_POST['menus']);
			$data=$_POST['menus'];
			$status=$this->m_services->change_order($count,$data);
		}
		if(isset($_POST['sales']) and !empty($_POST['sales'])){
			$count=count($_POST['sales']);
			$data=$_POST['sales'];
			$status=$this->m_services->change_order($count,$data);
		}
		if(isset($_POST['insurance']) and !empty($_POST['insurance'])){
			$count=count($_POST['insurance']);
			$data=$_POST['insurance'];
			$status=$this->m_services->change_order($count,$data);
		}
		if(isset($_POST['footer']) and !empty($_POST['footer'])){
			$count=count($_POST['footer']);
			$data=$_POST['footer'];
			$status=$this->m_services->change_order($count,$data);
		}
		echo json_encode($status);
	}
}
?>
