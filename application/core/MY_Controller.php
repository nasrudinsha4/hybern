<?php
class MY_Controller extends CI_Controller {
	function __construct(){
		parent::__construct();
		ini_set('memory_limit', '1024M');
	}

	function renderTemplate(){
		$admin_id=$this->session->userdata('admin_id');
		if(empty($admin_id))
			redirect('logout');
		$data['admin_user_name'] = $this->session->userdata('username');
		$this->content['header'] = $this->parser->parse('layout/header.php',$data,true);
		$this->content['footer'] = $this->parser->parse('layout/footer.php',$data,true);
		$this->parser->parse('layout/default.php', $this->content, false);
	}

	function renderclientTemplate(){
		$masters=$this->m_location->get_city_location();
		$cnt=0;
		foreach($masters as $mast){
			$data['masters'][$mast['location_name']][$cnt]=$mast;
			$cnt++;
		}
		$data['foot']= $this->m_frontend->get_footerpages();
		$this->content['header'] = $this->parser->parse('client/layout/header.php',$data,true);
		$this->content['footer'] = $this->parser->parse('client/layout/footer.php',$data,true);
		$this->parser->parse('client/layout/default.php', $this->content, false);
	}

	public function generate_sitemap()
	{
		$salesmenu=$this->m_services->service_list(null,1);
		$workshopmenu=$this->m_services->service_list(null,2);
		$insurancemenu=$this->m_services->service_list(null,3);
		$footer=$this->m_services->get_sorted_footer();
		$models=$this->m_models->fetch_models();

		$xml = new SimpleXMLElement('<urlset />');
		$xml->addAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
		$xml->addAttribute(
												'xsi:schemaLocation',
												'http://www.sitemaps.org/schemas/sitemap/0.9',
												'http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd'
											);
		$request=$xml->addChild('url');
		$request->addChild('loc',BASE_URL);
		$request->addChild('priority',1);
		$request1=$xml->addChild('url');
		$request1->addChild('loc',BASE_URL."models");
		$request1->addChild('priority',0.85);
		foreach($models as $model){
			$request12=$xml->addChild('url');
			$request12->addChild('loc',BASE_URL.'model/'. url_title($model['model_name'], '-', TRUE));
			$request12->addChild('priority',0.85);
			if(file_exists(DIR_BROCHURE.$model['brochure']) and !empty($model['brochure'])){
				$request13=$xml->addChild('url');
				$request13->addChild('loc',BASE_URL.DIR_BROCHURE.$model['brochure']);
				$request13->addChild('priority',0.61);
			}
		}

		foreach($salesmenu as $sale){
			$request2=$xml->addChild('url');
			$request2->addChild('loc',BASE_URL.strtolower(str_replace(" ",'-',$sale['service_title'])));
			$request2->addChild('priority',0.72);
		}

		foreach($workshopmenu as $workshop){
			$request3=$xml->addChild('url');
			$request3->addChild('loc',BASE_URL.strtolower(str_replace(" ",'-',$workshop['service_title'])));
			$request3->addChild('priority',0.72);
		}

		foreach($insurancemenu as $insurancemenu){
			$request4=$xml->addChild('url');
			$request4->addChild('loc',BASE_URL.$insurancemenu['route']);
			$request4->addChild('priority',0.72);
		}
		$request5=$xml->addChild('url');
		$request5->addChild('loc',BASE_URL."contact-us");
		$request5->addChild('priority',0.85);
		foreach($footer as $foot){
			$request6=$xml->addChild('url');
			$request6->addChild('loc',BASE_URL.strtolower(str_replace(" ",'-',$foot['service_title'])));
			$request6->addChild('priority',0.72);
		}
		$request7=$xml->addChild('url');
		$request7->addChild('loc',BASE_URL."careers");
		$request7->addChild('priority',0.72);

		$request8=$xml->addChild('url');
		$request8->addChild('loc',BASE_URL."googlemap");
		$request8->addChild('priority',0.72);

		$request9=$xml->addChild('url');
		$request9->addChild('loc',BASE_URL."sitemap");
		$request9->addChild('priority',0.72);

		Header('Content-type: application/xml');
		$doc=$xml->asXML();
		$xml->asXML(FCPATH.'sitemap.xml');
		//print($doc);
	}
}
