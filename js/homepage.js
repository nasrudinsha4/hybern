$(document).ready(function(){
	/**
	 *@Name:	validate.
	 *@Parameters:	#home
	 *@Importance:	This function validates the complete form of Homepage.
	 *@Compulsary Fields:	rules, messages
	 *@Success mesage:
	 *@Failure message:	Show Different Error Messages if errors detected.
	 *@Note:	The function executes once the user clicks on Save button to validate all the compulsory fields on the form and returs true if all the form elements are valid else returns false with error message and focuses on the first element which contains an error.
	*/
	$('#home_page12').validate({ // initialize the plugin

		rules: {
// 			fileupload: {required: true,
//  		  accept: "image/*"},
      filename: {required: true},
			//accept: true},
			//filetype: true},
			//extension: "png|jpg|gif|bmp"},
			//accept: "png|jpe?g|gif|bmp|png", filesize: 1048576  },
		  txt_caption: {required: true,
 		  validcontent: true},
		  txt_tagline: {required: true,
 		  validcontent: true},
			txt_title: {required: true,
 		  validcontent: true,
			 maxlength: 70},
		  seoinp: {required: true,
 		  validcontent: true,
			 maxlength: 155},
		  seo_details: {required: true,
			validcontent: true,
			maxlength: 155}
		  },
		messages: {
			filename: {required: "Enter file name.",
		  //filetype: "File must be JPG, GIF or PNG, less than 1MB"
 			},
		  txt_caption: {required: "Enter the text caption.",
		  validcontent: "hello world"
 			},
			txt_tagline: {required: "enter this field",
		  validcontent: "hello world"
 			},
			txt_title: {required: "enter this field",
		  validcontent: "hello world"
 			},
			seoinp: {required: "enter this field",
		  validcontent: "hello world"
 			},
			seo_details: {required: "enter this field",
		  validcontent: "hello world"
 			}
		},
// 		errorPlacement: function(error, element) {
//       //error.appendTo("#error");
// 			$('#error').addClass('error');
//       $('#error').css('display','block');
//  			$('#error').html(error);
//   	},
     submitHandler: function(home) {
//     	var banner = $("#filename").val();
// 			var caption = $("#txt_caption").val();
// 			var tagline = $("#txt_tagline").val();
// 			var title = $('#txt_title').val();
//       var keyword = $('#txt_keyword').val();
//       var details = $('#txt_details').val();
			var formData = new FormData($("#home_page")[0]);
      //var formData = new FormData(document.getElementById("home													"));
      //var formData = new FormData($(this)[0]);

      $.ajax({
				url: SITE_URL + 'admin/add_homepage',
 				data: formData,
				 async: false,

				 //$('form').serialize(),
				 //$('form').serialize(),//{
//           'banner_caption':caption,
//           'banner_tagline':tagline,
//           'banner_image':banner,
//           'seo_title':title,
//           'seo_keyword':keyword,
//           'seo_details':details
//         },
				//enctype: 'multipart/form-data',
				type: 'POST',
				dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
				success: function(msg){
					if(msg==1){
							location.href=SITE_URL+"admin/home-page";
					}
				}
			});
		 }
		});
});

/*validation for name */
  jQuery.validator.addMethod("validcontent", function(value, element)
  {
    if(value == '')
        return true;
    var temp1;
    temp1 = true;
    str = /^[a-zA-Z'\.\s]+$/;
    temp1 = str.test(value);
     return temp1;
  }, "Please fill this field.");

