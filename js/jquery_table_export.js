/*
 tableExport.jquery.plugin

 Copyright (c) 2015 hhurz, https://github.com/hhurz/tableExport.jquery.plugin

 Original work Copyright (c) 2014 Giri Raj, https://github.com/kayalshri/
 Licensed under the MIT License, http://opensource.org/licenses/mit-license
*/

(function ($) {
  $.fn.extend({
    tableExport: function (options) {
      var defaults = {
        consoleLog: false,
        csvEnclosure: '"',
        csvSeparator: ',',
        csvUseBOM: true,
        displayTableName: false,
        escape: false,
        excelstyles: ['border-bottom', 'border-top', 'border-left', 'border-right'],
        fileName: 'Statistic Report('+moment().format('DD/MMM/YYYY')+')',
        htmlContent: false,
        ignoreColumn: [],
        ignoreRow:[],
        numbers: {html: {decimalMark: '.',
                         thousandsSeparator: ','
                        },
                  output: {decimalMark: '.',
                           thousandsSeparator: ','
                          }
                 },
        onCellData: null,
        onCellHtmlData: null,
        outputMode: 'file', // 'file', 'string' or 'base64'
        tbodySelector: 'tr',
        theadSelector: 'tr',
        tableName: 'myTableName',
        type: 'csv', // 'csv', 'txt', 'sql', 'json', 'xml', 'excel', 'doc', 'png' or 'pdf'
        worksheetName: 'Statistic Report('+moment().format('DD/MMM/YYYY')+')'
      };

      var FONT_ROW_RATIO = 1.15;
      var el = this;
      var DownloadEvt = null;
      var $hrows = [];
      var $rows = [];
      var rowIndex = 0;
      var rowspans = [];
      var trData = '';
      var colNames = [];

      $.extend(true, defaults, options);

      colNames = GetColumnNames (el);

      if (defaults.type == 'csv' || defaults.type == 'txt') {
				var reportrange=$('#reportrange').find('span').html().replace(","," ");
				reportrange=reportrange.replace(","," ");
				var csvData ="Enquiry and Feedback Report ("+reportrange+")";
        var rowlength = 0;
        rowIndex = 0;

        function CollectCsvData (tgroup, tselector, rowselector, length) {

          $rows = $(el).find(tgroup).first().find(tselector);
          $rows.each(function () {
            trData = "";
            ForEachVisibleCell(this, rowselector, rowIndex, length + $rows.length,
                    function (cell, row, col) {
                      trData += csvString(cell, row, col) + defaults.csvSeparator;
                    });
            trData = $.trim(trData).substring(0, trData.length - 1);
            if (trData.length > 0) {

              if (csvData.length > 0)
                csvData += "\n";

              csvData += trData;
            }
            rowIndex++;
          });

          return $rows.length;
        }

        rowlength += CollectCsvData ('thead', defaults.theadSelector, 'th,td', rowlength);
        rowlength += CollectCsvData ('tbody', defaults.tbodySelector, 'td', rowlength);
        CollectCsvData ('tfoot', defaults.tbodySelector, 'td', rowlength);

        csvData += "\n";

        //output
        if (defaults.consoleLog === true)
          console.log(csvData);

        if (defaults.outputMode === 'string')
          return csvData;

        if (defaults.outputMode === 'base64')
          return base64encode(csvData);

        try {
          var blob = new Blob([csvData], {type: "text/" + (defaults.type == 'csv' ? 'csv' : 'plain') + ";charset=utf-8"});
          saveAs(blob, defaults.fileName + '.' + defaults.type, (defaults.type != 'csv' || defaults.csvUseBOM === false));
        }
        catch (e) {
          downloadFile(defaults.fileName + '.' + defaults.type,
                       'data:text/' + (defaults.type == 'csv' ? 'csv' : 'plain') + ';charset=utf-8,' + ((defaults.type == 'csv' && defaults.csvUseBOM)? '\ufeff' : ''),
                       csvData);
        }

      } else if (defaults.type == 'excel' || defaults.type == 'xls' || defaults.type == 'word' || defaults.type == 'doc') {

        var MSDocType = (defaults.type == 'excel' || defaults.type == 'xls') ? 'excel' : 'word';
        var MSDocExt = (MSDocType == 'excel') ? 'xls' : 'doc';
        var MSDocSchema = (MSDocExt == 'xls') ? 'xmlns:x="urn:schemas-microsoft-com:office:excel"' : 'xmlns:w="urn:schemas-microsoft-com:office:word"';
        var $tables = $(el).filter(function() {
            return $(this).data("tableexport-display") != 'none' &&
                   ($(this).is(':visible') ||
                    $(this).data("tableexport-display") == 'always');
          });
        var docData = '';

        $tables.each(function(){
          rowIndex = 0;

          colNames = GetColumnNames (this);

          docData += '<table style="border: 1px solid #7D7D7D;;"><thead><tr><th rowspan=2 colspan="15">Enquiry and Feedback Report ('+$('#reportrange').find('span').html()+') </th></tr><tr>';
          // Header
          $hrows = $(this).find('thead').first().find(defaults.theadSelector);
					var k=0;
					$hrows.each(function() {
            trData = "";k++;var left="";
						if(k==2) left='style="text-align:left;"';
            ForEachVisibleCell(this, 'th,td', rowIndex, $hrows.length,
              function(cell, row, col) {
                if (cell != null) {
                  trData += '<th style="';
                  for (var styles in defaults.excelstyles) {
                    if (defaults.excelstyles.hasOwnProperty(styles)) {
                      trData += defaults.excelstyles[styles] + ': ' + $(cell).css(defaults.excelstyles[styles]) + ';';
                    }
                  }
                  if ($(cell).is("[colspan]"))
                    trData += '" colspan="' + $(cell).attr('colspan');
                  if ($(cell).is("[rowspan]"))
                    trData += '" rowspan="' + $(cell).attr('rowspan');
                  trData += '">' + parseString(cell, row, col) + '</th>';
                }
              });
            if (trData.length > 0)
              docData += '<tr '+left+'>' + trData + '</tr>';
            rowIndex++;
          });

          docData += '</thead><tbody>';
          // Row Vs Column
          $rows = $(this).find('tbody').first().find(defaults.tbodySelector);
          $rows.each(function() {
            trData = "";
            ForEachVisibleCell(this, 'td', rowIndex, $hrows.length + $rows.length,
              function(cell, row, col) {
                if (cell != null) {
                  trData += '<td style="';
                  for (var styles in defaults.excelstyles) {
                    if (defaults.excelstyles.hasOwnProperty(styles)) {
                      trData += defaults.excelstyles[styles] + ': ' + $(cell).css(defaults.excelstyles[styles]) + ';';
                    }
                  }
                  if ($(cell).is("[colspan]"))
                    trData += '" colspan="' + $(cell).attr('colspan');
                  if ($(cell).is("[rowspan]"))
                    trData += '" rowspan="' + $(cell).attr('rowspan');
                  trData += '">' + parseString(cell, row, col) + '</td>';
                }
              });
            if (trData.length > 0)
              docData += '<tr>' + trData + '</tr>';
            rowIndex++;
          });

          if (defaults.displayTableName)
            docData += '<tr><td></td></tr><tr><td></td></tr><tr><td>' + parseString($('<p>' + defaults.tableName + '</p>')) + '</td></tr>';

          docData += '</tbody></table>';

          if (defaults.consoleLog === true)
            console.log(docData);
        });

        var docFile = '<html xmlns:o="urn:schemas-microsoft-com:office:office" ' + MSDocSchema + ' xmlns="http://www.w3.org/TR/REC-html40">';
        docFile += '<meta http-equiv="content-type" content="application/vnd.ms-' + MSDocType + '; charset=UTF-8">';
        docFile += "<head>";
        if (MSDocType === 'excel') {
          docFile += "<!--[if gte mso 9]>";
          docFile += "<xml>";
          docFile += "<x:ExcelWorkbook>";
          docFile += "<x:ExcelWorksheets>";
          docFile += "<x:ExcelWorksheet>";
          docFile += "<x:Name>";
          docFile += defaults.worksheetName;
          docFile += "</x:Name>";
          docFile += "<x:WorksheetOptions>";
          docFile += "<x:DisplayGridlines/>";
          docFile += "</x:WorksheetOptions>";
          docFile += "</x:ExcelWorksheet>";
          docFile += "</x:ExcelWorksheets>";
          docFile += "</x:ExcelWorkbook>";
          docFile += "</xml>";
          docFile += "<![endif]-->";
        }
        docFile += "</head>";
        docFile += "<body>";
        docFile += docData;
        docFile += "</body>";
        docFile += "</html>";

        if (defaults.consoleLog === true)
          console.log(docFile);

        if (defaults.outputMode === 'string')
          return docFile;

        if (defaults.outputMode === 'base64')
          return base64encode(docFile);

        try {
          var blob = new Blob([docFile], {type: 'application/vnd.ms-' + defaults.type});
          saveAs(blob, defaults.fileName + '.' + MSDocExt);
        }
        catch (e) {
          downloadFile(defaults.fileName + '.' + MSDocExt,
                       'data:application/vnd.ms-' + MSDocType + ';base64,',
                       docFile);
        }

      } else if (defaults.type == 'pdf') {
				generate_pdf();
      }

      function FindColObject (objects, colIndex, rowIndex) {
        var result = null;
        $.each(objects, function () {
          if (this.rowIndex == rowIndex && this.key == colIndex) {
            result = this;
            return false;
          }
        });
        return result;
      }

      function GetColumnNames (table) {
        var result = [];
        $(table).find('thead').first().find('th').each(function(index, el) {
          if ($(el).attr("data-field") !== undefined)
            result[index] = $(el).attr("data-field");
        });
        return result;
      }

      function isColumnIgnored($row, colIndex) {
        var result = false;
        if (defaults.ignoreColumn.length > 0) {
          if (typeof defaults.ignoreColumn[0] == 'string') {
            if (colNames.length > colIndex && typeof colNames[colIndex] != 'undefined')
              if ($.inArray(colNames[colIndex], defaults.ignoreColumn) != -1)
                result = true;
          }
          else if (typeof defaults.ignoreColumn[0] == 'number') {
            if ($.inArray(colIndex, defaults.ignoreColumn) != -1 ||
                $.inArray(colIndex-$row.length, defaults.ignoreColumn) != -1)
              result = true;
          }
        }
        return result;
      }

      function ForEachVisibleCell(tableRow, selector, rowIndex, rowCount, cellcallback) {
        if ($.inArray(rowIndex, defaults.ignoreRow) == -1 &&
            $.inArray(rowIndex-rowCount, defaults.ignoreRow) == -1) {

          var $row = $(tableRow).filter(function() {
            return $(this).data("tableexport-display") != 'none' &&
                   ($(this).is(':visible') ||
                    $(this).data("tableexport-display") == 'always' ||
                    $(this).closest('table').data("tableexport-display") == 'always');
          }).find(selector);

          var rowColspan = 0;
          var rowColIndex = 0;

          $row.each(function (colIndex) {
            if ($(this).data("tableexport-display") == 'always' ||
                ($(this).css('display') != 'none' &&
                 $(this).css('visibility') != 'hidden' &&
                 $(this).data("tableexport-display") != 'none')) {
              if (isColumnIgnored($row, colIndex) == false) {
                if (typeof (cellcallback) === "function") {
                  var c, Colspan = 0;
                  var r, Rowspan = 0;

                  // handle rowspans from previous rows
                  if (typeof rowspans[rowIndex] != 'undefined' && rowspans[rowIndex].length > 0) {
                    for (c = 0; c <= colIndex; c++) {
                      if (typeof rowspans[rowIndex][c] != 'undefined') {
                        cellcallback(null, rowIndex, c);
                        delete rowspans[rowIndex][c];
                        colIndex++;
                      }
                    }
                  }
                  rowColIndex = colIndex;

                  if ($(this).is("[colspan]")) {
                    Colspan = parseInt($(this).attr('colspan'));
                    rowColspan += Colspan > 0 ? Colspan - 1 : 0;
                  }

                  if ($(this).is("[rowspan]"))
                    Rowspan = parseInt($(this).attr('rowspan'));

                  // output content of current cell
                  cellcallback(this, rowIndex, colIndex);

                  // handle colspan of current cell
                  for (c = 0; c < Colspan - 1; c++)
                    cellcallback(null, rowIndex, colIndex + c);

                  // store rowspan for following rows
                  if (Rowspan) {
                    for (r = 1; r < Rowspan; r++) {
                      if (typeof rowspans[rowIndex + r] == 'undefined')
                        rowspans[rowIndex + r] = [];

                      rowspans[rowIndex + r][colIndex + rowColspan] = "";

                      for (c = 1; c < Colspan; c++)
                        rowspans[rowIndex + r][colIndex + rowColspan - c] = "";
                    }
                  }
                }
              }
            }
          });
          // handle rowspans from previous rows
          if (typeof rowspans[rowIndex] != 'undefined' && rowspans[rowIndex].length > 0) {
            for (c = 0; c <= rowspans[rowIndex].length; c++) {
              if (typeof rowspans[rowIndex][c] != 'undefined') {
                cellcallback(null, rowIndex, c);
                delete rowspans[rowIndex][c];
              }
            }
          }
        }
      }

      function jsPdfOutput(doc) {
        if (defaults.consoleLog === true)
          console.log(doc.output());

        if (defaults.outputMode === 'string')
          return doc.output();

        if (defaults.outputMode === 'base64')
          return base64encode(doc.output());

        try {
          var blob = doc.output('blob');
          saveAs(blob, defaults.fileName + '.pdf');
        }
        catch (e) {
          downloadFile(defaults.fileName + '.pdf',
                       'data:application/pdf;base64,',
                       doc.output());
        }
      }

      function prepareAutoTableText (cell, data, cellopt) {
        var cs = 0;
        if ( typeof cellopt != 'undefined' )
          cs = cellopt.colspan;

        if ( cs >= 0 ) {
          // colspan handling
          var cellWidth = cell.width;
          var textPosX = cell.textPos.x;
          var i = data.table.columns.indexOf(data.column);

          for (var c = 1; c < cs; c++) {
            var column = data.table.columns[i+c];
            cellWidth += column.width;
          }

          if ( cs > 1 ) {
            if ( cell.styles.halign === 'right' )
              textPosX = cell.textPos.x + cellWidth - cell.width;
            else if ( cell.styles.halign === 'center' )
              textPosX = cell.textPos.x + (cellWidth - cell.width) / 2;
          }

          cell.width = cellWidth;
          cell.textPos.x = textPosX;

          if ( typeof cellopt != 'undefined' && cellopt.rowspan > 1 )
            cell.height = cell.height * cellopt.rowspan;

          // fix jsPDF's calculation of text position
          if ( cell.styles.valign === 'middle' || cell.styles.valign === 'bottom' ) {
            var splittedText = typeof cell.text === 'string' ? cell.text.split(/\r\n|\r|\n/g) : cell.text;
            var lineCount = splittedText.length || 1;
            if (lineCount > 2)
              cell.textPos.y -= ((2 - FONT_ROW_RATIO) / 2 * data.row.styles.fontSize) * (lineCount-2) / 3 ;
          }
          return true;
        }
        else
          return false; // cell is hidden (colspan = -1), don't draw it
      }

      function drawCellElements (cell, elements, teOptions) {
        elements.each(function () {
          var kids = $(this).children();

          if ( $(this).is("div") ) {
            var bcolor = rgb2array(getStyle(this, 'background-color'), [255, 255, 255]);
            var lcolor = rgb2array(getStyle(this, 'border-top-color'), [0, 0, 0]);
            var lwidth = getPropertyUnitValue(this, 'border-top-width', defaults.jspdf.unit);

            var r = this.getBoundingClientRect();
            var ux = this.offsetLeft * teOptions.dw;
            var uy = this.offsetTop * teOptions.dh;
            var uw = r.width * teOptions.dw;
            var uh = r.height * teOptions.dh;

            teOptions.doc.setDrawColor.apply (undefined, lcolor);
            teOptions.doc.setFillColor.apply (undefined, bcolor);
            teOptions.doc.setLineWidth (lwidth);
            teOptions.doc.rect(cell.x + ux, cell.y + uy, uw, uh, lwidth ? "FD" : "F");
          }

          if (typeof kids != 'undefined' && kids.length > 0)
            drawCellElements (cell, kids, teOptions);
        });
      }

      function escapeRegExp(string) {
        return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
      }

      function replaceAll(string, find, replace) {
        return string.replace(new RegExp(escapeRegExp(find), 'g'), replace);
      }

      // Takes a string and encapsulates it (by default in double-quotes) if it
      // contains the csv field separator, spaces, or linebreaks.
      function csvString(cell, rowIndex, colIndex) {
        var result = '';

        if (cell != null) {
          var dataString = parseString(cell, rowIndex, colIndex);

          var csvValue = (dataString === null || dataString == '') ? '' : dataString.toString();

          if (dataString instanceof Date)
            result = defaults.csvEnclosure + dataString.toLocaleString() + defaults.csvEnclosure;
          else {
            result = replaceAll(csvValue, defaults.csvEnclosure, defaults.csvEnclosure + defaults.csvEnclosure);

            if (result.indexOf(defaults.csvSeparator) >= 0 || /[\r\n ]/g.test(result))
              result = defaults.csvEnclosure + result + defaults.csvEnclosure;
          }
        }

        return result;
      }

      function parseNumber(value) {
        value = value || "0";
        value = replaceAll(value, defaults.numbers.html.decimalMark, '.');
        value = replaceAll(value, defaults.numbers.html.thousandsSeparator, '');

        return typeof value === "number" || jQuery.isNumeric(value) !== false ? value : false;
      }


      function parseString(cell, rowIndex, colIndex) {
        var result = '';

        if (cell != null) {
          var $cell = $(cell);
          var htmlData;

          if ($cell[0].hasAttribute("data-tableexport-value"))
            htmlData = $cell.data("tableexport-value");
          else
            htmlData = $cell.html();

          if (typeof defaults.onCellHtmlData === 'function')
            htmlData = defaults.onCellHtmlData($cell, rowIndex, colIndex, htmlData);

          if (defaults.htmlContent === true) {
            result = $.trim(htmlData);
          }
          else {
            var text = htmlData.replace(/\n/g,'\u2028').replace(/<br\s*[\/]?>/gi, '\u2060');
            var obj = $('<div/>').html(text).contents();
            text = '';
            $.each(obj.text().split("\u2028"), function(i, v) {
              if (i > 0)
                text += " ";
              text += $.trim(v);
            });

            $.each(text.split("\u2060"), function(i, v) {
              if (i > 0)
                result += "\n";
              result += $.trim(v).replace(/\u00AD/g, ""); // remove soft hyphens
            });

            if (defaults.numbers.html.decimalMark != defaults.numbers.output.decimalMark ||
                defaults.numbers.html.thousandsSeparator != defaults.numbers.output.thousandsSeparator) {
              var number = parseNumber (result);

              if ( number !== false ) {
                var frac = ("" + number).split('.');
                if ( frac.length == 1 )
                  frac[1] = "";
                var mod = frac[0].length > 3 ? frac[0].length % 3 : 0;

                result = (number < 0 ? "-" : "") +
                         (defaults.numbers.output.thousandsSeparator ? ((mod ? frac[0].substr(0, mod) + defaults.numbers.output.thousandsSeparator : "") + frac[0].substr(mod).replace(/(\d{3})(?=\d)/g, "$1" + defaults.numbers.output.thousandsSeparator)) : frac[0]) +
                         (frac[1].length ? defaults.numbers.output.decimalMark + frac[1] : "");
              }
            }
          }

          if (defaults.escape === true) {
            result = escape(result);
          }

          if (typeof defaults.onCellData === 'function') {
            result = defaults.onCellData($cell, rowIndex, colIndex, result);
          }
        }

        return result;
      }

      function hyphenate(a, b, c) {
        return b + "-" + c.toLowerCase();
      }

      function rgb2array(rgb_string, default_result) {
        var re = /^rgb\((\d{1,3}),\s*(\d{1,3}),\s*(\d{1,3})\)$/;
        var bits = re.exec(rgb_string);
        var result = default_result;
        if (bits)
          result = [ parseInt(bits[1]), parseInt(bits[2]), parseInt(bits[3]) ];
        return result;
      }

      function getCellStyles (cell) {
        var a = getStyle(cell, 'text-align');
        var fw = getStyle(cell, 'font-weight');
        var fs = getStyle(cell, 'font-style');
        var f = '';
        if (a == 'start')
          a = getStyle(cell, 'direction') == 'rtl' ? 'right' : 'left';
        if (fw >= 700)
          f = 'bold';
        if (fs == 'italic')
          f += fs;
        if (f == '')
          f = 'normal';

        var result = {
          style: {
            align: a,
            bcolor: rgb2array(getStyle(cell, 'background-color'), [255, 255, 255]),
            color: rgb2array(getStyle(cell, 'color'), [0, 0, 0]),
            fstyle: f
          },
          colspan: (parseInt($(cell).attr('colspan')) || 0),
          rowspan: (parseInt($(cell).attr('rowspan')) || 0)
        };

        if (cell !== null) {
          var r = cell.getBoundingClientRect();
          result.rect = {
            width: r.width,
            height: r.height
          };
        }

        return result;
      }

      // get computed style property
      function getStyle(target, prop) {
        try {
          if (window.getComputedStyle) { // gecko and webkit
            prop = prop.replace(/([a-z])([A-Z])/, hyphenate);  // requires hyphenated, not camel
            return window.getComputedStyle(target, null).getPropertyValue(prop);
          }
          if (target.currentStyle) { // ie
            return target.currentStyle[prop];
          }
          return target.style[prop];
        }
        catch (e) {
        }
        return "";
      }

      function getUnitValue(parent, value, unit) {
        var baseline = 100;  // any number serves

        var temp = document.createElement("div");  // create temporary element
        temp.style.overflow = "hidden";  // in case baseline is set too low
        temp.style.visibility = "hidden";  // no need to show it

        parent.appendChild(temp); // insert it into the parent for em, ex and %

        temp.style.width = baseline + unit;
        var factor = baseline / temp.offsetWidth;

        parent.removeChild(temp);  // clean up

        return (value * factor);
      }

      function getPropertyUnitValue(target, prop, unit) {
        var value = getStyle(target, prop);  // get the computed style value

        var numeric = value.match(/\d+/);  // get the numeric component
        if (numeric !== null) {
          numeric = numeric[0];  // get the string

          return getUnitValue (target.parentElement, numeric, unit);
        }
        return 0;
      }

      function downloadFile(filename, header, data) {

        var ua = window.navigator.userAgent;
        if (ua.indexOf("MSIE ") > 0 || !!ua.match(/Trident.*rv\:11\./)) {
          // Internet Explorer (<= 9) workaround by Darryl (https://github.com/dawiong/tableExport.jquery.plugin)
          // based on sampopes answer on http://stackoverflow.com/questions/22317951
          // ! Not working for json and pdf format !
          var frame = document.createElement("iframe");

          if (frame) {
            document.body.appendChild(frame);
            frame.setAttribute("style", "display:none");
            frame.contentDocument.open("txt/html", "replace");
            frame.contentDocument.write(data);
            frame.contentDocument.close();
            frame.focus();

            frame.contentDocument.execCommand("SaveAs", true, filename);
            document.body.removeChild(frame);
          }
        }
        else {
          var DownloadLink = document.createElement('a');

          if (DownloadLink) {
            DownloadLink.style.display = 'none';
            DownloadLink.download = filename;

            if (header.toLowerCase().indexOf("base64,") >= 0)
              DownloadLink.href = header + base64encode(data);
            else
              DownloadLink.href = header + encodeURIComponent(data);

            document.body.appendChild(DownloadLink);

            if (document.createEvent) {
              if (DownloadEvt == null)
                DownloadEvt = document.createEvent('MouseEvents');

              DownloadEvt.initEvent('click', true, false);
              DownloadLink.dispatchEvent(DownloadEvt);
            }
            else if (document.createEventObject)
              DownloadLink.fireEvent('onclick');
            else if (typeof DownloadLink.onclick == 'function')
              DownloadLink.onclick();

            document.body.removeChild(DownloadLink);
          }
        }
      }

      function utf8Encode(string) {
        string = string.replace(/\x0d\x0a/g, "\x0a");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
          var c = string.charCodeAt(n);
          if (c < 128) {
            utftext += String.fromCharCode(c);
          }
          else if ((c > 127) && (c < 2048)) {
            utftext += String.fromCharCode((c >> 6) | 192);
            utftext += String.fromCharCode((c & 63) | 128);
          }
          else {
            utftext += String.fromCharCode((c >> 12) | 224);
            utftext += String.fromCharCode(((c >> 6) & 63) | 128);
            utftext += String.fromCharCode((c & 63) | 128);
          }
        }
        return utftext;
      }

      function base64encode(input) {
        var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;
        input = utf8Encode(input);
        while (i < input.length) {
          chr1 = input.charCodeAt(i++);
          chr2 = input.charCodeAt(i++);
          chr3 = input.charCodeAt(i++);
          enc1 = chr1 >> 2;
          enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
          enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
          enc4 = chr3 & 63;
          if (isNaN(chr2)) {
            enc3 = enc4 = 64;
          } else if (isNaN(chr3)) {
            enc4 = 64;
          }
          output = output +
                  keyStr.charAt(enc1) + keyStr.charAt(enc2) +
                  keyStr.charAt(enc3) + keyStr.charAt(enc4);
        }
        return output;
      }

      return this;
    }
  });
})(jQuery);

function generate_pdf() {
	var doc = new jsPDF('l', 'pt');var k=0;
	var text = 'Enquiry and Feedback Report ('+$('#reportrange').find('span').html()+')';
	xOffset = (doc.internal.pageSize.width / 2) - (doc.getStringUnitWidth(text) * doc.internal.getFontSize() / 2);
	doc.setFontSize(16);
	doc.setTextColor(82, 86, 89);
	doc.text(text, xOffset, 50);
	doc.setFontSize(10);
	doc.autoTable(getColumns(), getData($('#table tbody tr').length), {
		startY: 60,
		theme: 'grid',
		margin: {left: 20, right: 20, top: 10, bottom: 10},
		styles: {cellPadding: 2,rowHeight: 20,fontSize: 10,halign: 'left',valign: 'middle'},
		drawHeaderRow: function(row, data) {
					row.height = 46;
		},
		drawHeaderCell: function(cell, data) {
			k++;
			doc.setFillColor(100);
			cell.styles.halign='middle';
			doc.rect(cell.x, cell.y, cell.width, cell.height, cell.styles.fillStyle);
			doc.setFillColor(139, 144, 144);
			doc.rect(cell.x, cell.y + (cell.height / 2), cell.width, cell.height / 2, cell.styles.fillStyle);
			doc.autoTableText(cell.text, cell.textPos.x, cell.textPos.y+11, {
				halign: cell.styles.halign, valign: cell.styles.valign
			});
			doc.setTextColor(241, 249, 245);
			if(cell.raw=="Sales"){
				var text = '<--';
				doc.autoTableText(text, cell.textPos.x,71, {
					halign: cell.styles.halign, valign: cell.styles.valign
				});
			}
			if(cell.raw=="Extended Warranty"){
				var text = '-->';
				doc.autoTableText(text, cell.textPos.x+80,71, {
					halign: cell.styles.halign, valign: cell.styles.valign
				});
			}
			if(cell.raw=="Driving School"){
				var text = 'Enquiry Forms';
				doc.autoTableText(text, cell.textPos.x,71, {
					halign: cell.styles.halign, valign: cell.styles.valign
				});
			}
			if(k>8 && cell.raw=="Body Shop"){
				var text = '-->';
				doc.autoTableText(text, cell.textPos.x+49,71, {
							halign: 'center',valign: 'middle'
				});
				var text = 'Feedback Forms';
				doc.autoTableText(text, cell.textPos.x-7,71, {
							halign: 'center',valign: 'middle'
				});
			}
			return false;
		},
	});
	doc.save('Statistic Report('+moment().format('DD/MMM/YYYY')+').pdf');
}

function getColumns() {
	var data=[];
	var i=0;
	$('#table tr:nth-child(2) th').find('.th-inner').each(function(){
		data.push({
			title:$(this).html(),dataKey:'col'+i,
		});
		i++;
	});
	return data;
};

function getData(rowCount) {
	var cols=getColumns()
	var data = [];
	for (var j = 1; j <=rowCount; j++) {
		data[j]=[];
		for(var i=0;i<cols.length;i++){
			var k=i+1;
			data[j][cols[i]['dataKey']]=$('#table tbody tr:nth-child('+j+') td:nth-child('+k+')').html();
		}
	}
	data = data.filter(function(e){return e});
	return data;
}
