function initTable(getHeight) {
	$table = $('#table');

	if (! getHeight) {
		getHeight = function() {
			return Math.max(MIN_TABLE_HEIGHT, $(window).height() - MIN_PADDING_HEIGHT - $('.alert').outerHeight(true) - $('h3').outerHeight(true));
		}
	}

	$.extend($.fn.bootstrapTable.defaults, {
		toolbarAlign: 'left',
		striped: false,
		search: false,
		showFooter: false,
		sortOrder: 'asc',
		silentSort: true,
		clickToSelect: false,
		classes: 'table table-hover table-responsive',
		showRefresh: true,
		showExport: true,
		method: 'post',
	});

	$table.bootstrapTable({
		ajax: statistic_report,
		height: getHeight(),
	});

	$(window).resize(function () {
		$table.bootstrapTable('resetView', {
			height: getHeight()
		});
	});
}


function statistic_report(params){
	$.ajax({
		type: "POST",
		url: SITE_URL + "statistic-report",
		dataType:"json",
		data:{daterange:$('#range').html()},
		success: function(msg) {
			params.success(msg);
			params.complete();
		}
	});
}

	function cb(start, end) {
		$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		$('#range').html(start.format('Y-MM-DD') + '/' + end.format('Y-MM-DD'));
		$('#table').bootstrapTable('refresh');
	}
$(function() {
	cb(moment().subtract(29, 'days'), moment());
	var min=$('#disable_date').val();
	var max=moment().format('DD/MMM/YYYY');
	//set_datepickers(min,max);
	initTable();
	$(".export").children('button').prop("title", "Export")
	$('.columns').append('<button onclick=reset_dialog(); data-toggle="modal" data-target=".deletemodal" title="Delete" class="btn btn-default" id="destroy"><i class="glyphicon glyphicon-trash"></i></button>');
	$('.columns').removeClass('pull-right').addClass('pull-right');
	$('.fixed-table-header').css('background-color','rgba(46, 150, 202, 0.51)');
	$('.table-header tr:nth-child(2)').css({'background-color':'#d9edf7'});
	$('.table-header').css({'font-size':'small','font-family': 'helvetic'});
});

function reset_dialog(){
	$('#todate').val('');
	$('.datepicker').removeClass('error-display');
}

function set_datepickers(min,max){
	$('#reportrange').daterangepicker({
		locale: {
			format: 'DD/MMM/YYYY',
		},
		ranges: {
			'Today': [moment().format('DD/MMM/YYYY'), moment().format('DD/MMM/YYYY')],
			'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
			'Last 7 Days': [moment().subtract(6, 'days'), moment()],
			'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			'This Month': [moment().startOf('month'), moment().endOf('month')],
			'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
			'All': [min,max]
		},
		minDate: min,
		maxDate: max,
		linkedCalendars:false,
	}, cb);

	$('#todate').daterangepicker({
		locale: {
			format: 'DD/MMM/YYYY',
		},
		minDate: min,
		maxDate: max,
		singleDatePicker: true,
		showDropdowns: true
	});
}
