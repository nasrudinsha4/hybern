function add_radius_class(ids) {
	if ($(ids).length) {
		$(ids + ' li a').removeClass('pagination_radius_left');
		$(ids + ' li a').removeClass('pagination_radius_right');
		$(ids + ' li a:visible').first().addClass('pagination_radius_left');
		$(ids + ' li a:visible').last().addClass('pagination_radius_right');
	}	
}

$(document).ready(function(){
  if($('.flexslider').length > 0) {
	$('.flexslider').flexslider({
		animation: "slide",
		controlNav: "thumbnails"
	});
  }

	$('#billing_date,#delivery_date').datetimepicker({
		format: 'DD-MM-YYYY'
	});
	$('#model_thickness').change(function(){
		$('#model_thickness_text').val('');
		if($('#model_thickness option:selected').text().trim() == 'Other'){
		$('.thickness').show();
		}else{
		$('.thickness').hide();}
	});
	$('#model_size').change(function(){
		if(typeof sizes_arr != 'undefined')
		remove_duplicate_models();
	});
	
	if ($('#billing_body,#model_body').length){
		$('#billing_body,#model_body').pageMe({pagerSelector:'#myPagerView',showPrevNext:true,hidePageNumbers:false,perPage:8});
		add_radius_class('#myPagerView');
	}		
	$("#btnPrint").click(function () {
 			$("#invoicetable thead").html('');
			$("#invoicetable tbody").html('');
			$('#invoice_bill_no').html('');
			$('.billAdd').html('');
			$('.OwnName').html('');
			$('.OwnAdd').html('');
			$('.BillDate').html('');
		var res ='';
		if ($('.bill_order').is(':checked')) {
			var thead = $("#purchase_history thead").html();
			var tfoot = $("#purchase_history tfoot").html();
			$("#purchase_history tbody tr").each(function(tr) {
				if ($('.bill_td'+tr).is(':checked')) {
					res += '<tr>'+$('.bill_tr'+tr).html()+'</tr>';
				}
			});
			$("#invoicetable thead").html(thead);
			$("#invoicetable tbody").html(res);
			$("#invoicetable tfoot").html(tfoot);
			$('#invoicetable tr').find('td:eq(0),th:eq(0)').remove();
			$('#invoice_bill_no').html($('#hidden').val());
			var add = $('#billing_name').val()+'<br>';
			add += $('#billing_phone').val()+'<br>';
			add += $('#billing_email').val()+'<br>';
			add += 'Add: '+$('#billing_address').val()+'<br>';
			$('.billAdd').html(add);
			$('.OwnName').html(OWN_NAME);
			$('.OwnAdd').html(OWN_ADD);
			$('.BillDate').html(moment().format('MMM DD,YYYY'));
			var total_val = 0;
			$("#invoicetable tbody tr").each(function(tr) {
				total_val = (total_val + parseFloat($(this).find('td:last').html()));
			});
			$("#invoicetable tfoot tr td:last").html(total_val);
			var printWindow = window.open('', '', 'height=400,width=800');
			printWindow.document.write('<link media="screen, print" rel="stylesheet" type="text/css" href="'+SITE_URL+'css/style.css" />');
			printWindow.document.write('<link media="screen, print" rel="stylesheet" type="text/css" href="'+SITE_URL+'css/bootstrap.min.css" />');
			printWindow.document.write('<link media="screen, print" rel="stylesheet" type="text/css" href="'+SITE_URL+'css/bootstrap-select.min.css">');
			printWindow.document.write('<link media="screen, print" rel="stylesheet" type="text/css" href="'+SITE_URL+'css/font-awesome.min.css" />');
			printWindow.document.write($('#invoice').html());
			printWindow.document.close();
			printWindow.print();
		} else {
			$(".modal").scrollTop(0);
			$(".error").html(SELECT_ORDER);
			$(".error").css('display','inline-block');
			$(".modal").scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
		}
	});
	var jqXHR_img =$('#modelfile').fileupload({
		url: SITE_URL+"save_newmodel_files",
		dataType: 'json',
		success: function (msg) {
			$('#img_temp').val(msg['modelfile']);
			setTimeout(function(){$('.imgbar').hide();$('#img-bar').css('width',0+'%');}, 2000);
		},
		progressall: function (e, data) {
			$('.imgbar').show();
			$('').show();
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('#img-bar').css('width',progress + '%');
		},
	}).on('fileuploadadd', function (e, data) {
			$('#img-bar').css('width',0+'%');
			var size=$('#model_img_size').val().trim();
			var modelfile=$('#model_image').val();
			modelfile= modelfile.split(".").pop();
			if(modelfile!=""){
				if(modelfile!="png" && modelfile!="gif" && modelfile!="jpg" && modelfile!="jpeg")
					modelfile=true;
			}
			if(modelfile==true){
				$(".error").html(INVALID_FILE);
				$(".error").css('display','inline-block');
    $(".modal").scrollTop(0);
				$('.error').delay(TIMEOUT).fadeOut('slow');
				return false;
			}
			/* else	if(size>256000){
				$(".error").html(IMAGE_SIZE);
				$(".error").css('display','inline-block');
    			$(".modal").scrollTop(0);
				$('.error').delay(TIMEOUT).fadeOut('slow');
				return false;
			} */
			jqXHR_img = data.submit();
	});
	$('.abort-img').click(function (e) {
			qXHR_img.abort();
			$('#modelthumb').attr('src',SITE_URL+'images/350x200.png');
			$('#model_image,#modelfile').val('');
	});

	var jqXHR_pdf =$('#brochure').fileupload({
		url: SITE_URL+"save_newmodel_files",
		dataType: 'json',
		success: function (msg) {
			$('#bro_temp').val(msg['brochure']);
			setTimeout(function(){$('#pdfbar,.abort-pdf').hide();$('#pdfbar').css('width',0+'%');}, 3000);
		},
		progressall: function (e, data) {
			$('#pdfbar,.abort-pdf').show();
			$('').show();
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('#pdfbar').css('width',progress + '%');
		}
	}).on('fileuploadadd', function (e, data) {
			var brochure=$('#brochure_name').val();
			brochure= brochure.split(".").pop();
			if(brochure!=""){
				if(brochure!="pdf")
					brochure=true;
			}
			if(brochure==true){
				$(".error").html(INVALID_FILE);
				$(".error").css('display','inline-block');
    $(".modal").scrollTop(0);
				$('.error').delay(TIMEOUT).fadeOut('slow');
				return false;
			}
			jqXHR_pdf = data.submit();
	});
	$('.abort-pdf').click(function (e) {
			jqXHR_pdf.abort();
			$("#brochure,#brochure_name").val('');
			$('#pdfbar,.abort-pdf').hide();
			$('#pdfbar').css('width',0+'%');
	});

	var jqXHR_gal=$('#gallery_file').fileupload({
		url: SITE_URL+"save_gallery",
		dataType: 'json',
		success: function (msg) {
				setTimeout(function(){$('.bar').hide();$('.progress-bar').css('width',0+'%');}, 3000);
				$('#gallery_thumb').attr('src',SITE_URL+'images/970x380.jpg').width(388).height(152);
				$('#title,#image_src,#gallery_image,#gallery_file,#gallery_size').val('');
				$('.addbtns').prop('disabled',true);
				get_details($('#model_id').val(),'gallery_edit');
		},
		add: function (e, data) {
				data.context = $('.addbtns').one("click",function () {
					if($('#gallery_size').val()!=""){
						var flag=validate_gallery();
						if(flag)
							jqXHR_gal=data.submit();
					}
				});
		},
		progressall: function (e, data) {
				$('.bar').show();
			var progress = parseInt(data.loaded / data.total * 100, 10);
			$('.progress-bar').css('width',progress + '%');
		}
	});
	$('.abort').click(function (e) {
			jqXHR_gal.abort();
			$('#gallery_thumb').attr('src',SITE_URL+'images/970x380.jpg').width(388).height(152);
			$("#gallery_file,#gallery_image,#gallery_size,#image_src").val('');
			$('.bar').hide();
			$('.progress-bar').css('width',0+'%');
	});

	$('#bigimage,#lb_bigimg').click(function () {
		if($('#bigimage').prop('checked')){
			$('#imageupload').val('');
			$('.icon').hide();
			$('.thumb1').hide();
			$('.thumb2').show();
		}else{
			$('#imageupload').val('');
			$('.icon').show();
			$('.thumb2').hide();
			$('.thumb1').show();
		}
	});
/* script for home page form*/
		$('.Edit').click(function(){
			$("#txt_caption").attr("readonly",false);
			$("#txt_tagline").attr("readonly",false);
			$(".home_title").attr("readonly",false);
			$(".home_seoinp").attr("readonly",false);
			$(".home_details").attr("readonly",false);
			$("#for_browse").css("display",'inline-block');
			$(".save").css("display",'inline-block');
			$("#edit").css("display",'none');
			$(".cancel").css("display",'inline-block');
		});
		$('#cancel').click(function(){
			location.reload();
		});
		if($('#home_page_id').val()!=""){
			$("#txt_caption").attr("readonly",true);
			$("#txt_tagline").attr("readonly",true);
			$(".home_title").attr("readonly",true);
			$(".home_seoinp").attr("readonly",true);
			$(".home_details").attr("readonly",true);
			$("#for_browse").css("display",'none');
			$(".save").css("display",'none');
			$(".cancel").css("display",'none');
		}
/* script for flash message */
		$('.green_message').delay(TIMEOUT).fadeOut('slow');
		$('.red_message').delay(TIMEOUT).fadeOut('slow');

		$("input[type='text']").change(function() {
			$(this).val($.trim($(this).val()));
		});
		$("textarea").change(function() {
			$(this).val($.trim($(this).val()));
		});

	});
/* script for tiny MCE*/
	tinymce.init({
		selector: "#textarea1,#textarea2,#textarea3,#textarea4,#textarea5",
		menubar: false,
		plugins: "textcolor",
		entity_encoding: 'raw',
		toolbar: " bold italic underline |alignleft aligncenter alignright alignjustify | bullist numlist | forecolor ",
	});

/* script for browse file*/
	function HandleBrowseClick(id){
		var fileinput = document.getElementById(id);
		fileinput.click();
	}

	function reset_form(id){
		total_qty = 0;
		total_rate = 0;
		total_discount = 0;
		total_tv = 0;
		total_gst = 0;
		$(".active").removeClass('active');
		$("#ac").addClass('active');
		$("#pageadd").addClass('active');
		$("#billing").addClass('active');
		$(".new_billing").show();
		$('#billaddlist tbody').empty();
		$('#billaddlist,.purchase_his').hide();
		if(id=="loc_form"){
			$('.locationpage').html('Add Master'+"<div style='width:240px;' class='error master' align='center'></div>");
			$("#city0").css("display",'none');
			$("#email0").css("display",'none');
			$("#add0").css("display",'none');
			$("#location").attr("readonly",false);
			$("#status").val("add");
			$("#location").val("");
			$('.delnext1').nextAll('div').hide();
			$('.circle_txt').show();
			$('.circle_sel').hide();
		}else{
			$('.locationpage').html('Add Master Data'+"<div style='width:180px;' class='error master' align='center'></div>");
			$("#city0").css("display",'inline-block');
			$("#email0").css("display",'inline-block');
			$("#add0").css("display",'inline-block');
			$("#status").val("");
		}
		$('#'+id).trigger("reset");
		if(id!="location_form"){
			$('#'+id).find('input[type=hidden]').each(function(){
				this.value ="";
			});
		}
		$('#'+id).find('select').val("");
		$('input[type=checkbox]').attr('checked',false);
		if(id=="model"){
			$('#gp').show();
			tinyMCE.get('textarea1').setContent('');
			tinyMCE.get('textarea2').setContent('');
			$('#modelthumb').attr('src',SITE_URL+'images/350x200.png');
		}if(id=="services"){
			$('.icon').show();
			$('.thumb1').show();
			$('.thumb2').hide();
			$('.thumb1').attr('src',SITE_URL+'images/920x270.png');
			$('.thumb2').attr('src',SITE_URL+'images/1290x270.png');
			$('#logothumb').attr('src',SITE_URL+'images/230x115.png');
			$("#title").attr("readonly",false);
		}
		if(id=="location_form"){
			$("#location").attr("readonly",false);
			$('.delnext2').nextAll('div').remove();
			$('.delnext1').nextAll('div').show();
			$('.circle_txt').hide();
     		$('#company,#thickness,#group,#range,#feel,#hsn_sac,#make').next().hide();
			$('#warranty,#group,#features,#description').hide();
			$('.circle_sel').show();
			getlocation();
		}
		if(id=="services"){
			$('#is_display option[value=1]').attr('selected','selected');
			$('#minchar1').html(160);
			$('#minchar2').html(300);
			$('#minchar3').html(256);
		}
		if(id=="model"){
			$('.thickness').hide();
			$('#minchar1').html(70);
			$('#minchar2,#minchar3').html(155);
		}
		show_hide_billbtn('ac');
		$("#row_count,#actual_total").val(0);
		$("#total").html(0);
		$('.selectpicker').selectpicker('refresh');
	}
/* script for adding new row for city in location form*/
	function add_row() {
		var count=$('#hidden').val();
		var city="city"+count;
		var email="email"+count;
		var add="add"+count;
		var del="del"+count;
		var delrow='delrow'+count;
		var i=count;
		var tabcont_1 = "";
		tabcont_1 = '<div id='+delrow+' ><div class="form-group"><div class="col-md-6 ">';
		tabcont_1 += '<input data-toggle="tooltip" title="Enter a location" class="form-control" placeholder="Location" type="text" id='+city+' name='+city+'></div>';
		tabcont_1 += '<div class="col-md-5 ">';
		tabcont_1 += '<input  data-toggle="tooltip" title="Enter Email Id" class="form-control" placeholder="Email Id" type="text" id='+email+' name='+email+'></div>';
		tabcont_1 += '<img onclick="del_row();" id='+del+' src="'+SITE_URL+'images/minus.png" />&nbsp;<img onclick="add_row();" id='+add+' src="'+SITE_URL+'images/plus.png" /></div></div>';
		$('.contactustbl').append(tabcont_1);
		i++;
		$('#hidden').val(i);
		for(var j=0;j<i-1;j++){
			$('#add'+j).hide();
			$('#del'+j).hide();
		}
	}

/* script for deleting row for city in location form*/
	function del_row(){
		var count=$('#hidden').val();
		$('#delrow'+(count-1)).remove();
		$('#hidden').val(count-1);
		$('#add'+(count-2)).show();
		$('#del'+(count-2)).show();
	}

/* script for displaying image,name,size,and dimentions for all forms*/
	function image_name(img,id,thumb){
		if(img.files && img.files[0]){
			var image = new FileReader();
			image.onload = function (e){
				var image123 = new Image();
				image123.src = e.target.result;
				image123.onload = function () {
					if(thumb=="modelthumb"){
						$('#model_height').val(this.height);
						$('#model_width').val(this.width);
					}
					if(thumb=="bannerthumb"){
						$('#image_height').val(this.height);
						$('#image_width').val(this.width);
					}
					if(thumb=="logothumb"){
						$('#logo_height').val(this.height);
						$('#logo_width').val(this.width);
					}
					if(id=="filename"){
						$('#banner_height').val(this.height);
						$('#banner_width').val(this.width);
					}
					if(thumb=="gallery_thumb"){
						$('#gallery_height').val(this.height);
						$('#gallery_width').val(this.width);
					}
				};
				if(id=="gallery_edit_image"){
					$('#'+thumb).attr('src', e.target.result).width(160).height(108);
				}
				if(thumb=="bannerthumb"){
					if($('.thumb2').css('display') == 'none' )
						$('.thumb1').attr('src', e.target.result).width(131.43).height(38.57);
					else
						$('.thumb2').attr('src', e.target.result).width(184.29).height(38.57);
				}
				if(thumb=="thumbnail"){
					$('#'+thumb).attr('src', e.target.result).width(170).height(65);
					$('#homepage_popup').attr('src', e.target.result);
				}
				if(thumb=="logothumb")
					$('#'+thumb).attr('src', e.target.result).width(92).height(38.57);
				if(thumb=="modelthumb")
					$('#'+thumb).attr('src', e.target.result).width(140).height(80);
				if(thumb=="gallery_thumb"){
					$('.addbtns').prop('disabled',false);
					$('.clearbtns').prop("disabled",false);
					$('#image_src').val(e.target.result);
					$('#'+thumb).attr('src', e.target.result).width(388).height(152);
				}
			};

			image.readAsDataURL(img.files[0]);
			var reader = img.files[0]['name'];
			var size = img.files[0]['size'];
			if(thumb=="bannerthumb")
				$("#imagesize").val(size);
			if(thumb=="logothumb")
				$("#logosize").val(size);
			if(id=="filename")
				$("#banner_size").val(size);
			if(thumb=="modelthumb")
				$("#model_img_size").val(size);
			if(thumb=="gallery_thumb")
				$("#gallery_size").val(size);
			$('#'+id).val(reader);
		}
	}

/* script for validating billing  form*/
	function validate_billing(){
		var model_name=$("#model_name").val().trim();
		var namevalue=$("#billing_name").val().trim();
		var address=$("#billing_address").val().trim();
		var location=$("#model_location").val().trim();
		var phonesale=$("#billing_phone").val().trim();
		var emailsale = $("#billing_email").val().trim();
		var cus_gst = $('#customer_GSTIN').val().trim();
		var email_sale = emailsale.split(",");
		var hidden=$('#hidden').val();
		var email_flag=false;
		var name=/^[a-z A-Z'-.& ]+$/;
		var phoneno =/^(\+\d{1,3}[- ],?)?\d{10}$/;
		var emailReg = /(^[a-zA-Z0-9]+[\._-]{0,1})+([a-zA-Z0-9]+[_]{0,1})*@([a-zA-Z0-9]+[-]{0,1})+(\.[a-zA-Z0-9]+)*(\.[a-zA-Z]{2,3})$/;
		var pat1 = /^\d+$/;
		for(var a=0; a<(email_sale.length); a++){
			if(email_sale[a]!="" && !email_sale[a].match(emailReg))
				email_flag=true;
		}
		if(namevalue==""){
			$(".modal").scrollTop(0);
			$(".error").html(ENTER_NAME);
			$(".error").css('display','inline-block');
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}
		if(address==""){
			$(".modal").scrollTop(0);
			$(".error").html(ENTER_ADDRESS);
			$(".error").css('display','inline-block');
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}
		if (email_flag==true) {
			$(".modal").scrollTop(0);
			$(".error").html(INVALID_EMAIL_ERROR);
			$(".error").css('display','inline-block');
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}
		if(phonesale==""){
			$(".modal").scrollTop(0);
			$(".error").html(ENTER_MOBILE_ERROR);
			$(".error").css('display','inline-block');
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}
		/*if(cus_gst==""){
			$(".modal").scrollTop(0);
			$(".error").html(ENTER_GSTIN_ERROR);
			$(".error").css('display','inline-block');
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}
		if(billing_date==""){
			$(".modal").scrollTop(0);
			$(".error").html(ENTER_BILLING_DATE);
			$(".error").css('display','inline-block');
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}
		if(delivery_date==""){
			$(".modal").scrollTop(0);
			$(".error").html(ENTER_DELIVERY_DATE);
			$(".error").css('display','inline-block');
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}
		if(location==""){
			$(".modal").scrollTop(0);
			$(".error").html(ENTER_LOCATION);
			$(".error").css('display','inline-block');
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}*/						
		if($('#hidden').val()=="" && $('#billaddlist tbody tr').length == 0){
			$(".modal").scrollTop(0);
			$(".error").html(NO_BILLING_DETAILS);
			$(".error").css('display','inline-block');
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}
			var advance = parseFloat($('#advance').val()).toFixed(2);
			var total_amt = parseFloat($('#total').text()).toFixed(2);
			if($('#billaddlist tbody tr').length > 0)
			$('#balance').val((parseFloat($('#balance').val())+(total_amt - advance)).toFixed(2));
	}

/* script for validating services form*/
	function validate_services() {
		var image_height=$('#image_height').val().trim();
		var image_width=$('#image_width').val().trim();
		var logo_height=$('#logo_height').val().trim();
		var logo_width=$('#logo_width').val().trim();
		var logosize=$("#logosize").val().trim();
		var imagesize=$("#imagesize").val().trim();
		var title=$("#title").val().trim();
		var image=$("#image").val().trim();
		var caption=$("#caption").val().trim();
		var logo=$("#logo").val().trim();
		var textarea4=tinyMCE.get('textarea4').getContent();
		var textarea5=tinyMCE.get('textarea5').getContent();
		var email=$("#email").val().trim();
		var subject=$("#subject").val().trim();
		var seoinp1=$("#seoinp1").val().trim();
		var seoinp2=$("#seoinp2").val().trim();
		var description=$("#description").val().trim();
		var imageupload= $("#image").val();
		imageupload= imageupload.split(".").pop();
		var logoupload= $("#logo").val();
		logoupload= logoupload.split(".").pop();
		var checkedval="";
			checkedval=$('#is_display').val();
		if(imageupload!=""){
			if(imageupload!="png" && imageupload!="gif" && imageupload!="jpg" && imageupload!="jpeg")
				imageupload=true;
		}
		if(logoupload!=""){
			if(logoupload!="png" && logoupload!="gif" && logoupload!="jpg" && logoupload!="jpeg")
				logoupload=true;
		}

		if(image_height!="" && image_width!=""){
			var h="";var w="";
			if(!($('#bigimage').prop('checked'))){
				h=270;
				w=920;
			}else if($('#bigimage').prop('checked')){
				h=270;
				w=1290;
			}
			if(image_height!=h && image_width!=w){
				image_height=true;
				image_width=true;
			}
		}
		if(logo_height!="" && logo_width!=""){
			if(logo_height!=115 && logo_width!=230){
				logo_height=true;
				logo_width=true;
			}
		}
		if(checkedval=="" || title=="" || image=="" || caption=="" || email=="" || subject=="" || seoinp1=="" || seoinp2=="" || description=="" || textarea4=="" || textarea5=="" ){
			$(".error").html(EMPTY_ERROR);
			$(".error").css('display','inline-block');
   $(".modal").scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}else if(!($('#bigimage').prop('checked')) && logo==""){
			$(".error").html(EMPTY_ERROR);
			$(".error").css('display','inline-block');
   $(".modal").scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}else	if(imageupload==true){
			$(".error").html(INVALID_FILE);
			$(".error").css('display','inline-block');
   $(".modal").scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}else	if(!($('#bigimage').prop('checked')) && logoupload==true){
			$(".error").html(INVALID_FILE);
			$(".error").css('display','inline-block');
   $(".modal").scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}else	if(imagesize>256000){
			$(".error").html(IMAGE_SIZE);
			$(".error").css('display','inline-block');
   $(".modal").scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}else if(!($('#bigimage').prop('checked')) && logosize>51200){
			$(".error").html(IMAGE_SIZE);
			$(".error").css('display','inline-block');
   $(".modal").scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}else if(image_height==true  && image_width==true){
			if(!($('#bigimage').prop('checked')))
				$(".error").html(IMAGE_DIMENSION);
			else
				$(".error").html(IMAGE_DIMENSION2);
			$(".error").css('display','inline-block');
   $(".modal").scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}else if(!($('#bigimage').prop('checked')) && logo_height==true && logo_width==true){
			$(".error").html(LOGO_DIMENSION);
			$(".error").css('display','inline-block');
   $(".modal").scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}
	}

/* script for validating homepage form*/
	function validate_homepage() {
		var filename=$('#filename').val().trim();
		var txt_caption=$('#txt_caption').val().trim();
		var txt_tagline=$('#txt_tagline').val().trim();
		var txt_title=$('#txt_title').val().trim();
		var seoinp=$('#seoinp').val().trim();
		var image_height=$('#banner_height').val().trim();
		var image_width=$('#banner_width').val().trim();
		var size=$('#banner_size').val().trim();
		var seo_details=$('#seo_details').val().trim();
		var browse= $("#browse").val();
		var filename= $("#filename").val();
		browse= filename.split(".").pop();
		if(browse!=""){
			if(browse!="png" && browse!="gif" && browse!="jpg" && browse!="jpeg")
				browse=true;
		}
		if(filename=="" || txt_caption=="" || txt_tagline=="" || txt_title=="" || seoinp=="" || seo_details==""){
			$(".error").html(EMPTY_ERROR);
			$(".error").css('display','inline-block');
   $(".modal").scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}else	if(browse==true){
			$(".error").html(INVALID_FILE);
			$(".error").css('display','inline-block');
   $(".modal").scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}
		/* else if(image_height!=520 && image_width!=1366){
			if(image_height!="" && image_width!=""){
				$(".error").html(BANNER_DIMENSION);
				$(".error").css('display','inline-block');
    			$(".modal").scrollTop(0);
				$('.error').delay(TIMEOUT).fadeOut('slow');
				return false;
			}
		}else	if(size>307200){
			$(".error").html(IMAGE_SIZE);
			$(".error").css('display','inline-block');
   			$(".modal").scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		} */
	}

/* script for validating model form*/
	function validate_newmodel() {
		var model_name=$('#model_name').val().trim();
		var model_thickness=$('#model_thickness option:selected').text().trim();
		var model_thickness_text = $('#model_thickness_text').val().trim();
		var brochure=$('#brochure_name').val();
		check_field = new Array();
		check_field[0] = model_name;
		if($('#model_range').val() != null)
		check_field[1] = $('#model_range').val().trim();
		else
		check_field[1] = 0;
		brochure= brochure.split(".").pop();
		if(brochure!=""){
			if(brochure!="pdf")
				brochure=true;
		}
		var modelfile=$('#model_image').val();
		modelfile= modelfile.split(".").pop();
		if(modelfile!=""){
			if(modelfile!="png" && modelfile!="gif" && modelfile!="jpg" && modelfile!="jpeg")
				modelfile=true;
		}
		var req_flag = false
		$('form#model').find('input,select,textarea').each(function(){
			
			if($(this).hasClass('required') && $(this).val() == ''){
				if (this.type == 'select-one')
					$(this).next().find('button').addClass('required-error')
				else
					$(this).addClass('required-error');
				req_flag = true;
			} else {
				if (this.type == 'select-one')
					$(this).next().find('button').removeClass('required-error')
				else
					$(this).removeClass('required-error');
			}
		});
		if(req_flag==true){
			$(".error").html(EMPTY_ERROR);
			$(".error").css('display','inline-block');
   			$(".modal").scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}
		if((model_thickness == 'Other' && model_thickness_text == '') ){
			$(".error").html(EMPTY_ERROR);
			$(".error").css('display','inline-block');
   			$(".modal").scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}else if(modelfile==true){
			$(".error").html(INVALID_FILE);
			$(".error").css('display','inline-block');
   			$(".modal").scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}else if(brochure==true){
			$(".error").html(INVALID_FILE);
			$(".error").css('display','inline-block');
   			$(".modal").scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}
		/* else if(image_height!=200 && image_width!=350){
			if(image_height!="" && image_width!=""){
				$(".error").html(MODEL_DIMENSION);
				$(".error").css('display','inline-block');
    			$(".modal").scrollTop(0);
				$('.error').delay(TIMEOUT).fadeOut('slow');
				return false;
			}
		}else	if(size>256000){
			$(".error").html(IMAGE_SIZE);
			$(".error").css('display','inline-block');
   			$(".modal").scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		} */
		var flag=check_field_exist(check_field,"models");
		if(flag==true){
			$(".error").html(MODEL_NAME_EXIST);
			$(".error").show();
			$(document).scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}
		var new_models = '';
		if ($('#more_models_table tbody tr').length > 0) {			
			$('#more_models_table tbody tr').each(function(){
				if ($('#model_size option:selected').text() != this.firstChild.innerText && this.childNodes[1].innerText != 'Empty' && this.childNodes[2].innerText != 'Empty') {
					check_field = new Array();
					check_field[0] = $('#model_variety option:selected').text()+' '+this.firstChild.innerText+' '+$('#model_thickness option:selected').text();
					if($('#model_range').val() != null)
					check_field[1] = $('#model_range').val().trim();
					else
					check_field[1] = 0;
					var flag=check_field_exist(check_field,"models");
					if(flag==true){
            		new_models =1;
						$(".error").html(MODEL_NAME_EXIST+ ' ' +check_field[0]);
						$(".error").show();
						$(document).scrollTop(0);
						$('.error').delay(TIMEOUT).fadeOut('slow');
					}
				}
			});
		}
		if(new_models==1)
		return false;
		//return false;
		if(flag==true)
		return false;
		$('#freeze,.bar').show();
		tinyMCE.triggerSave();
		//var form = new FormData(document.getElementById('model'));
		//$('.progress-bar').width('0%');
		//var percentComplete="";
		/*$.ajax({
			xhr: function() {
				var xhr = new window.XMLHttpRequest();
				xhr.upload.addEventListener("progress", function(evt) {
					if (evt.lengthComputable) {
						percentComplete = evt.loaded / evt.total;
						percentComplete = parseInt(percentComplete * 100);
						$('.progress-bar').width(percentComplete+'%');
					}
				}, false);
				return xhr;
			},
			type: "POST",
			url: SITE_URL+"admin/models/save_newmodel",
			data: form,
			cache: false,
			contentType: false,
			processData: false,
			success: function(msg){
    		if(msg!='' && percentComplete==100){
    			//setTimeout(function(){location.reload();}, 5000);
				}
			},
		});*/
	}

/* script for validating location form*/
	function validate_location (){
		var inval_location=false; var id="";
		if($('#location').is(":visible"))
			id="location";
		else
			id="location_id";

		var location=$("#"+id).val().trim();
		var hidden=$("#hidden").val();
		
		if((id=="location_id" && $("#"+id+' option:selected').text()=='Variety') && ($('#company').val()=="" || $('#thickness').val()=="" || $('#range').val()=="" || $('#feel').val()=="" || $('#hsn_sac').val()=="" || $('#description').val()=="" || $('#features').val()==""))
			inval_location=true;
		
		if(id=="location_id" && $("#"+id+' option:selected').text()=='Size' && $('#group').val()=="")
			inval_location=true;

		if($("#status").val()=="edit" || $("#status").val()=="add"){
			if(location=="")
				inval_location=true;
		}else{
			for(var i=0;i<hidden;i++){
				var city=$("#city"+i).val().trim();
				var email=$("#email"+i).val().trim();
				if(location==""  || location=="Select circle" || city=="")
					inval_location=true;
			}
		}
		if(inval_location==true){
			$(".error").html(EMPTY_ERROR);
			$(".error").css('display','inline-block');
			$(".modal").scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}
		var flag=check_field_exist(null,"location");
		if(flag!=false){
			$(".error").html(flag);
			$(".error").css('display','inline-block');
   			$(".modal").scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}
		//return false;

	}

/* script for validating gallary form*/
	function validate_gallery(){
		var title=$("#title").val().trim();
		var gallery_image=$("#gallery_image").val();
		var image_height=$('#gallery_height').val().trim();
		var image_width=$('#gallery_width').val().trim();
		var size=$('#gallery_size').val().trim();
		var gallery_file=$('#gallery_image').val();
		gallery_file= gallery_file.split(".").pop();
		if(gallery_file!=""){
			if(gallery_file!="png" && gallery_file!="gif" && gallery_file!="jpg" && gallery_file!="jpeg")
				gallery_file=true;
		}
		if(title=="" || gallery_image==""){
			$(".error").html(EMPTY_ERROR);
			$(".error").css('display','inline-block');
   			$(".modal").scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}else	if(gallery_file==true){
			$(".error").html(INVALID_FILE);
			$(".error").css('display','inline-block');
  			 $(".modal").scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		}
		return true;
		/* else if(image_width!=970){
			if(image_width!=""){
				$(".error").html(GALLERY_DIMENSION);
				$(".error").css('display','inline-block');
    			$(".modal").scrollTop(0);
				$('.error').delay(TIMEOUT).fadeOut('slow');
				return false;
			}
		}
		else	if(size>256000){
			$(".error").html(IMAGE_SIZE);
			$(".error").css('display','inline-block');
   			$(".modal").scrollTop(0);
			$('.error').delay(TIMEOUT).fadeOut('slow');
			return false;
		} */
		/* if($('#gallery_size').val()==""){
		$.ajax({
			type: "POST",
			url: SITE_URL+"save_gallery",
			dataType:"json",
			data: {
				img_id: $('#img_id').val(),
				gallery_image: $('#gallery_image').val(),
				title:$('#title').val()
			},
			success: function(data){
				$('#gallery_thumb').attr('src',SITE_URL+'images/970x380.jpg').width(388).height(152);
				$("#gallery_file,#title,#image_src,#gallery_image,#gallery_size").val('');
				$('.addbtns').prop('disabled',true);
				$('.addbtns').html('Add');
				get_details($('#model_id').val(),'gallery_edit');
			}
		});
		} */
	}

	function delete_gallery(){
		var div_num=$('#sr_number').val();
		var img_id=$('#img_id').val();
		var img_name=$('#img_name').val();
		$('.new'+div_num).remove();
		$.ajax({
			type: "POST",
			url: SITE_URL+"delete_gallery",
			dataType:"json",
			data: {
				img_id: img_id,
				img_name: img_name
			},
			async: false,
			success: function(msg) {
			}
		});
		$('#gallery_file').prop('disabled',false);
		$('#gallery_thumb').attr('src',SITE_URL+'images/970x380.jpg').width(388).height(152);
		$('#title,#image_src,#gallery_image,#gallery_file,#sr_number,#img_id,#img_name,#gallery_size').val('');
		$('.addbtns').show();
		$('.delbtns').prop("disabled",true);
		$('.clearbtns').prop("disabled",true);
		$('.addbtns').prop("disabled",true);
		$('.addbtns').html("Add");
	}
/* script for fetching city name from db used in select box*/
	function getcity(val,ids){
		if(ids!="sort")
		var selected_city=$("#selected_city").val().trim();
		var sel="";
		$.ajax({
			type: "POST",
			url: SITE_URL+"findcity",
			dataType:"json",
			data: {
				id: val
			},
			async: false,
			success: function(msg) {
				if(ids=="sort"){
				save_order('location');
					if(msg!=""){
						var html3="";
						for(var i=0;i<msg.length;i++){
							html3+="<tr style='width:500px !important;' class='active'><td>"+msg[i]['city_name']+"</td><td class='sort_ids' style='display:none;'>"+msg[i]['city_id']+"</td></tr>";
						}
						$(".location_modal-body-sort").empty().append(html3);
					//	$.getScript(SITE_URL+"js/jquery-ui.js").done(function (script, textStatus) {
								$('#location_sortable tbody').sortable();
						//});
					}else
						$(".location_modal-body-sort").empty();
				}
				else{
					if(msg!=""){
						var html ="<option value='' >Select location</option>";
						for(var i=0;i<msg.length;i++){
							if(selected_city!="" && selected_city==msg[i]['city_id']){
								sel="selected";
							}
							html +="<option "+sel+" value='" + msg[i]['city_id'] + "'>" + msg[i]['city_name'] + "</option>";
							sel="";
						}
						$("#city_id").empty().append(html);
					}
					else
						$("#city_id").empty();
				}
			}
		});
	}

	function getlocation(sort){
		if(sort=="sort"){
			$('#circle_it,#me').addClass('active');
			$('#location_it,#gp').removeClass('active');
			$(".location_modal-body-sort").empty();
		}
		$.ajax({
			type: "POST",
			url: SITE_URL+"get-location",
			dataType:"json",
			async: false,
			success: function(msg) {
			if(msg!=""){
				var html="<option>Select master</option>";
				var html2="";
				for(var i=0;i<msg.length;i++){
					html +="<option value=" + msg[i]['location_id'] + ">" + msg[i]['location_name'] + "</option>";
					html2+="<tr style='width:500px !important;' class='active'><td>"+msg[i]['location_name']+"</td><td class='sort_ids' style='display:none;'>"+msg[i]['location_id']+"</td></tr>";
				}
				$("#location_id,#sort_location").empty().append(html);
				if(sort=="sort"){
					$(".circle_modal-body-sort").empty().append(html2);
				//	$.getScript(SITE_URL+"js/jquery-ui.js").done(function (script, textStatus) {
							$('#circle_sortable tbody').sortable();
						$(".sortmsg_hide").alert('close');
					//});
				}
			} $('.selectpicker').selectpicker('refresh');
			}
		});
	}
/* script for confirm delete popup */
	function confirmdelete(id,page){
		if(page=="location_delete"){
			var str=id.toString();
			id =str.replace(".","/");
		}
		$('.deleting').click(function(){
			window.location=SITE_URL+page+"/"+id;
		});
	}

function edit_loc(id,loc,page){
	$('.circle_txt').show();
	$('.circle_sel').hide();
	if(page=="circle_edit"){
		$('.locationpage').html('Edit Mater '+"<div style='width:240px;' class='error master' align='center'></div>");
		$("#city0").css("display",'none');
		$("#email0").css("display",'none');
		$("#add0").css("display",'none');
		$('.delnext1').nextAll('div').hide();
		$("#location").attr("readonly",false);
		$("#status").val("edit");
		$("#location,#hdnlocation").val(loc);
		$("#hidden_loc").val(id);
	}else if(page=="location_edit"){
		$('#company').next().hide();
		$('.locationpage').html('Edit Master Data'+"<div style='width:180px;' class='error master' align='center'></div>");
		$("#city0").css("display",'inline-block');
		$("#email0").css("display",'inline-block');
		$("#add0").css("display",'inline-block');
		$('.delnext1').nextAll('div').show();
		$('.delnext2').nextAll('div').remove();
		$('#hidden').val(1);
		$("#email0").val("");
		$("#city0").val("");
		$("#location").attr("readonly",false);
		$("#status").val("edit_location");
		$("#location,#hdnlocation").val(loc);
		$("#hidden_loc").val(id);
	}
}

function get_details(id,page,alternative = ''){
		$(".active").removeClass('active');
		$("#ac").addClass('active');
		$("#pageadd").addClass('active');
		$("#billing").addClass('active');
		$("#city0").css("display",'inline-block');
		$("#email0").css("display",'inline-block');
		$("#add0").css("display",'inline-block');
		$("#location").attr("readonly",true);
		$("#status").val("");
		$("#row_count,#actual_total").val(0);
		$('#billaddlist tbody').empty();
		$('#billaddlist').hide();
		var page_name = '';
		if(page=="gallery_edit") {
			var url = $(location).attr('href');
			url = url.split('/');
			url = url.pop();
			$('#page_name').val(url);
			page_name = '/'+url;
		}
		if(page=="model_edit"){
			$('#gp').hide();
		}
		
		$.ajax({
			type: "POST",
			url:SITE_URL+page+"/"+id+page_name,
			dataType:"json",
			async: false,
			success: function(msg) {
				if(page=="gallery_edit"){
					$('.addbtns').html('Add');
					$('.addbtns').show();
					$('.addbtns').prop("disabled",true);
					$('.delbtns').prop("disabled",true);
					$('.clearbtns').prop("disabled",true);
					$('#gallery_file').prop('disabled',false);
					$('#gallery_thumb').attr('src',SITE_URL+'images/970x380.jpg').width(388).height(152);
					$('#title,#image_src,#gallery_image,#gallery_file,#img_id,#img_name,#sr_number').val('');
					$('#model_id').val(id);
					var count=1;
					$('.preview_container').html('');
					if(msg!=null){
						for(var i=0;i<msg.length;i++){
							var abc="pre"+count;
							var img_name=msg[i]['gallery_image'];
							var tabcont_1=""
							tabcont_1 += '<div id="div1" class="cont new'+count+'" >';
							tabcont_1 += '<div class="label'+count+'" id="div2">'+msg[i]['gallery_title'];
							tabcont_1 += '</div>';
							tabcont_1 += '<div class="imageholder">';
							tabcont_1 += '<img onclick="edit_img('+count+','+msg[i]['gallery_id']+',\''+img_name+'\')" class=jslghtbx-thmb id="pre'+count+'" src="" >';
							tabcont_1 += '</div>';
							tabcont_1 += '</div>';
							$('.preview_container').append(tabcont_1);
							$('#pre'+count).attr('src',SITE_URL+DIR_THUMB+msg[i]['gallery_image']).width(111.78).height(46.22);
							count++;
						}
					}
					$('.preview_container').show();
				}
				if(page=="model_edit"){
					$('.thickness').hide();
					$('#img_temp,#bro_temp,#model_thickness_text').val('');
					$('#pageadd').find('select').each(function(){
						$(this).val(msg[0][this.id]);
					});

					get_category(msg[0]['model_company'],msg[0]['model_variety']);
					$('#model_variety').trigger('change');
					$('#model_thickness').val(msg[0]['model_thickness']);
					$('#model_feel').val(msg[0]['model_feel']);
					$('#model_range').val(msg[0]['model_range']);
					$('#model_HSN_SAC').val(msg[0]['model_HSN_SAC']);
					$('#model_make').val(msg[0]['model_make']);
					$('#model_name').val(msg[0]['model_name']);
					$('#model_warranty').val(msg[0]['model_warranty']);
					$('#model_name_xml').val(msg[0]['model_name']);
					$('#model_price').val(msg[0]['model_price']);	
					$('#model_image').val(msg[0]['model_image']);
					$('#model_group').val(msg[0]['model_group']);
					$('#model_group').trigger('change');
					$('#model_size').val(msg[0]['model_size']);
					if(msg[0]['model_thickness_text']!= ''){
						$('#model_thickness_text').val(msg[0]['model_thickness_text']);
						$('.thickness').show();
					}
					$('#brochure_name').val(msg[0]['brochure']);
					$('#model_quantity').val(msg[0]['model_quantity']);					
					$('#modelthumb').attr("src",SITE_URL+DIR_THUMB+msg[0]['model_image']);
					$('#model_caption').val(msg[0]['model_caption']);
					$('#model_features').val(msg[0]['model_features']);
					tinyMCE.get('textarea1').setContent(msg[0]['model_overview']);
					tinyMCE.get('textarea2').setContent(msg[0]['model_details']);
					if(msg[0]['is_display']==1)
						$("#is_display").prop('checked',true);
					if(msg[0]['is_display']==0)
						$("#is_display").prop('checked',false);
					$('#txt_title').val(msg[0]['seo_title']);
					$('#seoinp').val(msg[0]['seo_keyword']);
					$('#seo_details').val(msg[0]['seo_details']);
					$('#hidden').val(msg[0]['model_id']);
					set_char('txt_title','minchar1',70);
					set_char('seoinp','minchar2',155);
					set_char('seo_details','minchar3',155);
				}
				if(page=="get_services"){
					$('.thumb1').attr('src',SITE_URL+'images/920x270.png');
					$('.thumb2').attr('src',SITE_URL+'images/1290x270.png');
					$('#logothumb').attr('src',SITE_URL+'images/230x115.png');
					$('#is_display').val(msg[0]['is_display']);
					if(msg[0]['non_deletable']==1)
						$("#title").attr("readonly",true);
					else
						$("#title").attr("readonly",false);
					$('#title').val(msg[0]['service_title']);
					$('#title_xml').val(msg[0]['service_title']);
					$('#image').val(msg[0]['service_image']);
					$('#caption').val(msg[0]['service_caption']);
					$('#logo').val(msg[0]['service_icon']);
					if(msg[0]['service_icon']!="")
						$('#logothumb').attr("src",SITE_URL+DIR+msg[0]['service_icon']);
					tinyMCE.get('textarea4').setContent(msg[0]['service_overview']);
					tinyMCE.get('textarea5').setContent(msg[0]['service_details']);
					$('#email').val(msg[0]['enquiry_form_email']);
					$('#subject').val(msg[0]['enquiry_form_subject']);
					$('#seoinp1').val(msg[0]['seo_title']);
					$('#seoinp2').val(msg[0]['seo_keyword']);
					$('#description').val(msg[0]['seo_details']);
					$('#hidden').val(msg[0]['service_id']);
					set_char('seoinp1','minchar1',160);
					set_char('seoinp2','minchar2',300);
					set_char('description','minchar3',256);
					if(msg[0]['service_icon']=="" || msg[0]['service_icon']==null){
						$("#bigimage").prop('checked',true);
						$('.icon').hide();
						$('.thumb1').hide();
						$('.thumb2').show();
						$('.thumb2').attr("src",SITE_URL+DIR_THUMB+msg[0]['service_image']);
					}else{
						$("#bigimage").prop('checked',false);
						$('.icon').show();
						$('.thumb1').show();
						$('.thumb2').hide();
						$('.thumb1').attr("src",SITE_URL+DIR_THUMB+msg[0]['service_image']);
					}
				}
				if(page=="location_edit"){
					$('.locationpage').html('Edit Master Data'+"<div style='width:180px;' class='error master' align='center'></div>");
					$('.circle_txt').show();
					$('.circle_sel').hide();
					$('.delnext1').nextAll('div').show();
					$('.delnext2').nextAll('div').remove();
					$('#hidden').val(1);
					$('#location,#hdnlocation').val(msg[0]['location_name']);
					$('#city0').val(msg[0]['city_name']);
					$('#email0').val(msg[0]['city_email_code']);
					$('#cit_id').val(msg[0]['city_id']);
					$('#loc_id').val(msg[0]['location_id']);
					if(msg[0]['location_name']=='Variety'){
						if(msg[0]['company']==0)
							msg[0]['company']="";
						$('#company').val(msg[0]['company']);
						$('#thickness').val(msg[0]['thickness']);
						$('#make').val(msg[0]['make']);
						$('#range').val(msg[0]['range']);
						$('#feel').val(msg[0]['feel']);
						$('#hsn_sac').val(msg[0]['hsn_sac']);
						$('#features').val(msg[0]['features']);
						$('#description').val(msg[0]['description']);
						$('#warranty').val(msg[0]['warranty']);
						$('#company,#thickness,#make,#range,#feel,#hsn_sac').next().show();
						$('#features,#description,#warranty').show();
						$('#group').next().hide();
						$('#group').hide();
					} else if (msg[0]['location_name']=='Size') {
						$('#group').val(msg[0]['group']);
						$('#company,#thickness,#make,#range,#feel,#hsn_sac').val('').next().hide();
						$('#features,#description,#warranty').val('').hide();
						$('#group').next().show();
						$('#group').show();
					} else {
						$('#company,#group,#thickness,#make,#range,#feel,#hsn_sac').val('').next().hide();
						$('#features,#group,#description,#warranty').val('').hide();
					}
				}
				if(page=="billing_edit"){
					if (alternative=="print_view") {
						$('#gp a').click();
						$('.new_billing').hide();
						$('.purchase_his').show();
					} else {
						$('.new_billing,.purchase_his').show();
					}
					$('#myPager').empty();
					$('select,#VAT_rate,#price,#quantity').val("");
					$('select').selectpicker('refresh');
					$('#billing_name').val(msg[0]['billing_name']);
					$("#location_id").val(msg[0]['location_id']);
					$('#billing_address').val(msg[0]['billing_address']);
					$('#billing_phone').val(msg[0]['billing_phone']);
					$('#billing_email').val(msg[0]['billing_email']);
					$('#customer_GSTIN').val(msg[0]['customer_GSTIN']);
					$('#billing_notes').val(msg[0]['billing_notes']);
					$('#hidden').val(msg[0]['billing_id']);
					$('#billing_date,#delivery_date,#discount').val('');
					$('#billing_date').val(moment(msg[0]['dt_created']).format('DD-MM-YYYY'));
					$('#balance').val(msg[0]['balance']);
					var htm="";var total=0;
					for(var k=0;k<msg.length;k++){
						var invoice_id = msg[k]['dt_created'].replace(":", "");
						invoice_id = invoice_id.replace("-", "");
						invoice_id = invoice_id.replace("-", "");
						invoice_id = invoice_id.replace(":", "");
						invoice_id = invoice_id.replace(" ", "");
						total=(total+parseFloat(msg[k]['amount']));
						htm+="<tr class='bill_tr"+k+"'><td><input class='bill_order bill_td"+k+"' type='checkbox' id='"+msg[k]['id']+"'></td><td onclick='check_checkbox2("+msg[k]['id']+");'>"+invoice_id+"</td><td onclick='check_checkbox2("+msg[k]['id']+");'>"+msg[k]['HSN_SAC']+"</td>";
						htm+="<td onclick='check_checkbox2("+msg[k]['id']+");'>"+msg[k]['model']+"</td>";
						htm+="<td onclick='check_checkbox2("+msg[k]['id']+");'>"+msg[k]['location']+"</td>";
						htm+="<td onclick='check_checkbox2("+msg[k]['id']+");'>"+msg[k]['billing_date']+"</td>";
						htm+="<td onclick='check_checkbox2("+msg[k]['id']+");'>"+msg[k]['delivery_date']+"</td>";
						htm+="<td onclick='check_checkbox2("+msg[k]['id']+");'>"+msg[k]['invoice_advance']+"</td>";
						htm+="<td onclick='check_checkbox2("+msg[k]['id']+");'>"+msg[k]['quantity']+"</td>";
						htm+="<td onclick='check_checkbox2("+msg[k]['id']+");'>"+msg[k]['price']+"</td>";
						htm+="<td onclick='check_checkbox2("+msg[k]['id']+");'>"+msg[k]['discount']+"</td>";
						htm+="<td onclick='check_checkbox2("+msg[k]['id']+");'>"+msg[k]['taxable_value']+"</td>";
						htm+="<td onclick='check_checkbox2("+msg[k]['id']+");'>"+msg[k]['gst_rate']+"</td>";
						htm+="<td class='align-rit' onclick='check_checkbox2("+msg[k]['id']+");'>"+msg[k]['gst_amt']+"</td>";
						htm+="<td onclick='check_checkbox2("+msg[k]['id']+");'>"+msg[k]['gst_rate']+"</td>";
						htm+="<td class='align-rit' onclick='check_checkbox2("+msg[k]['id']+");'>"+msg[k]['gst_amt']+"</td>";
						htm+="<td class='align-rit' onclick='check_checkbox2("+msg[k]['id']+");'>"+msg[k]['amount']+"</td></tr>";
						$('#purchase_history tbody').html(htm);
					}
					$('#purchase_total').html(total.toFixed(2));
 					if ($('#myTable').length){
 						$('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:4});		
					}				
				}
				$('.selectpicker').selectpicker('refresh');
			}
		});
	}

/* script for setting  title value to caption and seo title  in services form*/
	function set_titles() {
		$('#caption').val($('#title').val().trim());
		$('#seoinp1').val($('#title').val().trim());
	}

/* script for displaying the number of characters that can be entered while typing in textarea*/
	function set_char(id,lable,max){
		var val=$('#'+id).val();
		$("#"+lable).html((max - val.length)+"/"+max);
	}

	$(function() {
		$('.required-icon').tooltip({
			placement: 'left',
			title: 'Required field'
		});
	});

	$(function () {
		$('.selectpicker').selectpicker();
		$('.bootstrap-select').css('width','100%');
		$('[data-toggle="tooltip"],[data-toggle="tooltip"] + .bootstrap-select > button').tooltip();
	});

	function filter_lat_long(cont,id){
		cont=$.trim(cont);
		var index = cont.indexOf(",");
		if(index!=-1){
		var lat = cont.substr(0, index);
		var lon = cont.substr(index + 1);
		$('#google_latitude').val(lat);
		$('#google_longitude').val(lon);
		}
	}

	function edit_img(count,id,name) {
		$('.clearbtns').prop('disabled',false);
		$('#title').val($('.label'+count).text());
		$('#gallery_thumb').attr("src",SITE_URL+GALLERY+name);
		$('#img_id').val(id);
		$('#img_name').val(name);
		$('#gallery_image').val(name);
		$('#sr_number').val(count);
		$('.delbtns').prop("disabled",false);
		$('.addbtns').prop("disabled",true);
		$('.addbtns').html("Save");
		$('#gallery_size').val('');
	}

	function clear_image(){
		$('#gallery_size,#img_id,#img_name,#sr_number,#title,#gallery_image').val('');
		$('#gallery_thumb').attr("src",SITE_URL+'images/970x380.jpg').width(388).height(152);
		$('#gallery_file').prop('disabled',false);
		$('.addbtns').show();
		$('.delbtns').prop("disabled",true);
		$('.addbtns').prop('disabled',true);
		$('.clearbtns').prop('disabled',true);
		$('.addbtns').html("Add");
	}

	function set_gallery_name(name){
		$('.gallery_name h3').html('Gallery - '+name+"<div style='width:250px;' class='error gal' align='center'></div>");
	}

	function check_field_exist(name,page){
		var id="";var url="";var as="";
		if(page=="models"){
			id=$('#hidden').val();
			url="check_model_name";
			as=ajaxfor_check_field_exist(name,url,id,page);
		}else	if(page=="location"){
			if(as==""){
				if($('#location').prop("readonly") == false){
					id=$('#hidden_loc').val();
					url="check_circle_name";
					name=$('#location').val();
					if (name != '')
					as=ajaxfor_check_field_exist(name,url,id,page,"loc");
				}
			}
			if(as==""){
				if($(".delnext2").is(':visible')){
					url="check_location_name";
					id=$('#cit_id').val();
					name=$('#city0').val();
				as=ajaxfor_check_field_exist(name,url,id,page,"cit");
				}
			}
		}
		return as;
	}

	function ajaxfor_check_field_exist(name,url,id,page,status){
		var flag=false; var location_id="";
		if(status=='cit' && $("#location").is(':visible'))
			location_id=$('#loc_id').val();
		else if(status=='cit' && $("#location").is(':hidden'))
			location_id=$('#location_id').val();
		$.ajax({
			type: "POST",
			url: SITE_URL+url,
			dataType:"json",
			data: {
				name: name,
				location_id: location_id
			},
			async: false,
			success: function(msg) {
				if(page=="models"){
					if(msg!="" && msg[0]['model_id']!="" && msg[0]['model_id']!=id)
					flag=true;
				}
				if(page=="location"){
					if(status=="loc"){
						if(msg!="" && msg[0]['location_id']!="" && msg[0]['location_id']!=id){
							flag=CIRCLE_EXIST;
						}
					}
					if(status=="cit"){
						if(msg!="" && msg[0]['city_id']!="" && msg[0]['city_id']!=id){
							flag=CITY_EXIST;
						}
					}
				}
			}
		});
		return flag;
	}
/* for pages*/
	function check_checkbox(id){
		if(id=="bigimage" && $('#'+id).is(':checked'))
			$('#'+id).prop("checked", false);
		else
			$('#'+id).prop("checked", true);
	}
/* for dashboard to check/uncheck checkboxes*/
	function check_checkboxes(classes,id){
		if($('#'+id).is(':checked'))
			$('.'+classes).prop("checked", true);
		else
			$('.'+classes).prop("checked", false);
	}
/* for dashboard to check/uncheck checkboxes*/
	function delete_statistic_report(){
		if(validate_report()){
		$.ajax({
			type: "POST",
			dataType : 'json',
			url: SITE_URL + "delete_statistic_report",
			data:{todate:$('#todate').val(),},
			success: function(msg) {
				$('#statistic_model,.modal-backdrop').hide();
				if(msg!=0){
					$(".green_message").show().html(RECORDS_DELETED).delay(2500).hide(10);
					$('#available_records').html(AVAILABLE_DATA+msg['date_created']);
					set_datepickers(msg['start_date'],msg['end_date']);
					$('#table').bootstrapTable('refresh');
				}else{
					$(".red_message").show().html(RECORDS_NOT_DELETED).delay(2500).hide(10);
				}
			}
		});
		}
	}
/* for dashboard */
	function validate_report(){
		var isValid = true;
		if($('#todate').val()==""){
			isValid = false;
			$(".datepicker").addClass('error-display');
		}
		return isValid;
	}
/* for billing,dashboard and models page*/
	function check_checkbox2(id){
			if($('#'+id).is(':checked'))
				$('#'+id).prop("checked", false);
			else
				$('#'+id).prop("checked", true);
	}

	function get_sorted_pages(page){
		var url="";
		$(".active").removeClass('active');
		$("#me").addClass('active');
		if(page=="services"){
			url=SITE_URL+"services-get_sorted_pages";
			$("#sales_it").addClass('active');
		}else if(page=="models"){
			url=SITE_URL+"models-get_sorted_pages";
			$("#Hatchbacks").addClass('active');
		}
		$.ajax({
			type: "POST",
			url: url,
			dataType:"json",
			async: false,
			success: function(msg) {
				if(page=="services"){
					var html="";
					for(var i=0;i<msg['menu'].length;i++){
						html+="<tr style='width:500px !important;' class='active'><td>"+msg['menu'][i]['service_title']+"</td><td class='sort_ids' style='display:none;'>"+msg['menu'][i]['service_id']+"</td></tr>";
					}
					var html1="";
					for(var i=0;i<msg['sales'].length;i++){
						html1+="<tr style='width:500px !important;' class='active'><td>"+msg['sales'][i]['service_title']+"</td><td class='sort_ids' style='display:none;'>"+msg['sales'][i]['service_id']+"</td></tr>";
					}
					var html2="";
					for(var i=0;i<msg['insurance'].length;i++){
						html2+="<tr style='width:500px !important;' class='active'><td>"+msg['insurance'][i]['service_title']+"</td><td class='sort_ids' style='display:none;'>"+msg['insurance'][i]['service_id']+"</td></tr>";
					}
					var html3="";
					for(var i=0;i<msg['footer'].length;i++){
						html3+="<tr style='width:500px !important;' class='active'><td>"+msg['footer'][i]['service_title']+"</td><td class='sort_ids' style='display:none;'>"+msg['footer'][i]['service_id']+"</td></tr>";
					}
					$(".menu_modal-body-sort").empty().append(html1);
					$(".sales_modal-body-sort").empty().append(html);
					$(".insurance_modal-body-sort").empty().append(html2);
					$(".footer_modal-body-sort").empty().append(html3);
				}else if(page=="models"){
					$.each(CATEGORY, function( index, value ) {
						var html="";
						for(var i=0;i<msg[value].length;i++){
							html+="<tr class='active'><td>"+msg[value][i]['model_name']+"</td><td class='sort_ids' style='display:none;'>"+msg[value][i]['model_id']+"</td></tr>";
						}
					$("."+value+"_modal-body-sort").empty().append(html);
					});
				}
			}
		});
		//$.getScript(SITE_URL+"js/jquery-ui.js").done(function (script, textStatus) {
			if(page=="services"){
				$('#menu_sortable tbody').sortable();
				$('#sales_sortable tbody').sortable();
				$('#insurance_sortable tbody').sortable();
				$('#footer_sortable tbody').sortable();
			}else if(page=="models"){
				$.each(CATEGORY, function( index, value ) {
					$('#'+value+"_sortable tbody").sortable();
				});
			}
			$(".sortmsg_hide").alert('close');
		//});
	}

	function make_spinner(){
		var opts = {
			lines: 11 // The number of lines to draw
			, length: 23 // The length of each line
			, width: 11 // The line thickness
			, radius: 20 // The radius of the inner circle
			, scale: 0.50 // Scales overall size of the spinner
			, corners: 1 // Corner roundness (0..1)
			, color: '#FFF' // #rgb or #rrggbb or array of colors
			, opacity: 0.25 // Opacity of the lines
			, rotate: 0 // The rotation offset
			, direction: 1 // 1: clockwise, -1: counterclockwise
			, speed: 1 // Rounds per second
			, trail: 60 // Afterglow percentage
			, fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
			, zIndex: 2e9 // The z-index (defaults to 2000000000)
			, className: 'spinner' // The CSS class to assign to the spinner
			, top: '50%' // Top position relative to parent
			, left: '50%' // Left position relative to parent
			, shadow: false // Whether to render a shadow
			, hwaccel: false // Whether to use hardware acceleration
			, position: 'absolute' // Element positioning
		}
		var target = document.getElementById('spinner');
		var spinner = new Spinner(opts).spin(target);
		return spinner;
	}

	function changepassword(id) {
		if($("#lpassword").val().trim()=="" || $("#ccpassword").val().trim()==""){
			$(".perror").html(EMPTY_ERROR);
			$(".perror").show();
   $(".modal").scrollTop(0);
			$('.perror').delay(TIMEOUT).fadeOut('slow');
		}else if ($("#lpassword").val().trim() != $("#ccpassword").val().trim()){
			$(".perror").html(UNMATCH_PASS);
			$(".perror").show();
   $(".modal").scrollTop(0);
			$('.perror').delay(TIMEOUT).fadeOut('slow');
		}else {
			var password = $("#lpassword").val();
			password = md5(password);
			$.ajax({
				url: SITE_URL + "changepassword",
				type: 'POST',
				data: {
					user_id: id,
					password: password
				},
				dataType: "json",
				async: false,
				success: function(data) {
					$('#chupdate').attr("data-dismiss", "modal");
				}
			});
		}
	}

	function delfiles(){
		var img_path=$('#img_temp').val();
		var bro_path=$('#bro_temp').val();
		if(img_path!="")
			img_path=img_path+$('#model_image').val();
		if(bro_path!="")
			bro_path=bro_path+$('#brochure_name').val();
			$.ajax({
				url: SITE_URL + "delfiles",
				type: 'POST',
				data: {
					img_path: img_path,
					bro_path: bro_path
				},
				dataType: "json",
				async: false,
				success: function(data) {
				}
			});
	}

	function enable_gal_btn(){
		if($('.delbtns').prop('disabled')==false)
			$('.addbtns').prop('disabled',false);
	}


	function save_order(page){
		var url="";
		if(page=="services"){
			url=SITE_URL+"services-change_order";
			var menus = []
			jQuery.each(jQuery('#menu_sortable tr'), function(i,e ) {
				menus.push(jQuery('.sort_ids', e).map(function(i,e) {
					return e.innerHTML;
				}).get());
			});
			var sales = []
			jQuery.each(jQuery('#sales_sortable tr'), function(i,e ) {
				sales.push(jQuery('.sort_ids', e).map(function(i,e) {
					return e.innerHTML;
				}).get());
			});
			var insurance = []
			jQuery.each(jQuery('#insurance_sortable tr'), function(i,e ) {
				insurance.push(jQuery('.sort_ids', e).map(function(i,e) {
					return e.innerHTML;
				}).get());
			});
			var footer = []
			jQuery.each(jQuery('#footer_sortable tr'), function(i,e ) {
				footer.push(jQuery('.sort_ids', e).map(function(i,e) {
					return e.innerHTML;
				}).get());
			});
		}else if(page=="models"){
			url=SITE_URL+"models-change_order";
				var models={};
			$.each(CATEGORY, function( index, value ) {
				var model_val=new Array();
				jQuery.each(jQuery('#'+value+'_sortable tr'), function(i,e ) {
					model_val.push(jQuery('.sort_ids', e).map(function(i,e) {
						return e.innerHTML;
					}).get());
				});
				models[value]=model_val;
			});
		}else if(page=="location"){
			url=SITE_URL+"location-change_order";
			var location = []
			jQuery.each(jQuery('#location_sortable tr'), function(i,e ) {
				location.push(jQuery('.sort_ids', e).map(function(i,e) {
					return e.innerHTML;
				}).get());
			});
			var circle = []
			jQuery.each(jQuery('#circle_sortable tr'), function(i,e ) {
				circle.push(jQuery('.sort_ids', e).map(function(i,e) {
					return e.innerHTML;
				}).get());
			});
		}
		$.ajax({
			type: "POST",
			url: url,
			dataType:"json",
			data: {
				menus: menus,
				sales: sales,
				insurance: insurance,
				footer:footer,
				models:models,
				location:location,
				circle:circle
			},
			async: false,
			success: function(msg) {
				if(msg==true)
					$('#service_view').submit();
			}
		});
		
	}
/*   latest code   */
	function generate_modelname(){
		if(($('#model_variety').val()!="") && $('#model_size').val()!=""){
			var size = $('#model_size option:selected').text().trim();
			size = size.split(' ');
			if($('#model_thickness option:selected').text() == 'Other')
			$('#model_name').val(size[0]+' '+$('#model_thickness_text').val().trim()+' '+$('#model_variety option:selected').text().trim());
			else
			$('#model_name').val(size[0]+' '+($('#model_thickness option:selected').text().trim() != 'Select thickness' ? ' '+$('#model_thickness option:selected').text().trim() : '')+' '+$('#model_variety option:selected').text().trim());
		}else
			$('#model_name').val('');
	}
	function showhide_filter(){
		if($('#fiter_tools').is(':hidden')==true)
			$('#fiter_tools,#srt_apply').fadeIn("slow");
		else	
			$('#fiter_tools').fadeOut("slow");
	}
	function srt_model_list(){
			$('#sortmodal').submit();	
	}	
	function master_attr(){
		if($('#location_id option:selected').text()=='Variety') {
			$('#warranty,#features,#description').show();	
			$('#company,#thickness,#range,#feel,#hsn_sac,#make').next().show();
			$('#group').hide();	
			$('#group').next().hide();
		} else if ($('#location_id option:selected').text()=='Size') {
			$('#warranty,#features,#description').hide();	
			$('#company,#thickness,#range,#feel,#hsn_sac,#make').next().hide();
			$('#group').show();	
			$('#group').next().show();
		}	else	{
			$('#warranty,#group,#features,#description').hide();	
			$('#company,#thickness,#range,#group,#feel,#hsn_sac,#make').next().hide();
		}
	}
	function get_category(company,sel,sort_pop){
    //var spinner = make_spinner();
  //$('.modal.bs-example-modal-lg').css('z-index','9');
		$.ajax({
			type: "POST",
			async:false,
			url: SITE_URL+"get_category",
			dataType:"json",
			data: {
				company: company,
			},
			success: function(msg){
				var html ="<option value='' >Select variety</option>";
				var selected="";
				for(var i=0;i<msg.length;i++){
					if(sel==msg[i]['city_id'])
						selected="selected";
					else
						selected="";
					html +="<option "+selected+" value='" + msg[i]['city_id'] + "'>" + msg[i]['city_name'] + "</option>";
				}
				$("#model_variety,#srt_variety").empty().append(html);
				$("#model_variety,#srt_variety").selectpicker('refresh');
			}
		});
	}

	function get_sizes_groupwise(group){
		//var spinner = make_spinner();
	  //$('.modal.bs-example-modal-lg').css('z-index','9');
			$.ajax({
				type: "POST",
				async:false,
				url: SITE_URL+"get_sizes_groupwise",
				dataType:"json",
				data: {
					group: group,
				},
				success: function(msg){
					var html ="<option value='' >Select size</option>";
					var selected="";
					for(var i=0;i<msg.length;i++){
						html +="<option "+selected+" value='" + msg[i]['city_id'] + "'>" + msg[i]['city_name'] + "</option>";
					}
					$("#model_size").empty().append(html);
					$("#model_size").selectpicker('refresh');
				}
			});
		}

	function get_master_data(id){
		//var spinner = make_spinner();
		//$('.modal.bs-example-modal-lg').css('z-index','9');
		var categories = new Array("Thickness", "Range", "Feel", "Make", "HSN_SAC");
		$.ajax({
			type: "POST",
			async:false,
			url: SITE_URL+"get_master_data",
			dataType:"json",
			data: {
				id: id,
			},
			success: function(msg){
				$.each( categories, function( key, value ) {
					(msg[0][value.toLowerCase()] == null ? msg[0][value.toLowerCase()] = '' : '');
					var html = '';
					$.each( msg.masters[value], function( c, d ) {
						if (msg[0][value.toLowerCase()] == false || msg[0][value.toLowerCase()] != '') {
							if (msg[0][value.toLowerCase()] == d.city_id || $.inArray(d.city_id,msg[0][value.toLowerCase()]) != -1)
								html += '<option  value='+d.city_id+'>'+d.city_name+'</option>';
						}
					});
					$("#model_"+value.toLowerCase()+",#model_"+value).html(html);	
					$("#model_"+value.toLowerCase()+",#model_"+value).selectpicker('refresh');
					if ($("#model_"+value.toLowerCase()+" option").length == 2 || $("#model_"+value+" option").length == 2 )
						$("#model_"+value.toLowerCase()+" option:eq(1),#model_"+value+" option:eq(1)").attr('selected','selected');

				});
				$("#model_features").val(msg[0]['features']);
				$("#model_warranty").val(msg[0]['warranty']);
				if(msg[0]['description'] != null) {
					tinyMCE.get('textarea1').setContent(msg[0]['description']);
					tinyMCE.get('textarea2').setContent(msg[0]['description']);
				}
				$('.selectpicker').selectpicker('refresh');
			}
		});
	}	
	function show_hide_other(){
// 		if(($('#size option:selected').text()=='Other') || ($('#model_thickness option:selected').text()=='Other')){
// 			$('.others').show();
// 			if($('#size option:selected').text()=='Other')
// 				$('.other_size').show();
// 			else{
// 				$('.other_size').hide();
// 				$('.other_size').val('');
// 			}if($('#model_thickness option:selected').text()=='Other')
// 				$('.other_model_inches').show();
// 			else{
// 				$('.other_model_inches').hide();
// 				$('.other_model_inches').val('');
// 			}
// 		}else{
// 			$('.others').hide();
// 			$('.other_model_inches').val('');
// 			$('.other_size').val('');
// 		}	
	}
  var total_qty = 0;
  var total_rate = 0;
  var total_discount = 0;
  var total_tv = 0;
  var total_gst = 0;


	function billaddlist(){
		var quantity=$('#quantity').val().trim();
		var price=$('#price').val().trim();
		var model_name=$('#model_name').val();
		var discount =$('#discount').val().trim();
		var dup = true;
		var advance=$('#advance').val().trim();
		var balance=$('#balance').val().trim();
		var model_HSN_SAC = $('#model_name option:selected').attr('data-hsn');
    	var model_HSN_SAC_text = $('#model_name option:selected').attr('data-hsn-text');
		var model_text = $('#model_name option:selected').text();
		var html=$('#billaddlist tbody').html();
		var len=$('#row_count').val();
		var loc = $('#model_location option:selected').val();
		var billing_date=$("#billing_date").val().trim();
		var delivery_date=$("#delivery_date").val().trim();
		if(delivery_date!="" && delivery_date!="" && model_name!="" && quantity!="" && price!="" && loc != "" && !isNaN(quantity) && !isNaN(price)){
			$("#billaddlist tbody tr").each(function(tr) {
				if ($('[name ^=model_'+this.id+']').val().trim() == model_text.trim()) {
					$(".modal").scrollTop(0);
					$(".error").html(DUPLICATE_MODEL);
					$(".error").css('display','inline-block');
					$(".modal").scrollTop(0);
					$('.error').delay(TIMEOUT).fadeOut('slow');
					dup = false;
				}
			});
			if (dup != true)
			return dup;
			$.ajax({
				type: "POST",
				url: SITE_URL+"check-avl-bal",
				dataType:"json",
				data: {
					model_id: model_name,
					quantity: quantity,
					model_range:$('#model_name option:selected').attr('data-range')
				},
				success: function(data){
					if(data == 1){
						price = parseFloat(price).toFixed(2);
						if(discount != "" && !isNaN(discount))
            				discount = parseFloat(discount);
						else
            				discount = 0;
						if (parseFloat(discount) > parseFloat(price)) {
							$(".modal").scrollTop(0);
							$(".error").html(DISCOUNT_GREATER);
							$(".error").css('display','inline-block');
							$('.error').delay(TIMEOUT).fadeOut('slow');
							return false;
						}

						var gst = parseFloat(model_HSN_SAC)/2;
						var total = parseFloat((quantity*(price - discount))).toFixed(2);
           				var taxable_val = (total/((parseFloat(model_HSN_SAC)+100)/100)).toFixed(2);
						var amt = ((gst/100) * taxable_val).toFixed(2);
						var btn='<a><img onclick=delrow('+len+') src='+SITE_URL+'images/delete.png alt="Remove" title="Remove" height="15px" width="15px"></a>';
						html+="<tr id='row_"+len+"'>";
						html+="<td>"+model_HSN_SAC_text+"<input type='hidden' name='HSN_SAC_row_"+len+"' value='"+model_HSN_SAC_text+"'></td>";
						html+="<td><input type='hidden' name='modelno_row_"+len+"' value='"+model_name+"'><input type='hidden' name='model_row_"+len+"' value='"+model_text+"'>"+model_text+"</td>";
						html+="<td>"+loc+"</td>";
						html+="<td><input type='hidden' name='quantity_row_"+len+"' value='"+quantity+"'>"+quantity+"</td>";
            			total_qty = total_qty + parseInt(quantity);
						$('.total_qty').html(total_qty);
						html+="<td><input type='hidden' name='price_row_"+len+"' value='"+price+"'>"+price+"</td>";
            			total_rate = total_rate + parseFloat(price);
            			$('.total_rate').html((total_rate).toFixed(2));
						html+="<td><input type='hidden' name='discount_row_"+len+"' value='"+discount+"'>"+discount.toFixed(2)+"</td>";
           				total_discount = total_discount + parseFloat(discount);
           				$('.total_discount').html((total_discount).toFixed(2));
						$('#total_dis').html((total_discount*total_qty).toFixed(2));
						html+="<td><input type='hidden' name='taxable_val_row_"+len+"' value='"+taxable_val+"'>"+taxable_val+"</td>";
						total_tv = (parseFloat(total_tv) + parseFloat(taxable_val)).toFixed(2);
						$('.total_tv').html(total_tv);
						html+="<td>"+gst+"</td>";
						total_gst = total_gst + parseFloat(amt);
						$('.total_gst').html(total_gst);
						html+="<td class='align-rit'><input type='hidden' name='gst_amount_row_"+len+"' value='"+amt+"'><input type='hidden' name='gstrate_row_"+len+"' value='"+gst+"'>"+amt+"</td>";
						html+="<td>"+gst+"</td>";
						html+="<td class='align-rit'>"+amt+"</td>";
						html+="<td>"+total+"</td>";
						html+="<td class='hide align-rit'><input type='hidden' name='amount_row_"+len+"' value='"+total+"'><input type='hidden' name='location_row_"+len+"' value='"+model_location+"'>"+btn+"</td></tr>";
						var total_amt;
						if($('#actual_total').val()!="")
							total_amt=(parseFloat(total)+parseFloat($('#actual_total').val())).toFixed(2);
						else
							total_amt=parseFloat(total).toFixed(2);
						var advance = parseFloat($('#advance').val()).toFixed(2);
						$('#total_advance').html(advance);
						$('#actual_total').val(total_amt);
						$('#total').html(total_amt);
						$('#billaddlist tbody').html(html);
						$('#model_variety').empty().html('<option value="">Select variety</option>');
						len++;
						$('#row_count').val(len);
						$('#billaddlist').show();
						//$('#model_name,#quantity,#price,#VAT_rate,#discount,#taxable_val').val('');
						///$('select').val('');
					} else if(data == 2) {
						$(".error").html(QNT_LESS);
						$(".error").css('display','inline-block');
						$(".modal").scrollTop(0);
						$('.error').delay(TIMEOUT).fadeOut('slow');
					} else if(data == 0) {
						$(".error").html(NOT_AVAILABLE);
						$(".error").css('display','inline-block');
						$(".modal").scrollTop(0);
						$('.error').delay(TIMEOUT).fadeOut('slow');
					}
					//$('.selectpicker').selectpicker('refresh');
				}
			});
		} else {
			$(".modal").scrollTop(0);
			$(".error").html(PROD_DETAILS);
			$(".error").css('display','inline-block');
			$('.error').delay(TIMEOUT).fadeOut('slow');
		}
	}
	function delrow(len){
		$('#total').html(parseFloat($('#actual_total').val()-parseFloat($('#row_'+len+' td:nth(11)').html())).toFixed(2));
		$('#actual_total').val(parseFloat($('#actual_total').val()-parseFloat($('#row_'+len+' td:nth(11)').html())).toFixed(2));
		$('#row_'+len).remove();
		if($('#billaddlist tr').length==2){
			$('#billaddlist').hide();
			$('#actual_total,#row_count').val(0);
		}
	}

	function submit_search(){
		if($('#searchbox').val()!=''){
			$('#dt_created,#bill_id,#bill_name,#bill_address,#bill_phone,#bill_email').val($('#searchbox').val());
			$('#search_form').submit();
		}
	}

	function show_hide_billbtn(id){
		if($('#'+id+' a').attr('href') == '#history'){			
			$('.billbtn').hide();
			$('.printbtn').show();
			$('.col-offsets').removeClass('col-sm-offset-3').addClass('col-sm-offset-4');
		} else {
			$('.billbtn').show();
			$('.printbtn').hide();
			$('.col-offsets').removeClass('col-sm-offset-4').addClass('col-sm-offset-3');
		}	
	}


  $.fn.pageMe = function(opts){
    var $this = this,
        defaults = {
            perPage: 7,
            showPrevNext: true,
            hidePageNumbers: true
        },
        settings = $.extend(defaults, opts);

    var listElement = $this;
    var perPage = settings.perPage;
    var children = listElement.children();
    var pager = $('.pagination');

    if (typeof settings.childSelector!="undefined") {
        children = listElement.find(settings.childSelector);
    }

    if (typeof settings.pagerSelector!="undefined") {
        pager = $(settings.pagerSelector);
    }

    var numItems = children.size();
    var numPages = Math.ceil(numItems/perPage);

    pager.data("curr",0);

    if (settings.showPrevNext){
        $('<li><a href="#" class="prev_link"><<</a></li>').appendTo(pager);
    }

    var curr = 0;
    while(numPages > curr && (settings.hidePageNumbers==false)){
        $('<li><a href="#" class="page_link">'+(curr+1)+'</a></li>').appendTo(pager);
        curr++;
    }

    if (settings.showPrevNext){
        $('<li><a href="#" class="next_link">>></a></li>').appendTo(pager);
    }

    pager.find('.page_link:first').addClass('active');
    pager.find('.prev_link').hide();
    if (numPages<=1) {
        pager.find('.next_link').hide();
    }
      pager.children().eq(1).addClass("active");

    children.hide();
    children.slice(0, perPage).show();

    pager.find('li .page_link').click(function(){
        var clickedPage = $(this).html().valueOf()-1;
        goTo(clickedPage,perPage);
		var temp_id = $(this).parent().parent();
		add_radius_class('#'+temp_id[0]['id']);
        return false;
    });
    pager.find('li .prev_link').click(function(){
        previous();
        return false;
    });
    pager.find('li .next_link').click(function(){
        next();
        return false;
    });

    function previous(){
        var goToPage = parseInt(pager.data("curr")) - 1;
        goTo(goToPage);
    }

    function next(){
        goToPage = parseInt(pager.data("curr")) + 1;
        goTo(goToPage);
    }

    function goTo(page){
        var startAt = page * perPage,
            endOn = startAt + perPage;

        children.css('display','none').slice(startAt, endOn).show();

        if (page>=1) {
            pager.find('.prev_link').show();
        }
        else {
            pager.find('.prev_link').hide();
        }

        if (page<(numPages-1)) {
            pager.find('.next_link').show();
        }
        else {
            pager.find('.next_link').hide();
        }

        pager.data("curr",page);
      	pager.children().removeClass("active");
        pager.children().eq(page+1).addClass("active");

    }
};

function remove_duplicate_models() {
	var more_html = '';
	if ($('#more_models_table tbody tr').length == 0 && $('#model_name').val() != '' && $('#hidden').val() == '') {
		$.each( sizes_arr, function( key, value ) {
			more_html += "<tr class='more_model_tr'><td>";
			more_html += "<input type='hidden' name='more_model_name[]' value="+value.city_name+">";
			more_html += "<input type='hidden' class='more_sizes' name='more_sizes[]' value="+value.city_id+">"+value.city_name+"</td>";
			more_html += "<td><input type='hidden' name='more_prices[]'><a class='myeditable editable editable-empty' data-type='text' data-original-title='Enter Price'></a></td>";
			more_html += "<td><input type='hidden' name='more_qnty[]'><a class='myeditable editable editable-empty' data-type='text' data-original-title='Enter Quantity'></a></td>";
			more_html += "</tr>";
		});
		$('#more_models_table tbody').html(more_html);
		$('.myeditable').editable();
		$('.myeditable').editable('option', 'validate', function(v) {
			if(v != '' && !$.isNumeric(v)) return 'Numeric field!';	
		});

		$('.myeditable').on('save', function(e, params) {
			$(this).prev().val(params.newValue);
		});
	}
	if ($('#hidden').val() != '') {
		$('#more_models_table tbody').html(more_html);	
	}
	$('.more_model_tr').show();
	$('input.more_sizes').each(function(e) {
     if ($(this).val() == $('#model_size').val()) {
		$(this).parent().parent().hide();	
		//$($(this).parent().parent()+' td:gt(0) input').val('');
			$($(this).parent().parent()).find('td:gt(0) input,td:gt(0) a').each(function(e) {
				if ($(this).is("input"))
					$(this).val('');
				else
					$(this).editable('setValue', '').removeClass('editable-unsaved');
	 		});
	}
	});
}

function set_prod_loc(ele){
	var data_location = $('#'+ele.id+' option:selected').attr('data-location');
	var html = '<option value="">Select location</option>';
	$.each(location_list, function( ind, value ) {
		if(value['city_id'] == data_location)
		html += '<option value="'+value['city_name']+'">'+value['city_name']+'</option>'; 
	});
	var data_price = $('#'+ele.id+' option:selected').attr('data-price');	
	$('#price').val(data_price);
	$('#model_location').empty().html(html);
	$('#model_location').selectpicker('refresh');
}
