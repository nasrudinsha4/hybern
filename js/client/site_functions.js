$(document).ready(function(){
	$('input[name="qnty"]').trigger('change');
})

function check_cart_sess(sess) {
	if($('#cart-bal').html() != '0')
	location.href = SITE_URL + "cart";
}

function addtocart(prod_id){
	$('.loader-6').show();
	$.ajax({
		type: "POST",
		url: SITE_URL+"addtocart",
		dataType:"json",
		data: {
			prod_id: prod_id,
		},
		success: function(data){
			if(data != "0"){
				$('#cart-bal').html(data);
				$('#view_cart').prop('readonly',false);
				$('#view_cart').prop('disabled',false);
				$('.loader-6').hide();
			}			
		}
	});	
}

function validate_bill() {
	var namevalue=$("#billing_name").val().trim();
	var address=$("#billing_address").val().trim();
	var pincode=$("#billing_pincode").val().trim();
	var phonesale=$("#billing_phone").val().trim();
	var emailsale = $("#billing_email").val().trim();
	var notes = $('#billing_notes').val().trim();
	
	var hidden=$('#hidden').val();
	var email_flag=false;
	var emailReg = /(^[a-zA-Z0-9]+[\._-]{0,1})+([a-zA-Z0-9]+[_]{0,1})*@([a-zA-Z0-9]+[-]{0,1})+(\.[a-zA-Z0-9]+)*(\.[a-zA-Z]{2,3})$/;
	
	if(emailsale !="" && !emailsale.match(emailReg))
		email_flag=true;
	if(namevalue==""){
		$(".error").html(ENTER_NAME);
		$(".error").css('display','inline-block');
		$('.error').delay(TIMEOUT).fadeOut('slow');
		return false;
	}
	if (email_flag==true) {
		$(".error").html(INVALID_EMAIL_ERROR);
		$(".error").css('display','inline-block');
		$('.error').delay(TIMEOUT).fadeOut('slow');
		return false;
	}
	if(phonesale==""){
		$(".error").html(ENTER_MOBILE_ERROR);
		$(".error").css('display','inline-block');
		$('.error').delay(TIMEOUT).fadeOut('slow');
		return false;
	}
	if(pincode==""){
		$(".error").html(ENTER_PIN);
		$(".error").css('display','inline-block');
		$('.error').delay(TIMEOUT).fadeOut('slow');
		return false;
	}
	if(address==""){
		$(".error").html(ENTER_ADDRESS);
		$(".error").css('display','inline-block');
		$('.error').delay(TIMEOUT).fadeOut('slow');
		return false;
	}
	if(notes==""){
		$(".error").html(ENTER_LANDMARK);
		$(".error").css('display','inline-block');
		$('.error').delay(TIMEOUT).fadeOut('slow');
		return false;
	}
}
function change_total(val,sub_id,prod_id){
	var sub_total = parseFloat($('#cart'+sub_id).attr('data-price'))*parseFloat(val);
	var total = 0;
	$('#cart'+sub_id).html(sub_total.toFixed(2));
	$('#cart tbody tr').each(function(){
		total = total + parseFloat($(this).find("td[data-th='Subtotal']").html());
	});
	$('.cart-total').html(total.toFixed(2));
	$('.loader-6').show();
	$.ajax({
		type: "POST",
		url: SITE_URL+"addtocart",
		dataType:"json",
		data: {
			prod_id: prod_id,
			sub_total : sub_total,
			qnty : val,
			amount : total.toFixed(2) 
		},
		success: function(data){
			if(data != "0"){
				$('#cart-bal').html(data);
				$('.loader-6').hide();
			}			
		}
	});		
}

function delete_cart(prod_id){
	$('.loader-6').show();
	$.ajax({
		type: "POST",
		url: SITE_URL+"delete_cart",
		dataType:"json",
		data: {
			prod_id: prod_id,
		},
		success: function(data){
			if(data == "1"){
				$('#tr-'+prod_id).remove();
				if($('#cart tbody tr').length > 0){
					$('#cart tbody tr').each(function(){	
						change_total($(this).find("input[type='number']").val(),$(this).find("input[type='number']").attr('data-sub-id'));
					});
				}
				$('.loader-6').hide();
				if($('#cart tbody tr').length == 0)
					location.href = SITE_URL + "products/0/0";
				
			}
			
		}
	});
}

function filter_results(){
	var formData = new FormData($("#sidebar")[0]);
	$.ajax({
	type: "POST",
	url: SITE_URL + "filter_results",
	data:formData,
	dataType: 'html',	
	processData: false,
	contentType: false,
	success: function(msg) {
		$('.main-filter-data').html(msg);	
	}
	});

}
function view_products(url) {
	window.location.href = url;
}