/* script for the burger menu onClick menu button show/hide */
$(document).ready(function(){
	$( "#burger-menu-btn" ).click(function() {
		if ( $( this ).hasClass( "is-active" ) ) {
			$("#mainmenu").hide(400,function(){
				$('#mainmenu').css('display','');
				$('#burger-menu-btn').removeClass("is-active");
			});
		}else{
			$("#mainmenu").show(400);
			$('#burger-menu-btn').addClass("is-active");
		}
	});

	var our_ciders = $('.bxslider').bxSlider({
		nextSelector: '.next',
 		prevSelector: '.prev',
		pager: false,
	});

});



