$(function() {
	if ($( "#slider-range" ).length > 0){	
		$("#from").val(3000); 
		$("#to").val(30000);
		$("#from,#to").on('change focusout',function(){
			var from = $('#from').val();
			var to = $('#to').val();
			$( "#slider-range" ).slider('values',0,from);
			$( "#slider-range" ).slider('values',1,to);
		});
		
		

		$( "#slider-range" ).slider({
			range: true,
			min: 3000,
			max: 30000,
			values: [ $("#from").val(),  $("#to").val()],
			change: function( event, ui ) {
				$("#from").val(ui.values[0]);
				$("#to").val(ui.values[1]);
				$( "#price" ).val(ui.values[ 0 ] + "-" + ui.values[ 1 ] );
				filter_results();		
			},	
		});

	}	
});